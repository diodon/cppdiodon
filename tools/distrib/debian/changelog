cppdiodon (0.4-1) unstable; urgency=medium

  * Compatibility with chameleon 1.3.0 and update to fmr 0.3

-- Florent Pruvost <florent.pruvost@inria.fr>  Fri, 26 Feb 2025 17:00:00 +0200

cppdiodon (0.3-1) unstable; urgency=medium

  * Add rsvd and revd functions in python wrapper
  * Add regression testing on plafrim
  * Update documentation
  * Tested on Ubuntu 24.04

-- Florent Pruvost <florent.pruvost@inria.fr>  Fri, 17 May 2024 10:10:10 +0200

cppdiodon (0.2-1) unstable; urgency=medium

  * Add a more rigorous MDS check
  * Add DIODON_DLOPEN_HDF5 cmake option
  * Add TBC mapping for symmetric matrices
  * Use Astat Chameleon matrix product depending on the size of matrices
  * Improve python wrapper performances with Chameleon (directly using the python input array without copy)
  * Remove sleep(1) used because of a StarPU bug (fixed in the master branch)

 -- Florent Pruvost <florent.pruvost@inria.fr>  Fri, 11 May 2023 10:10:10 +0200

cppdiodon (0.1-1) unstable; urgency=medium

  * Initial release of cppdiodon : a parallel C++ library for Multivariate Data Analysis of large datasets
  * Contains methods to compute Singular Value Decomposition (SVD), Randomized SVD, Principal Component Analysis (PCA), Multidimensional Scaling (MDS) and Correspondence Analysis (CoA).
  * Handles text and hdf5 files
  * Parallel (mpi, threads, cuda) randomized SVD and EVD (for symmetric matrices) provided by FMR
  * Use multithreaded Lapack or Chameleon (distributed systems + GPUs)

 -- Florent Pruvost <florent.pruvost@inria.fr>  Fri, 25 Nov 2022 17:36:16 +0200
