FROM ubuntu:24.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y
RUN apt-get install git build-essential wget tar curl devscripts dh-make quilt pbuilder sbuild lintian svn-buildpackage git-buildpackage jo -y
RUN apt-get install gfortran cmake python-is-python3 pkg-config zlib1g-dev libbz2-dev libmkl-dev libhdf5-dev -y

ARG CI_COMMIT_TAG
ARG CI_PROJECT_ID
ARG CI_JOB_TOKEN
ARG DEBIANDIST=ubuntu_24.04
ARG DEBIANARCH="1_amd64"
ARG RELEASEURL=https://gitlab.inria.fr/diodon/cppdiodon.git

# download cppdiodon on the CI_COMMIT_TAG version
RUN git clone --recursive --branch $CI_COMMIT_TAG $RELEASEURL cppdiodon-$CI_COMMIT_TAG

# make a source archive (without git files)
RUN wget https://raw.githubusercontent.com/Kentzo/git-archive-all/master/git_archive_all.py
RUN python3 git_archive_all.py -C cppdiodon-$CI_COMMIT_TAG --force-submodules cppdiodon-$CI_COMMIT_TAG.tar.gz

# upload the source package on gitlab
RUN curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./cppdiodon-$CI_COMMIT_TAG.tar.gz "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/cppdiodon-$CI_COMMIT_TAG.tar.gz"

# try to remove the release if it already exists
RUN curl --request DELETE --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG"

# generate the ubuntu package
RUN cp cppdiodon-$CI_COMMIT_TAG.tar.gz cppdiodon_$CI_COMMIT_TAG.orig.tar.gz
ENV LOGNAME=root
ENV DEBEMAIL=florent.pruvost@inria.fr
ENV DEB_BUILD_OPTIONS='nocheck'
RUN cd cppdiodon-$CI_COMMIT_TAG/ && dh_make --library --yes
RUN cp cppdiodon-$CI_COMMIT_TAG/tools/distrib/debian/changelog cppdiodon-$CI_COMMIT_TAG/debian/
RUN cp cppdiodon-$CI_COMMIT_TAG/tools/distrib/debian/control cppdiodon-$CI_COMMIT_TAG/debian/
RUN cp cppdiodon-$CI_COMMIT_TAG/tools/distrib/debian/copyright cppdiodon-$CI_COMMIT_TAG/debian/
RUN cp cppdiodon-$CI_COMMIT_TAG/tools/distrib/debian/rules cppdiodon-$CI_COMMIT_TAG/debian/
RUN cd cppdiodon-$CI_COMMIT_TAG/ && debuild -us -uc

# check the ubuntu package installation
RUN apt-get install -y ./cppdiodon_$CI_COMMIT_TAG-$DEBIANARCH.deb
RUN pca -t 2 -if cppdiodon-$CI_COMMIT_TAG/data/atlas_guyane_trnH.h5 -v 1 -svdm rsvd -of

# upload on gitlab the ubuntu package
RUN curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./cppdiodon_$CI_COMMIT_TAG-$DEBIANARCH.deb "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/$DEBIANDIST/$CI_COMMIT_TAG/cppdiodon_$CI_COMMIT_TAG-$DEBIANARCH.deb"

# create the release and the associated tag
RUN CHANGELOG=`cat cppdiodon-$CI_COMMIT_TAG/tools/distrib/debian/changeloglatest` && \
    ASSETSDATA="{ \"links\": [{ \"name\": \"cppdiodon_$CI_COMMIT_TAG-$DEBIANARCH.deb\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/$DEBIANDIST/$CI_COMMIT_TAG/cppdiodon_$CI_COMMIT_TAG-$DEBIANARCH.deb\" }, \
                              { \"name\": \"cppdiodon-$CI_COMMIT_TAG.tar.gz\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/cppdiodon-$CI_COMMIT_TAG.tar.gz\" }]}" && \
    CURLDATA=$(jo tag_name="$CI_COMMIT_TAG" description="$CHANGELOG" assets="$ASSETSDATA") && \
    echo $CURLDATA && \
    curl --header 'Content-Type: application/json' \
         --header "JOB-TOKEN: $CI_JOB_TOKEN" \
         --data "$CURLDATA" \
         --request POST "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases"
