#!/bin/bash
set -ex

# configure
export CC=gcc
export CXX=g++
export FC=gfortran
export CXXFLAGS="-O0 -g -fPIC --coverage -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wcomment -pedantic -fdiagnostics-show-option -fno-inline -fno-omit-frame-pointer"
[[ -d build ]] && rm build -rf
cmake -B build \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      -DCMAKE_CXX_FLAGS="-O0 -g -fPIC --coverage -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wcomment -pedantic -fdiagnostics-show-option -fno-inline -fno-omit-frame-pointer" \
      -DCMAKE_INSTALL_PREFIX=$PWD/build/install \
      -DCMAKE_BUILD_TYPE=Debug \
      -DDIODON_USE_CHAMELEON=ON \
      -DDIODON_DATA4TESTS=$PWD/data4tests

# make (+clang analyzer)
scan-build -plist --exclude CMakeFiles -o analyzer_reports cmake --build build/ -j5 2>&1 | tee build.log

# install
cmake --install build/

# Make sure threads are not bound
#export STARPU_MPI_NOBIND=1
#export STARPU_WORKERS_NOBIND=1
#export FMR_CHAMELEON_NWORKER_READ=1
export HFI_NO_CPUAFFINITY=1
# test
ctest --test-dir build/ --timeout 30 --output-on-failure --no-compress-output --output-junit ../junit.xml

# coverage
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .

# clang-tidy : too long for now
#run-clang-tidy -checks='*' -header-filter=. -p build -j5 > clang-tidy-report

# cppcheck
cppcheck -v --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem --project=build/compile_commands.json . 2> cppcheck.xml
