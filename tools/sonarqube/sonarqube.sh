#!/bin/bash
set -ex

# to avoid a lock during fetching branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

if [ ! -d "data4tests" ]; then
  git clone https://gitlab.inria.fr/diodon/data4tests.git
fi

#guix time-machine --channels=./tools/ci/guix-channels.scm -- shell
guix shell -D fmr-mpi --without-tests=hdf5-parallel-openmpi clang coreutils \
  cppcheck curl gawk git grep lcov perl python python-gcovr \
  python-lcov-cobertura \
  -- /bin/bash --norc ./tools/sonarqube/analysis.sh

guix shell --preserve=CI -D python-cppdiodon-chameleon python-pydiodon \
  python-coverage python-pytest python-pexpect pybind11 \
  -- /bin/bash --norc ./tools/sonarqube/analysis_python.sh

sonar-scanner --debug &> sonar.log

# clean tmp
rm -rf /tmp/guix-$$
