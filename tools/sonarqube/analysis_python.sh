#!/bin/bash
set -ex

# test python wrapper
cd python/
export CC=gcc
export CXX=g++
export FC=gfortran
[[ -d build ]] && rm build -rf
python3 setup.py install --prefix=$PWD/install
CPPDIODON_PATH=`find $PWD/install -name "cppdiodon.py" | sed -e "s#\/cppdiodon\.py##"`
export PYTHONPATH=$GUIX_ENVIRONMENT/lib/python3.10/site-packages
export PYTHONPATH=$PYTHONPATH:$CPPDIODON_PATH
export LD_PRELOAD=$GUIX_ENVIRONMENT/lib/libmkl_rt.so
coverage run --source . --omit=setup.py -m pytest --junit-xml=python-test.xml
coverage report
coverage xml -o python-coverage.xml
cd ..
