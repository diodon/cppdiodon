import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import h5py
import sys
import os
from matplotlib.colors import Normalize
import matplotlib.patches as mpatches

def custom_load(filename):
    _, ext = os.path.splitext(filename)
    X = None
    if (ext == ".h5"):
        f = h5py.File(filename, 'r')
        X = np.array(f.get('points'))
    else:
        X = np.loadtxt(filename)
    return X

if (len(sys.argv) < 2):
    exit()

filename = sys.argv[1]

X = custom_load(filename)

A1 = X[:,0]
A2 = X[:,1]

nbin = 256

H, _, _ = np.histogram2d(A1, A2, bins=nbin)
H = np.log(1+H)
plt.imshow(H)

name = os.path.basename(filename)
name =(os.path.splitext(name)[0])
name =(os.path.splitext(name)[0])
plt.savefig(name, dpi=300)
