#!/bin/bash
set -ex

# build and test cppdiodon with cmake/ctest
[[ -d build ]] && rm build -rf
cmake -B build \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_CXX_FLAGS="-O0 -g -fPIC --coverage -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wcomment -pedantic -fdiagnostics-show-option -fno-inline -fno-omit-frame-pointer" \
    -DCMAKE_INSTALL_PREFIX=$PWD/build/install \
    -DCMAKE_BUILD_TYPE=Debug \
    -DDIODON_DATA4TESTS=$PWD/data4tests \
    -DDIODON_USE_CHAMELEON=$1 \
    -DDIODON_USE_MKL_AS_BLAS=$2 \
    -DDIODON_USE_OPENBLAS_AS_BLAS=$3

cmake --build build -j5 > /dev/null

# Make sure threads are not bound
#export STARPU_MPI_NOBIND=1
#export STARPU_WORKERS_NOBIND=1
#export FMR_CHAMELEON_NWORKER_READ=1
export HFI_NO_CPUAFFINITY=1
ctest --test-dir build --timeout 120 --output-on-failure --no-compress-output --output-junit ../junit.xml
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .
