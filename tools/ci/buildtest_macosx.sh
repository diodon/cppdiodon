#!/bin/bash
set -ex

# ensure starpu and chameleon are installed
for dep in starpu chameleon
do
  DEP_INSTALLED=`brew ls --versions ${dep} | cut -d " " -f 2`
  if [[ -z "${DEP_INSTALLED}" ]]; then
    # dep is not installed, we have to install it
    brew install --build-from-source ./tools/ci/${dep}.rb
  else
    # dep is already installed, check the version with our requirement
    DEP_REQUIRED=`brew info --json ./tools/ci/${dep}.rb |jq -r '.[0].versions.stable'`
    if [[ "${DEP_INSTALLED}" != "${DEP_REQUIRED}" ]]; then
      # if the installed version is not the required one, re-install
      brew remove --force --ignore-dependencies ${dep}
      brew install --build-from-source ./tools/ci/${dep}.rb
    fi
  fi
done

export PATH=/usr/local/opt/llvm/bin:$PATH
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/opt/openblas/lib/pkgconfig
export CC=/usr/local/bin/gcc-14
export CXX=/usr/local/bin/g++-14
export FC=/usr/local/bin/gfortran-14

# add tests about Load function (depends on external data)
git clone https://gitlab.inria.fr/diodon/data4tests.git

# build and test cppdiodon with cmake/ctest
[[ -d build ]] && rm -rf build
cmake -B build -DDIODON_USE_CHAMELEON=ON -DDIODON_DLOPEN_HDF5=OFF \
               -DDIODON_USE_CHAMELEON_MPI=OFF \
               -DBLA_PREFER_PKGCONFIG=ON -DCMAKE_INSTALL_PREFIX=$PWD/build/install \
               -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-Wfatal-errors" \
               -DDIODON_DATA4TESTS=$PWD/data4tests
cmake --build build > /dev/null
# Make sure threads are not bound
#export STARPU_MPI_NOBIND=1
#export STARPU_WORKERS_NOBIND=1
#export FMR_CHAMELEON_NWORKER_READ=1
ctest --test-dir build --output-on-failure --no-compress-output --output-junit ../junit.xml
