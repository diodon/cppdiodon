set terminal pngcairo noenhanced font "arial,20" fontscale 1.0 size 1920, 1200
set output 'diodon_plafrim.png'
filename="diodon_plafrim.dat"

set grid y
set style data histograms
set style fill solid border -1
set boxwidth 0.75 absolute
set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key invert samplen 4 spacing 1 width 1 height 1
#set key below
set style histogram rowstacked title textcolor lt -1
set xtics nomirror rotate by -45 scale 0
set ylabel "CPU time in s"
set yrange [0:1200]

do for [i=1:13] {
set style line i linecolor rgb hsv2rgb(0.083*(i-1), 1, 1)
}

plot filename using 9 ls 1, \
     filename using 10 ls 2, \
     filename using 11 ls 5, \
     filename using 13:xticlabels(3) ls 7
