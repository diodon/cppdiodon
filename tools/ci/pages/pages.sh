#!/bin/bash
set -ex

[[ -d public ]] && rm public -rf
mkdir public

# generate figures from last benchmarks
# get the most recent commit date for which benchs have been performed
commit_sha=`curl -X GET "https://elasticsearch.bordeaux.inria.fr/hiepacs-diodon_perf/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "sort": [
    { "Commit_date_diodon": "desc" }
  ],
  "size": 1
}
' | grep "Commit_sha_diodon" | awk '{ print $3" "$4}' |sed -e "s#,##g" |sed -e "s#\"##g" |sed -e "s# ##g"`
echo $commit_sha

# generate a csv
python3 ./tools/ci/plafrim/get_result.py -e https://elasticsearch.bordeaux.inria.fr -i hiepacs-diodon_perf -c 5a23ea3fbfdff8be8d1837cf4e4264dd5cb0d3b6 -o diodon_plafrim.dat

# plot figure: generate diodon_plafrim.png
gnuplot tools/ci/pages/plot_benchmarks.gp

cp diodon_plafrim.dat public/
cp diodon_plafrim.png public/

# create the doxygen file (page) for benchmarks
cat > docs/benchmarks.dox <<EOF
/*! \page Benchmarks
@section benchmarks_plafrim Plafrim

Performance results on [Plafrim](https://www.plafrim.fr/)
[bora nodes](https://plafrim-users.gitlabpages.inria.fr/doc/#bora001-044)
at commit: *$commit_sha*.

<center>

<img align="center" src="./diodon_plafrim.png" width="960">

CPU times of Diodon, see associated [data file](./diodon_plafrim.dat).

</center>
*/
EOF

# generate main doxygen doc
[[ -d build ]] && rm build -rf
cmake -B build -DDIODON_USE_CHAMELEON=ON
cmake --build build --target doc
cp -r build/docs/html/* public/
