#!/bin/bash
set -ex

# to avoid a lock during fetching branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

if [ ! -d "data4tests" ]; then
  git clone https://gitlab.inria.fr/diodon/data4tests.git
fi

if [ "$BUILDNAME" = "lapack-mkl" ]; then
  #guix time-machine --channels=./tools/ci/guix-channels.scm -- shell \
  guix shell --pure -D fmr python-gcovr \
  -- /bin/bash --norc ./tools/ci/buildtest_linux.sh OFF ON OFF
elif [ "$BUILDNAME" = "lapack-openblas" ]; then
  guix shell --pure -D fmr --with-input=intel-oneapi-mkl=openblas python-gcovr \
  -- /bin/bash --norc ./tools/ci/buildtest_linux.sh OFF OFF ON
elif [ "$BUILDNAME" = "chameleon-mkl-mpi" ]; then
  guix shell --pure -D fmr-mpi --without-tests=hdf5-parallel-openmpi python-gcovr \
  -- /bin/bash --norc ./tools/ci/buildtest_linux.sh ON ON OFF
elif [ "$BUILDNAME" = "chameleon-openblas-mpi" ]; then
  guix shell --pure -D fmr-mpi --with-input=chameleon-mkl-mt=chameleon --with-input=intel-oneapi-mkl=openblas --without-tests=hdf5-parallel-openmpi python-gcovr \
  -- /bin/bash --norc ./tools/ci/buildtest_linux.sh ON OFF ON
elif [ "$BUILDNAME" = "pages" ]; then
  guix shell --pure -D fmr-mpi --without-tests=hdf5-parallel-openmpi bash coreutils doxygen curl nss-certs python python-certifi python-click python-elasticsearch python-gitpython python-matplotlib python-pandas gnuplot sed \
  -- /bin/bash --norc ./tools/ci/pages/pages.sh
fi

# clean tmp
rm -rf /tmp/guix-$$
