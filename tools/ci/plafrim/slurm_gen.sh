#!/bin/bash
# benchmark : submit slurm jobs and wait for completion before exiting
set -ex

# build diodon with chameleon + MKL multithreaded + MPI enabled
[[ -d build ]] && rm build -rf
cmake -B build -DDIODON_USE_CHAMELEON=ON -DDIODON_USE_MKL_AS_BLAS=ON
cmake --build build -j10
[[ ! -f ./build/tests/testmds ]] && echo "./build/tests/testmds does not exist." && exit 1
[[ ! -f ./build/tests/testpca ]] && echo "./build/tests/testpca does not exist." && exit 1

HOST="bora"
PREC=s

### MDS ###
ALGO=mds

SIZE=100000
NP=1
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZE $SIZE $RANK $PREC &

SIZE=200000
NP=2
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZE $SIZE $RANK $PREC &

SIZE=400000
NP=8
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZE $SIZE $RANK $PREC &

# ask bash to wait for the completion of all background commands
wait

# generate csv and dat files
#export LISTJOB="diodon_mds_s_1_100000_10000 diodon_mds_s_2_200000_10000 diodon_mds_s_8_400000_10000"
#export HOST="bora"
export HOST="$HOST"
export LISTJOB="$LISTJOB"
./tools/ci/plafrim/parse_results_gen.sh time_gen_mds.csv time_gen_mds.dat

# send results from time.csv to the elasticsearch server
cat time_gen_mds.csv
python3 ./tools/ci/plafrim/add_result.py -e https://elasticsearch.bordeaux.inria.fr -t hiepacs -p "diodon" time_gen_mds.csv

# generate gnuplot figure
export RESULT_TIME="time_gen_mds.dat"
cat time_gen_mds.dat
gnuplot ./tools/ci/plafrim/script_time_gen.gp
mv time_gen.png time_gen_mds.png

### PCA ###
ALGO=pca

SIZEM=500000
SIZEN=10000
NP=1
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZEM\_$SIZEN\_$RANK
LISTJOB="$JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZEM $SIZEN $RANK $PREC &

SIZEM=1000000
SIZEN=10000
NP=2
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZEM\_$SIZEN\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZEM $SIZEN $RANK $PREC &

SIZEM=4000000
SIZEN=10000
NP=8
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$ALGO\_$PREC\_$NP\_$SIZEM\_$SIZEN\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage --constraint $HOST \
       ./tools/ci/plafrim/diodon_gen.sl $ALGO $SIZEM $SIZEN $RANK $PREC &

# ask bash to wait for the completion of all background commands
wait

# generate csv and dat files
#export LISTJOB="diodon_pca_s_1_500000_10000_10000 diodon_pca_s_2_1000000_10000_10000 diodon_pca_s_8_2000000_10000_10000"
#export HOST="bora"
export HOST="$HOST"
export LISTJOB="$LISTJOB"
./tools/ci/plafrim/parse_results_gen.sh time_gen_pca.csv time_gen_pca.dat

# send results from time.csv to the elasticsearch server
cat time_gen_pca.csv
python3 ./tools/ci/plafrim/add_result.py -e https://elasticsearch.bordeaux.inria.fr -t hiepacs -p "diodon" time_gen_pca.csv

# generate gnuplot figure
export RESULT_TIME="time_gen_pca.dat"
cat time_gen_pca.dat
gnuplot ./tools/ci/plafrim/script_time_gen.gp
mv time_gen.png time_gen_pca.png
