set terminal pngcairo noenhanced font "arial,20" fontscale 1.0 size 1920, 1200
set output 'time.png'

filename=system("echo $RESULT_TIME")

set grid y
set style data histograms
set style fill solid border -1
set boxwidth 0.75 absolute
set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key invert samplen 4 spacing 1 width 0 height 0
set key below
set style histogram rowstacked title textcolor lt -1
set xtics nomirror rotate by -45 scale 0
set ylabel "CPU time in s"

plot filename using 9, \
     filename using 10, \
     filename using 21, \
     filename using 22, \
     filename using 23, \
     filename using 25:xticlabels(3)
