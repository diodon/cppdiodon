#!/bin/bash
#SBATCH --constraint bora
#SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --threads-per-core=1

set -x

# check number of arguments
if [ "$#" -lt 4 ]
then
  echo -e "This script requires\n 1. The testcase name\n 2. The path to a HDF5 file\n 3. The RSVD prescribed rank\n 4. The floating point operation precision (s or d)\n Example:"
  echo "sbatch  `basename $0` atlas_guyane_trnH /beegfs/diodon/gordon/atlas_guyane_trnH.h5 960 d"
  exit 1
fi
TESTCASE=$1
HDF5FILE=$2
RANK=$3
PREC=$4

# location where to read and write matrices
MATRIXDIR=/beegfs/diodon/gordon

# arithmetic precision s (simple) or d (double)
DIODONPREC=""
if [ $PREC == "d" ]; then DIODONPREC="-d"; fi

# activate timings of Diodon and FMR operations
export FMR_TIME=1
export DIODON_TIME=1

# tile size in Chameleon
export FMR_CHAMELEON_TILE_SIZE=320

# mds use a symmetric matrix as input data
export FMR_CHAMELEON_READH5_SYM=1

# test mpi and get hostnames
time mpiexec --bind-to board hostname 1>&2

# test mpi PingPong between two nodes
time mpiexec --bind-to board -np 2 IMB-MPI1 PingPong 1>&2

# Chameleon+StarPU better performances to dedicate one cpu for the
# scheduler and one for MPI comms
NTHREADS=34

# test performance of the GEMM on one node for reference
B=$FMR_CHAMELEON_TILE_SIZE
K=$((5*B))
M=$((NTHREADS*K))
if [ $PREC == "s" ]; then time mpiexec --bind-to board -np 1 chameleon_stesting -o gemm -b $B -k $K -m $M -n $M; fi
if [ $PREC == "d" ]; then time mpiexec --bind-to board -np 1 chameleon_dtesting -o gemm -b $B -k $K -m $M -n $M; fi

# launch the diodon application: mds which performs a MultiDimensional Scaling
# analysis of a distance matrix. The main input is the distance matrix given
# with one or several h5 files. The main output is the cloud of points given by
# the MDS saved as a h5 file locally.
time mpiexec --bind-to board ./build/tests/testmds -t $NTHREADS -if $HDF5FILE -svdm rsvd -r $RANK -os 0 $DIODONPREC -of -ocf /beegfs/diodon/gordon/$$.h5 -v 1

rm /beegfs/diodon/gordon/$$.h5
