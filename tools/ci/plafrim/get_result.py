#!/usr/bin/env python3
import pandas
import click
import csv
from elasticsearch import Elasticsearch

@click.command()
@click.option("-e", "--elastic-url", default="http://localhost:9200", help="elasticsearch instance url")
@click.option("-i", "--elastic_index", required=True, help="elastic index name")
@click.option("-c", "--commit", required=True, help="project commit")
@click.option("-o", "--output", required=True, help="output file name to save data")
def main(
    elastic_url: str,
    elastic_index: str,
    commit: str,
    output: str
):
    """Get a result from an elasticsearch database, e.g.
    https://elasticsearch.bordeaux.inria.fr."""
    es = Elasticsearch(elastic_url)

    search_param = {
      "query": {
        "bool": {
          "must": [
            {"term": {"Commit_sha_diodon": {"value": commit}}}
          ]
        }
      },
      "size": 1000
    }
    response = es.search(index=elastic_index, body=search_param)
    elastic_docs = response["hits"]["hits"]

    docs = pandas.DataFrame(columns=['Hostname','Algorithm','Testcase',
                                     'Precision','Size','Rank','Nthread',
                                     'Nmpi','Buildmat','Pre','Rsvd','Post',
                                     'Save','Overall'])
    for num, doc in enumerate(elastic_docs):

        # get _source data dict from document
        source_data = doc["_source"]

        #define the keys to remove
        keys = ['Commit_date_diodon','Commit_sha_diodon','Commit_sha_guix',
                'Commit_sha_guix_hpc','Commit_sha_guix_hpcnonfree']
        for key in keys:
          source_data.pop(key, None)

        # append the Series object to the DataFrame object
        docs.loc[num] = source_data

    docs = docs.astype({"Hostname": str, "Algorithm": str, "Testcase": str, "Precision": str, "Size": int, "Rank": int, "Nthread": int, "Nmpi": int})
    docs = docs.rename(columns=str.lower)
    docs.to_csv(output, " ", index=False)

if __name__ == "__main__":
    main()
