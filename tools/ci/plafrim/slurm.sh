#!/bin/bash
# benchmark : submit slurm jobs and wait for completion before exiting
set -ex

# build diodon with chameleon + MKL multithreaded + MPI enabled
[[ -d build ]] && rm build -rf
cmake -B build -DDIODON_USE_CHAMELEON=ON -DDIODON_USE_MKL_AS_BLAS=ON -DDIODON_DLOPEN_HDF5=ON
cmake --build build -j10
[[ ! -f ./build/tests/testmds ]] && echo "./build/tests/testmds does not exist." && exit 1

HOSTNAME="bora"
MATRIXDIR=/beegfs/diodon/gordon
PREC=s

TESTCASE=10V-RbcL_S74
LISTMAT="$TESTCASE"
SIZE=23214
HDF5FILE=$MATRIXDIR/10V-RbcL_S74.h5
NP=1
RANK=1000
TIME=00:05:00
JOB_NAME=diodon\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage \
       ./tools/ci/plafrim/diodon\_$HOSTNAME.sl $TESTCASE $HDF5FILE $RANK $PREC &

TESTCASE=L6
LISTMAT="$LISTMAT $TESTCASE"
SIZE=99594
HDF5FILE=$MATRIXDIR/L6.h5
NP=2
RANK=5000
TIME=00:15:00
JOB_NAME=diodon\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage \
       ./tools/ci/plafrim/diodon\_$HOSTNAME.sl $TESTCASE $HDF5FILE $RANK $PREC &

TESTCASE=L2_L3_L6
LISTMAT="$LISTMAT $TESTCASE"
SIZE=270983
HDF5FILE=$MATRIXDIR/L2_L3_L6.txt
NP=6
RANK=10000
TIME=00:30:00
JOB_NAME=diodon\_$PREC\_$NP\_$SIZE\_$RANK
LISTJOB="$LISTJOB $JOB_NAME"
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --error="$JOB_NAME.err" \
       --nodes=$NP --time=$TIME --partition=routage \
       ./tools/ci/plafrim/diodon\_$HOSTNAME.sl $TESTCASE $HDF5FILE $RANK $PREC &

# ask bash to wait for the completion of all background commands
wait

# generate csv and dat files
# export LISTJOB="diodon_s_1_23214_1000 diodon_s_2_99594_5000 diodon_s_6_270983_10000" && export LISTMAT="10V-RbcL_S74 L6 L2_L3_L6"
export ALGORITHM="MDS"
export HOSTNAME="$HOSTNAME"
export LISTJOB="$LISTJOB"
export LISTMAT="$LISTMAT"
./tools/ci/plafrim/parse_results.sh

# send results from time.csv to the elasticsearch server
cat time.csv
python3 ./tools/ci/plafrim/add_result.py -e https://elasticsearch.bordeaux.inria.fr -t hiepacs -p "diodon" time.csv

# generate gnuplot figure
export RESULT_TIME="time.dat"
cat time.dat
gnuplot ./tools/ci/plafrim/script_time.gp
