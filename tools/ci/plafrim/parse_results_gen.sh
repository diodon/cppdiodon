#!/bin/bash

# parse output files given in the LISTJOB env. var.
# loop over LISTJOB and associated LISTMAT (related input matrix/sample)
# takes 2 arguments:
# 1. csv file name which will be created
# 2. dat file name which will be created

set -ex

FILETIME=$1
echo "Create $FILETIME"
echo "HOSTNAME,ALGORITHM,TESTCASE,PRECISION,SIZEMAT,RANK,NTHDS,NPMPI,BUILDMAT,PRE,GEMMAO,QRY,QY,GEMMAtQ,QRC,RC,QC,SVDRC,GEMMQVt,GEMMURCtQCt,SVD,NORM,POST,SAVE,OVERALL" > $FILETIME

echo $LISTJOB

listjob=($LISTJOB)
length=${#listjob[@]}
for (( i = 0; i < length; i++ ))
do
  JOB=${listjob[$i]}
  FILEIN="$JOB.out"
  if [ -f "$FILEIN" ]; then
      echo "$FILEIN exists."
  else
      echo "$FILEIN does not exists, exit."
      exit 1
  fi

  ALGO=""
  if [[ "$JOB" == *"mds"* ]]; then
    ALGO="MDS"
  fi
  if [[ "$JOB" == *"pca"* ]]; then
    ALGO="PCA"
  fi

  PREALGO=""
  if [ $ALGO == "MDS" ]; then PREALGO="GRAM"; fi
  if [ $ALGO == "PCA" ]; then PREALGO="PRE"; fi

  SIZE=""
  if [ $ALGO == "MDS" ]; then SIZE="N"; fi
  if [ $ALGO == "PCA" ]; then SIZE="M"; fi

  prec=`grep -o -m 1 "\[diodon\] Real: [a-z]*" $FILEIN | cut -d: -f2`
  sizemat=`grep -o -m 1 "\[diodon\] $SIZE: [0-9]*" $FILEIN | cut -d: -f2`
  rank=`grep -o -m 1 "\[diodon\]   Prescribed rank: [0-9]*" $FILEIN | cut -d: -f2`
  nthds=`grep -o -m 1 "\[diodon\] Threads: [0-9]*" $FILEIN | cut -d: -f2`
  npmpi=`grep -o -m 1 "\[diodon\] MPI: [0-9]*" $FILEIN | cut -d: -f2`
  buildmat=`grep -o -m 1 "TIME:$ALGO:BUILDMAT=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  pre=`grep -o -m 1 "TIME:$ALGO:$PREALGO=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmao=`grep -o -m 1 "TIME:RSVD:GEMM(A,Omega)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  qry=`grep -o -m 1 "TIME:RSVD:QR(Y)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getqy=`grep -o -m 1 "TIME:RSVD:GETQ(Y)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmaq=`grep -o -m 1 "TIME:RSVD:GEMM(A^t,Q)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  qrc=`grep -o -m 1 "TIME:RSVD:QR(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getrc=`grep -o -m 1 "TIME:RSVD:GETR(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getqc=`grep -o -m 1 "TIME:RSVD:GETQ(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  svdrc=`grep -o -m 1 "TIME:RSVD:SVD(R_C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmqcurc=`grep -o -m 1 "TIME:RSVD:GEMM(Q_C,U_R_C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmvtrcq=`grep -o -m 1 "TIME:RSVD:GEMM(UT_R_C,QT_c)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  rsvd=`grep -o -m 1 "TIME:SVD=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  frobenius=`grep -o -m 1 "TIME:$ALGO:FROBENIUS([A-Z])=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  post=`grep -o -m 1 "TIME:$ALGO:POSTTREATMENTS=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  algo=`grep -o -m 1 "TIME:$ALGO=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`

  # write data in FILETIME
  echo "$HOST,$ALGO,$ALGO-$sizemat-$rank-$npmpi,$prec,$sizemat,$rank,$nthds,$npmpi,$buildmat,$pre,$gemmao,$qry,$getqy,$gemmaq,$qrc,$getrc,$getqc,$svdrc,$gemmqcurc,$gemmvtrcq,$rsvd,$frobenius,$post,0,$algo" >> $FILETIME
done

# delete empty space
sed -i -e "s# ##g" $1

# for gnuplot figure
cp $1 $2
sed -i -e "s#,# #g" $2
