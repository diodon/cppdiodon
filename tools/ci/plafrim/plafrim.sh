#!/bin/bash
set -x

# to avoid a lock during fetching diodon branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

# save guix commits
#guix time-machine --channels=./tools/ci/guix-channels.scm -- describe --format=json > guix.json
guix describe --format=json > guix.json

# execute benchmark in an isolated guix environment

# get the most recent cppdiodon commit date for which benchs have been performed
commit_sha=`curl -X GET "https://elasticsearch.bordeaux.inria.fr/hiepacs-diodon_perf/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "sort": [
    { "Commit_date_diodon": "desc" }
  ],
  "size": 1
}
' | grep "Commit_sha_diodon" | awk '{ print $3" "$4}' |sed -e "s#,##g" |sed -e "s#\"##g" |sed -e "s# ##g"`
echo $commit_sha
echo $CI_COMMIT_SHA

err1=0
err2=0
if [[ "$commit_sha" != "$CI_COMMIT_SHA" ]]
then

  # MDS with Inrae matrices
  #guix time-machine --channels=./tools/ci/guix-channels.scm -- shell --pure --preserve=^CI --preserve=proxy$ \
  guix shell --pure --preserve=^CI --preserve=proxy$ \
    -D fmr-mpi --without-tests=hdf5-parallel-openmpi \
    bash slurm nss-certs openssh intel-mpi-benchmarks hdf5-parallel-openmpi chameleon-mkl-mt \
    python python-certifi python-click python-elasticsearch python-gitpython python-numpy python-matplotlib python-h5py \
    coreutils inetutils vim emacs gawk gnuplot grep sed \
    -- /bin/bash --norc ./tools/ci/plafrim/slurm.sh
  err1=$?

  [[ ! -f time.csv ]] && echo "time.csv does not exist." && exit 1
  [[ ! -f time.png ]] && echo "time.png does not exist." && exit 1

  # MDS and PCA with randomly generated matrices
  #guix time-machine --channels=./tools/ci/guix-channels.scm -- shell --pure --preserve=^CI --preserve=proxy$ \
  guix shell --pure --preserve=^CI --preserve=proxy$ \
    -D fmr-mpi --without-tests=hdf5-parallel-openmpi \
    bash slurm nss-certs openssh intel-mpi-benchmarks hdf5-parallel-openmpi chameleon-mkl-mt \
    python python-certifi python-click python-elasticsearch python-gitpython python-numpy python-matplotlib python-h5py \
    coreutils inetutils vim emacs gawk gnuplot grep sed \
    -- /bin/bash --norc ./tools/ci/plafrim/slurm_gen.sh
  err2=$?

  [[ ! -f time_gen_mds.csv ]] && echo "time_gen_mds.csv does not exist." && exit 1
  [[ ! -f time_gen_pca.csv ]] && echo "time_gen_pca.csv does not exist." && exit 1
  [[ ! -f time_gen_mds.png ]] && echo "time_gen_mds.png does not exist." && exit 1
  [[ ! -f time_gen_pca.png ]] && echo "time_gen_pca.png does not exist." && exit 1

fi

# clean tmp
rm -rf /tmp/guix-$$

err=$(($err1 + $err2))
exit $err
