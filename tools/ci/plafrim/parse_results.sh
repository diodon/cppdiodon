#!/bin/bash

# parse output files given in the LISTJOB env. var.
# loop over LISTJOB and associated LISTMAT (related input matrix/sample)

set -ex

FILETIME=time.csv
echo "Create $FILETIME"
echo "HOSTNAME,ALGORITHM,TESTCASE,PRECISION,SIZEMAT,RANK,NTHDS,NPMPI,BUILDMAT,PRE,GEMMGO,QRY,QY,GEMMGQ,QRC,RC,QC,SVDRC,GEMMQCU,GEMMVtQ,SVD,NORM,POST,WRITEH5,SAVE,OVERALL" > $FILETIME

echo $LISTJOB
echo $LISTMAT

listjob=($LISTJOB)
listmat=($LISTMAT)
length=${#listjob[@]}
for (( i = 0; i < length; i++ ))
do
  JOB=${listjob[$i]}
  MAT=${listmat[$i]}
  FILEIN="$JOB.out"
  if [ -f "$FILEIN" ]; then
      echo "$FILEIN exists."
  else
      echo "$FILEIN does not exists, exit."
      exit 1
  fi

  prec=`grep -o -m 1 "\[diodon\] Real: [a-z]*" $FILEIN | cut -d: -f2`
  sizemat=`grep -o -m 1 "\[diodon\] N: [0-9]*" $FILEIN | cut -d: -f2`
  rank=`grep -o -m 1 "\[diodon\]   Prescribed rank: [0-9]*" $FILEIN | cut -d: -f2`
  nthds=`grep -o -m 1 "\[diodon\] Threads: [0-9]*" $FILEIN | cut -d: -f2`
  npmpi=`grep -o -m 1 "\[diodon\] MPI: [0-9]*" $FILEIN | cut -d: -f2`
  buildmat=`grep -o -m 1 "TIME:MDS:BUILDMAT=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gram=`grep -o -m 1 "TIME:MDS:GRAM=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmao=`grep -o -m 1 "TIME:RSVD:GEMM(A,Omega)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  qry=`grep -o -m 1 "TIME:RSVD:QR(Y)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getqy=`grep -o -m 1 "TIME:RSVD:GETQ(Y)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmaq=`grep -o -m 1 "TIME:RSVD:GEMM(A^t,Q)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  qrc=`grep -o -m 1 "TIME:RSVD:QR(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getrc=`grep -o -m 1 "TIME:RSVD:GETR(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  getqc=`grep -o -m 1 "TIME:RSVD:GETQ(C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  svdrc=`grep -o -m 1 "TIME:RSVD:SVD(R_C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmqcurc=`grep -o -m 1 "TIME:RSVD:GEMM(Q_C,U_R_C)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  gemmvtrcq=`grep -o -m 1 "TIME:RSVD:GEMM(UT_R_C,QT_c)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  rsvd=`grep -o -m 1 "TIME:SVD=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  frobenius=`grep -o -m 1 "TIME:MDS:FROBENIUS(G)=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  post=`grep -o -m 1 "TIME:MDS:POSTTREATMENTS=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  writeh5=`grep -o -m 1 "TIME:CHAMELEON:writeH5:WRITE=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  save=`grep -o -m 1 "TIME:MDS:SAVE:DATA=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`
  mds=`grep -o -m 1 "TIME:MDS=[0-9]*.[0-9]*" $FILEIN | cut -d= -f2`

  # write data in FILETIME
  echo "$HOSTNAME,$ALGORITHM,$MAT,$prec,$sizemat,$rank,$nthds,$npmpi,$buildmat,$gram,$gemmao,$qry,$getqy,$gemmaq,$qrc,$getrc,$getqc,$svdrc,$gemmqcurc,$gemmvtrcq,$rsvd,$frobenius,$post,$writeh5,$save,$mds" >> $FILETIME
done

# delete empty space
sed -i -e "s# ##g" time.csv

# for gnuplot figure
cp time.csv time.dat
sed -i -e "s#,# #g" time.dat
