#!/bin/bash
#SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --threads-per-core=1

set -x

# check number of arguments
if [ "$#" -lt 5 ]
then
  echo -e "This script requires\n 1. The algorithm name (mds, pca)\n 2. The size M\n 3. The size N\n 4. The RSVD prescribed rank\n 5. The floating point operation precision (s or d)\n Example:"
  echo "sbatch  `basename $0` mds 9600 3200 960 d"
  exit 1
fi
ALGO=$1
NROWS=$2
NCOLS=$3
RANK=$4
PREC=$5

# arithmetic precision s (simple) or d (double)
DIODONPREC=""
if [ $PREC == "d" ]; then DIODONPREC="-d"; fi

# activate timings of Diodon and FMR operations
export FMR_TIME=1
export DIODON_TIME=1

# tile size in Chameleon
export FMR_CHAMELEON_TILE_SIZE=320

# test mpi and get hostnames
time mpiexec --bind-to board hostname 1>&2

# test mpi PingPong between two nodes
time mpiexec --bind-to board -np 2 IMB-MPI1 PingPong 1>&2


if [[ "$SLURM_NODELIST" == *"zonda"* ]]; then
  # Specific Intel MKL software tuning for AMD architectures
  export MKL_DEBUG_CPU_TYPE=5
  export MKL_ENABLE_INSTRUCTIONS=AVX512
fi

# Chameleon+StarPU better performances to dedicate one cpu for the
# scheduler and one for MPI comms
NTHREADS=34

# test performance of the GEMM on one node for reference
B=$FMR_CHAMELEON_TILE_SIZE
K=$((5*B))
M=$((NTHREADS*K))
if [ $PREC == "s" ]; then time mpiexec --bind-to board -np 1 chameleon_stesting -o gemm -b $B -k $K -m $M -n $M; fi
if [ $PREC == "d" ]; then time mpiexec --bind-to board -np 1 chameleon_dtesting -o gemm -b $B -k $K -m $M -n $M; fi

DIODON_SIZE_ARGS=""
if [ $ALGO == "mds" ]; then DIODON_SIZE_ARGS="-svdm rsvd -rs $NROWS -rk $RANK"; fi
if [ $ALGO == "pca" ]; then DIODON_SIZE_ARGS="-svdm rsvd -rsm $NROWS -rsn $NCOLS -rsk $RANK"; fi

# launch the diodon application: mds which performs a MultiDimensional Scaling
# analysis of a distance matrix. The main input is the distance matrix given
# with one or several h5 files. The main output is the cloud of points given by
# the MDS saved as a h5 file locally.
time mpiexec --bind-to board ./build/tests/test$ALGO -t $NTHREADS -gen 1 $DIODON_SIZE_ARGS -os 0 $DIODONPREC -v 1
