FROM ubuntu:24.04

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y
RUN apt-get install -y curl wget tar git python-is-python3 python3-h5py python3-matplotlib python3-numpy python3-pip python3-pybind11 python3-scipy cmake build-essential zlib1g-dev libbz2-dev gfortran pkg-config libmkl-dev
RUN apt-get autoremove -y
RUN apt-get install -y libstarpu-dev libhdf5-mpi-dev hdf5-tools

# Install chameleon (optional dependency for cppdiodon)
RUN wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz && \
    tar xvf chameleon-1.3.0.tar.gz && \
    cd chameleon-1.3.0 && mkdir build && cd build && \
    cmake .. -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=Intel10_64lp -DCHAMELEON_KERNELS_MT=ON -DBUILD_SHARED_LIBS=ON && \
    make -j3 install

ADD . /root/cppdiodon
#RUN cd /root/ && git clone --recursive https://gitlab.inria.fr/diodon/cppdiodon.git

# Install cppdiodon+chameleon
RUN cd /root/cppdiodon/ && \
    cmake -B build -DDIODON_USE_CHAMELEON=ON -DDIODON_USE_MKL_AS_BLAS=ON && \
    cmake --build build/ -j2 && \
    cmake --install build/

# Test cppdiodon
#RUN export OMPI_ALLOW_RUN_AS_ROOT=1 && export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1 && export FMR_CHAMELEON_NWORKER_READ=1 && cd /root/cppdiodon/build && ctest -V -E testLoad

# Install python wrapper for cppdiodon
RUN cd /root/cppdiodon/python/ && python setup.py install --user

# Install pydiodon
RUN git clone https://gitlab.inria.fr/diodon/pydiodon.git && cd pydiodon && python3 setup.py install --user

# Test pydiodon and python+cppdiodon
RUN cd /root/cppdiodon/python/tests/ && \
    export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libmkl_rt.so && \
    export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libmkl_rt.so && \
    export CPPDIODON_PYTHON_DEMO=1 && \
    python test_revd.py 10 5 && \
    python test_rsvd.py 10 7 5 && \
    python test_pca.py ../../data/pca_template_withnames.txt 3 && \
    python test_mds.py ../../data/atlas_guyane_trnH.h5 distance 100 && \
    python test_coa.py ../../data/coa4tests_nonames.txt
