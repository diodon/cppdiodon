find_package(ZLIB QUIET REQUIRED)
if (NOT TARGET ZLIB::ZLIB)
  if (ZLIB_INCLUDE_DIRS)
    set_target_properties(ZLIB::ZLIB PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${ZLIB_INCLUDE_DIRS}")
  endif()
  if (ZLIB_LIBRARIES)
    set_target_properties(ZLIB::ZLIB PROPERTIES INTERFACE_LINK_LIBRARIES "${ZLIB_LIBRARIES}")
  endif()
endif()

find_package(BZip2 QUIET REQUIRED)
if (NOT TARGET BZip2::BZip2)
  if (BZIP2_INCLUDE_DIRS)
    set_target_properties(BZip2::BZip2 PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${BZIP2_INCLUDE_DIRS}")
  endif()
  if (BZIP2_LIBRARIES)
    set_target_properties(BZip2::BZip2 PROPERTIES INTERFACE_LINK_LIBRARIES "${BZIP2_LIBRARIES}")
  endif()
endif()

# gzstream C++ wrapper to zlib: https://www.cs.unc.edu/Research/compgeom/gzstream/
# package libgzstream-dev on ubuntu
# files in Packages/gzstream/
#add_library(gzstream ${${PROJECT_NAME}_SOURCE_DIR}/Packages/gzstream/gzstream.C)
#target_include_directories(gzstream PUBLIC
#                           $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/Packages/gzstream>
#                           $<INSTALL_INTERFACE:include>)

# or header only compress_stream
# https://github.com/natir/compress_stream
add_library(gzstream INTERFACE)
target_include_directories(gzstream INTERFACE
                           $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/Packages/compress_stream/include>
                           $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
# link gzstream to zlib
if (TARGET ZLIB::ZLIB)
  target_link_libraries(gzstream INTERFACE ZLIB::ZLIB)
endif()
# link gzstream to bzip2
if (TARGET BZip2::BZip2)
  target_link_libraries(gzstream INTERFACE BZip2::BZip2)
endif()

# link fmr to gzstream (C++ wrappers to zlib and bzip2 to use them as C++ i/o stream)
target_link_libraries(fmr INTERFACE gzstream)
# install rules
install(TARGETS gzstream
        EXPORT gzstreamTargets
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        )
install(FILES ${${PROJECT_NAME}_SOURCE_DIR}/Packages/compress_stream/include/bzstream.hpp
              ${${PROJECT_NAME}_SOURCE_DIR}/Packages/compress_stream/include/gzstream.hpp
        DESTINATION include )
install(EXPORT gzstreamTargets
        NAMESPACE ${PROJECT_NAME}::
        DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME}/cmake
        )
