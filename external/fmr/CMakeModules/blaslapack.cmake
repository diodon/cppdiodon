﻿# specific case if user wants to force Intel MKL or OpenBLAS
if(FMR_USE_MKL_AS_BLAS)
  if(APPLE)
    set(BLA_VENDOR "Intel10_64lp_seq")
  else()
    set(BLA_VENDOR "Intel10_64lp")
  endif()
endif()
if(FMR_USE_OPENBLAS_AS_BLAS)
  set(BLA_VENDOR "OpenBLAS")
endif()
if(FMR_USE_BLIS_AS_BLAS)
  set(BLA_VENDOR "FLAME")
endif()

# look for blas/lapack
find_package(LAPACK QUIET REQUIRED)

if (LAPACK_LIBRARIES MATCHES "libmkl")
  target_compile_definitions(fmr INTERFACE FMR_USE_MKL_AS_BLAS)
  target_compile_definitions(fmr INTERFACE FMR_MKL_RANDOM)
elseif (LAPACK_LIBRARIES MATCHES "libopenblas")
  target_compile_definitions(fmr INTERFACE FMR_USE_OPENBLAS_AS_BLAS)
elseif (LAPACK_LIBRARIES MATCHES "libblis")
  target_compile_definitions(fmr INTERFACE FMR_USE_BLIS_AS_BLAS)
endif()

if(LAPACK_FOUND)

  if (NOT TARGET LAPACK::LAPACK)
    # define lapack target
    add_library(LAPACK::LAPACK INTERFACE IMPORTED)

    # add libraries
    set(_lapack_libs "${LAPACK_LIBRARIES}")
    if(_lapack_libs AND TARGET BLAS::BLAS)
      # remove the ${BLAS_LIBRARIES} from the interface and replace it
      # with the BLAS::BLAS target
      list(REMOVE_ITEM _lapack_libs "${BLAS_LIBRARIES}")
      list(APPEND _lapack_libs BLAS::BLAS)
    endif()

    if(_lapack_libs)
      set_target_properties(LAPACK::LAPACK PROPERTIES
        INTERFACE_LINK_LIBRARIES "${_lapack_libs}"
      )
    endif()
    unset(_lapack_libs)

    if (FMR_USE_BLIS_AS_BLAS)
      # libflame does not link to a blas so that we need to explicitly make the
      # link, even with dynamic libraries
      set_target_properties(LAPACK::LAPACK PROPERTIES
                            INTERFACE_LINK_LIBRARIES "${LAPACK_LIBRARIES};${BLAS_LIBRARIES}")
    endif()

  endif(NOT TARGET LAPACK::LAPACK)

  # add headers directory if mkl or openblas
  if (LAPACK_LIBRARIES MATCHES "libmkl")
    set(LAPACK_hdrs_to_find "mkl.h")
  elseif (LAPACK_LIBRARIES MATCHES "libopenblas")
    set(LAPACK_hdrs_to_find "cblas.h")
  endif()

  if (LAPACK_hdrs_to_find)
    unset(_inc_env)
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{C_INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{CPATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
    list(APPEND _inc_env "${CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES}")
    list(REMOVE_DUPLICATES _inc_env)
    set(LAPACK_${LAPACK_hdrs_to_find}_DIRS "LAPACK_${LAPACK_hdrs_to_find}_DIRS-NOTFOUND")
    find_path(LAPACK_${LAPACK_hdrs_to_find}_DIRS
      NAMES ${LAPACK_hdrs_to_find}
      HINTS ${_inc_env}
      PATH_SUFFIXES "cblas" "mkl")
    mark_as_advanced(LAPACK_${LAPACK_hdrs_to_find}_DIRS)
    # If found, add path to cmake variable
    # ------------------------------------
    if (LAPACK_${LAPACK_hdrs_to_find}_DIRS)
      set(LAPACK_INCLUDE_DIRS "${LAPACK_${LAPACK_hdrs_to_find}_DIRS}")
      set_target_properties(LAPACK::LAPACK PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${LAPACK_INCLUDE_DIRS}")
    else ()
      set(LAPACK_INCLUDE_DIRS "LAPACK_INCLUDE_DIRS-NOTFOUND")
      if(NOT LAPACK_FIND_QUIETLY)
        message(STATUS "Looking for LAPACK -- ${LAPACK_hdrs_to_find} not found")
      endif()
    endif()
  endif (LAPACK_hdrs_to_find)

  # link fmr target to blas/lapack libraries
  target_link_libraries(fmr INTERFACE LAPACK::LAPACK)
  # give additional info about mangling available
  #######################################################################
  #   Fortran Mangling
  #
  message(STATUS "Check Fortran mangling for BLAS/LAPACK symbols")
  set(CMAKE_REQUIRED_LIBRARIES "${BLAS_LIBRARIES}")
  include(FortranCInterface)
  FortranCInterface_HEADER(FCMangling.hpp MACRO_NAMESPACE "FortranName_")
  unset(CMAKE_REQUIRED_LIBRARIES)
  # add definition for the Fortran mangling
  if ( FortranCInterface_GLOBAL_FOUND)
      target_include_directories(${PROJECT_NAME} INTERFACE $<BUILD_INTERFACE:${${PROJECT_NAME}_BINARY_DIR}>
          $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
      install(FILES "${${PROJECT_NAME}_BINARY_DIR}/FCMangling.hpp"
          DESTINATION  ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/Utils)
  else()
      message(FATAL_ERROR "FortranC Mangling not found.")
  endif()
else()
    message(WARNING "BLAS/LAPACK has not been found, FMR will continue to compile but some applications will be disabled.")
endif()

