if(FMR_USE_CHAMELEON)
  find_package(CHAMELEON QUIET REQUIRED)
  if(CHAMELEON_FOUND)
    # link fmr to chameleon
    target_link_libraries(fmr INTERFACE CHAMELEON::chameleon)
    target_compile_definitions(fmr INTERFACE FMR_CHAMELEON)
    list(APPEND CMAKE_INSTALL_RPATH "${CHAMELEON_LIB_DIR}")
  endif()
  # ChameleonDenseWrapper depends also on StarPU
  # target MORSE::STARPU may be provided directly by chameleon if recent
  # enough, else we should find it with the FindSTARPU.cmake module delivered
  # in old chameleon
  if (NOT TARGET MORSE::STARPU)
    find_package(STARPU 1.4 QUIET REQUIRED)
  endif()
  if (TARGET MORSE::STARPU)
    target_link_libraries(fmr INTERFACE MORSE::STARPU)
  else()
    message(FATAL_ERROR "Starpu is required but not found")
  endif()
endif()

