﻿#
# Module internal
# ------------
# cpp_tools
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/modules/internals/cpp_tools/)
set(CPP_TOOLS_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/modules/internals/cpp_tools/)

set(CPP_TOOLS_USE_CL_PARSER ON)
set(CPP_TOOLS_USE_COLORS ON)
set(CPP_TOOLS_USE_TIMERS OFF)
set(CPP_TOOLS_USE_PARALLEL_MANAGER OFF)
include(cmake/init-cpptools)
