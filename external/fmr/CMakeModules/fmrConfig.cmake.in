@PACKAGE_INIT@

# dependencies of FMR

include(CMakeFindDependencyMacro)

# propagate the CMAKE_PREFIX_PATH informations used during fmr installation to give paths to dependencies
list(APPEND CMAKE_PREFIX_PATH "@PACKAGE_PREFIX_PATH@")

if(@FMR_USE_MKL_AS_BLAS@)
  set(BLA_VENDOR "Intel10_64lp")
endif()
if(@FMR_USE_OPENBLAS_AS_BLAS@)
  set(BLA_VENDOR "OpenBLAS")
endif()
if(@FMR_USE_BLIS_AS_BLAS@)
  set(BLA_VENDOR "FLAME")
endif()

find_dependency(LAPACK)

if(LAPACK_FOUND)

  if (NOT TARGET LAPACK::LAPACK)
    # define lapack target
    add_library(LAPACK::LAPACK INTERFACE IMPORTED)

    # add libraries
    set(_lapack_libs "${LAPACK_LIBRARIES}")
    if(_lapack_libs AND TARGET BLAS::BLAS)
      # remove the ${BLAS_LIBRARIES} from the interface and replace it
      # with the BLAS::BLAS target
      list(REMOVE_ITEM _lapack_libs "${BLAS_LIBRARIES}")
      list(APPEND _lapack_libs BLAS::BLAS)
    endif()

    if(_lapack_libs)
      set_target_properties(LAPACK::LAPACK PROPERTIES
        INTERFACE_LINK_LIBRARIES "${_lapack_libs}"
      )
    endif()
    unset(_lapack_libs)

  endif(NOT TARGET LAPACK::LAPACK)

  # add headers directory if mkl or openblas
  if (LAPACK_LIBRARIES MATCHES "libmkl")
    set(LAPACK_hdrs_to_find "mkl.h")
  elseif (LAPACK_LIBRARIES MATCHES "libopenblas")
    set(LAPACK_hdrs_to_find "cblas.h")
  endif()
  if (LAPACK_hdrs_to_find)
    unset(_inc_env)
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{C_INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{CPATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
    list(APPEND _inc_env "${CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES}")
    list(REMOVE_DUPLICATES _inc_env)
    set(LAPACK_${LAPACK_hdrs_to_find}_DIRS "LAPACK_${LAPACK_hdrs_to_find}_DIRS-NOTFOUND")
    find_path(LAPACK_${LAPACK_hdrs_to_find}_DIRS
      NAMES ${LAPACK_hdrs_to_find}
      HINTS ${_inc_env}
      PATH_SUFFIXES "cblas" "mkl")
    mark_as_advanced(LAPACK_${LAPACK_hdrs_to_find}_DIRS)
    # If found, add path to cmake variable
    # ------------------------------------
    if (LAPACK_${LAPACK_hdrs_to_find}_DIRS)
      set(LAPACK_INCLUDE_DIRS "${LAPACK_${LAPACK_hdrs_to_find}_DIRS}")
      set_target_properties(LAPACK::LAPACK PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${LAPACK_INCLUDE_DIRS}")
    else ()
      set(LAPACK_INCLUDE_DIRS "LAPACK_INCLUDE_DIRS-NOTFOUND")
      if(NOT LAPACK_FIND_QUIETLY)
        message(STATUS "Looking for LAPACK -- ${LAPACK_hdrs_to_find} not found")
      endif()
    endif()
  endif (LAPACK_hdrs_to_find)

endif()

if(@FMR_USE_HDF5@)
  find_dependency(HDF5)
endif()

find_dependency(ZLIB)
find_dependency(BZip2)

if(@FMR_USE_MPI@)
  find_dependency(MPI)
endif()

if(@FMR_USE_CHAMELEON@)
  find_dependency(CHAMELEON)
  if (NOT TARGET MORSE::STARPU)
    find_dependency(STARPU)
  endif()
endif()

find_dependency(OpenMP)

# targets
include("${CMAKE_CURRENT_LIST_DIR}/../../../lib/cmake/cpp_tools/cpp-tools-targets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/gzstreamTargets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/@PROJECT_NAME@Targets.cmake")
check_required_components("@PROJECT_NAME@")
