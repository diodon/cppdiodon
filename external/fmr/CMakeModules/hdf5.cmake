if(FMR_USE_HDF5)
  if (FMR_USE_CHAMELEON AND FMR_USE_MPI)
    set(HDF5_PREFER_PARALLEL ON)
  endif()
  find_package(HDF5 QUIET REQUIRED)
  if(HDF5_FOUND)
    if (NOT TARGET hdf5::hdf5)
      add_library(hdf5::hdf5 INTERFACE IMPORTED)
      set_target_properties(hdf5::hdf5 PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${HDF5_INCLUDE_DIRS}"
        INTERFACE_COMPILE_DEFINITIONS "${HDF5_DEFINITIONS}"
        INTERFACE_LINK_LIBRARIES "${HDF5_LIBRARIES}")
    endif()
    # link fmr to hdf5
    target_link_libraries(fmr INTERFACE hdf5::hdf5)
    target_compile_definitions(fmr INTERFACE FMR_HDF5)
    if(HDF5_LIBS_DIRS)
      list(APPEND CMAKE_INSTALL_RPATH "${HDF5_LIBS_DIRS}")
    endif()
  endif(HDF5_FOUND)
endif()
