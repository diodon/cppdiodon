﻿# FMR Documentation

**C++ API for the Fast Methods for Randomized numerical linear algebra**

**Inria**

* * *

[TOC]

* * *

## About

This package is a subset of the master branch of FMR, that contains only classes over LAPACK and Chameleon.

Authors:
       P. Blanchard, [O. Coulaud](mailto:Olivier.Coulaud@inria.fr)

Developpers:
    [F. Pruvost](mailto:florent.pruvost@inria.fr), R. Peressoni


Fast and accurate Methods for Randomized numerical linear algebra. This project provides routines for performing low-rank matrix approximations based on randomized techniques:

* Random __projection__ based LRA: Randomized {EVD, SVD, Cholesky, ID, ...}


Please refer to

- Pierre Blanchard, Olivier Coulaud, Eric Darve, Alain Franc. FMR: Fast randomized algorithms for covariance matrix computations. Platform for Advanced Scientific Computing (PASC), Jun 2016, Lausanne, Switzerland. 2016. (https://hal.inria.fr/hal-01334747v1)
- Pierre Blanchard, Olivier Coulaud, Eric Darve. Fast hierarchical algorithms for generating Gaussian random fields. [Research Report] 8811, Inria Bordeaux Sud-Ouest. 2015. ⟨https://hal.inria.fr/hal-01228519v2⟩
- Pierre Blanchard. Fast hierarchical algorithms for the low-rank approximation of matrices, with applications to materials physics, geostatistics and data analysis.  Université de Bordeaux, 2017.  ⟨https://hal.inria.fr/tel-01534930⟩
- Emmanuel Agullo, Olivier Coulaud, Alexandre Denis, Mathieu Faverge, Alain A. Franc, Jean-Marc Frigerio, Nathalie Furmento, Samuel Thibault, Adrien Guilbaud, Emmanuel Jeannot, Romain Peressoni, and Florent Pruvost. Task-based randomized singular value decomposition and multidimensional scaling. Research Report 9482, Inria Bordeaux - Sud Ouest ; Inrae - BioGeCo, September 2022. (https://inria.hal.science/hal-03773985v2)

and for details on algorithms
- [Halko et al "Finding structure with randomness: Probabilistic algorithms for constructing approximate matrix decompositions", SIAM Rev., Survey and Review section, Vol. 53, num. 2, pp. 217-288, June 2011.](https://arxiv.org/abs/0909.4061)


## What's inside ?

* `CMakeModules/`: CMake scripts to find libraries installed on your machine
* `Data/`: data for examples and tests
* `doc/`: documentation
* `examples/`: basic examples usage
* `include/`:
     * `fmr/`: contains all the directories of the header only library
          * `Algorithms`:	low-rank approximation algorithms such as truncated SVD, random projection methods, ...
          * `MatrixWrappers`:	wrappers for matrix
          * `StandardLRA`: classical matrix operation or factorization GEMM, QR, EVD, SVD
          * `RandomizedLRA`: randomized classes for algorithms
          * `Utils`: utilitary tools such as matrix display, matrix generation, norm computation, input/output operations (HDF5, zlib, etc), wrappers to Starpu and Chameleon
* `modules/`: C++ submodules
* `Packages/`: external libraries
* `Scripts/`: scripts for continuous integration
* `UTests/`: source files for testing FMR


## Get and Build FMR

### How to get it

To get fmr

```git clone --recursive https://gitlab.inria.fr/compose/legacystack/fmr.git```

### Dependencies
  * Blas/Lapack
  * zlib and bzip2
  * optional
      * [Chameleon-v1.3.0](https://gitlab.inria.fr/solverstack/chameleon), MPI, StarPU
      * MKL random generators
      * HDF5

### Install FMR

FMR was tested on Linux system and MacosX (10.15) with
  * compilers (Intel, clang, GNU)
  * Blas/Lapack: openblas and MKL, Chameleon (release v1.3.0)
  * MPI and runtime: OpenMPI and StarPU latest release
  * zlib and bzip2
  * HDF5 1.10 or later (with the `--with-default-api-version=v110` configure flag)

FMR is a header only library, if you don't build the tests.

git clone https://gitlab.inria.fr/compose/legacystack/fmr.git

Create a directory to build and to install FMR with cmake.

    ```
    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX:PATH=/your/installation/path_for_fmr
    cmake --build . --config Release --target install
    ```

### Installation examples with dependencies

#### Docker image

We provide a docker image with FMR installed with Chameleon and MPI.
Please refer to this [dockerfile](Packages/docker/dockerfile-ubuntu) for the recipe.
The docker image name is __registry.gitlab.inria.fr/compose/legacystack/fmr/ubuntu__ and you can use it as follows:
```
docker run -it registry.gitlab.inria.fr/compose/legacystack/fmr/ubuntu
```

#### Linux Debian/Ubuntu
```
sudo apt update -y
sudo apt install -y git python-is-python3 cmake build-essential gfortran pkg-config libmkl-dev libstarpu-dev libhdf5-mpi-dev zlib1g-dev libbz2-dev

# install chameleon
cd $HOME
git clone --recursive https://gitlab.inria.fr/solverstack/chameleon.git
cd chameleon
export CHAMELEON_ROOT=$PWD/install
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$CHAMELEON_ROOT -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=Intel10_64lp -DCHAMELEON_KERNELS_MT=ON -DBUILD_SHARED_LIBS=ON
make -j3 install

# install fmr
cd $HOME
git clone https://gitlab.inria.fr/compose/legacystack/fmr.git
cd fmr
export FMR_ROOT=$PWD/install
mkdir build && cd build
# check
cmake .. -DCMAKE_INSTALL_PREFIX=$FMR_ROOT -DFMR_BUILD_TESTS=ON -DFMR_USE_HDF5=ON -DFMR_USE_CHAMELEON=ON -DFMR_USE_MKL_AS_BLAS=ON
make -j5
ctest -V
# install
cmake .. -DFMR_BUILD_TESTS=OFF
make install
```
Then to use FMR in your CMake project: `CHAMELEON_ROOT` and `FMR_ROOT` (or `fmr_ROOT`) environment variables should be defined to the installation directories of Chameleon and FMR. Alternatively one can directly add these directories to the [CMAKE_PREFIX_PATH](https://cmake.org/cmake/help/latest/variable/CMAKE_PREFIX_PATH.html#variable:CMAKE_PREFIX_PATH) CMake variable, *e.g.* during your project configuration use `-DCMAKE_PREFIX_PATH="$HOME/chameleon/install;$HOME/fmr/install"`.

#### MacOSX

Considering git, gcc and cmake are already installed you need to install the
following: zlib, bzip2, CBLAS/LAPACKE (example here with OpenBLAS but you can
install Intel MKL), StarPU and Chameleon.
````
brew install automake autoconf libtool zlib bzip2 hwloc pkgconfig openblas openmpi

# use pkg-config .pc files to detect some dependencies
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/Cellar/openblas/0.3.13/lib/pkgconfig/:${PKG_CONFIG_PATH}
# cmake checks blas.pc not openblas.pc
sudo cp /usr/local/Cellar/openblas/0.3.13/lib/pkgconfig/openblas.pc /usr/local/Cellar/openblas/0.3.13/lib/pkgconfig/blas.pc

# install hdf5 parallel openmpi
# download a source tarball here https://www.hdfgroup.org/downloads/hdf5/source-code/
tar xvf hdf5-1.12.0.tar.gz
cd hdf5-1.12.0
CC=/usr/local/bin/mpicc CFLAGS="-fPIC" CXXFLAGS="-fPIC" ./configure --with-zlib=/usr/local/opt --prefix=/usr/local/ --enable-shared --disable-fortran --disable-static --disable-tests --enable-threadsafe --with-pthread --enable-unsupported --enable-parallel --with-default-api-version=v110
make -j5
sudo make install

# install starpu
# download a source tarball here https://files.inria.fr/starpu/
tar xvf starpu-1.4.0.tar.gz
cd starpu-1.4.0/
./configure --enable-blas-lib=none --disable-starpufft --disable-mlr --disable-opencl --disable-cuda --disable-hdf5 --disable-fortran --prefix=/usr/local
make -j5
sudo make install

# install chameleon
cd $HOME
git clone --recursive https://gitlab.inria.fr/solverstack/chameleon.git
cd chameleon
export CHAMELEON_ROOT=/usr/local/
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=${CHAMELEON_ROOT} -DCHAMELEON_USE_MPI=ON -DCHAMELEON_KERNELS_MT=ON -DBLA_PREFER_PKGCONFIG=ON -DBUILD_SHARED_LIBS=ON
make -j5
sudo make install

# install fmr
cd $HOME
git clone https://gitlab.inria.fr/compose/legacystack/fmr.git
cd fmr
mkdir build && cd build
cmake .. -DFMR_BUILD_TESTS=ON -DFMR_USE_HDF5=ON -DFMR_USE_CHAMELEON=ON -DFMR_USE_OPENBLAS_AS_BLAS=ON
make -j5
sudo make install
````

### Usefull define to enable some FMR features

These following keywords allow you to access other functionalities or improve performance
  * `FMR_HDF5` if you want to fill a matrix from a dataset in ```hdf5`` file
  * `FMR_DLOPEN_HDF5` to enable reading HDF5 file in parallel with several processes (4 by default) on each MPI rank
  * `FMR_USE_MKL_AS_BLAS` if used with Intel MKL
  * `FMR_MKL_RANDOM`  if the `MKL` library is used for Blas/Lapack or parallel random generator
  * `FMR_USE_OPENBLAS_AS_BLAS` if used with OpenBlas
  * `FMR_CHAMELEON` if you consider Chamelon library (Blas/Lapack) - Mandatory for distributed version of FMR
  * `FMR_USE_MPI` to use Chameleon+MPI version (use `ON` if Chameleon is configured with `CHAMELEON_USE_MPI=ON`, else disable with `OFF`)

### How to use FMR in other project

When FMR is installed, we add files to find FMR as a package through cmake.
These files are located in `dir_to_install_fmr/lib/cmake/fmr`
So to use FMR in a project, for instance diodon, we add in CMakeLists.txt the
following lines

```
    ## Example with a project called diodon

    # use for example CMAKE_PREFIX_PATH to give the fmr root install dir.
    find_package(FMR CONFIG REQUIRED)
    #
    # link diodon target to fmr libraries
    #
    target_link_libraries(diodon INTERFACE fmr::fmr)
```

### Environment variables to be used during execution

The following environment variables can be used to enable some features at runtime:
  - __FMR_TIME__: if set to 1, enables timing of some functions
  - __FMR_CHAMELEON_NUM_THREADS__: number of threads (cpu workers) to be used by chameleon
  - __FMR_CHAMELEON_NUM_CUDAS__: number of gpus (cuda devices) to be used by chameleon
  - __FMR_CHAMELEON_TILE_SIZE__: size nb of tiles (square data sub-blocks nb*nb)
  - __FMR_CHAMELEON_ASYNC__: if set to 1, enables the chameleon async interface (avoid tasks synchronization between some algorithms)
  - __FMR_CHAMELEON_READH5_BYTILE__: if set to 1, read HDF5 files directly by chameleon tiles instead of the default StarPU handmade task algorithm (read by panel of rows + build tiles + tiles migration to get 2d block-cyclic distribution)
  - __FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D__: to be used when __FMR_CHAMELEON_READH5_BYTILE__ is used, if set to 1, read by tile but follow a 1D distribution of tiles over MPI processes instead of 2d block-cyclic
  - __FMR_CHAMELEON_READH5_SYM__: if set to 1, read HDF5 files considering matrix is symmetric in main memory
  - __FMR_CHAMELEON_READH5_MAXTASKS__: limit the number of submitted tasks (e.g. =3000) to avoid memory over consumption
  - __FMR_CHAMELEON_SBC__: set to 1 to enable SBC MPI distribution for symmetric matrices
  - __FMR_CHAMELEON_SBC_R__: to set a specific value for the parameter R of the SBC MPI distribution
  - __FMR_CHAMELEON_TBC__: set to 1 to enable TBC MPI distribution for symmetric matrices
  - __FMR_CHAMELEON_TBC_FILE__: the path to the file containing the TBC mapping and generated by `Scripts/tbc_mapping.py`
  - __FMR_CHAMELEON_NWORKER_READ__: control the number of processes that read HDF5 files on each MPI rank (tricks with dlopen with `FMR_DLOPEN_HDF5` enabled)


## Development part.

Developements
--------------------------------------------------------------------------------
### Improvements todo

 * HDF5 reads are not stable and take the main part of the cpu time for large
  matrix.
       * Check the chunk_size in compress matrix
       * improve the parallel file system access
       * block version of the read (the Upper part is stored by block) works in
         the Blas version (shared memory)

 * Consider parallel random generator in shared memory if the MKL is used. The
    blocking method is available in the MKL vector library
    (https://software.intel.com/en-us/mkl-vsnotes)  (done September 11,  2019)


### New features to add

 * Put the Nystrom version in the new FMR class format (wrapper and LA)
