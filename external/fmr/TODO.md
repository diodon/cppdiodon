# A list of things __TODO__

## General design

* Move addons to separate projects?

* Integrate `HieMat` functionalities to global sources? Or leave it in addons?

* Decide whether to use `long long int` for sizes or only for products since we rely so heavily on blas/lapack routines.

## Interfaces

* Use python to generate slurm/qsub scripts and run jobs on clusters.
    * [Run jobs from python](https://github.com/jrs65/scalapy/blob/master/bin/cbench/mkjobscript.py)

* Spack: Handle dependencies through (well-maintained) python scripts that are taylored for libraries like plasma/magma/chameleon/...
    * First link 
    * Conflicts with Nix?

* Nix:
    * Discuss with Violaine
    * Why? How?
        * [COMMENT] Seems clear to me
        * [COMMENT] Nix provides magma!!
        * [QUESTIO] Chameleon into Nix??

* Format matrice et modif structures si utilisation de Chameleon/Magma/Scalapack ?
    * [QUESTION] Quel est l'effort à faire pour adapter le format?

* Swig: 
    * [DONE] Build python interface via cmake
    * Ensure that all *.so are available
    * List functionalities that need to be wrapped
    * Start matlab interface

## UTests

* List fundamental capabilities of `fmr` like
    * reading/writing vectors and matrices
    * performing basic mathematical operations
        * basics
        * computing norms and errors
    * performing SVD, EVD, QR, ...
        * computing numerical rank
        * The test of Arpack is crucial since we need it for all RSVD verifications!!
    * handling matrix wrappers
        * setting values
        * performing matrix multiplications
    * evaluating a correlation kernel

## Tests

* testMpiMatrixMultiplication
    * Manual version
    * Scalapack version
    * Chameleon version

* testMpiRandSVD
    * For now we consider that $`n`$-by-$`r`$ matrices fit in RAM 
    * Therefore only matrix multiplication need to be distributed.
    * [TODO] Requires creating/fixing the mpiDense matrix wrapper

## Factorizers

* factMpiRandSVD: Once associated test is done!

## Addons

Please refer to the dedicated TODO lists

* [MDS](Addons/MDS/TODO.md) 

* [GRF (not yet included!)](Addons/GRF/TODO.md) 

## Src

* Matrix wrappers: Define prototypes
    * [DONE] Dense
    * [DONE] DenseBlas
    * [DONE] DenseSimi




