﻿#
# Check UTests/
#

# All tests cannot be called with the same command
#foreach(_test ${tests})
#  message(STATUS "_test  ${_test}")
#  add_test(${_test})
#endforeach()

set(flags_common "-v")

set(mpicmd "")
set(mpiarg "")
set(mpiargblas "")
if (FMR_USE_MPI)
  set(mpicmd "mpiexec")
  set(mpiarg "--oversubscribe" "-n" "2")
  set(mpiargblas "-n" "1")
endif()

# TODO: tests should have the same interface, the same procedure for execution
if (NOT FMR_USE_CHAMELEON)
  add_test(test_blas_gemm ${mpicmd} ${mpiargblas} testGemm ${flags_common})
  add_test(test_blas_qr ${mpicmd} ${mpiargblas} testQRF ${flags_common} -nr 15 -nc 15)
endif()
add_test(test_blas_svd ${mpicmd} ${mpiargblas} testSVD ${flags_common} -b)
add_test(test_blas_rsvd ${mpicmd} ${mpiargblas} testRSVD ${flags_common} -nr 30 -nc 15 -pr 8 -b)
add_test(test_blas_revd ${mpicmd} ${mpiargblas} testREVD ${flags_common} -nr 30 -pr 8 -b)

if (FMR_USE_CHAMELEON)

  add_test(test_cham_gemm ${mpicmd} ${mpiarg} testGemm ${flags_common})
  set_tests_properties(test_cham_gemm PROPERTIES ENVIRONMENT "MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_qr ${mpicmd} ${mpiarg} testQRF ${flags_common} -nr 15 -nc 15)
  set_tests_properties(test_cham_qr PROPERTIES ENVIRONMENT "MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_rsvd ${mpicmd} ${mpiarg} testRSVD ${flags_common} -nr 30 -nc 15 -pr 8)
  set_tests_properties(test_cham_rsvd PROPERTIES ENVIRONMENT "MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_revd ${mpicmd} ${mpiarg} testREVD ${flags_common} -nr 30 -pr 8)
  set_tests_properties(test_cham_revd PROPERTIES ENVIRONMENT "MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")

  # test FMR_CHAMELEON_ASYNC
  add_test(test_cham_gemm_async ${mpicmd} ${mpiarg} testGemm ${flags_common})
  set_tests_properties(test_cham_gemm_async PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_qr_async ${mpicmd} ${mpiarg} testQRF ${flags_common} -nr 15 -nc 15)
  set_tests_properties(test_cham_qr_async PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_rsvd_async ${mpicmd} ${mpiarg} testRSVD ${flags_common} -nr 30 -nc 15 -pr 8)
  set_tests_properties(test_cham_rsvd_async PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")
  add_test(test_cham_revd_async ${mpicmd} ${mpiarg} testREVD ${flags_common} -nr 30 -pr 8)
  set_tests_properties(test_cham_revd_async PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;MKL_NUM_THREADS=1;OPENBLAS_NUM_THREADS=1")

endif()

add_test(test_densewrappers testDenseWrappers)

if (FMR_USE_HDF5)

  add_test(test_read_h5 ${mpicmd} ${mpiarg} testReadH5 -if sym17.h5)
  set_tests_properties(test_read_h5 PROPERTIES ENVIRONMENT "FMR_CHAMELEON_TILE_SIZE=3")

  if (FMR_USE_CHAMELEON)
    # test FMR_CHAMELEON_ASYNC
    add_test(test_read_h5_cham_async ${mpicmd} ${mpiarg} testReadH5 -if sym17.h5)
    set_tests_properties(test_read_h5_cham_async PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;FMR_CHAMELEON_TILE_SIZE=3")

    # test FMR_CHAMELEON_READH5_BYTILE
    add_test(test_read_h5_cham_bytile ${mpicmd} ${mpiarg} testReadH5 -if sym17.h5)
    set_tests_properties(test_read_h5_cham_bytile PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;FMR_CHAMELEON_READH5_BYTILE=1;FMR_CHAMELEON_TILE_SIZE=3")

    # test FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D
    add_test(test_read_h5_cham_distrib1d ${mpicmd} ${mpiarg} testReadH5 -if sym17.h5)
    set_tests_properties(test_read_h5_cham_distrib1d PROPERTIES ENVIRONMENT "FMR_CHAMELEON_ASYNC=1;FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D=1;FMR_CHAMELEON_TILE_SIZE=3")

    # test multiple HDF5 blocks
    add_test(test_read_h5_cham_multiblocks ${mpicmd} ${mpiarg} testReadH5 -if ${CMAKE_CURRENT_BINARY_DIR}/sym17.txt)
    set_tests_properties(test_read_h5_cham_multiblocks PROPERTIES ENVIRONMENT "FMR_CHAMELEON_TILE_SIZE=3")

    if (FMR_USE_CHAMELEON AND FMR_USE_MPI)
      add_test(test_read_h5_cham_sbc ${mpicmd} "--oversubscribe" "-n" "3" testReadH5 -if sym17.h5)
      set_tests_properties(test_read_h5_cham_sbc PROPERTIES ENVIRONMENT "FMR_CHAMELEON_TILE_SIZE=2;FMR_CHAMELEON_SBC=1")
      add_test(test_read_h5_cham_tbc ${mpicmd} "--oversubscribe" "-n" "6" testReadH5 -if sym17.h5)
      set_tests_properties(test_read_h5_cham_tbc PROPERTIES ENVIRONMENT "FMR_CHAMELEON_TILE_SIZE=2;FMR_CHAMELEON_TBC=1;FMR_CHAMELEON_TBC_FILE=${PROJECT_SOURCE_DIR}/Scripts/ci/mapping.txt")
    endif()

  endif()

endif()
