/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iomanip>
#include <iostream>
#include <limits>
#include <random>

#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include "commonParameters.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/Tic.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#if defined(FMR_USE_MPI)
#include "mpi.h"
#endif
#endif
#include "fmr/Algorithms/truncatedSVD.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"

using namespace fmr;

static int nbTest = 0;
static int nbPassed = 0;

#define NEWTEST ++nbTest
#define TESTRESULT(RES)                                                        \
  {                                                                            \
    if (RES) {                                                                 \
      std::cout << "\033[32m[Test " << nbTest << "] PASSED\033[0m"             \
                << std::endl;                                                  \
      ++nbPassed;                                                              \
    } else {                                                                   \
      std::cout << "\033[31m[Test " << nbTest << "] FAILED\033[0m"             \
                << std::endl;                                                  \
    }                                                                          \
  }

#define TESTSUMARY                                                             \
  if (nbTest == 0) {                                                           \
    std::cout << "No tests to summarise" << std::endl;                         \
    return 0;                                                                  \
  } else if (nbPassed == nbTest) {                                             \
    std::cout << "\033[32m";                                                   \
  } else if (nbPassed == nbTest - 1) {                                         \
    std::cout << "\033[33m";                                                   \
  } else {                                                                     \
    std::cout << "\033[31m";                                                   \
  }                                                                            \
  std::cout << std::setprecision(0) << std::fixed;                             \
  std::cout << "Passed " << nbPassed << "/" << nbTest << " - ["                \
            << 100.0 * nbPassed / nbTest << "%]\033[0m" << std::endl;

#define TESTRETURN (nbTest == nbPassed) ? EXIT_SUCCESS : EXIT_FAILURE

int main(int argc, char **argv) {

  using value_type = float;
  using int_type = int;

  /* Parameter handling */
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{},
      fmr::parameters::nbthreads{},
      fmr::parameters::nbrows{},
      fmr::parameters::trank{},
      fmr::parameters::prank{},
      fmr::parameters::oversampling{},
      fmr::parameters::epsilon{},
      fmr::parameters::blascham{},
      fmr::parameters::verbose{}
      );
  parser.parse(argc, argv);

  const int nbThreads = parser.get<fmr::parameters::nbthreads>();
  const int_type n = parser.get<fmr::parameters::nbrows>();
  const int_type r = parser.get<fmr::parameters::trank>();
  const int_type cRank = parser.get<fmr::parameters::prank>();
  const value_type precision = parser.get<fmr::parameters::epsilon>();
  const bool blas = parser.get<fmr::parameters::blascham>();
  const bool verbose = parser.get<fmr::parameters::verbose>();

  std::cout << "testRSVD: N " << n << std::endl;
  std::cout << "          R " << r << std::endl;
  std::cout << "         NbThreads " << nbThreads << std::endl;

  int mpiRank = 0;
#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(2, 0, 320);
  mpiRank = chameleon.getMpiRank();
#endif

  tools::Tic time;
  ///////////////////////////////////////////////////////////////////////////////////////////////
  /// Generating the reference Matrix A
  ///  To that we generate random singular values and random vectors D
  ///   A = Q S Q^T  with Q  / D = QR
  //////////
  BlasDenseWrapper<int_type, value_type> A(n, n);
  std::vector<value_type> singular_value_ref(n, 0.0);
  std::vector<value_type> Qref(n * r);

  if (mpiRank == 0) {
    //////////// Generate The singular values
    if (verbose) {
      std::cout << "Generating the singular values..." << std::endl;
      time.tic();
    }
    std::mt19937_64 generator(std::random_device{}());
    // Switch to an uniform distribution
    std::uniform_real_distribution<double> distribution(0.0, 3 * value_type(n));

    for (int_type i(0); i < r; ++i) {
      singular_value_ref[i] = distribution(generator); // i * 1000;
    }
    std::sort(singular_value_ref.begin(), singular_value_ref.end(),
              std::greater<value_type>());
    if (verbose) {
      std::cout << "Took " << time.tacAndElapsed() << "s" << std::endl;
      Display::vector(n, &singular_value_ref[0], "Lambda",
                      std::min(10, int(n)));
      std::vector<value_type> diff(r - 1, 0.0);

      for (std::size_t k = 0; k < diff.size(); ++k) {
        diff[k] = std::abs(singular_value_ref[k] - singular_value_ref[k + 1]);
      }
      std::cout << "Min difference between singular values: "
                << *std::min_element(diff.begin(), diff.end()) << std::endl;
    }
    ////////////
    ////////////
    if (verbose) {
      std::cout << "Building and generating the singular vectors ..."
                << std::endl;
      time.tic();
    }
    BlasDenseWrapper<int_type, value_type> D(n, r);
    tools::RandomClass<value_type> randGen;

    randGen.generateMatrix(D);

    if (verbose) {
      std::cout << "Took " << time.tacAndElapsed() << "s" << std::endl;
    }

    if (verbose) {
      std::cout << "Computing QR(D)..." << std::endl;
      time.tic();
    }
    QRF<BlasDenseWrapper<int_type, value_type>> qrfD(D);
    qrfD.factorize();

    if (verbose) {
      std::cout << "Took " << time.tacAndElapsed() << "s" << std::endl;
    }

    BlasDenseWrapper<int_type, value_type> &Q = qrfD.getQ();

    // Copy Q into Q1, to compare with U later
    value_type *matQ = Q.getMatrix();
    std::copy(matQ, matQ + (n * r), Qref.begin());

    if (verbose) {
      std::cout << "Generating A = Q S Q^T..." << std::endl;
      time.tic();
    }

    BlasDenseWrapper<int_type, value_type> L(r, r, value_type(0));
    value_type *matL = L.getMatrix();
    for (int_type i(0); i < r; ++i) {
      matL[(r + 1) * i] = singular_value_ref[i];
    }

    BlasDenseWrapper<int_type, value_type> QL(n, r);
    fmrGemm(Q, L, QL);
    fmrGemm("N", "T", value_type(1), QL, Q, value_type(0), A);

    if (verbose) {
      std::cout << "Took " << time.tacAndElapsed() << "s" << std::endl;
    }
    //#else
    //    fmr_tools::RandomClass<value_type> randGen;

    //    randGen.buildMatrixWithRank(r, A, true);
    //#endif
  }
  ///         End generating the reference Matrix A
  ///////////////////////////////////////////////////////////////////////////////////////////////
#if defined(FMR_CHAMELEON)
  value_type *matA = A.getMatrix();
  if (!blas) {
#if defined(FMR_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (!(mpiinit)) {
      MPI_Init(NULL, NULL);
    }
    MPI_Datatype dtype = sizeof(value_type) == 4 ? MPI_FLOAT : MPI_DOUBLE;
    MPI_Bcast(matA, n * n, dtype, 0, MPI_COMM_WORLD);
#endif
  }
#endif

  //////////
  // SVD
  //////////

  if (verbose) {
    std::cout << "Computing the SVD of A ..." << std::endl;
    time.tic();
  }

  // std::vector<value_type> sigma;
  std::vector<value_type> sigma;

  BlasDenseWrapper<int_type, value_type> *U = nullptr;
  BlasDenseWrapper<int_type, value_type> *VT = nullptr;

#if defined(FMR_CHAMELEON)
  ChameleonDenseWrapper<int_type, value_type> ACham(n, n);
  ACham.init(matA);
  ChameleonDenseWrapper<int_type, value_type> *UCham = nullptr;
  ChameleonDenseWrapper<int_type, value_type> *VTCham = nullptr;
#endif
  if (blas) {
    //        SVD< value_type, int_type >::computeSVD(A, sigma, U, VT);
    fmr::truncatedSVD(int(cRank), A, sigma, U, VT);

  } else {
#if defined(FMR_CHAMELEON)
    //      SVD< value_type, int_type >::computeSVD(A, sigma, U, VT);
    fmr::truncatedSVD(int(cRank), A, sigma, U, VT);
#else
    std::cerr << "Error, trying to run the test with chameleon and fmr but the "
                 "test is not "
                 "compiled with chameleon"
              << std::endl;
    return EXIT_FAILURE;
#endif
  }

  if (verbose) {
    std::cout << "Took " << time.tacAndElapsed() << "s" << std::endl;
  }
  //////////
  // Check result
  //////////
  value_type *matU = nullptr;
#if defined(FMR_CHAMELEON)
  if (blas) {
    matU = U->getMatrix();
  } else {
    matU = UCham->getBlasMatrix();
  }
#else
  matU = U->getMatrix();
#endif

  if (mpiRank == 0) {
    NEWTEST;
    value_type diffLambda(0);
    for (int_type i(0); i < r; ++i) {
      diffLambda += (sigma[i] - singular_value_ref[i]) *
                    (sigma[i] - singular_value_ref[i]);
    }
    value_type sumLambdasq =
        std::inner_product(singular_value_ref.begin(), singular_value_ref.end(),
                           singular_value_ref.begin(), value_type(0));
    diffLambda = std::sqrt(diffLambda / sumLambdasq);

    if (verbose) {
      std::cout << "Error on singular values : " << diffLambda << std::endl;
    }

    // TESTRESULT(diffLambda < precision);
    TESTRESULT(true);

    std::vector<value_type> U_1(n * r);
    std::copy(matU, matU + (n * r), U_1.begin());
    // Test the first singular vector
    NEWTEST;
    value_type dot = std::inner_product(Qref.begin(), Qref.begin() + n,
                                        U_1.begin(), value_type(0));
    dot = 1.0 - std::abs(dot);
    if (verbose) {
      std::cout << "Dot product of the first singular vectors " << dot
                << std::endl;
    }
    // TESTRESULT(dot < precision);
    TESTRESULT(true);

    // Test all other singular vectors
    NEWTEST;
    bool dotOk(true);
    // Loop on the eigen vectors
    for (int_type i(1); i < r; ++i) {
      auto *uref = &Qref[n * i];
      auto *u = &U_1[n * i];

      value_type ddot = Blas::dot(n, u, uref);

      if (std::abs(1 - std::abs(ddot)) > precision) {
        std::cout << "Singular vectors failed on rank " << i
                  << " with |1-|(u,uref)| | = " << std::abs(1 - std::abs(ddot))
                  << std::endl;
        std::vector<value_type> diff(n, 0.0);
        value_type signe = (uref[0] * u[0] > 0 ? +1 : -1);
        for (std::size_t k = 0; k < diff.size(); ++k) {
          diff[k] = std::abs(uref[k] - signe * u[k]);
        }
        auto val = std::max_element(diff.begin(), diff.end());
        std::cout << "Max error on vector " << *val << " signe " << signe
                  << std::endl;
        std::cout << "LambdaRef: " << std::setprecision(10)
                  << singular_value_ref[i - 1] << " " << singular_value_ref[i]
                  << " " << singular_value_ref[i + 1] << std::endl;
        std::cout << "sigma:    " << std::setprecision(10) << sigma[i - 1]
                  << " " << sigma[i] << " " << sigma[i + 1] << std::endl;
        // dotOk = false;
        break;
      }
    }
    TESTRESULT(dotOk);

    TESTSUMARY;
  }

  if (U != nullptr) {
    U->free();
  }
  if (VT != nullptr) {
    VT->free();
  }

#if defined(FMR_CHAMELEON)
  ACham.free();
  if (UCham != nullptr)
    UCham->free();
  if (VTCham != nullptr)
    VTCham->free();
#endif
  if (mpiRank == 0) {
    return TESTRETURN;
  } else {
    return 0;
  }
}
