/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iomanip>
#include <iostream>
#include <limits>
#include <random>

#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include "commonParameters.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/Algorithms/randomSVD.hpp"
#include "fmr/Algorithms/truncatedSVD.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/StandardLRA/SVD.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/Tic.hpp"

using namespace fmr;

static int nbTest = 0;
static int nbPassed = 0;

#define NEWTEST ++nbTest
#define TESTRESULT(RES)                                                        \
  {                                                                            \
    if (RES) {                                                                 \
      std::cout << "\033[32m[Test " << nbTest << "] PASSED\033[0m"             \
                << std::endl;                                                  \
      ++nbPassed;                                                              \
    } else {                                                                   \
      std::cout << "\033[31m[Test " << nbTest << "] FAILED\033[0m"             \
                << std::endl;                                                  \
    }                                                                          \
  }

#define TESTSUMARY                                                             \
  if (nbTest == 0) {                                                           \
    std::cout << "No tests to summarise" << std::endl;                         \
    return 0;                                                                  \
  } else if (nbPassed == nbTest) {                                             \
    std::cout << "\033[32m";                                                   \
  } else if (nbPassed == nbTest - 1) {                                         \
    std::cout << "\033[33m";                                                   \
  } else {                                                                     \
    std::cout << "\033[31m";                                                   \
  }                                                                            \
  std::cout << std::setprecision(0) << std::fixed;                             \
  std::cout << "Passed " << nbPassed << "/" << nbTest << " - ["                \
            << 100.0 * nbPassed / nbTest << "%]\033[0m" << std::endl;

#define TESTRETURN (nbTest == nbPassed) ? EXIT_SUCCESS : EXIT_FAILURE

template <class DenseWrapper> int testRSVD(int argc, char *argv[]) {

  using value_type = typename DenseWrapper::value_type;
  using int_type = typename DenseWrapper::int_type;

  /* Parameter handling */
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{},
      fmr::parameters::nbrows{},
      fmr::parameters::nbcols{},
      fmr::parameters::prank{},
      fmr::parameters::oversampling{},
      fmr::parameters::epsilon{},
      fmr::parameters::sym{},
      fmr::parameters::blascham{},
      fmr::parameters::verbose{}
      );
  parser.parse(argc, argv);

  const int_type n = parser.get<fmr::parameters::nbrows>();
  const int_type p = parser.get<fmr::parameters::nbcols>();
  const int_type rank = parser.get<fmr::parameters::prank>();
  int_type os = parser.get<fmr::parameters::oversampling>();
  const value_type precision = parser.get<fmr::parameters::epsilon>();
  const bool symm = parser.get<fmr::parameters::sym>();
  const bool verbose = parser.get<fmr::parameters::verbose>();

  int mpiRank = 0;
#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon;
  mpiRank = chameleon.getMpiRank();
#endif

  if (rank + os > p) {
    os = p - rank;
  }

  if (mpiRank == 0) {
    std::cout << "testRSVD:\n";
    std::cout << "      Nrows        " << n << std::endl
              << "      Ncols        " << p << std::endl;
    std::cout << "      Rank         " << rank << std::endl;
    std::cout << "      Oversampling " << os << std::endl;
  }
  if (rank > std::min(p, n)) {
    std::cerr << "Error: rank > std::min(p, n) but should be lower\n";
    std::exit(EXIT_FAILURE);
  }
  bool b_symm = n != p ? false : symm;
  DenseWrapper A(n, p);

  /////////////////////////////////////////////////////////////////////////
  /// Generate the matrix A with given rank and given singular values
  tools::RandomClass<value_type> randGen;
  std::vector<value_type> singular_value_ref(rank, 0.0);
  {
    /// Generate the reference singular value s;
    std::mt19937_64 generator(
        0 /*std::random_device{}()*/); // use the same seed

    // Switch to an uniform distribution
    std::uniform_real_distribution<value_type> distribution(
        0.0, 3 * value_type(std::min(p, n)));

    for (int_type i(0); i < rank; ++i) {
      singular_value_ref[i] = distribution(generator);
    }
    std::sort(singular_value_ref.begin(), singular_value_ref.end(),
              std::greater<value_type>());
    randGen.buildMatrixWithRank(singular_value_ref, A, b_symm, verbose);
  }
  ///
  /////////////////////////////////////////////////////////////////////////
  ///
  std::cout << "Reference singular values \n";
  for (int_type i = 0; i < std::min(10, rank); ++i) {
    std::cout << " " << singular_value_ref[i];
  }
  std::cout << std::endl;
  ///
  /// Perform an SVD
  ///
  //  Display::matrix(n, p, A.getBlasMatrix(), "A", n, p);
  int_type m = std::min(n, p);

  std::vector<value_type> ApproxSingularValues(m);
  DenseWrapper U;
  DenseWrapper VT;
  value_type energy, estimator;

  //  fmr::truncatedSVD(m, A, ApproxSingularValues, U, VT);
  //

  fmr::randomSVD<value_type, DenseWrapper>(
      int(rank), os, 0, A, ApproxSingularValues, U, VT, energy, estimator);

  std::cout << "Computed singular values \n";
  for (int_type i = 0; i < std::min(10, rank); ++i) {
    std::cout << " " << ApproxSingularValues[i];
  }
  std::cout << std::endl;
  value_type error{0.0};
  for (int_type i = 0; i < rank; ++i) {
    error = std::max(error,
                     std::abs(ApproxSingularValues[i] - singular_value_ref[i]) /
                         singular_value_ref[i]);
  }
  std::cout << " Relative Error(singular values) " << error << std::endl;
  if (error < precision) {
    std::cout << "Test is Ok\n";
  } else {
    std::cout << "Test is False\n";
  }
  TESTRESULT(error < precision);
  return EXIT_SUCCESS;
}

////
/// \brief main
/// \param argc
/// \param argv
/// \return
///  \code
///  // Check for a rank 4 30x15 matrix
///  UTests/testRSVD --verbose 1 --nbrows 30 --nbcols 15  --rank 8
///   // Check for a rank 10 symetric 30x130 matrix
///  UTests/testRSVD --verbose --nbrows 30 --nbcols 30 --sym 0 --rank 10
/// \endcode
///
int main(int argc, char *argv[]) {

  using value_type = float;
  using int_type = int;

#ifdef FMR_CHAMELEON
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{},
      fmr::parameters::nbrows{},
      fmr::parameters::nbcols{},
      fmr::parameters::prank{},
      fmr::parameters::oversampling{},
      fmr::parameters::epsilon{},
      fmr::parameters::sym{},
      fmr::parameters::blascham{},
      fmr::parameters::verbose{}
      );
  parser.parse(argc, argv);
  const bool blas = parser.get<fmr::parameters::blascham>();
  if (blas) {
    testRSVD<BlasDenseWrapper<int_type, value_type>>(argc, argv);
  } else {
    testRSVD<ChameleonDenseWrapper<int_type, value_type>>(argc, argv);
  }
#else
  testRSVD<BlasDenseWrapper<int_type, value_type>>(argc, argv);
#endif

  return EXIT_SUCCESS;
}
