/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <cstring>
#include <iomanip>
#include <iostream>
#include <random>

#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include "commonParameters.hpp"
#include "fmr/Utils/Display.hpp" // To display the matrices in verbose mode
#include "fmr/Utils/MatrixNorms.hpp"
#include "fmr/Utils/Tic.hpp" // Chrono
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#if defined(FMR_USE_MPI)
#include "mpi.h"
#endif
#endif
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"

using namespace fmr;

static int nbTest = 0;
static int nbPassed = 0;

#define NEWTEST ++nbTest
#define TESTRESULT(RES)                                                        \
  {                                                                            \
    if (RES) {                                                                 \
      std::cout << "\033[32m[Test " << nbTest << "] PASSED\033[0m"             \
                << std::endl;                                                  \
      ++nbPassed;                                                              \
    } else {                                                                   \
      std::cout << "\033[31m[Test " << nbTest << "] FAILED\033[0m"             \
                << std::endl;                                                  \
    }                                                                          \
  }

#define TESTSUMARY                                                             \
  if (nbTest == 0) {                                                           \
    std::cout << "No tests to summarise" << std::endl;                         \
    return 0;                                                                  \
  } else if (nbPassed == nbTest) {                                             \
    std::cout << "\033[32m";                                                   \
  } else if (nbPassed == nbTest - 1) {                                         \
    std::cout << "\033[33m";                                                   \
  } else {                                                                     \
    std::cout << "\033[31m";                                                   \
  }                                                                            \
  std::cout << std::setprecision(0) << std::fixed;                             \
  std::cout << "Passed " << nbPassed << "/" << nbTest << " - ["                \
            << 100.0 * nbPassed / nbTest << "%]\033[0m" << std::endl;

#define TESTRETURN (nbTest == nbPassed) ? EXIT_SUCCESS : EXIT_FAILURE

template <class T1, class T2>
static void test(const int verbose, const int displaySize, T1 *cmp,
                 T1 *cmpArray, T1 *q, T1 *r, T1 *tab, T2 *A, T2 *R, T2 *CMP,
                 T2 *Q, T1 precision);

#define CALL_TEST                                                              \
  MatrixWrapper A(l, c);                                                       \
  MatrixWrapper R(l, c);                                                       \
  MatrixWrapper CMP(l, c);                                                     \
  MatrixWrapper Q(l, k);                                                       \
                                                                               \
  test(verbose, displaySize, cmp, cmpArray, q, r, tab, &A, &R, &CMP, &Q,       \
       precision);                                                             \
                                                                               \
  Q.free();                                                                    \
  A.free();                                                                    \
  R.free();                                                                    \
  CMP.free();

int main(int argc, char **argv) {

  using Real = float;
  using FSize = unsigned long long int;

  /* Parameter handling */
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{}, fmr::parameters::nbrows{},
      fmr::parameters::nbcols{}, fmr::parameters::nbthreads{},
      fmr::parameters::epsilon{}, fmr::parameters::verbose{},
      fmr::parameters::blascham{}
      );
  parser.parse(argc, argv);

  const FSize l = parser.get<fmr::parameters::nbrows>();
  const FSize c = parser.get<fmr::parameters::nbcols>();
  const int displaySize = (l == 3 && c == 3) ? 3 : 10;
  const FSize k = std::min(l, c); // This will represent the number of
                                  // Householder's transformation that defines Q
  const int nbThreads = parser.get<fmr::parameters::nbthreads>();
  const Real precision = parser.get<fmr::parameters::epsilon>();
  const bool verbose = parser.get<fmr::parameters::verbose>();
  // to choose to test either Blas (0) or Chameleon (1)
  const int BlasChameleon = parser.get<fmr::parameters::blascham>();

  if (verbose) {
      std::cout << "Tests the QRF class by Generating a random matix of size 2000x2000 by default,\n\
                    then computes it's QR factorization and Q*R to compares that to the original matrix.\n\
                    If the size specified is 3x3, a fixed matrix is used." << std::endl;
  }

  std::cout << "testQRF: NbRows " << l << std::endl;
  std::cout << "         NbCols " << c << std::endl;
  std::cout << "         NbThreads " << nbThreads << std::endl;
  std::cout << "         BlasChameleon " << BlasChameleon << std::endl;

  int mpiRank = 0;
#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(nbThreads, 0, 320);
  // Initialize MPI
  mpiRank = chameleon.getMpiRank();
#endif

  // Initializes Random Number Generator
  std::mt19937_64 generator(std::random_device{}());
  std::normal_distribution<Real> distribution(0.0, 1.0);
  distribution(generator);
  if (verbose && mpiRank == 0) {
    std::cout << "A is " << l << " by " << c << std::endl << std::endl;
  }
  tools::Tic time;
  time.tic();
  if (verbose && mpiRank == 0) {
    std::cout << "Generating A.... ";
  }
  Real *cmp = new Real[l * c];
  Real *cmpArray = new Real[l * c];
  Real *q = new Real[l * k];
  Real *r = new Real[l * c];
  Real *rtmp = new Real[k * c];
  Real *tab = new Real[l * c];

  for (FSize i = 0; i < l * c; ++i) {
    tab[i] = Real(distribution(generator));
  }
  /* If the input parameters specify the matrix to be 3x3,
   * generates the following matrix :
   *  12 -51   4
   *   6 167 -68
   *  -4  24 -41
   * Wich QR decomposition should be
   *
   * Q :  6/7 -69/175 -58/175
   *      3/7 158/175   6/175
   *     -2/7    6/35  -33/35
   *
   * R : 14  21 -14
   *      0 175 -70
   *      0   0  35
   *
   */
  if (l == 3 && c == 3) {
    tab[0] = 12;
    tab[1] = 6;
    tab[2] = -4;
    tab[3] = -51;
    tab[4] = 167;
    tab[5] = 24;
    tab[6] = 4;
    tab[7] = -68;
    tab[8] = -41;
  }

  /*
   * For a 4 by 3 Matrix, we use
   * 1 -1  4
   * 1  4 -2
   * 1  4  2
   * 1 -1  0
   * We should get Q :
   *
   * 1/2 -1/2  1/2
   * 1/2  1/2 -1/2
   * 1/2  1/2  1/2
   * 1/2 -1/2 -1/2
   *
   * And R :
   * 2  3  2
   * 0  5 -2
   * 0  0  4
   *
   */
  if (l == 4 && c == 3) {
    tab[0] = 1;
    tab[1] = 1;
    tab[2] = 1;
    tab[3] = 1;
    tab[4] = -1;
    tab[5] = 4;
    tab[6] = 4;
    tab[7] = -1;
    tab[8] = 4;
    tab[9] = -2;
    tab[10] = 2;
    tab[11] = 0;
  }

#if defined(FMR_USE_MPI)
  int mpiinit;
  MPI_Initialized(&mpiinit);
  if (!(mpiinit)) {
    MPI_Init(NULL, NULL);
  }
  MPI_Datatype dtype = sizeof(Real) == 4 ? MPI_FLOAT : MPI_DOUBLE;
  MPI_Bcast(tab, l * c, dtype, 0, MPI_COMM_WORLD);
#endif
  time.tac();
  if (verbose && mpiRank == 0) {
    std::cout << "took : " << time.elapsed() << "s" << std::endl;
  }
  time.reset();

  if (BlasChameleon == 0) {
    using MatrixWrapper = BlasDenseWrapper<FSize, Real>;
    CALL_TEST;
  } else {
#if defined(FMR_CHAMELEON)
    using MatrixWrapper = ChameleonDenseWrapper<FSize, Real>;
    CALL_TEST;
#endif
  }

  // Free memory
  delete[] cmp;
  delete[] cmpArray;
  delete[] q;
  delete[] r;
  delete[] rtmp;
  delete[] tab;

  if (mpiRank == 0) {
    TESTSUMARY;
    return TESTRETURN;
  } else {
    return 0;
  }
}

template <class T1, class T2>
static void test(const int verbose, const int displaySize, T1 *cmp,
                 T1 *cmpArray, T1 *q, T1 *r, T1 *tab, T2 *A, T2 *R, T2 *CMP,
                 T2 *Q, T1 precision) {
  (void)Q;
  int mpiRank = 0;
#if defined(FMR_USE_MPI)
  // Initialize MPI
  int mpiinit;
  MPI_Initialized(&mpiinit);
  if (mpiinit) {
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  }
#endif

  //  using FSize = unsigned long long int;
  tools::Tic time;
  auto l = A->getNbRows();
  auto c = A->getNbCols();
  auto k = std::min(l, c);

  A->init(tab);

  if (verbose && mpiRank == 0) {
    Display::matrix(3, 3, tab, "A", displaySize);
    std::cout << "Factorizing A = QR....";
  }
  Display::matrix(A->getNbRows(), A->getNbCols(), A->getBlasMatrix(),
                  "A_ori: ", displaySize, displaySize);
  time.tic();
  QRF<T2> qrf(*A); // Creating a QR wrapper
  qrf.factorize();
  time.tac();
  Display::matrix(A->getNbRows(), A->getNbCols(), A->getBlasMatrix(),
                  "Q.factorize: ", displaySize, displaySize);
  if (verbose && mpiRank == 0) {
    std::cout << " took " << time.elapsed() << "s" << std::endl;
  }
  time.reset();

  R = qrf.getR();
  R->copyMatrix(r);

  if (mpiRank == 0) {
    T1 normQInf = 0.0;
    for (std::size_t i = 0; i < k; ++i) {
      for (std::size_t j = i + 1; j < k; ++j) {
        normQInf += r[i * k + j] * r[i * k + j];
      }
    }
    NEWTEST;
    if (verbose) {
      std::cout << "Testing that R is Triangular : ";
    }

    TESTRESULT(std::sqrt(normQInf) < precision);

    if (std::sqrt(normQInf) < precision) {
      if (verbose) {
        std::cout << "OK" << std::endl;
      }
    } else {
      if (verbose) {
        std::cout << "KO : R is not triangular" << std::endl;
      }
    }
  }
  if (verbose && mpiRank == 0) {
    Display::matrix(3, 3, r, "R", displaySize);
  }
  if (verbose && mpiRank == 0) {
    std::cout << "Calculating Q*R....";
  }
  T2 QR(l, c); // init QR data with R before applying Q to it to obtain QR
  QR.init(R->getMatrix());
  time.tic();
  qrf.multiplyQ("N", QR);
  time.tac();
  if (verbose && mpiRank == 0) {
    std::cout << " took " << time.elapsed() << "s" << std::endl;
  }
  QR.copyMatrix(r);
  if (verbose) {
    Display::matrix(3, 3, r, "Q*R", displaySize);
  }

  for (std::size_t i = 0; i < l * c; ++i) {
    cmp[i] = tab[i] - r[i];
  }
  NEWTEST;
  CMP->init(cmp);
  auto normCMP = computeFrobenius(*CMP);
  auto normA = computeFrobenius(*A);
  if (verbose && mpiRank == 0) {
    std::cout << "Absolute error : ||Q*R - A|| / ||A|| " << normCMP / normA
              << std::endl;
  }
  if (mpiRank == 0) {
    TESTRESULT(normCMP / normA < precision);
  }
  // Testing Q generation.
  time.reset();
  if (verbose && mpiRank == 0) {
    std::cout << "Rebuilding Q.... ";
  }
  time.tic();

  qrf.makeQ();
  time.tac();

  A->copyMatrix(q);
  if (verbose) {
    Display::matrix(l, c, q, "Q", displaySize);
  }
  NEWTEST;
  if (verbose && mpiRank == 0) {
    std::cout << "took " << time.elapsed() << "s" << std::endl;
    std::cout << "Computing Qt * Q...";
  }
  time.reset();
  time.tic();
  T2 Q2(l, c);
  Q2.init(q);
  T2 QtQ(c, c);
  fmrGemm("T", "N", T1(1.0), Q2, *A, T1(0.0), QtQ);
  time.tac();
  if (verbose && mpiRank == 0) {
    std::cout << " took " << time.elapsed() << "s" << std::endl;
  }
  time.reset();
  if (verbose && mpiRank == 0) {
    std::cout << "Checking that Qt * Q is the identity matrix..." << std::endl;
  }
  T1 *qtBlas = QtQ.getBlasMatrix();
  for (std::size_t i(0); i < c; ++i) { // Computing (Qt * Q) - Id
    qtBlas[i * c + i] -= 1;
  }
  QtQ.init(qtBlas);
  auto normQtQ = computeFrobenius(QtQ);
  if (verbose && mpiRank == 0) {
    std::cout << "||(Qt * Q) - Id|| = " << normQtQ << std::endl;
  }
  TESTRESULT(normQtQ < precision);
  // Reseting old matrices
  for (std::size_t i = 0; i < l * c; ++i) {
    cmp[i] = 0.;
  }
  CMP->init(cmp);
  NEWTEST;
  time.reset();
  if (verbose && mpiRank == 0) {
    std::cout << "Computing Q*R.... ";
  }
  time.tic();

  fmrGemm(*A, *R, *CMP);
  normA = computeFrobenius(*A);
  CMP->copyMatrix(cmpArray);
  time.tac();
  if (verbose && mpiRank == 0) {
    std::cout << "took " << time.elapsed() << "s" << std::endl;
  }
  if (mpiRank == 0) {
    if (verbose && mpiRank == 0) {
      Display::matrix(l, c, cmpArray, "Q*R", displaySize);
    }

    // Computing Q*R - A
    for (std::size_t i = 0; i < l * c; ++i) {
      cmp[i] = cmpArray[i] - tab[i];
    }

    T1 normCMP = 0.0;
    for (std::size_t i = 0; i < l * c; ++i) {
      normCMP += cmp[i] * cmp[i];
    }
    normCMP = std::sqrt(normCMP);
    if (verbose && mpiRank == 0) {
      std::cout << "Absolute error : ||Q*R - A|| / ||A|| " << normCMP / normA
                << std::endl;
    }
    if (mpiRank == 0) {
      TESTRESULT(normCMP / normA < precision);
    }
  }
}
