/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_TESTS_COMMONPARAMETERS
#define FMR_TESTS_COMMONPARAMETERS

#include <cpp_tools/cl_parser/tcli.hpp>

namespace fmr::parameters {

struct inputfile : cpp_tools::cl_parser::required_tag
{
  cpp_tools::cl_parser::str_vec flags = {"--input-file", "-if"};
  const char* description =
      "Path to the input file (.csv, .txt, .tar.gz, .bz2, .h5, accepted)";
  using type = std::string;
};

struct inputh5dataset : cpp_tools::cl_parser::required_tag
{
  cpp_tools::cl_parser::str_vec flags = {"--input-h5dataset", "-id"};
  const char* description =
      "Name of the hdf5 dataset";
  using type = std::string;
  type def = "distance";
};

struct nbthreads
{
  cpp_tools::cl_parser::str_vec flags = {"--nthreads", "-nt"};
  const char* description = "Number of threads (int)";
  using type = int;
  type def = 2;
};

struct nbrows
{
  cpp_tools::cl_parser::str_vec flags = {"--nrows", "-nr"};
  const char* description = "Number of rows of the main matrix (int)";
  using type = int;
  type def = 100;
};

struct nbcols
{
  cpp_tools::cl_parser::str_vec flags = {"--ncols", "-nc"};
  const char* description = "Number of column of the main matrix (int)";
  using type = int;
  type def = 70;
};

struct nbcolsb
{
  cpp_tools::cl_parser::str_vec flags = {"--ncolsb", "-ncb"};
  const char* description = "Number of column in B to test GEMM (int)";
  using type = int;
  type def = 70;
};

struct trank
{
  cpp_tools::cl_parser::str_vec flags = {"--true-rank", "-tr"};
  const char* description = " The rank to generate the matrix (int)";
  using type = int;
  type def = 10;
};

struct prank
{
  cpp_tools::cl_parser::str_vec flags = {"--p-rank", "-pr"};
  const char* description = "Prescribed rank (int)";
  using type = int;
  type def = 10;
};

struct oversampling
{
  cpp_tools::cl_parser::str_vec flags = {"--oversampling", "-os"};
  const char* description = "Over sampling parameter (int)";
  using type = int;
  type def = 10;
};

struct epsilon
{
  cpp_tools::cl_parser::str_vec flags = {"--epsilon", "-ep"};
  const char* description = "Prescribed accuracy to accept the test (float, e.g. 1e-6)";
  using type = double;
  type def = 1.e-4;
};

struct sym
{
  cpp_tools::cl_parser::str_vec flags = {"--sym", "-s"};
  const char* description = "To generate a symetric matrix during testRSVD (1) or not (0, default)";
  using type = bool;
  enum { flagged };
};

struct blascham
{
  cpp_tools::cl_parser::str_vec flags = {"--blas", "-b"};
  const char* description = "Enable Blas instead of Chameleon when both are available (1) or not (0, default)";
  using type = bool;
  enum { flagged };
};

struct verbose
{
  cpp_tools::cl_parser::str_vec flags = {"--verbose", "-v"};
  const char* description = "Enable verbose (1) or not (0, default)";
  using type = bool;
  enum { flagged };
};

}
#endif
