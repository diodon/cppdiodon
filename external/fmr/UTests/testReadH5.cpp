/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iostream>
#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include "commonParameters.hpp"
#include "fmr/Utils/MatrixIO.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

using namespace fmr;

template<typename FSize, typename FReal>
int testReadH5Blas(int, const std::string&, std::string);
#if defined(FMR_CHAMELEON)
template<typename FSize, typename FReal>
int testReadH5Cham(int, const std::string&, std::string);
#endif

int main(int argc, char **argv) {
  using FReal = float;
  using FSize = int;
  int mpiRank = 0;

  /* Parameter handling */
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{},
      fmr::parameters::inputfile{},
      fmr::parameters::inputh5dataset{}
      );
  parser.parse(argc, argv);

  /* input matrix filename and datasetname */
  const std::string filename = parser.get<fmr::parameters::inputfile>();
  const std::string dataset = parser.get<fmr::parameters::inputh5dataset>();

#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(2);
  mpiRank = chameleon.getMpiRank();
#endif
  // Call Tests
  if (mpiRank == 0) {
      std::cout << "----------------------------------" << std::endl;
      std::cout << "[FMR] TestReadH5 Parameters Summary" << std::endl;
      std::cout << "[FMR] Dataset filename " << filename << std::endl;
      std::cout << "[FMR] Dataset name " << dataset << std::endl;
      std::cout << "----------------------------------" << std::endl;
  }

  int ret;

  /* only perform blas test if file format is h5 (an not .txt metadata for
  multiple blocks) */
  if (filename.find(".h5") != std::string::npos) {
      if (mpiRank == 0) {
          std::cout << "[FMR] Test TestReadH5 Blas" << std::endl;
      }
      ret = testReadH5Blas<FSize, FReal>(mpiRank, filename, dataset);
      if (ret != EXIT_SUCCESS){
          std::cout << "[FMR] Test TestReadH5 Blas error" << std::endl;
      }
  }

#if defined(FMR_CHAMELEON)
  if (mpiRank == 0) {
      std::cout << "[FMR] Test TestReadH5 Chameleon" << std::endl;
  }
  ret = testReadH5Cham<FSize, FReal>(mpiRank, filename, dataset);
  if (ret != EXIT_SUCCESS){
      std::cout << "[FMR] Test TestReadH5 Chameleon error" << std::endl;
  }
#endif

  return EXIT_SUCCESS;
}

template<typename FSize, typename FReal>
int testReadH5Blas(int mpiRank, const std::string& filename, std::string dataset){

  if (mpiRank == 0) {
      std::cout << "Starting test " << ": BlasDenseWrapper constructor read hdf5 dataset" << std::endl;
  }
  if (filename.find(".h5") == std::string::npos) {
    const std::size_t found = filename.find_last_of("/");
    const std::string datasetpathname(filename.substr(0,found));
    dataset = datasetpathname;
  }
  std::cout << "filename " << filename << std::endl;
  std::cout << "dataset " << dataset << std::endl;
  BlasDenseWrapper<FSize, FReal> mat_h5;
  io::readHDF5(mat_h5, filename, dataset);

  std::cout << "Matrix size " << mat_h5.getNbRows() << " " << mat_h5.getNbCols() << std::endl;

  if (mat_h5.getNbRows() > 0 && mat_h5.getNbCols() > 0 && mpiRank == 0) {
    std::string filenameout = filename + ".out";
    std::cout << "Starting test " << ": DenseWrapper write hdf5 dataset" << std::endl;
    io::writeHDF5(mat_h5, filenameout, "/matrix");
  }

  return EXIT_SUCCESS;
}

#if defined(FMR_CHAMELEON)
template<typename FSize, typename FReal>
int testReadH5Cham(int mpiRank, const std::string& filename, std::string dataset){

  if (mpiRank == 0) {
      std::cout << "Starting test " << ": ChameleonDenseWrapper constructor read hdf5 dataset" << std::endl;
  }
  if (filename.find(".h5") == std::string::npos) {
    const std::size_t found = filename.find_last_of("/");
    const std::string datasetpathname(filename.substr(0,found));
    dataset = datasetpathname;
  }
  std::cout << "filename " << filename << std::endl;
  std::cout << "dataset " << dataset << std::endl;

  /* Try reading considering matrix is symmetric (we only read the upper
  triangular part) */
  ChameleonDenseWrapper<FSize, FReal> mat_h5(0, 0, true);
  io::readHDF5(mat_h5, filename, dataset);
  std::cout << "Symmetric Matrix size " << mat_h5.getNbRows() << " " << mat_h5.getNbCols() << std::endl;

  /* need to add a runtime barrier here because we will call MPI on our own just after */
  Chameleon::barrier();

  /* Try reading the full matrix */
  mat_h5.reset(0, 0, false);
  io::readHDF5(mat_h5, filename, dataset);
  std::cout << "Full Matrix size " << mat_h5.getNbRows() << " " << mat_h5.getNbCols() << std::endl;

  if (mat_h5.getNbRows() > 0 && mat_h5.getNbCols() > 0) {
      std::string filenameout = filename + ".out";
      if (mpiRank == 0) {
          std::cout << "Starting test " << ": DenseWrapper write hdf5 dataset" << std::endl;
      }
      io::writeHDF5(mat_h5, filenameout, "/matrix");
  }

  return EXIT_SUCCESS;
}
#endif
