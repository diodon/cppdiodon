/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_EXAMPLES_COMMONPARAMETERS
#define FMR_EXAMPLES_COMMONPARAMETERS

#include <cpp_tools/cl_parser/tcli.hpp>

namespace fmr::parameters {

struct prank {
  cpp_tools::cl_parser::str_vec flags = {"--p-rank", "-pr"};
  const char* description = "Prescribed rank (int)";
  using type = int;
};

struct pacc {
  cpp_tools::cl_parser::str_vec flags = {"--p-epsilon", "-ps"};
  const char* description = "Prescribed accuracy (double)";
  using type = double;
  type def = 1.e-3;
};
struct pacc_maxrank  : cpp_tools::cl_parser::required_tag{
  cpp_tools::cl_parser::str_vec flags = {"--max-rank", "-ms"};
  const char* description = "maximal rank for prescribed accuracy (int)";
  using type = int;
};
struct pacc_blocsize {
  cpp_tools::cl_parser::str_vec flags = {"--block-size", "-bs"};
  const char* description = "Block size for prescribed accuracy (int)";
  using type = int;
  type def = 10;
    };
struct trank {
  cpp_tools::cl_parser::str_vec flags = {"--true-rank", "-tr"};
  const char* description = " The rank to generate the matrix (int)";
  using type = int;
  type def = 10;
};

struct nbrows {
  cpp_tools::cl_parser::str_vec flags = {"--nrows", "-nr"};
  const char* description = "Number of rows (int)";
  using type = int;
  type def = 100;
};

struct nbcols {
  cpp_tools::cl_parser::str_vec flags = {"--ncols", "-nc"};
  const char* description = "Number of column (int)";
  using type = int;
  type def = 70;
};

struct oversampling {
  cpp_tools::cl_parser::str_vec flags = {"--oversampling", "-os"};
  const char* description = "Over sampling parameter (int)";
  using type = int;
  type def = 10;
};

struct nbSubspaceIter {
  cpp_tools::cl_parser::str_vec flags = {"--subspace-iter", "-si"};
  const char* description = "number of subspace iteration method (int)";
  using type = int;
  type def = 2;
};
struct inputFile  {
  cpp_tools::cl_parser::str_vec flags = {"--input-file", "-if"};
  const char* description =
      "Name of the intput file with extention (.txt, .bin, .h5)";
  using type = std::string;
};
}
#endif
