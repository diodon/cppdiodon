FROM ubuntu:24.04

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y
RUN apt-get install -y git python-is-python3 cmake build-essential gfortran pkg-config libmkl-dev libstarpu-dev libhdf5-mpi-dev zlib1g-dev libbz2-dev wget
RUN apt-get autoremove -y
#RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

RUN wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz && \
    tar xvf chameleon-1.3.0.tar.gz && \
    cd chameleon-1.3.0 && \
    mkdir build && \
    cd build && \
    cmake .. -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=Intel10_64lp -DCHAMELEON_KERNELS_MT=ON -DBUILD_SHARED_LIBS=ON && \
    make -j3 install

ADD . /root/fmr

RUN cd /root/fmr/ && \
    mkdir -p build && \
    git submodule update --init --recursive && \
    cd build && \
    cmake .. -DFMR_BUILD_TESTS=ON -DFMR_USE_HDF5=ON -DFMR_USE_CHAMELEON=ON -DFMR_DLOPEN_HDF5=OFF -DFMR_USE_MKL_AS_BLAS=ON && \
    make VERBOSE=1 && \
    export OMPI_ALLOW_RUN_AS_ROOT=1 && export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1 && export FMR_CHAMELEON_NUM_THREADS=2 && ctest -V && \
    cmake .. -DFMR_BUILD_TESTS=OFF && \
    make install
