/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef AdaptiveRRF_HPP
#define AdaptiveRRF_HPP

// standard includes
#include <cstdio>
#include <cstdlib>
#include <vector>
//
#include <cpp_tools/colors/colorized.hpp>

// FMR includes
#include "fmr/RandomizedLRA/CommonAlgorithm.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/Tic.hpp"
#include "fmr/Utils/constants.hpp"
#include "fmr/Utils/MatrixNorms.hpp"

namespace fmr {

/**
 * @authors Pierre Blanchard (pierre.blanchard@inria.fr)
 *          Olivier Coulaud
 *
 * @date July 1st, 2014
 *
 */

/**
 * @brief The Adaptive Random Range Finder class
 *
 *
 * This a blocked version to compute the rank
 */
template <class Real, class MatrixWrapperClass> class AdaptiveRRF {
  const int NVALS = 10;
public:
  using MatrixWrapperClass_type = MatrixWrapperClass;
  using Size = typename MatrixWrapperClass::int_type;
  using value_type = Real;

 private:
  // needed for matrix multiplications
  MatrixWrapperClass
      *_MatrixWrapper; ///< A pointer on the matrix class we search the rank
  // prescribed rank (only needed to get range size with a generic function)
  const Size _epsilon;  ///< the prescribed accuracy
  // blocking parameter
  int _blockSize; ///< blocking parameter
  // the found rank,
  const Size _finalRank; ///< the found rank
  // the maximal rank
  const Size _max_rank; ///< maximum rank allowed to be computed
  // number of subspace iteations, the parameter q in Halko paper
  const int _numberOfSubspaceIteration; ///< number of subspace iterations
  // final rank (only needed to get range size with a generic function)

  const int _rbal;



public:
  /*
   * Ctor
   */
  explicit AdaptiveRRF(MatrixWrapperClass inMatrixWrapper,
                       const int &in_magnitude, const int in_rbal,
                       const int &in_max_rank, const int &in_qPOW)
      : _MatrixWrapper(inMatrixWrapper),
        _epsilon(Real(std::pow(Real(.1), Real(in_magnitude)))),  _blockSize(in_rbal),_rbal(in_rbal),
        _finalRank(0), _max_rank(in_max_rank), _numberOfSubspaceIteration(in_qPOW) {}
  explicit AdaptiveRRF(MatrixWrapperClass *inMatrixWrapper, const Real &inEps,
                       const int &in_rbal, const int &in_max_rank,
                       const int &in_qPOW)
      : _MatrixWrapper(inMatrixWrapper), _epsilon(inEps), _blockSize(in_rbal), _rbal(in_rbal),
        _finalRank(0), _max_rank(in_max_rank), _numberOfSubspaceIteration(in_qPOW) {}
  /*
   * Copy ctor
   */
  /*
   * Dtor
   */
  ~AdaptiveRRF() {}
  ///
  /// \brief retrun the found rank of the matrix
  ///
  auto getRangeSize() { return _finalRank; }
  ///
  /// \brief getMatrixWrapper accessor
  /// \return the pointer on the current matrix
  MatrixWrapperClass *getMatrixWrapper() const { return _MatrixWrapper; }

#ifdef NOT_TO_REMOVE

  /*
   * computeError: This allows the computeError() function from MatrixWrapper to
   * be called from RRF. Needed by RandLowRank Algo
   */
  void computeError(const Size nbRows, const Size nbCols, const Size rank,
                    Real *W, Real *Y) {

    _MatrixWrapper->computeError(nbRows, nbCols, rank, W, Y);
  }
#endif
  /*
   * updateMaxNormYbal:
   */
  void updateMaxNormYbal(const Size nbRows, Real *Y, Real &max_norm_Ybal,
                         Real &norm_Y) {

    Real tmp_Y, norm_Yr;

    // r=0
    norm_Yr = Real(0.);
    for (Size i = 0; i < nbRows; ++i) {
      tmp_Y = Y[i];
      norm_Yr += tmp_Y * tmp_Y;
    }
    // update norm of full Y matrix [nbRows*offset]
    std::cout << "Norm of Yj: " << std::sqrt(norm_Yr) << std::endl;
    norm_Y += norm_Yr;
    // initialize maximum norm of last rbal vectors
    max_norm_Ybal = std::sqrt(norm_Yr);

    // r>0
    for (int r = 1; r < _rbal; ++r) {
      norm_Yr = Real(0.);
      for (Size i = 0; i < nbRows; ++i) {
        tmp_Y = Y[r * nbRows + i];
        norm_Yr += tmp_Y * tmp_Y;
      }
      // update maximum norm of last _rbal vectors
      max_norm_Ybal = std::max(std::sqrt(norm_Yr), max_norm_Ybal);
    }

    // normalize maximum norm of last _rbal vectors by norm of full matrix Y
    max_norm_Ybal /= std::sqrt(norm_Y);
  }
  template <typename FSize>
  Real computeMaxNormY(std::vector<Real> &Y, FSize n, FSize offset,
                       FSize nbVects) {
    FSize i(0);
    Real norm(0);
    Real tmp(0);

    for (i = 0; i < nbVects; ++i) {
      tmp = 0;
      for (int j(0); j < n; ++j) {
        tmp += Y.at((i + offset) * n + j) * Y.at((i + offset) * n + j);
      }
      norm = std::max(norm, tmp);
    }
    return std::sqrt(norm);
  }

  ///
  ///
  ///
  ///
  /// \brief Perform the orthonormalization of the columns of X
  ///
  ///  In input A is the matrix to orthonormailse. We perform a QR factorisation
  ///    of X ans the matrix Q replace the matrix X at the output
  ///
  /// \param[inout] X the matrix to orthonormalize
  ///   ///
  void orthogonalize(MatrixWrapperClass &X) {
    std::clog << cpp_tools::colors::green << "Begin AdaptiveRRF::orthogonalize(mat)"
              << cpp_tools::colors::reset << std::endl;

    QRF<MatrixWrapperClass> QR(X);

    QR.factorize();

    QR.makeQ() ;

    std::cerr << " orthogonalize not yet checked\n";
    std::clog << cpp_tools::colors::green << "End AdaptiveRRF::orthogonalize(mat)"
              << cpp_tools::colors::reset << std::endl;
  }
  void orthogonalize(QRF<MatrixWrapperClass> &X,
                     std::vector<QRF<MatrixWrapperClass>> &QOLD) {
    std::clog << cpp_tools::colors::green << "Begin AdaptiveRRF::orthogonalize(set of mat)"
              << cpp_tools::colors::reset << std::endl;
    value_type zero = value_type(0.0);
    value_type one = value_type(1.0);
    value_type mone = value_type(-1.0);
    auto &Qi = X.getQ();

    int i = QOLD.size();
    const int nbRows = X.getNbRows();
    for (int j = 0; j < i; ++j) {
      auto &Qj = QOLD[j].getQ();
      MatrixWrapperClass C(Qj.getNbRows(), Qi.getNbCols());
      // perform Qi = X ; Qi = Qi - Qj Qj^T Qi
      /// C = Qj^T Qi
          fmrGemm("T", "N", one, Qj, Qi, zero, C);
      // C := alpha*A*B+beta*C, A, B, C
      /// Qi = Qi - Qj C
      fmrGemm("N", "N", mone, Qj, C, one, Qi);
    }
    this->orthogonalize(Qi);
    std::clog << cpp_tools::colors::green << "End AdaptiveRRF::orthogonalize(set of mat)"
              << cpp_tools::colors::reset << std::endl;
  }

  void assemble(QRF<MatrixWrapperClass> &Q, MatrixWrapperClass& B,
                const std::vector<QRF<MatrixWrapperClass>>& listOfQ,
                const std::vector<MatrixWrapperClass>& listOfB){
    std::size_t nb_cols{0};
    auto nb_rows = listOfB[0].getNbRows();
    for (auto &v : listOfB) {
      nb_cols += v.getNbCols();
    }
    std::cout << "Number of Rows " << nb_rows << " nbCols " << nb_cols
              << std::endl;
    std::cerr << " assemble not yet implemented\n";

  }

  /*
   * Find the range of the input matrix with or without explicit A(if A is not
   assembled explicitely then A=n)
   * @brief findRange build an orthogonal matrix Q for a given accuracy epsilon
   *    Algorithm 4.2 in Halko paper
   *     Matrix A nbRows x nbCols matrix and hidden in MatrixWrapperClass
   *      r -> _tbal
   * _numberOfSubspaceIteration should be >0 id the eigenvalues decay slowly. In
   this cas,
   *  we consider the Randomized subspace iteration algorithm (algorithm 4.4
   page 244)

   * using the adaptative variant of RRF algo (Halko).
   * Parameter rank is used as output in order to return the size of the
   computed range
   * but also as input to specify an upper bound for this rank.
   */
  ///
  /// \brief findRange_pb
  ///
  ///
  /// The algorithm is
  /// for i = 1,2, ...
  ///    \f$\Omega_i = randn(n, b)\f$
  ///    \f$Y = A*\Omega_i\f$
  ///    \f$Q = ortho(Y)\f$  compute a QR factorization of \f$Y\f$
  ///     perform the subspaceIterationMethod(A,Q)
  ///
  /// \param Q
  /// \param verbose
  /// \return
  ///
  Size findRange(QRF<MatrixWrapperClass> *&Q, const bool verbose) {
    //
    std::clog << cpp_tools::colors::green << "Begin AdaptiveRRF::findRange(QRF<MatrixWrapperClass>)"
              << cpp_tools::colors::reset << std::endl;
    int mpirank = 0;
     value_type zero = value_type(0.0);
     value_type one = value_type(1.0);
     value_type mone = value_type(-1.0);
    if (Q != nullptr) {
      throw std::invalid_argument("AdaptiveRangeFinder::findRange_pb Q should "
                                  "not be initialised prior to calling "
                                  "findRange.");
    }
    bool istimed = true;
    const char *envTime_s = getenv("FMR_TIME");
    if (envTime_s != NULL) {
      int envTime_i = std::stoi(envTime_s);
      if (envTime_i == 1) {
        istimed = true;
      }
    }
    tools::Tic time;
    double time_rand{0}, time_gemm{0}, time_qr{0}, time_sim{0};
    double timee, gflops;
    const auto shape = _MatrixWrapper->getShape();
    const auto &nbRows = shape[0];
    const auto &nbCols = shape[1];
    // QRF<MatrixWrapperClass>
    std::vector< QRF<MatrixWrapperClass>> listOfQ;
    std::vector<MatrixWrapperClass> listOfB;
    tools::RandomClass<Real> randGen;
    //
    int max_iter =
        1 + static_cast<int>(_max_rank / _blockSize);  //  to find the rank
    for (int i = 0; i < max_iter; ++i) {
      ///////////////////////////////////////////////////////////////////////////////////////////
      //    Generate the random matrix of size nbCols x _blockSize
      ///////////////////////////////////////////////////////////////////////////////////////////
      //
      // Declare and fill the random matrix Omega
      time.tic();
      MatrixWrapperClass Omega(nbCols, _blockSize);
      randGen.generateMatrix(Omega);
      time.tac();
      timee = time.elapsed();
      time_rand += timee;
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:RSVD:SET(Omega)=" << timee << " s"
                  << std::endl;
      }
      ///////////////////////////////////////////////////////////////////////////////////////////
      ///                Compute Y = A Omega    (step 2)
      ///  the matrix A is in the wrapper !!!
      ///////////////////////////////////////////////////////////////////////////////////////////
      MatrixWrapperClass *Y = new MatrixWrapperClass(nbRows, _blockSize);
      time.tic();
      fmrGemm(*_MatrixWrapper, Omega, *Y);
      time.tac();
      timee = time.elapsed();
      time_gemm += timee;
      if (istimed && mpirank == 0) {

        std::cout << "[fmr] TIME:RSVD:GEMM(A,Omega)=" << timee << " s"
                  << std::endl;
        gflops = (1e-9 * FLOPS_GEMM(nbRows, _blockSize, nbCols)) / timee;
        std::cout << "[fmr] PERF:RSVD:GEMM(A,Omega)=" << gflops << " gflop/s"
                  << std::endl;
      }
      // Display::matrix(Y->getNbRows(),Y->getNbCols(),Y->getBlasMatrix(),"Y: ",
      // (int)Y->getNbRows(), (int)Y->getNbCols());
      ///////////////////////////////////////////////////////////////////////////////////////////
      /// Compute thin QR decomposition of Y (step 3) to build an orthogonal
      /// matrix
      ///      Y = Q R  and Q = QRD(Y)
      ///////////////////////////////////////////////////////////////////////////////////////////
      ///
      Q = new QRF<MatrixWrapperClass>(*Y);
      time.tic();
      Q->factorize();
      time.tac();
      timee = time.elapsed();
      time_qr += timee;
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:RSVD:QR(Y)=" << timee << " s" << std::endl;
        gflops = (1e-9 * FLOPS_GEQRF(nbRows, _blockSize)) / timee;
        std::cout << "[fmr] PERF:RSVD:QR(Y)=" << gflops << " gflop/s"
                  << std::endl;
      }
      ///////////////////////////////////////////////////////////////////////////////////////////
      /// Perform Subspace Iterations if required
      ///
      if (_numberOfSubspaceIteration > 0) {
        time.tic();
        const bool formQmatrix = false;
        /// QRFMatrixWrapper_TYPE Y and Q
        /// pointer on MatrixWrapperClass _MatrixWrapper,
        /// In Y contains the QR factoriation on A Omega
        rla::subspaceIterationMethod(_numberOfSubspaceIteration,
                                        *_MatrixWrapper, *Y, *Q, formQmatrix,
                                        verbose);
        time.tac();
        timee = time.elapsed();
        time_sim += timee;
      }
      Y = nullptr; // because Q.Q is an alias on Y
      ///////////////////////////////////////////////////////////////////////////////////////////
      /// Perform the orthogonalization between the current column and the
      /// previous one Qi : orth(Qi-sum_{<j<i-1} Qj Qj^* Qi)
      ///
       this->orthogonalize(*Q, listOfQ);
      /// A = A - Qi Bi_i
      ///
      MatrixWrapperClass &A = *_MatrixWrapper;
      MatrixWrapperClass Bi;
      MatrixWrapperClass Ci;
      time.tic();
      /// Bi = Qi^T A
      fmrGemm("T", "N", one, Q->getQ(), A, zero, Bi);
      // C := alpha*A*B+beta*C, A, B, C
      /// A = A - Qi Bi
      fmrGemm("N", "N", mone, Q->getQ(), Bi, one, A);
      //
      // Check the convergence
      //
      value_type norm =  fmr::computeFrobenius(A);
      std::cout << "Iter= " << i << " norm(A) " << norm << std::endl;
      if (norm < _epsilon) {
        break;
      }
      ///
//      time.tac();
//      timee = time.elapsed();
//      time_gemm += timee;
//      if (istimed && mpirank == 0) {

//        std::cout << "[fmr] TIME:RSVD:GEMM(A,Omega)=" << timee << " s"
//                  << std::endl;
//        gflops = (1e-9 * FLOPS_GEMM(nbRows, _blockSize, nbCols)) / timee;
//        std::cout << "[fmr] PERF:RSVD:GEMM(A,Omega)=" << gflops << " gflop/s"
//                  << std::endl;
//      }

      ///
      listOfQ.push_back(*Q);
      listOfB.push_back(Bi);
      ///  ||A|| < epsilon
      ///
    } // end loop iteration
    //
    // Assemble Q and B
    MatrixWrapperClass Bfinal, Qfinal;
    QRF<MatrixWrapperClass> QR(Qfinal);
    this->assemble(QR, Bfinal, listOfQ, listOfB);
    std::clog << cpp_tools::colors::green << "End AdaptiveRRF::findRange(QRF<MatrixWrapperClass>)"
              << cpp_tools::colors::reset << std::endl;
    return _finalRank;
  }

  /*
   * Find the range of the input matrix with or without explicit C (if C is not
   * assembled explicitely then C=NULL) using the adaptative variant of RRF algo
   * (Halko). Parameter rank is used as output in order to return the size of
   * the computed range but also as input to specify an upper bound for this
   * rank.
   */

  Size findRange_old(QRF<MatrixWrapperClass> *&Q) {
    const auto &nbRows = _MatrixWrapper->getNbRows();
    const auto &nbCols = _MatrixWrapper->getNbCols();
    /// Init random number generator (can be moved to ctor)
    // std::default_random_engine generator(/*no seed*/);
    std::default_random_engine generator(std::random_device{}());
    std::normal_distribution<double> distribution(0.0, 1.0);
    distribution(generator); // init

    /// Draw $r$ standards gaussian vectors w
    std::cout << "\nGenerate white noise W... \n";
    tools::Tic timeNoise;
    timeNoise.tic();
    Real *W = new Real[nbCols * _rbal];
    for (int i = 0; i < nbCols; ++i)
      for (Size j = 0; j < _rbal; ++j)
        W[i + j * nbCols] = Real(distribution(generator));
    // Declare and fill the random matrix Omega
    MatrixWrapperClass Omega(nbCols, _rbal, W);
    double tNoise = timeNoise.tacAndElapsed();
    std::cout << "... took @tNoise = " << tNoise << "\n";

    /// Prepare FMM MMPs
    // Test whether rbal is equal to NVALS
    // rq: rbal=10 (almost always) and 10 is a good choice for NVALS
    // (besides the first MMP is of size: rbals*size).
    // On the other hand the sampling period should be set to a few NVALS (but
    // NEVER less than NVALS) FMM MMPs will be divided in several NVALS*size
    // MMPs !
    if (_MatrixWrapper->getID() == 2 && _rbal != NVALS)
      throw std::runtime_error("The adaptive balance parameter rbal and NVALS "
                               "should be equal! Preferably equal to 10.");
    // The sampling period allows for performing the sampling of C (i.e. the CW
    // MMPS) occuring inside the loop in a blocked way.
    Size sampling_period = 10;
    if (_MatrixWrapper->getID() == 2 && sampling_period < NVALS)
      throw std::runtime_error("The sampling period should be superior or "
                               "equal to NVALS! Preferably equal to NVALS.");
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///                Compute Y = A Omega    (step 2)
    ///  the matrix A is in the wrapper !!!
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// Compute Y = A W
    std::cout << "\nPerform Y=AW... \n";
    tools::Tic timeCW;
    timeCW.tic();
    // The maximum rank should be choosen adequatly given N the size of the
    // problem Better off with a fixed value ~100 rather than a fixed fraction
    // of N
    Real *Y = new Real[nbRows * (_max_rank + _rbal + sampling_period)];
    // MatrixWrapperClass *Yw = new MatrixWrapperClass(nbRows, _max_rank + _rbal
    // + sampling_period);

    is_int(int(nbRows * (_max_rank + _rbal + sampling_period)));
    Blas::setzero(int(nbRows * (_max_rank + _rbal + sampling_period)), Y);
    multiplyMatrix(nbRows, nbCols, _rbal, W, Y);
    // fmrGemm(*_MatrixWrapper, Omega, *Yw);

    double tCW = timeCW.tacAndElapsed();
    std::cout << "... took @tCW = " << tCW << "\n";

    ///////////////////////////////////////////////////////////////////////////////////////////
    /// Perform Subspace Iterations if required
    ///
    if (_numberOfSubspaceIteration > 0) {
      // this->subspaceIterationMethod(Y, *Q);  // We need to share that
    }

    /// Init algo
    std::cout << "\nPerform Q=adapQRD(Y)... \n";
    tools::Tic timeAQR;
    timeAQR.tic();
    // counter
    int offset = 0; // $j$ in algo 4.2 of Halko
    // temporary arrays
    Real *q = new Real[nbRows];
    Real *fullQ =
        new Real[nbRows * (_max_rank + _rbal)]; // temporary Q has maximal size
    Real *w = new Real[sampling_period * nbCols];

    // output criterion
    const Real scaled_epsilon =
        _epsilon *
        std::sqrt(fmr::math::pi<Real>() / Real(200.)); //~ epsilon * 0.125
    Real max_norm_Ybal(0.);
    Real norm_Y(0.);
    this->updateMaxNormYbal(nbRows, Y, max_norm_Ybal, norm_Y);
    //    std::cout << "Initial Max_r(|y_r|)="<< max_norm_Ybal << std::endl;

    /// loop until maximum norm of family of $r$ consecutive $y$ reaches
    /// prescribed norm This ensures the error bound 4.3 in Halko
    while (max_norm_Ybal > scaled_epsilon && offset < _max_rank) {
      std::cout << "Error: " << max_norm_Ybal << std::endl;
      //// Display current rank
      // std::cout << "offset="<< offset << "\r";
      // std::cout.flush();

      // Overwrite Y_j by (I-Q_{j-1}Q_{j-1}*)Y_j
      if (offset >= 1) {
        // apply -QQ*
        // ... apply Q*
        Real *QTy = new Real[offset];
        is_int(nbRows);
        is_int(offset);
        Blas::gemtv(int(nbRows), int(offset), Real(1.), fullQ,
                    Y + offset * nbRows, QTy);
        // ... apply Q
        Blas::gemva(int(nbRows), int(offset), Real(-1.), fullQ, QTy,
                    Y + offset * nbRows);
        // ...free QTy
        delete[] QTy;
      } else { /* nothing to overwrite in Y_j*/
      }

      // write normalized Y_j in q
      is_int(nbRows);
      Blas::copy(int(nbRows), Y + offset * nbRows, q); // q = Y_j
      Real normq = 0.;
      for (int i = 0; i < nbRows; ++i)
        normq += q[i] * q[i];
      normq = std::sqrt(normq);
      is_int(nbRows);
      Blas::scal(int(nbRows), Real(1.) / normq, q); // q /= |q|
      // update matrix Q by adding 1 column.
      is_int(nbRows);
      Blas::copy(int(nbRows), q, fullQ + offset * nbRows);

      // Blocked random sampling
      if (offset % sampling_period == 0) {

        // Draw a set of standard gaussian vectors w_{j+r}
        for (int r = 0; r < sampling_period; ++r)
          for (int i = 0; i < nbCols; ++i)
            w[r * nbCols + i] = Real(distribution(generator));

        // Apply C to the set of vectors w_{j+r}
        multiplyMatrix(nbRows, nbCols, sampling_period, w,
                       Y + (offset + _rbal) * nbRows);

        // Do power iterations if required
        for (int i = 0; i < _numberOfSubspaceIteration; ++i) {
          Real *CTy = new Real[nbCols * sampling_period];
          // Apply C^t to (CW)
          multiplyMatrix(nbRows, nbCols, sampling_period,
                         Y + (offset + _rbal) * nbRows, CTy, true);
          // Apply C to C^t(CW)
          multiplyMatrix(nbRows, nbCols, sampling_period, CTy,
                         Y + (offset + _rbal) * nbRows);
          delete[] CTy;
        }

      } // end blocked random sampling

      // Compute Y_{j+r} = (I-Q_{j}Q_{j}*)z ...
      // ... apply Q*
      Real *QTz = new Real[offset + 1];
      is_int(nbRows);
      is_int(offset + 1);
      Blas::gemtv(int(nbRows), int(offset + 1), Real(1.), fullQ,
                  Y + (offset + _rbal) * nbRows, QTz);
      // ... apply Q
      is_int(nbRows);
      is_int(offset + 1);
      Blas::gemva(int(nbRows), int(offset + 1), Real(-1.), fullQ, QTz,
                  Y + (offset + _rbal) * nbRows);
      // ...free QTz
      delete[] QTz;

      // for i in [j+1,j+r-1] overwrite Y_i by Y_i - q * <q,Y_i>
      for (Size i = offset + 1; i < offset + _rbal; ++i) {
        // compute <q,Y_i>
        is_int(nbRows);
        Real qdoty = Blas::scpr(int(nbRows), q, Y + i * nbRows);
        // compute Y_i = Y_i - q * <q,Y_i>
        is_int(nbRows);
        Blas::axpy(int(nbRows), -qdoty, q, Y + i * nbRows);
      }

      // update max of Y norms
      this->updateMaxNormYbal(nbRows, Y + (offset + 1) * nbRows, max_norm_Ybal,
                              norm_Y);

      // increment offset
      offset++;

    } // end while
    double tAQR = timeAQR.tacAndElapsed();
    std::cout << "... took @tAQR = " << tAQR << "\n";

    // Inform user if loop ended because we reached the maximum rank
    if (offset == _max_rank)
      std::cout << "Prescribed accuracy NOT reached for rank=_max_rank="
                << _max_rank << "\n";

    /// Copy Q_j in output matrix Q
    _finalRank = offset;

    Q = new Real[_finalRank * nbRows];
    // If matvec products are performed with Blas then do not reorder
    is_int(_finalRank * nbRows);
    Blas::copy(int(_finalRank * nbRows), fullQ, Q);

    std::cout << "(eps=" << scaled_epsilon
              << " - max_r(|y_r|)=" << max_norm_Ybal << " - rank=" << _finalRank
              << ")";

    // free memory
    delete[] W;
    delete[] Y;
    delete[] q;
    delete[] w;
    delete[] fullQ;

    // return rank
    return _finalRank;
  }

  /*
   * Find the range of the input matrix with or without explicit A(if A is not
   assembled explicitely then A=n)
   * @brief findRange build an orthogonal matrix Q for a given accuracy epsilon
   *    Algorithm 4.2 in Halko paper
   *     Matrix A nbRows x nbCols matrix and hidden in MatrixWrapperClass
   *      r -> _tbal
   * _numberOfSubspaceIteration should be >0 id the eigenvalues decay slowly. In
   this cas,
   *  we consider the Randomized subspace iteration algorithm (algorithm 4.4
   page 244)

   * using the adaptative variant of RRF algo (Halko).
   * Parameter rank is used as output in order to return the size of the
   computed range
   * but also as input to specify an upper bound for this rank.
   */
  Size findRange(MatrixWrapperClass *&Q) {
    // const bool readW, const Size nbRows, const Size nbCols, Real* &Q){

    const int &nbRows = _MatrixWrapper->getNbRows();
    const int &nbCols = _MatrixWrapper->getNbCols();
    ///////////////////////////////////////////////////////////////////////////////////////////
    //    Generate white noise W...
    ///////////////////////////////////////////////////////////////////////////////////////////
    //
    // Declare W the random matrix composed of r vectors of size nbCols
    tools::RandomClass<Real> randGen;
    MatrixWrapperClass W(nbCols, _rbal);
    W.allocate();
    randGen.buildMatrix(W);

    // Display::matrix(W.getNbRows(),W.getNbCols(),W.getMatrix()," W(init)
    // ",10);

    //        /// Prepare FMM MMPs
    //        // Test whether rbal is equal to NVALS
    //        // rq: rbal=10 (almost always) and 10 is a good choice for NVALS
    //        // (besides the first MMP is of size: rbals*size).
    //        // On the other hand the sampling period should be set to a few
    //        NVALS (but NEVER less than NVALS)
    //        // FMM MMPs will be divided in several NVALS*size MMPs !
    //        if(_MatrixWrapper->getID()==2 && _rbal!=NVALS)
    //            throw std::runtime_error("The adaptive balance parameter rbal
    //            and NVALS should be equal! Preferably equal to 10.");
    //        // The sampling period allows for performing the sampling of C
    //        (i.e. the CW MMPS) occuring inside the loop in a blocked way.
    constexpr Size sampling_period = 10;
    //        if(_MatrixWrapper->getID()==2 && sampling_period<NVALS)
    //            throw std::runtime_error("The sampling period should be
    //            superior or equal to NVALS! Preferably equal to NVALS.");
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///                Compute Y = A W    (step 2)
    ///  the matrix C is in the wrapper !!!
    ///////////////////////////////////////////////////////////////////////////////////////////

    MatrixWrapperClass Y(nbRows, (_max_rank + _rbal + sampling_period));
    Y.allocate();
    //_MatrixWrapper->multiplyMatrix(W,Y,false);
    fmrGemm(*_MatrixWrapper, W, Y);
    W.free(); // Free random matrix no more needed
    ///////////////////////////////////////////////////////

    //        /// If power iterations required
    //        // test q=1
    //        for ( int i=0; i<_numberOfSubspaceIteration; ++i) {
    //            Real* CTy = new Real[nbCols*_rbal];
    //            // Apply C^t to (CW)
    //            multiplyMatrix(nbRows,nbCols,_rbal,Y,CTy,true);
    //            // Apply C to C^t(CW)
    //            multiplyMatrix(nbRows,nbCols,_rbal,CTy,Y);
    //            delete [] CTy;
    //        }

    // counter
    int offset = 0, j = 0; // $j$ in algo 4.2 of Halko
    // temporary arrays
    Real *q = new Real[nbRows];
    Real *fullQ =
        new Real[nbRows * (_max_rank + _rbal)]; // temporary Q has maximal size
    //    Real *w     = new Real[sampling_period*nbCols];

    // output criterion
    const Real scaled_epsilon =
        _epsilon *
        std::sqrt(fmr::math::pi<Real>() / Real(200.)); //~ epsilon * 0.125
    Real max_norm_Ybal(0.);
    Real norm_Y(0.);
    this->updateMaxNormYbal(nbRows, Y.getMatrix(), max_norm_Ybal, norm_Y);
    //    std::cout << "Initial Max_r(|y_r|)="<< max_norm_Ybal << std::endl;

    ///
    ///  loop until maximum norm of family of $r$ consecutive $y$ reaches
    ///  prescribed norm
    /// This ensures the error bound 4.3 in Halko (page 241)
    ///
    ///
    _max_rank = 1;
    std::vector<Real> w(nbCols);

    while (max_norm_Ybal > scaled_epsilon && j < _max_rank) {
      ///
      /// Overwrite Y_j by (I-Q^{j-1}Q^{j-1}^T) Y^j
      ++j;
      //            if(offset>=1){
      //                // apply -QQ*
      //                // ... apply Q*
      //                Real *QTy = new Real[offset];
      //                is_int(nbRows); is_int(offset);
      //                Blas::gemtv(int(nbRows),int(offset),Real(1.),fullQ,Y+offset*nbRows,QTy);
      //                // ... apply Q
      //                Blas::gemva(int(nbRows),int(offset),Real(-1.),
      //                fullQ,QTy,Y+offset*nbRows);
      //                // ...free QTy
      //                delete [] QTy;
      //            }
      //            else{/* nothing to overwrite in Y_j*/}

      //            // write normalized Y_j in q
      //            is_int(nbRows);
      //            Blas::copy(int(nbRows),Y+offset*nbRows,q); // q = Y_j
      //            Real normq=0.;
      //            for ( int i=0; i<nbRows; ++i)
      //              normq+=q[i]*q[i];
      //            normq=std::sqrt(normq);
      //            is_int(nbRows);
      //            Blas::scal(int(nbRows),Real(1.)/normq,q); // q /= |q|
      //            // update matrix Q by adding 1 column.
      //            is_int(nbRows);
      //            Blas::copy(int(nbRows),q,fullQ+offset*nbRows);

      //            // Blocked random sampling
      //            if(offset%sampling_period==0) {
      //                 random.buildArray(nbCols*sampling_period,w);
      //                // Draw a set of standard gaussian vectors w_{j+r}
      ///
      /// Draw a standard Gaussian vector w
      ///
      randGen.fillArray(nbCols, &w[0]);

      ////                for ( int r=0; r<sampling_period; ++r)
      ////                    for ( int i=0; i<nbCols; ++i)
      ////                        w[r*nbCols+i] = Real(distribution(generator));

      ///
      /// Apply A to the set of vectors w_{j+r}
      ///
      //                multiplyMatrix(nbRows,nbCols,sampling_period,w,Y+(offset+_rbal)*nbRows);
      // To write          _MatrixWrapper->multiplyVector(w,Y,false);

      //                // Do power iterations if required
      //                for ( int i=0; i<_numberOfSubspaceIteration; ++i) {
      //                    Real* CTy = new Real[nbCols*sampling_period];
      //                    // Apply C^t to (CW)
      //                    multiplyMatrix(nbRows,nbCols,sampling_period,Y+(offset+_rbal)*nbRows,CTy,true);
      //                    // Apply C to C^t(CW)
      //                    multiplyMatrix(nbRows,nbCols,sampling_period,CTy,Y+(offset+_rbal)*nbRows);
      //                    delete [] CTy;
      //                }

      //            } // end blocked random sampling

      //            // Compute Y_{j+r} = (I-Q_{j}Q_{j}*)z ...
      //            // ... apply Q*
      //            Real *QTz = new Real[offset+1];
      //            is_int(nbRows); is_int(offset+1);
      //            Blas::gemtv(int(nbRows),int(offset+1),Real(1.),fullQ,Y+(offset+_rbal)*nbRows,QTz);
      //            // ... apply Q
      //            is_int(nbRows); is_int(offset+1);
      //            Blas::gemva(int(nbRows),int(offset+1),Real(-1.),fullQ,QTz,Y+(offset+_rbal)*nbRows);
      //            // ...free QTz
      //            delete [] QTz;

      //            // for i in [j+1,j+r-1] overwrite Y_i by Y_i - q * <q,Y_i>
      //            for ( Size i=offset+1; i<offset+_rbal; ++i){
      //                // compute <q,Y_i>
      //                is_int(nbRows);
      //                Real qdoty = Blas::scpr(int(nbRows),q,Y+i*nbRows);
      //                // compute Y_i = Y_i - q * <q,Y_i>
      //                is_int(nbRows);
      //                Blas::axpy(int(nbRows),-qdoty,q,Y+i*nbRows);
      //            }

      //            // update max of Y norms
      //            this->updateMaxNormYbal(nbRows,Y+(offset+1)*nbRows,max_norm_Ybal,norm_Y);

      //            // increment offset
      //            offset++;

    } // end while
      //        double tAQR = timeAQR.tacAndElapsed();
      //        std::cout << "... took @tAQR = "<< tAQR <<"\n";

    //        // Inform user if loop ended because we reached the maximum rank
    //        if(offset==_max_rank) std::cout << "Prescribed accuracy NOT
    //        reached for rank=_max_rank=" << _max_rank << "\n";

    //        /// Copy Q_j in output matrix Q
    //        _finalRank=offset;

    //        Q = new Real[_finalRank*nbRows];
    //        // If matvec products are performed with Blas then do not reorder
    //        is_int(_finalRank*nbRows);
    //        Blas::copy(int(_finalRank*nbRows),fullQ,Q);

    //        std::cout << "(eps="<< scaled_epsilon << " - max_r(|y_r|)=" <<
    //        max_norm_Ybal << " - rank=" << _finalRank << ")";

    //        // free memory
    //        delete [] W;
    //        delete [] Y;
    //        delete [] q;
    //        delete [] w;
    //        delete [] fullQ;

    // return rank
    return _finalRank;
  }
  /*
   * AdaptativeRandomizedRangeFinder as descibed in Halko's paper on
   * algorithm 4.2 p243
   *
   *
   */
  template <typename FSize>
  FSize findRange(BlasDenseWrapper<FSize, Real> *&Q, Real epsilon, FSize r = 10,
                  FSize blockSize = 10, FSize powerIterations = 1) {

    std::cout << "Entering findRange" << std::endl;
    FSize m = _MatrixWrapper->getNbRows();
    FSize n = _MatrixWrapper->getNbCols();
    FSize j = 0;
    const Real normThreshold =
        epsilon / (Real(10.) * std::sqrt(Real(2.) / fmr::math::pi<Real>()));

    std::vector<Real> Q_array(r * m); // First we allocate r Column for Q
    std::vector<Real> Y(r * m);       // y_i = A*w_i

    // Init Random Number Generator
    std::default_random_engine generator(std::random_device{}());
    std::normal_distribution<double> distribution(0.0, 1.0);
    distribution(generator);

    std::vector<Real> Omega(r * n); // Contains the r vectors w
    for (FSize i(0); i < r * n; ++i) {
      Omega[i] = Real(distribution(generator));
    }
    /* Code to generate a random matrix A of rank R
    Real *A = new Real[n*n];
    // Generates r vectors w_1 to w_r of length n
    for(FSize i(0); i < R*n; ++i){
        Omega[i] = Real(distribution(generator));
    }

    // Normalizing Omega for generating A
    // TODO: This is only for tests purposes
    for (int i(0); i < R; ++i){
        Real normOmega(0);
        for(int j(0); j < n; ++j){
            normOmega += Omega.at(i*n+j);
        }
        Blas::scal(n, 1/normOmega, &Omega[n*i]);
    }
    // Generate A = Omega*Omega_t
    // TODO: Remove this A when tests are done
    Blas::gemmt(n, R, n, 1.0, &Omega[0], n, &Omega[0], n, A, n);
    */

    // Computing E = ||A||
    Real E = _MatrixWrapper->frobenius();
    E = E * E;

    // Compute Y = A Omega
    // Equivalent to y_i = A * w_i
    Blas::gemm(m, n, r, 1.0, _MatrixWrapper->getMatrix(), m, &Omega[0], n,
               &Y[0], m);

    // Resize Omega for the rest of the execution
    Omega.resize(n * blockSize);

    std::cout << "E: " << std::sqrt(E) << std::endl;
    std::cout << "Threshold: " << std::sqrt(E) * normThreshold << std::endl;
    // Main loop, the test for j != 0 is to avoid the *very* unlikely case where
    // we never enter the loop computeMaxNorm returns Max(||y_(j)||,
    // ||y_(j+1)||,...,||y_(j+r-1)||) There is a shift in the index compared to
    // Halko's paper because arrays start at 0
    Real max_norm_Ybal(0);
    Real norm_Y(0);
    // this->updateMaxNormYbal(m,&Y[0],max_norm_Ybal,norm_Y);
    while ((j == 0 || computeMaxNormY(Y, m, j, r) /* max_norm_Ybal */ >
                          (std::sqrt(E) * normThreshold)) &&
           j < n) {

      // std::cout << "MaxNorm: " << computeMaxNormY(Y, m, j, r) /*
      // max_norm_Ybal */ << std::endl;
      ++j;
      /*
       *  First we make an exeption with j=1 because Q_0 is empty
       *  So there is no need to compute I - (Q_0 Q_0t)
       */
      if (j > 1) {
        // Overwrite y_j with (I - ( Q_(j-1) Q_(j-1)t))y_j
        // Compute Qt y
        Real *QTy = new Real[j - 1];
        for (int iter(0); iter < powerIterations; ++iter) {
          is_int(m);
          is_int(j);
          Blas::gemtv(int(m), int(j - 1), Real(1.), &Q_array[0],
                      &Y[(j - 1) * m], QTy);
          // Compute y = y - Q QTy
          Blas::gemva(int(m), int(j - 1), Real(-1.), &Q_array[0], QTy,
                      &Y[(j - 1) * m]);
        }
        delete[] QTy;
      }

      // q_j = y_j
      Blas::copy(int(m), &Y[(j - 1) * m], &Q_array[(j - 1) * m]);
      Real normq(0);

      for (int i(0); i < m; ++i) {
        normq += (Q_array[(j - 1) * m + i] * Q_array[(j - 1) * m + i]);
      }
      normq = std::sqrt(normq);

      Blas::scal(int(m), Real(1.) / normq,
                 &Q_array[(j - 1) * m]); // q_j /= ||q_j||

      // Draw blockSize standard Gaussian vector w_(j+r)

      if (j == r || (j - r - 1) % blockSize ==
                        0) { // Y and Q are full, we need to reallocate memory
        // add blocksize columns to matrices
        Q_array.resize(m * (r + j + blockSize - 1));
        Y.resize(m * (r + j + blockSize - 1));
        // generate vectors
        for (int i(0); i < n * blockSize; ++i) {
          Omega[i] = Real(distribution(generator));
        }
        // Compute Y = A*Omega for the new vectors
        Blas::gemm(m, n, blockSize, 1.0, _MatrixWrapper->getMatrix(), m,
                   &Omega[0], n, &Y[(j + r - 1) * m], m);
      }

      // Compute y_(j+r) = (I - Q_jQ_j*)y_(j+r)
      Real *QTy = new Real[j];

      for (int iter(0); iter < powerIterations; ++iter) {
        is_int(m);
        is_int(j);
        Blas::gemtv(int(m), int(j), Real(1.), &Q_array[0], &Y[(j + r - 1) * m],
                    QTy);
        // Compute y = y - Q QTy
        Blas::gemva(int(m), int(j), Real(-1.), &Q_array[0], QTy,
                    &Y[(j + r - 1) * m]);
      }
      delete[] QTy;

      // overwrite y_(j) to j_(j+r-1) to
      for (FSize i(j); i < j + r - 2; ++i) {
        // compute <q_j,Y_i>
        is_int(m);
        Real qdoty = Blas::scpr(int(m), &Q_array[(j - 1) * m], &Y[i * m]);
        // compute Y_i = Y_i - q * <q,Y_i>
        Blas::axpy(int(m), -qdoty, &Q_array[(j - 1) * m], &Y[i * m]);
      }
      std::cout << "MaxNorm: " << computeMaxNormY(Y, m, j, r) << std::endl;
    }

    std::cout << "Leaving while" << std::endl;
    Q = new BlasDenseWrapper<FSize, Real>(m, j);
    Q->init(&Q_array[0]);
    _finalRank = j;
    std::cout << "Leaving Findrange" << std::endl;
    return j;
  }
};

} /* namespace fmr */

#endif /* AdaptiveRRF_HPP */
