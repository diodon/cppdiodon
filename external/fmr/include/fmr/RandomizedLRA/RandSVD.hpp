/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef RANDSVD_HPP
#define RANDSVD_HPP
#include <array>
#include <iostream>

// Utilities
#include "fmr/Utils/Blas.hpp"
#include "fmr/Utils/Global.hpp"
#include "fmr/Utils/Parameters.hpp"
#include "fmr/Utils/Tic.hpp"
//#ifdef DIODON_USE_CHAMELEON
//#include "fmr/Utils/Chameleon.hpp"
//#endif
#ifdef FMR_USE_MPI
#include "mpi.h"
#endif
// FMR includes
#include "AdaptiveRandomizedRangeFinder.hpp"
#include "RandomizedRangeFinder.hpp"
//#include "fmr/RandomizedLRA/AdaptiveRandomizedRangeFinder.hpp"
//#include "fmr/RandomizedLRA/RandomizedRangeFinder.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/StandardLRA/SVD.hpp"

namespace fmr {
/**
 * @brief The RandSVD class
 *
 * Perform the random SVD  \f$A ~ U S V^T\f$
 *
 * TODO had ctor dtor ptr to eigen vectors U and values S
 * TODO possibly writers to file for SQRT, U and S
 *
 */
template <class Real, class MatrixWrapper, class RandomizedRangeFinderClass>
class RandSVD {
  using Size = typename MatrixWrapper::int_type;

private:
  // Dimensions
  const Size _nbRows;
  const Size _nbCols;
  // Range finder based on random projection
  RandomizedRangeFinderClass *_RandRangeFinder;
  // Approximate singular vectors
  std::array<Size, 2> _shapeU; ///< shape = { _nbRows, _nbColsU = rank}
  MatrixWrapper *_ApproxU;     ///< the matrix U
  MatrixWrapper *_ApproxVT;     ///< the matrix VT
  // Approximate singular values
  Real *_ApproxSingularValues;    ///< an array on the singular value
  // Estimators
  Real _energy2;
  Real _estimator_order_approx;

public:
  /*
   * Ctor
   */
  explicit RandSVD(const Size& inNbRows, const Size& inNbCols,
                   RandomizedRangeFinderClass *const inRandRangeFinder)
      : _nbRows(inNbRows), _nbCols(inNbCols),
        _RandRangeFinder(inRandRangeFinder), _shapeU({0, 0}), _ApproxU(nullptr),
        _ApproxVT(nullptr), _ApproxSingularValues(nullptr), _energy2(0.0),
        _estimator_order_approx(-1.0) {}

  /*
   * Dtor
   */
  ~RandSVD() {
      if ( _ApproxU != nullptr ) {
          delete _ApproxU;
          _ApproxU = nullptr;
      }
      if ( _ApproxVT != nullptr ) {
          delete _ApproxVT;
          _ApproxVT = nullptr;
      }
      if ( _ApproxSingularValues != nullptr ) {
          delete[] _ApproxSingularValues;
          _ApproxSingularValues = nullptr;
      }
  }
  ///
  /// \brief Return the shape of the matrix U (orthogonal matrix)
  ///
  /// \return  the shape
  ///
  std::array<Size, 2> &getShapeU() { return _shapeU; }
  ///
  /// \brief return a pointer on the wrapper of matrix U
  ///
  ///
  MatrixWrapper *getApproxU() { return _ApproxU; }
  ///
  /// \brief return a pointer on the wrapper of matrix VT
  ///
  ///
  MatrixWrapper *getApproxVT() { return _ApproxVT; }
  ///
  /// \brief return a pointer on the array of singular values
  ///
  Real *getApproxSingularValues() { return _ApproxSingularValues; }
  ///
  ///
  /// \return  the sum of the squared singular eigenvalues. This corresponds to an estimate of the squared norm of the matrix A
  ///
  Real getEnergy2() { return _energy2; }
  ///
  ///
  /// \return the first truncated singular eigenvalues (if oversampling > 0)
  ///
  Real getApproxBaselineSpec() { return _estimator_order_approx; }

  /**
   * @brief factorize compute a randomized SVD on matrix A (n by p) for a given
   * rank Algorithm is described page 227 in Halko paper. The algorithm works in
   * 2 steps
   *        * From A, n x p
   *        * find a range k and an orthogonal matrix Q (algorithm 4.2 or 4.4),
   * n x k
   *        * Compute the SVD factors of matrix  A^t Q  (algorithm 5.1)
   *          * We first perform a QR factorization of C = A^t Q, p x k
   *          * We Compute the SVD on the R matrix, k x k
   *          * We rebuild the SVD decomposition of A
   *
   * @return _prescribedRank
   */
  Size factorize(const int verbose = 0) throw() {
    tools::Tic time;
    double timee, gflops;
    int mpirank = 0;
#if defined(FMR_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (mpiinit) {
      MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    }
#endif
    bool istimed = false;
    const char *envTime_s = getenv("FMR_TIME");
    if (envTime_s != NULL) {
      int envTime_i = std::stoi(envTime_s);
      if (envTime_i == 1) {
        istimed = true;
      }
    }
    ////////////////////////////////////////////////////////////////////
    ///
    /// Stage A
    ///     Find the range(rank) of A and the matrix Q  (n x rank)
    ///
    ////////////////////////////////////////////////////////////////////
    // object to perform QR factorization on Y = A Omega
    // matQ is the Gaussian test matrix
    QRF<MatrixWrapper> *matQ = nullptr;
    // compute the QR factorization of Y = A Omega (N x rank) and the rank
    // Omega and Y are computed and deleted in this subroutine
    // matQ will be initialized with Y and will be replaced in place by its QR
    const auto rank = _RandRangeFinder->findRange(matQ, verbose > 0);
    //    QRF<MatrixWrapper> matQQ(*Y);
    //    matQ = &matQQ;
    // get size of range (os_rank for RRF, final rank for ARRF)
    const Size computedRank = _RandRangeFinder->getRangeSize();
    if (verbose > 0) {
      std::cout << "[fmr]  rank: " << rank << " computedRank: " << computedRank
                << std::endl;
      std::cout << "[fmr]  matQ->getNbRows(): " << matQ->getNbRows()
                << " matQ->getNbCols(): " << matQ->getNbCols() << std::endl;
    }
    //      std::clog << " ============ END STEP A ========\n";
    ////////////////////////////////////////////////////////////////////
    ///
    /// Stage B
    ///     Input  A ( n x p ) and the matrix Q  (n x rank)
    ///     compute  approximated SVD by through algorithm 5.1 (Halko paper
    ///     page 247)
    ///   Rather than using B = Q^T A we consider C = B^T = A^T Q
    ///
    ////////////////////////////////////////////////////////////////////

    // A is the matrix given by the user on which we perform RSVD
    auto &A = *(_RandRangeFinder->getMatrixWrapper());

    const Size p = A.getNbCols();
    const Size n = matQ->getNbRows();
    const Size l = matQ->getNbCols(); // prescribed rank + oversampling
    ///
    /// The algorithm for step B
    ///   step 1 compute
    ///             C = A^t Q,
    ///   step 2 factorize
    ///            C = Qc Rc,
    ///   step 3 compute the svd of Rc
    ///           Rc = Uc S Vc^t -> SVD(Rc)
    ///   step 4 compute the final U and VT
    ///          Ua = Q Vc, Va^t = U^t Qc^t
    ///
    /// extract Q from the QR factorization of Y = A Omega
    MatrixWrapper C(p, l, Real(0.0));
    time.tic();
//    std::clog << "   Alias on Q \n "
//              << "   n " << n << " l " << l << "\n";
    // Generate and get the orthogonal matrix
    MatrixWrapper &Q = matQ->getQ();
    time.tac();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GETQ(Y)=" << time.elapsed() << " s"
                << std::endl;
    }
   // std::clog << "  beg compute C = A^t Q\n ";

    /*  step 1  compute C = A^t Q */
    time.tic();
//    std::cout << "A shape: " << A.getNbRows() << " " << A.getNbCols()
//              << " Q shape: " << Q.getNbRows() << " " << Q.getNbCols()
//              << " C shape: " << C.getNbRows() << " " << C.getNbCols()
//              << std::endl;
    if ( A.isSymmetric() ) {
      fmrGemm<Size, Real>("N", "N", 1., A, Q, 0., C);
    } else {
      fmrGemm<Size, Real>("T", "N", 1., A, Q, 0., C);
    }
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GEMM(A^t,Q)=" << timee << " s" << std::endl;
      gflops = (1e-9 * FLOPS_GEMM(p, n, l)) / timee;
      std::cout << "[fmr] PERF:RSVD:GEMM(A^t,Q)=" << gflops << " gflop/s"
                << std::endl;
    }
  //  std::clog << "  end compute C = A^t Q\n ";

    /// step 2 compute QR factorization of C, compute Q_C and R_C
    QRF<MatrixWrapper> qrf_C(C);
    time.tic();
    qrf_C.factorize();
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:QR(C)=" << timee << " s" << std::endl;
      gflops = (1e-9 * FLOPS_GEQRF(p, l)) / timee;
      std::cout << "[fmr] PERF:RSVD:QR(C)=" << gflops << " gflop/s"
                << std::endl;
    }
    time.tic();
    MatrixWrapper *R_C = qrf_C.getR();
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GETR(C)=" << timee << " s" << std::endl;
    }
    time.tic();
    MatrixWrapper &Q_C = qrf_C.getQ();
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GETQ(C)=" << timee << " s" << std::endl;
    }
    //std::clog << "  end c QR factorization of C, compute Q_C and R_C\n";

    /// step 3  compute an SVD on R_C (l x l)  R_C = U_RC _ApproxSingularValues VT_RC
    _ApproxSingularValues = new Real[l];
    MatrixWrapper U_RC(l, l);
    MatrixWrapper VT_RC(l, l);

    time.tic();
    SVD<Real, Size>::computeSVD(*R_C, _ApproxSingularValues, U_RC, VT_RC);
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:SVD(R_C)=" << timee << " s" << std::endl;
    }
    //Display::vector(l, _ApproxSingularValues, "[fmr] S: ", l, 1);
//    std::clog << "  end SVD on R_C (l x l)  R_C = U_RC _ApproxSingularValues "
//                 "VT_RC \n ";

    /// step 4-1 compute U_A = Q V_RC //
    _ApproxU = new MatrixWrapper(n, l, Real(0));
    time.tic();
    fmrGemm<Size, Real>("N", "T", 1., Q, VT_RC, 0., *_ApproxU);
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GEMM(Q_C,U_R_C)=" << timee << " s"
                << std::endl;
      gflops = (1e-9 * FLOPS_GEMM(n, l, l)) / timee;
      std::cout << "[fmr] PERF:RSVD:GEMM(Q_C,U_R_C)=" << gflops << " gflop/s"
                << std::endl;
    }

    /// step 421 compute V^t_A = U_RC^t Q_c^t
    _ApproxVT = new MatrixWrapper(l, p, Real(0));
    time.tic();
    fmrGemm<Size, Real>("T", "T", 1., U_RC, Q_C, 0., *_ApproxVT);
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GEMM(UT_R_C,QT_c)=" << timee << " s"
                << std::endl;
      std::cout << "[fmr] PERF:RSVD:GEMM(UT_R_C,QT_c)=" << gflops << " gflop/s"
                << std::endl;
    }
   // std::clog << " ============ END STEP B ========\n";

    _shapeU[0] = n;
    _shapeU[1] = l;

    /* clean memory */
    delete matQ;
    delete &Q;

    ///
    ////////////////////////////////////////////////////////////////////
    /// Error estimation

    /* Compute frobenius norm of C_(k) (i.e. energy norm) */
    _energy2 = Real(0.);
    for (Size i = 0; i < rank; ++i) {
      _energy2 += _ApproxSingularValues[i] * _ApproxSingularValues[i];
    }

    // Beware! This is NOT the estimator baseline of Halko!!
    // This is just a test! To verify that this value can be used as estimator.
    // Compute an APPROXIMATE error estimator order in spectral norm
    // The baseline $\sigma_{k+1}$ introduced by Halko is a singular value of C,
    // here we take the $k+1$-th SV of $C_{(k)}$. Which approximates the
    // $k+1$-th SV of $C$. As the oversampling grows, both value converges.
    if (rank < l) {
      _estimator_order_approx = _ApproxSingularValues[rank];
    } else {
      _estimator_order_approx = -1;
    }


    return rank;
  }

  Size compute(const int fFACT, const bool readW) {

    // Is matrix symmetric? Ask randomized range finder, he knows matrix pretty
    // well.
    const bool matrixIsSymmetric = _RandRangeFinder->isMatrixSymmetric();

    ////////////////////////////////////////////////////////////////////
    /// Find range of C
    std::cout << "[fmr] \nFind range of C...  $$$$$$$$$$$$$$$$$$$\n";
    Real *Q = nullptr; // Q allocated in range finder's QRD
    MatrixWrapper *matQ;
    std::cout << "[fmr] begin RandRangeFinder->findRange( Q); " << Q
              << std::endl;
    const Size rank =
        _RandRangeFinder->findRange(readW, _nbRows, _nbCols, Q); //
    //
    //   CHECK Q TODO HERE
    //  const Size rank = _RandRangeFinder->findRange( matQ ); //

    std::cout << "[fmr] end RandRangeFinder->findRange( Q); " << Q << std::endl;

    // get size of range (os_rank for RRF, final rank for ARRF)
    const Size init_rank = _RandRangeFinder->getRangeSize();
    std::cout << "[fmr]   $$$$$$   Final range " << rank << "  " << init_rank
              << std::endl;
    Real *S;
    Real *B;
    if (!matrixIsSymmetric) {
      ////////////////////////////////////////////////////////////////////
      /// NON SYMMETRIC CASE
      ////////////////////////////////////////////////////////////////////
      std::cout << "[fmr] \nCompute the SVD-FACT of small CQ matrix... \n";
      ////////////////////////////////////////////////////////////////////
      /// Compute Q^tC ([TODO] for FMM, how to I compute Q^tC? directly? C^tQ
      /// then transpose?)
      tools::Tic timeCTQ;
      timeCTQ.tic();
      std::cout << "[fmr] \nForm CTQ... \n";
      // form CQ
      MatrixWrapper CTQwrapper(_nbCols, init_rank);
      CTQwrapper.allocate();
      Real *CTQ = new Real[_nbCols * init_rank];
      // MatrixWrapper CTQwrapper(_nbCols, init_rank, CTQ);

      fmrGemm("T", "N", 1.0, *_RandRangeFinder, Q, 0.0, CTQwrapper);
      //_RandRangeFinder->multiply Matrix(_nbRows, _nbCols, init_rank, Q, CTQ,
      //                                 true);
      CTQwrapper.getMatrix(CTQ);
      double tCTQ = timeCTQ.tacAndElapsed();
      std::cout << "[fmr] ... took @tCTQ = " << tCTQ << "\n";

      // Either perform SVD of QTC: pb need to transpose

      // Transposing CTQ, returns (CTQ)^T=QTC
      Real *QTC = new Real[init_rank * _nbCols];
      for (int i = 0; i < _nbCols; ++i)
        for (int j = 0; j < init_rank; ++j)
          QTC[j + i * init_rank] = CTQ[i + j * _nbCols];
      delete[] CTQ;
      CTQwrapper.free();
      // SVD(r<n): QTC^(r x n)= U^(r x r=min(r,n)) S^(r=min(r,n)) VT^(r=min(r,n)
      // x n)
      std::cout << "[fmr] \nCompute SVD of small matrix Q^TC... \n";
      tools::Tic timeSmallSVD;
      timeSmallSVD.tic();
      const Size minMR = std::min(_nbCols, init_rank);
      AssertLF(minMR == init_rank);
      B = new Real[init_rank * minMR];
      S = new Real[minMR];
      Real *VT = new Real[minMR * _nbCols];
      SVD<Real>::computeSVD(
          init_rank, _nbCols, QTC, S, B,
          VT); // call to gesvd, hence first init_rank rows of V'(CQ) in VT
      double tSmallSVD = timeSmallSVD.tacAndElapsed();
      std::cout << "[fmr] ... took @tSmallSVD = " << tSmallSVD << "\n";
      // Truncate VT at prescribed rank
      _ApproxVT = new Real[rank * _nbCols];
      for (int i = 0; i < rank; ++i)
        for (int j = 0; j < _nbCols; ++j)
          _ApproxVT[i + j * rank] = VT[i + j * init_rank];

      delete[] VT;

    } else {
      ////////////////////////////////////////////////////////////////////
      /// SYMMETRIC CASE
      ////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////
      /// Compute CQ [TODO] try to compute QtCQ at once
      tools::Tic timeCQ;
      timeCQ.tic();
      std::cout << "[fmr] \nForm C Q... \n";
      // form CQ

      MatrixWrapper *CQ(_nbRows,
                        init_rank); // Real *CQ = new Real[_nbRows * init_rank];
      CQ->allocate();
      //_RandRangeFinder->multiplyMatrix(_nbRows, _nbCols, init_rank, Q, CQ);
      fmrGemm(*_RandRangeFinder, Q, CQ);
      double tCQ = timeCQ.tacAndElapsed();
      std::cout << "[fmr] ... took @tCQ = " << tCQ << "\n";
      ////////////////////////////////////////////////////////////////////
      /// Compute Q^tCQ using dense Q
      tools::Tic timeQTCQ;
      //
      timeQTCQ.tic();
      std::cout << "[fmr] \nForm QTCQ... ";
      // form Q^T(CQ)
      Real *QTCQ = new Real[init_rank * init_rank];
      MatrixWrapper QtcqWrapper(init_rank, init_rank);
      QtcqWrapper.allocate();
      // MatrixWrapper QtcqWrapper(init_rank, init_rank, QTCQ);
      is_int(_nbRows);
      is_int(init_rank);
      fmrGemm("T", "N", 1.0, Q, CQ, 0.0, QtcqWrapper);
      // Blas::gemtm(int(_nbRows), int(init_rank), int(init_rank), Real(1.), Q,
      //            int(_nbRows), CQ, int(_nbRows), QTCQ, int(init_rank));
      double tQTCQ = timeQTCQ.tacAndElapsed();
      std::cout << "[fmr] ... took @tQCQ = " << tQTCQ << "\n";
      CQ->free(); // delete[] CQ;
      QtcqWrapper.getMatrix(QTCQ);
      QtcqWrapper.free();
      ////////////////////////////////////////////////////////////////////
      /// Compute SVD of small matrix Q^tCQ (sym.) or CQ (no sym.)
      // Either:
      // - Compute US^{1/2}, i.e. the SVD::SQRT of Q^tCQ
      // - Compute U and S,  i.e. the SVD::SYMSVD of Q^tCQ (sym.)
      // - Compute U and S,  i.e. the SVD::SVD of CQ (no sym.)
      if (fFACT == 0)
        std::cout
            << "[fmr] \nCompute B, the SVD-SQRT of small QtCQ matrix... \n";
      else if (fFACT == 1)
        std::cout
            << "[fmr] \nCompute B, the SVD-FACT of small QtCQ matrix... \n";
      else
        throw std::runtime_error("fFACT has invalid value!");

      B = new Real[init_rank * init_rank];
      is_int(init_rank * init_rank);
      Blas::setzero(int(init_rank * init_rank), B);
      S = new Real[init_rank];
      tools::Tic timeSmallSVD;
      timeSmallSVD.tic();
      if (fFACT == 0)
        SVD<Real>::computeSQRT_withSV(
            init_rank, QTCQ, B,
            S); // if QTCQ=USigmaU' then B=US^{1/2} and S=Sigma
      else if (fFACT == 1) {
        SVD<Real>::computeSYMSVD(init_rank, QTCQ, S,
                                 B); // if QTCQ=USigmaU' then B=U and S=Sigma
      } else
        throw std::runtime_error("fFACT has invalid value!");
      double tSmallSVD = timeSmallSVD.tacAndElapsed();
      std::cout << "[fmr] ... took @tSmallSVD = " << tSmallSVD << "\n";

      // free memory
      delete[] QTCQ;
    }

    ////////////////////////////////////////////////////////////////////
    /// Assemble final factorization:
    /// If symmetric:
    /// - square root of C=QUS^{1/2} or simply QU if only eigenvectors are
    /// required. else
    /// - ApproxU=QU for SVD factorization of rectangular matrix
    tools::Tic timeQB;
    timeQB.tic();
    std::cout << "[fmr] \nAssemble final factorization/sqrt... ";
    // Allocate sqrtC
    if (!_ApproxU)
      //_ApproxU = new Real[_nbRows * rank];
      _ApproxU = new MatrixWrapper(_nbRows, rank);
    else
      throw std::runtime_error("sqrtC NOT empty!");
    // Form QB=Q(Q^tCQ)^{1/2} or QU
    is_int(_nbRows);
    is_int(init_rank);
    is_int(rank);
    Blas::gemm(int(_nbRows), int(init_rank), int(rank), Real(1.), Q,
               int(_nbRows), B, int(init_rank), _ApproxU->getBlasMatrix(),
               int(_nbRows));

    double tQB = timeQB.tacAndElapsed();
    std::cout << "[fmr] ... took @tQB = " << tQB << "\n";

    // Ensure unicity of sqrt
    // by imposing first line to be positive
    if (matrixIsSymmetric)
      ensureUnicityOfSqrt(rank, _ApproxU->getBlasMatrix());

    delete[] B;

    ////////////////////////////////////////////////////////////////////
    /// Copy singular values to local array
    _ApproxSingularValues = new Real[rank];
    is_int(rank);
    Blas::copy(int(rank), S, _ApproxSingularValues);

    ////////////////////////////////////////////////////////////////////
    /// Error estimation

    // Compute frobenius norm of C_(k) (i.e. energy norm)
    _energy2 = Real(0.);
    for (Size i = 0; i < rank; ++i)
      _energy2 += S[i] * S[i];

    // Beware! This is NOT the estimator baseline of Halko!!
    // This is just a test! To verify that this value can be used as estimator.
    // Compute an APPROXIMATE error estimator order in spectral norm
    // The baseline $\sigma_{k+1}$ introduced by Halko is a singular value of C,
    // here we take the $k+1$-th SV of $C_{(k)}$. Which approximates the
    // $k+1$-th SV of $C$. As the oversampling grows, both value converges.
    /*if(oversampling) */
    _estimator_order_approx = S[rank];

    /// free memory
    delete[] Q;
    delete[] S;

    // return rank
    return rank;
  }

  /*
   * Ensure
   */
  void ensureUnicityOfSqrt(const Size& rank, Real *&sqrtC) {
    for (Size j = 0; j < rank; ++j) {
      // if first entry of line is negative then change sign of entire column
      if (sqrtC[0 + j * _nbRows] < 0)
        for (Size i = 0; i < _nbRows; ++i)
          sqrtC[i + j * _nbRows] *= -Real(1.);
    }
  }
};

} /* namespace fmr */

#endif // RANDSVD_HPP
