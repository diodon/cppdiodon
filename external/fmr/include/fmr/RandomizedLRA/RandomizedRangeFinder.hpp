/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_RANDOMIZEDLRA_RANGEFINDER_HPP
#define FMR_RANDOMIZEDLRA_RANGEFINDER_HPP

// standard includes
#include <cstdio>
#include <cstdlib>
#include <random> // for normal distribution generator
#include <stdexcept>
//
// FMR includes
#include "fmr/RandomizedLRA/CommonAlgorithm.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/Utils/Display.hpp" // To display the matrices in verbose mode
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/Tic.hpp"

/**
 * @author Pierre Blanchard (pierre.blanchard@inria.fr)
 * @author Olivier Coulaud  (olivier.coulaud@inria.fr)

 * @date July 1st, 2014
 *
 *
 */

namespace fmr {
/**
 * @brief Randomized Range Finder class
 *
 *
 * See Halko paper for more details
 */
template <typename Real, typename MatrixWrapperClass>
class RandomizedRangeFinder {
  using Size = typename MatrixWrapperClass::int_type;

private:
  // needed for matrix multiplications
  MatrixWrapperClass
      *_MatrixWrapper; ///< A pointer on the matrix class we search the rank
  // prescribed rank (only needed to get range size with a generic function)
  const Size _prescribedRank;  ///< the given rank
  // oversampling parameter
  const Size _oversampling; ///< oversampling parameter
  // oversampled rank,   the parameter l in Halko paper
  const Size _oversampledRank; ///< oversampling + rank
  // number of subspace iterations, the parameter q in Halko paper
  const int _numberOfSubspaceIteration; ///< number of subspace iterations

public:
  using MatrixWrapperClass_type = MatrixWrapperClass;
  /*
   * Ctor
   */
  explicit RandomizedRangeFinder(MatrixWrapperClass *inMatrixWrapper,
                                 const Size &inPrescribedRank,
                                 const Size &inOversampling,
                                 const int &inQRSI = 0)
      : _MatrixWrapper(inMatrixWrapper), _prescribedRank(inPrescribedRank),
        _oversampling(inOversampling),
        _oversampledRank(inPrescribedRank + inOversampling),
        _numberOfSubspaceIteration(inQRSI) {}

  /*
   * Dtor
   */
  ~RandomizedRangeFinder() { _MatrixWrapper = nullptr; }

  /*
   * In RRF algorithm the size of the range is the oversampled rank
   */
  ///
  /// \brief Return the size of the orthogonal matrix
  ///
  /// This size is the sum of the prescribed rank and the oversanpling parameter
  ///
  ///
  Size getRangeSize() { return _oversampledRank; }
  ///
  /// \brief getMatrixWrapper accessor
  /// \return the pointer on the current matrix
  ///
  MatrixWrapperClass *getMatrixWrapper() const { return _MatrixWrapper; }

#ifdef NOT_TO_REMOVE
 // bool isMatrixSymmetric() { return _MatrixWrapper->isSymmetric(); }

  /*
   * computeError: This allows the computeError() function from MatrixWrapper to
   * be called from RRF. Needed by RandLowRank Algo
   */
  void computeError(const Size nbRows, const Size nbCols, const Size rank,
                    Real *W, Real *Y) {

    _MatrixWrapper->computeError(nbRows, nbCols, rank, W, Y);
  }
#endif
  /**
   * @brief findRange build an orthogonal matrix Q for a given rank
   *    Algorithm 4.1 in Halko paper
   *     A is a  nbRows xéuwjÌ nbCols matrix and it is hidden in
   * MatrixWrapperClass
   * output: rank and QR decomposition on Y = A Omega has been performed
   * here l  = _oversampledRank = prescribedRank + oversampling
   *      q  = _numberOfSubspaceIteration should  be >0 if the eigenvalues decay
   * slowly. In this case, we consider the Randomized subspace iteration
   * algorithm (algorithm 4.4 page 244).
   *
   * To see the timer you should set
   * export FMR_TIME=1
   *
   * @param[out] Q  the orthogonal matrix of size (nbRows x l)
   * @param[in] istimed boolean to tell if the timers are used or not.
   *           [optional, false by default]
   * @return _prescribedRank the final rank
   */

  template <class QRFMatrixWrapper_TYPE>
  Size findRange(QRFMatrixWrapper_TYPE *&Q, const bool verbose) {
    int mpirank = 0;

    if (Q != nullptr) {
      throw std::invalid_argument("RandomizedRangeFinder::findRange Q should "
                                  "not be initialised prior to calling "
                                  "findRange.");
    }
#if defined(FMR_USE_MPI)
    // Should be outside this function
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (mpiinit) {
      MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    }
#endif
    bool istimed = false;
    const char *envTime_s = getenv("FMR_TIME");
    if (envTime_s != NULL) {
      int envTime_i = std::stoi(envTime_s);
      if (envTime_i == 1) {
        istimed = true;
      }
    }
    tools::Tic time;
    double timee, gflops;

    const Size &nbRows = _MatrixWrapper->getNbRows();
    const Size &nbCols = _MatrixWrapper->getNbCols();
    ///////////////////////////////////////////////////////////////////////////////////////////
    //    Generate white noise W...
    ///////////////////////////////////////////////////////////////////////////////////////////
    //
    // Declare and fill the random matrix Omega
    MatrixWrapperClass Omega(nbCols, _oversampledRank);
    //
    time.tic();
    tools::RandomClass<Real> randGen;
    randGen.generateMatrix(Omega);
    time.tac();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:SET(Omega)=" << time.elapsed() << " s"
                << std::endl;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///                Compute Y = A Omega    (step 2)
    ///  the matrix A is in the wrapper !!!
    ///////////////////////////////////////////////////////////////////////////////////////////
    MatrixWrapperClass *Y = new MatrixWrapperClass(nbRows, _oversampledRank);
    time.tic();
    fmrGemm(*_MatrixWrapper, Omega, *Y);
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:GEMM(A,Omega)=" << timee << " s"
                << std::endl;
      gflops = (1e-9 * FLOPS_GEMM(nbRows, _oversampledRank, nbCols)) / timee;
      std::cout << "[fmr] PERF:RSVD:GEMM(A,Omega)=" << gflops << " gflop/s"
                << std::endl;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////
    /// Compute thin QR decomposition of Y (step 3) to build an orthogonal
    /// matrix
    ///      Y = Q R  and Q = QRD(Y)
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///
    Q = new QRFMatrixWrapper_TYPE(*Y);

    time.tic();
    Q->factorize();
    time.tac();
    timee = time.elapsed();
    if (istimed && mpirank == 0) {
      std::cout << "[fmr] TIME:RSVD:QR(Y)=" << timee << " s" << std::endl;
      gflops = (1e-9 * FLOPS_GEQRF(nbRows, _oversampledRank)) / timee;
      std::cout << "[fmr] PERF:RSVD:QR(Y)=" << gflops << " gflop/s"
                << std::endl;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////
    /// Perform Subspace Iterations if required
    ///
    if (_numberOfSubspaceIteration > 0) {
      const bool formQmatrix = true;
      /// QRFMatrixWrapper_TYPE Y and Q
      /// pointer on MatrixWrapperClass _MatrixWrapper,
      /// In Y contains the QR factorization on A Omega

      rla::subspaceIterationMethod(_numberOfSubspaceIteration,
                                      *_MatrixWrapper, *Y, *Q, formQmatrix,
                                      verbose);
    }
    Y = nullptr; // because Q.Q is an alias on Y
    ///
    ///////////////////////////////////////////////////////////////////////////////////////////
    //
    // return rank and QR decomposition on Y = A Omega has been performed
    return _prescribedRank;
  }
};

} /* namespace fmr */

#endif /* RRF_HPP */
