/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_UTILS_CONSTANTS_HPP
#define FMR_UTILS_CONSTANTS_HPP

namespace fmr { namespace math {
#include <cmath>
template <class ValueType> constexpr static ValueType pi() {
  return ValueType(M_PI);
}
template <class ValueType> constexpr static ValueType two_pi() {
  return ValueType(2.0 * M_PI);
}
template <class ValueType> constexpr static ValueType half_pi() {
  return ValueType(M_PI_2);
}
template <class ValueType> constexpr static ValueType Epsilon() {
  return ValueType(0.00000000000000000001);
}
}} // namespace fmr::math

#endif
