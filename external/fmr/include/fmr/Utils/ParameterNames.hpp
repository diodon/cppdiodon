/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifdef PARAMETERNAMES_HPP
#error ParameterNames must be included only once by each main file!
#else
#define PARAMETERNAMES_HPP

#include "fmr/Utils/Global.hpp"
#include "fmr/Utils/Parameters.hpp"

#include <iostream>
#include <vector>
#include <chrono>
#include <unistd.h>

/**
 * This file contains some useful classes/functions to manage the parameters,
 * but it also contains all the parameters definitions!
 * A FMR executable must define a static object to propose a parameter to the user.
 */

namespace fmr {
namespace param {

/** A parameter descriptor */
struct ParameterNames {
    std::vector<const char*> options;
    const char* description;
};

static const ParameterNames Help = {
    {"-help", "--help"} ,
     "To have print the options used by the application."
};

static const ParameterNames Compile = {
    {"-show-compile", "--show-compile", "--flags"} ,
     "To have the list of flags and lib linked to fmr."
};

static const ParameterNames DateHost = {
    {"-show-info", "--show-host", "--datehost"} ,
     "To have to print the current host and the execution date."
};

static const ParameterNames UserParams = {
    {"-show-param", "--show-param", "-show-params", "--show-params"} ,
     "To print out the paremeters passed to the command line."
};


//
// Parameters that are specific to FMR library
//

static const ParameterNames DebugMode = {
    {"-debugMode", "--debug-mode", "-dbg"} ,
     "Activate some extra debugging features in release mode."
};

static const ParameterNames GridSize = {
    {"-gridSize", "--grid-size"} ,
     "The number of points/particles in the grid/cloud."
};


// Kernel matrices
static const ParameterNames CorrelationIdentifier = {
    {"-correlationIdentifier", "--correlation-identifier", "-corr"} ,
     "The number that identifies the correlation kernels to be used."
};

static const ParameterNames LengthScale = {
    {"-lengthScale", "--length-scale"} ,
     "The number of points/particles in the grid/cloud."
};

// Linear Algebra
static const ParameterNames NbRows = {
    {"-nbRows", "--number-of-rows"} ,
     "The number of rows in the input matrix if not read from file."
};

static const ParameterNames NbCols = {
    {"-nbCols", "--number-of-columns"} ,
     "The number of columns in the input matrix if not read from file."
};

static const ParameterNames MatrixRank = {
    {"-rank", "-matrixRank", "--matrix-rank", "-r", "-pr", "--prescribed-rank"} ,
     "The prescribed rank of the approximation technique."
};
static const ParameterNames PrescribedRank = {
    {"-rank", "-matrixRank", "--matrix-rank", "-r", "-pr", "--prescribed-rank"} ,
     "The prescribed rank of the approximation technique."
};

static const ParameterNames OverSampling = {
    {"-overSampling", "--over-sampling" , "-os", "-ospl"} ,
     "The oversampling parameters, that increases the rank of the algorithm without changing the output rank of the approximation."
};

static const ParameterNames PowerIterations = {
    {"-powerIterations", "--power-iterations" , "-qpow", "-q"} ,
     "The number of power/subspace iterations, i.e. q in (AA^t)^qA."
};

static const ParameterNames IsAdaptive = {
    {"-isAdaptive", "--is-adaptive", "-ad"} ,
     "specify how the rank is computed.\n   0 given rang (set by MatrixRank),\n   1 accuracy given (set by PrescribedAccuracy)"
};
static const ParameterNames RangeFinderMethod = {
    {"-isAdaptive", "--is-adaptive", "-ad"} ,
     "specify how the rank is computed.\n\t   0 given rank (set by PrescribedRank),\n\t   1 given accuracy  (set by PrescribedAccuracy)"
};
static const ParameterNames PrescribedMagnitudeAccuracy = {
    {"-PrescribedMagnitudeAccuracy", "--prescribed-magnitude-accuracy", "-acc"} ,
     "The prescribed magitude accuracy (10^-m)of the approximation technique to find the rank."
};
static const ParameterNames PrescribedAccuracy = {
    {"-prescribedAccuracy", "--prescribed-accuracy", "-eps"} ,
     "The prescribed accuracy of the approximation technique to find the rank."
};
static const ParameterNames BalanceParameter = {
    {"-balanceParameter", "--balance-parameter" , "-r", "-bal"} ,
     "The adaptive counterpart of the oversampling parameters (r in Halko paper)."
};

static const ParameterNames MaximumRank = {
    {"-maximumRank", "--maximum-rank" , "-rmax"} ,
     "The maximum output rank allowed to the adaptive randomized range finder."
};


// Random Smapling / Column Selection

static const ParameterNames NbSampledColsOverRank = {
    {"-nbSampledColsOverRank", "--nbsampledcols-over-rank", "-a"} ,
     "The ratio bbetween the number of sampled columns and the input rank."
};

static const ParameterNames SampleWithReplacement = {
    {"-sampleWithReplacement", "--sample-with-replacement", "-sw", "-wR"} ,
     "Sample with replacement?"
};

static const ParameterNames LeverageScoresApproach = {
    {"-leverageScoresApproach", "--leverage-scores-approach", "-fLS"} ,
     "Select approach/algorithm for computing leverage scores/coherence."
};

static const ParameterNames SamplingTechnique = {
    {"-samplingTechnique", "--sampling-technique", "-sT"} ,
     "What sampling technique should be used?"
};

static const ParameterNames ImprovedIntersection = {
    {"-improvedIntersection", "--improved-intersection", "-iI"} ,
     "Use improved intersection matrix?"
};

static const ParameterNames AdaptiveSampling = {
    {"-adaptiveSampling", "--adaptive-sampling", "-aS"} ,
     "Use adaptive sampling?"
};


// Matrix free?

static const ParameterNames BuildMatrix = {
    {"-buildMatrix", "-buildC", "--build-matrix" } ,
     "Build (covariance) matrix?"
};

static const ParameterNames StoreMatrixInFile = {
    {"-storeMatrix", "-storeC", "--store-matrix" } ,
     "Store matrix in file?"
};

// Matrix Multiplication
static const ParameterNames FlagMatrixMultiplication = {
    {"-flagMatrixMultiplication", "-flagMMP", "--flag-matrix-multiplication" } ,
     "What technique for matrix multiplication?"
};
static const ParameterNames VerifyMatrixMultiplication = {
    {"-verifyMatrixMultiplication", "-verifyMMP", "--verify-matrix-multiplication" } ,
     "Verify accuracy of matrix multiplication?"
};
static const ParameterNames ReadRandomMatrix = {
    {"-readRandomMatrix", "--read-random-matrix"} ,
     "Do I read the random sketching matrix used in random projection?"
};
static const ParameterNames TransposeMatrixMultiplication = {
    {"-transpose"} ,
     "To apply the multiplication with the transpose matrix in testDenseMatrixMultiplication"
};

//
// Parameters that are specific to Chameleon library
//
static const ParameterNames BlasChameleon = {
    {"-c", "--chameleon"} ,
     "To indicate whether to use Chameleon (1) or Blas (0)."
};

static const ParameterNames TileSize = {
    {"-nb", "--nb"} ,
     "Size (nbRows) of the squared blocks used internally to Chameleon."
};

// Generators
static const ParameterNames GenerateGRF = {
    {"-generateGRF", "-geneGRF", "--generate-grf" } ,
     "Generate Gaussian Random Field (GRF)?"
};

static const ParameterNames NumberOfRuns = {
    {"-nbRuns", "-nbSimu", "--number-of-runs" } ,
     "How many runs/simulations to perform?"
};

static const ParameterNames NumberOfRealizations = {
    {"-nbRealizations", "-nbReal", "--number-of-realizations" } ,
     "Number of realizations (per run/simulation) to generate (please specify the power of 10)?"
};

static const ParameterNames RatioFullGridOverCheckGrid = {
    {"-ratiofullcheck", "--ratio-full-check" } ,
     "Ratio between full and check grid size."
};


// Files
static const ParameterNames WriteOutput = {
    {"-writeOutput", "--write-output"} ,
     "Write output to file."
};

static const ParameterNames PathToInputFile = {
    {"-pathToInput", "--path-to-input"} ,
     "Write output to file."
};

static const ParameterNames FullPathToInputFile = {
    {"-fullPathToInput", "--full-path-to-input"} ,
     "Full path to input file, including basename."
};

static const ParameterNames ComputeFullSVD = {
    {"-computeFullSVD", "--compute-full-svd"} ,
     "Compute the full Singular Value Decomposition (SVD)?"
};

static const ParameterNames VerifyApproxSVD = {
    {"-verifyApproxSVD", "--verify-approx-svd"} ,
     "Verify error of approximate SVD (tSVD or rSVD)?"
};

static const ParameterNames VerifyMatrixApproximation = {
    {"-verifyMatrixApproximation", "--verify-matrix-approximation"} ,
     "Verify matrix approximation (Frobenius norm, Spectral norm and matrix multiplication)?"
};



//
// The following parameters are specific to scalfmm
// TODO Define in seperate file???
//

static const ParameterNames NbParticles = {
    {"-nb", "--number-of-particles", "-N"} ,
     "The number of particles if they are generated by the executable."
};
//
static const ParameterNames OctreeHeight = {
    {"-h", "--height", "-depth"} ,
     "The number of levels in the octree (at least 2 for the root and the leaves)."
};
//
static const ParameterNames OctreeSubHeight = {
    {"-sh", "--sub-height", "-subdepth"} ,
     "The number of allocated levels in the sub octree."
};
//
//static const ParameterNames Epsilon = {
//    {"-epsilon", "--epsilon"} ,
//     "The epsilon needed for the application."
//};
static const ParameterNames IsKernelSmooth = {
    {"-isKernelSmooth", "--is-kernel-smooth", "-appNF"} ,
     "Is the kernel smooth? Approximate nearfield interactions?"
};

static const ParameterNames DistributionName = {
    {"-d", "--distribution-name", "-distributionname"} ,
     "To give a distribution name."
};

/*
* General case of a single input file
*/
static const ParameterNames InputFile = {
    {"-f", "-fin", "--input-filename", "-filename"} ,
     "To give an input file."
};

static const ParameterNames InputBinFormat = {
    {"-binin", "-bininput", "--binary-input" } ,
     "To input is in binary format."
};


/*
* General case of 2 input files
*/
static const ParameterNames InputFileOne = {
    {"-f1", "-fin1", "--file-one"} ,
     "To give the first input file."
};

static const ParameterNames InputFileTwo = {
    {"-f2", "-fin2", "--file-two"} ,
     "To give the second input file."
};

/*
* In case matrix read from file
*/
static const ParameterNames InputMatrixName = {
    {"-imat", "--input-matrix-name", "-imatrixname"} ,
     "To give a matrix name."
};
static const ParameterNames InputMatrixPath = {
    {"-pimat", "--input-matrix-path", "-imatrixpath"} ,
     "To give a path to input matrix."
};
static const ParameterNames InputMatrixFileName = {
    {"-fimat", "--input-matrix-filename", "-imatrixfilename"} ,
     "To give a full matrix filename."
};

/*
* In case matrix written in file
*/
static const ParameterNames OutputMatrixName = {
    {"-omat", "--output-matrix-name", "-omatrixname"} ,
     "To give a matrix name."
};

static const ParameterNames OutputMatrixPath = {
    {"-pomat", "--output-matrix-path", "-omatrixpath"} ,
     "To give a path to the matrix file."
};

static const ParameterNames OutputMatrixFileName = {
    {"-fomat", "--output-matrix-filename", "-omatrixfilename"} ,
     "To give a full matrix filename."
};


/*
* Output files
*/
static const ParameterNames OutputFile = {
    {"-fout", "--output-filename"} ,
     "To give the output filename with extension .fma or  .bfma (binary fma format)."
};

static const ParameterNames OutputVisuFile = {
    {"-fvisuout"} ,
     "Specify the name (with extension) and the format of the file for visualization purpose.\n                     Available format are  vtk, vtp, cvs or cosmo. vtp is the default."
};

//static const ParameterNames FormatVisuFile{
//              {"-visufmt","-visu-fmt"},
//              "To specify format for the visu file (vtk, vtp, cvs or cosmo). vtp is the default"
//          };

static const ParameterNames OutputBinFormat = {
    {"-binout", "-binoutput"} ,
     "To output in binary format."
};


/*
* Other options
*/
static const ParameterNames NbThreads = {
    {"-t", "-nbthreads"} ,
     "To choose the number of threads."
};

static const ParameterNames EnabledVerbose = {
    {"-verbose", "--verbose", "-v"} ,
     "To have a high degree of verbosity."
};


/** To print a list of parameters */
inline void PrintUsedOptions(const std::vector<ParameterNames>& options){
    std::cout << ">> Here is the list of the parameters you can pass to this application :\n";
    for(const ParameterNames& option : options ){
        std::cout << "\t";
        for(const char* name : option.options ){
            std::cout << name << ", ";
        }
        std::cout << "\n\t\t" << option.description << "\n";
        std::cout << "\n";
    }
}

inline bool CheckValidParameters(const int argc, char* argv[], const std::vector<ParameterNames> options){
    bool isValide = true;
    bool previousIsCorrectParameter = false;
    for(int idxParameter = 1 ; idxParameter < argc ; ++idxParameter){
        bool paramExist = false;
        for(unsigned idxTest = 0 ; idxTest < options.size() ; ++idxTest){
            paramExist = existParameter(1, &argv[idxParameter], options[idxTest].options);
            if(paramExist){
                break;
            }
        }
        if(paramExist == true){
            previousIsCorrectParameter = true;
        }
        else if(previousIsCorrectParameter){
            previousIsCorrectParameter = false;
        }
        else{
            previousIsCorrectParameter = false;
            isValide = false;
            std::cout << "[fmr] [PARAMETER-ERROR] Parameter " << (idxParameter-1) << " = \"" << argv[idxParameter] << "\" seems incorrect." << "\n";
        }
    }
    if(isValide == false){
        std::cout << "[fmr] To know more about correct parameters ask for help by passing:\n\t";
        for(const char* param : Help.options){
            std::cout << "\t" << param << ",";
        }
        std::cout << std::endl;
    }
    return isValide;
}

inline void PrintFlags(){
    std::cout << "[fmr] This executable has been compiled with:\n";
//    std::cout << "    Flags: " << FMRCompileFlags << "\n";
//    std::cout << "    Libs;   " << FMRCompileLibs   << "\n";
    std::cout << std::endl;     std::cout.flush();
}

inline void PrintDateHost(){
    std::cout << "[fmr] This execution is on:\n";
    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::cout << "    Date: " << std::ctime(&now) ;
    char hostname[1024];
    gethostname(hostname, 1024);
    std::cout << "    Host: " << hostname << "\n";
    std::cout << std::endl;     std::cout.flush();
}

inline void PrintGivenParams(int argc, const char* const * const argv){
    std::cout << "[fmr] Given Parameters are:\n";
    for(int idx = 0 ; idx < argc ; ++idx){
        std::cout << " | " << argv[idx];
    }
    std::cout << " | \n\n";
    std::cout.flush();
}

/** This should be include at the beginin of all main file
 *  @code FHelpAndExit(argc, argv, NbParticles, ParameterNames OctreeSubHeight );
 */
#define HelpAndExit(argc, argv, ...) \
    if(existParameter(argc, argv, Compile.options)) {\
        PrintFlags();\
    } \
    if(existParameter(argc, argv, DateHost.options)) {\
        PrintDateHost();\
    } \
    if(existParameter(argc, argv, UserParams.options)) {\
        PrintGivenParams(argc, argv);\
    } \
    if(existParameter(argc, argv, Help.options)) {\
        const std::vector<ParameterNames> optionsvec = {Compile, DateHost, \
                                                         UserParams, __VA_ARGS__};\
        PrintUsedOptions(optionsvec);\
        exit(0);                                              \
    } \
    if(CheckValidParameters(argc, argv, {Compile, DateHost, \
                    UserParams, __VA_ARGS__}) == false){ \
        return 121;                                                     \
    }                                                                   \

/** This should be include at the beginin of all main file
 *  @code FHelpDescribeAndExit(argc, argv,
 *  @code       "This executable is doing this and this.",
 *  @code       NbParticles, ParameterNames OctreeSubHeight );
 */
#define HelpDescribeAndExit(argc, argv, description, ...) \
    if(fmr::param::existParameter(argc, argv, fmr::param::Compile.options)) {\
        fmr::param::PrintFlags();\
    } \
    if(fmr::param::existParameter(argc, argv, fmr::param::DateHost.options)) {\
        fmr::param::PrintDateHost();\
    } \
    if(fmr::param::existParameter(argc, argv, fmr::param::UserParams.options)) {\
        fmr::param::PrintGivenParams(argc, argv);\
    } \
    if(fmr::param::existParameter(argc, argv, fmr::param::Help.options)) { \
        std::cout << argv[0] << " : " << description << "\n";           \
        const std::vector<fmr::param::ParameterNames> optionsvec = {fmr::param::Compile, fmr::param::DateHost, \
                                                         fmr::param::UserParams, __VA_ARGS__}; \
        fmr::param::PrintUsedOptions(optionsvec);            \
        exit(0);                                                          \
    }                                                                   \
    if(fmr::param::CheckValidParameters(argc, argv, {fmr::param::Compile, fmr::param::DateHost, \
                                         fmr::param::UserParams, __VA_ARGS__}) == false){ \
        return 121;\
    }

}} /* namespace fmr::param */

#endif // PARAMETERNAMES_HPP
