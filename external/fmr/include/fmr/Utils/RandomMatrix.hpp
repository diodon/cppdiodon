/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef RANDOMMATRIX_HPP
#define RANDOMMATRIX_HPP
#include <algorithm>
#include <cstdlib>
#include <random> // for normal distribution generator
#include <vector>

#define FMR_FIXED_SEED 100

#ifdef FMR_MKL_RANDOM
#include "mkl_vsl.h"
// see https://software.intel.com/en-us/mkl-vsnotes-example-of-vs-use
// Interface

int vRngGaussian(VSLStreamStatePtr stream, const MKL_INT NBloc, float *X,
                 const float a, const float sigma) {
  // VSL_RNG_METHOD_GAUSSIAN_ICDF
  // The normal distribution with parameters a and s is transformed to the
  // random number y by scaling and the shift y = sx+a.
  return vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_ICDF, stream, NBloc, X, a,
                       sigma);
}
int vRngGaussian(VSLStreamStatePtr stream, const MKL_INT NBloc, double *X,
                 const double a, const double sigma) {
  return vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_ICDF, stream, NBloc, X, a,
                       sigma);
}
  // int vDeleteStream(VSLStreamStatePtr stream) { return
  // vslDeleteStream(stream); } int vDeleteStream(VDLStreamStatePtr stream) {
  // return vdlDeleteStream(stream); }

#endif
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/Utils/Display.hpp"

#ifdef _OPENMP
#include "omp.h"
#endif

namespace fmr {
namespace tools {
template <typename FReal_t>
///
////// \brief The class used to generate random matrices
///
///  With blas matrix rapper Gaussian random matrix hile for Chameleon ....
///  TO COMPLETE
/// If the MKL library is used, the FMR_MKL_RANDOM should be set to ON in order
/// to use parallel construction of the random matrix.
///
class RandomClass {
private:
  // Mersenne twister random engine
#ifdef FMR_MKL_RANDOM
  VSLStreamStatePtr *_streams;
  int _num_threads;
#else
  std::mt19937_64 *_generator;                      //(std::random_device{}() );
  std::normal_distribution<FReal_t> *_distribution; //(0.0,1.0) ;
#endif
public:
  RandomClass()
#ifdef FMR_MKL_RANDOM
      : _streams(nullptr), _num_threads(1) {

#pragma omp parallel shared(_num_threads)
    {
#pragma omp single nowait
      { _num_threads = omp_get_num_threads(); }
    }

    // std::cout << "Num_threads " << _num_threads << std::endl;
    _streams = new VSLStreamStatePtr[_num_threads];
    for (int k = 0; k < _num_threads; ++k) {
      // Initialize non-deterministic random number generator
      // https://software.intel.com/content/www/us/en/develop/documentation/onemkl-developer-reference-c/top/statistical-functions/random-number-generators/service-routines/vslnewstream.html
      int errcode =
          vslNewStream(&_streams[k], VSL_BRNG_NONDETERM, /* VSL_BRNG_MT19937,*/
#ifdef FMR_USE_FIXED_SEED
                          FMR_FIXED_SEED );
#else
                           VSL_BRNG_RDRAND ); // VSL_BRNG_SFMT19937 simd version

#endif

      if (errcode == VSL_RNG_ERROR_NONDETERM_NOT_SUPPORTED) {
        std::cout << "CPU does not support non-deterministic generator \n";
        std::exit(EXIT_FAILURE);
      }
      //  vslSkipAheadStream(stream[k], nskip * k); // nskip ???
    }
  }

#else
#ifdef FMR_USE_FIXED_SEED
     : _generator(new std::mt19937_64(FMR_FIXED_SEED)),
#else
     : _generator(new std::mt19937_64(std::random_device{}())),
#endif
        _distribution(new std::normal_distribution<FReal_t>(0.0, 1.0)) {

    (*_distribution)(*_generator); // init
  }
#endif
  ~RandomClass() {
#ifdef FMR_MKL_RANDOM
    for (int i = 0; i < _num_threads; i++) {
      vslDeleteStream(&_streams[i]);
    }
    delete[] _streams;
#endif
  }
  RandomClass(const RandomClass &other) = delete;
  RandomClass(const RandomClass &&other) = delete;
  RandomClass &operator=(const RandomClass &other) = delete;
  RandomClass &operator=(RandomClass &&other) = delete;

  ///
  /// \brief buildMatrixWithRank construct a square matrix of rank r given
  ///
  /// We construct the matrix by its SVD decomposition A = U Diag V^T
  /// The Algorithm is
  ///   Generate rank sorted random singular value to construct Diag
  ///   Generate a random matrix C of size n x r
  ///      Perform a QR factorization of C = Q_C R_C
  ///    if A is symmetric then we return A = Q_C Diag Q_C^T
  ///    otherwise
  ///      Generate a random matrix D of size p x r
  ///      Perform a QR factorization of D = Q_D R_D
  ///     then return the matrix  A = Q_C Diag Q_D ^T
  ///
  /// \param[in] rank the rank of the finalmatrix
  /// \param[in/out] A The final rank r matrix
  /// \param[in] symm if true A is symmetric (default: false)
  /// \param[in] verbose the verbose parameter (default: false)
  ///
  template <typename MATRIX_T>
  void buildMatrixWithRank(const int rank, MATRIX_T &A, bool verbose = false) {
    using value_type = typename MATRIX_T::value_type;
    using int_type = typename MATRIX_T::int_type;
    auto nbCols = A.getNbCols();
    auto nbRows = A.getNbRows();
    // construct random eigenvalues in [0, 3*nbRows]
    auto m = std::min(nbCols, nbRows);
    if (rank > m) {
      std::clog << " rank " << rank << " m=std::min(nbCols, nbRows) " << m << std::endl;
      throw std::invalid_argument(
          "RandomMAtrix::buildMatrixWithRank rank should be lower than nbRos and nbCols. \n");
    }
    std::vector<value_type> diag(rank, 0.0);
#ifdef FMR_USE_FIXED_SEED
    std::mt19937_64 generator(FMR_FIXED_SEED);
#else
    std::mt19937_64 generator(std::random_device{}());
#endif
    // Consider an uniform distribution for the singuar values
    std::uniform_real_distribution<value_type> distribution(
        0.0, value_type(3 * nbRows));

    for (int_type i = 0; i < diag.size(); ++i) {
      diag[i] = distribution(generator);
    }
    std::sort(diag.begin(), diag.end(), std::greater<value_type>());
    if (verbose) {
      Display::vector(nbRows, &diag[0], "Diag", std::min(rank, int(nbRows)));
    }
    auto symm = A.isSymmetric();
    this->buildMatrixWithRank(diag, A, symm, verbose);
  }
  ///
  /// \brief buildMatrixWithRank construct a square matrix of rank r given
  ///
  /// We construct the matrix by its SVD decomposition A = U Diag V^T
  /// The Algorithm is
  ///   Generate rank sorted random singular value to construct Diag
  ///   Generate a random matrix C of size n x r
  ///      Perform a QR factorization of C = Q_C R_C
  ///    if A is symmetric then we return A = Q_C Diag Q_C^T
  ///    otherwise
  ///      Generate a random matrix D of size p x r
  ///      Perform a QR factorization of D = Q_D R_D
  ///     then return the matrix  A = Q_C Diag Q_D ^T
  ///
  /// \param[in] rank the rank of the finalmatrix
  /// \param[in/out] A The final rank r matrix
  /// \param[in/out] diag tvector of  generated on zero singular values (the
  /// size is rank)
  /// \param[in] symm if true A is symmetric (default: false)
  /// \param[in] verbose the verbose parameter (default: false)
  ///
  template <typename MATRIX_T>
  void buildMatrixWithRank(const int rank, MATRIX_T &A,
                           std::vector<typename MATRIX_T::value_type> &diag,
                           bool verbose = false) {
    using value_type = typename MATRIX_T::value_type;
    auto nbCols = A.getNbCols();
    auto nbRows = A.getNbRows();
    // construct random eigenvalues in [0, 3*nbRows]
    auto m = std::min(nbCols, nbRows);
    if (rank > m) {
      std::clog << " rank " << rank << " m=std::min(nbCols, nbRows) " << m << std::endl;
      throw std::invalid_argument(
          "RandomMAtrix::buildMatrixWithRank 2 rank should be lower than nbRos and nbCols. \n");
    }

    /*std::vector<value_type>*/ diag.resize(rank, 0.0);
#ifdef FMR_USE_FIXED_SEED
    std::mt19937_64 generator(FMR_FIXED_SEED);
#else
    std::mt19937_64 generator(std::random_device{}());
#endif
    // Consider an uniform distribution for the singuar values
    std::uniform_real_distribution<value_type> distribution(
        0.0, value_type(3 * nbRows));

    for (std::size_t i = 0; i < diag.size(); ++i) {
      diag[i] = distribution(generator);
    }
    std::sort(diag.begin(), diag.end(), std::greater<value_type>());
    if (verbose) {
      Display::vector(nbRows, &diag[0], "Diag", std::min(rank, int(nbRows)));
    }
    auto symm = A.isSymmetric();
    this->buildMatrixWithRank(diag, A, symm, verbose);
  }
  ///
  /// \brief buildMatrixWithRank construct a square matrix of rank r =
  /// diag.size()
  ///
  /// We construct the matrix by its SVD decomposition A = U Diag V^T
  /// The Algorithm is
  ///   Generate a random matrix C of size n x r
  ///      Perform a QR factorization of C = Q_C R_C
  ///    if A is symmetric then we return A = Q_C Diag Q_C^T
  ///    otherwise
  ///      Generate a random matrix D of size p x r
  ///      Perform a QR factorization of D = Q_D R_D
  ///     then return the matrix  A = Q_C Diag Q_D ^T
  ///
  /// \param[in] diag vector of size rank containing the non zeros eigenvalues
  /// \param[in/out] A The final rank r matrix
  /// \param[in] symm if true A is symmetric (default: false)
  /// \param[in] verbose the verbose parameter (default: false)
  ///
  template <typename MATRIX_T, typename VECTOR_T>
  void buildMatrixWithRank(VECTOR_T &diag, MATRIX_T &A, bool symm = false,
                           bool verbose = false) {

    using value_type = typename MATRIX_T::value_type;
    //
    const auto nbRows = A.getNbRows();
    const auto nbCols = A.getNbCols();

    auto rank = diag.size();
    // Generate random eigen vector
    MATRIX_T D(nbRows, rank);
    this->generateMatrix(D);

    if (verbose) {
      std::cout << "Orthogonalize column vectors QR(D)..." << std::endl;
    }
    QRF<MATRIX_T> qrfD(D);
    qrfD.factorize(); // D is no longuer available

    MATRIX_T &U = qrfD.getQ(); // Build Q from reflectors and U is an alias on Q
    // Construct QL =  U Diag
    MATRIX_T QS(nbRows, rank);
    QS.init(U.getMatrix());
    QS.scaleCols(rank, diag.data());

    if (symm) {
      if (verbose) {
        std::cout << "Computing A =  U Diag  U^T..." << std::endl;
      }
      // Construct A =  U S U^T
      fmrGemm("N", "T", value_type(1.0), QS, U, value_type(0.0), A);
    } else {
      if (verbose) {
        std::cout << "Generating V an othogonal matrix ..." << std::endl;
      }
      MATRIX_T Random_VT(nbCols, rank);
      this->generateMatrix(Random_VT);
      QRF<MATRIX_T> qrfD(Random_VT);
      qrfD.factorize();

      MATRIX_T &VT = qrfD.getQ();
      if (verbose) {
        std::cout << "Computing A =  U Diag  V^T " << std::endl;
      }
      // Construct A =  U S V^T
      fmrGemm("N", "T", value_type(1.0), QS, VT, value_type(0.0), A);
    }
  }

#if defined(FMR_CHAMELEON)
  template <typename INT>
  void generateMatrix(ChameleonDenseWrapper<INT, FReal_t> &Omega) {
    // Generate a random matrix
    Chameleon::plrnt(Omega.getMatrix(), rand() % 100 + 1);
  }
#endif
  ///
  /// \brief Contruct a randomize matrix with Gaussian values
  /// \param Omega The matrix to fill
  ///
  template <class MATRIX_T> void generateMatrix(MATRIX_T &Omega) {
    auto *W = Omega.getMatrix();
    auto nbCols = Omega.getNbCols();
    auto nbRows = Omega.getNbRows();
    ///
    /// Draw $r$ standard gaussian vectors w
    /// // Column major !!
#ifdef FMR_MKL_RANDOM
    this->generateMatrix(nbRows * nbCols, W);
#else
    using int_type = typename MATRIX_T::int_type;
    for (int_type i = 0; i < nbRows; ++i) {
      for (int_type j = 0; j < nbCols; ++j) {
        W[j + i * nbCols] = (*_distribution)(*_generator);
      }
    }
#endif
  }

private:
  ///
  ////// \brief build an array of size N with random gausian values
  ////// \param N size of the Array X
  ////// \param X pointer on the first element of array X
  template <typename Size> void generateMatrix(const Size &N, FReal_t *X) {
#ifdef FMR_MKL_RANDOM
#pragma omp parallel default(none) shared(N, X, _streams, std::cout)
    {
      int NBloc = N / _num_threads;
      auto id_t = omp_get_thread_num();
      auto pos = NBloc * id_t;
      if (id_t == _num_threads - 1) {
        NBloc = N - NBloc * id_t;
      }
      vRngGaussian(_streams[id_t], NBloc, &X[pos], 0.0, 1.0);
    }
#else
    for (auto i = 0; i < N; ++i) {
      X[i] = (*_distribution)(*_generator);
    }
#endif
  }
};
#ifdef USE_TO_REMOVE_FUNCTIONS
///
/// \brief setRandomizeMatrix generate a random matrix
///
/// \param[inout] Omega
///
///    Generate a random matrix
/// Init random number generator (can be moved to ctor)
/// std::default_random_engine generator(/*no seed*/);
/// default random engine
/// std::default_random_engine
template <typename INT>
[[deprecated("Use randGen.buildMatrix(Omega) instead.")]] void
setRandomizeMatrix(BlasDenseWrapper<INT, double> &Omega) {
#ifdef FMR_MKL_RANDOM
  VSLStreamStatePtr *_streams;
  int num_threads = 1;
#pragma omp parallel shared(num_threads)
  {
#pragma omp single nowait
    { num_threads = omp_get_num_threads(); }
  }
  _streams = new VSLStreamStatePtr[num_threads];
  for (int k = 0; k < num_threads; ++k) {
    int errcode =
        vslNewStream(&_streams[k], VSL_BRNG_MT19937,
                     VSL_BRNG_RDRAND); // VSL_BRNG_SFMT19937 simd version
    if (errcode == VSL_RNG_ERROR_NONDETERM_NOT_SUPPORTED) {
      std::cout << "CPU does not support non-deterministic generator \n";
      std::exit(EXIT_FAILURE);
    }
    //              vslSkipAheadStream( stream[k], nskip*k ); // nskip ???
  }
  // VSL_RNG_METHOD_GAUSSIAN_ICDF
  // The normal distribution with parameters a and s is transformed to the
  // random number y by scaling and the shift y = sx+a.
#else
  std::mt19937_64 generator(std::random_device{}());
  std::normal_distribution<double> distribution(0.0, 1.0);
  //
  // generator.seed(0); // for determistic tests
  distribution(generator); // init
#endif
  //
  auto W = Omega.getMatrix();
  auto nbCols = Omega.getNbCols();
  auto nbRows = Omega.getNbRows();
  ///
  /// Draw $r$ standards gaussian vectors w
#ifdef FMR_MKL_RANDOM
#pragma omp parallel
  {
    auto N = nbCols * nbRows;
    auto num_threads = omp_get_num_threads();
    int NBloc = N / num_threads;
    auto id_t = omp_get_thread_num();
    auto pos = NBloc * id_t;
    if (id_t == num_threads - 1) {
      NBloc = N - NBloc * id_t;
    }
    auto errcode = vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_ICDF, _streams[id_t],
                                 NBloc, &W[pos], 0.0, 1.0);
  }

  /* De-initialize */
  for (int i = 0; i < num_threads; i++) {
    auto errcode = vslDeleteStream(&_streams[i]);
  }
  //    Display::matrix(nbRows,nbCols,W," W(init) ",10);
#else
  for (auto i = 0; i < nbCols * nbRows; ++i) {
    W[i] = distribution(generator);
  }
#endif
}

///
/// \brief setRandomizeMatrix generate a random matrix
///
/// \param[inout] Omega
///
///    Generate a random matrix
/// Init random number generator (can be moved to ctor)
/// std::default_random_engine generator(/*no seed*/);
/// default random engine
/// std::default_random_engine generator(std::random_device{}());
/// Mersenne twister random engine
template <typename INT>
[[deprecated("Use randGen.buildMatrix(Omega) instead.")]] void
setRandomizeMatrix(BlasDenseWrapper<INT, float> &Omega) {
  //
#ifdef FMR_MKL_RANDOM
  VSLStreamStatePtr *_streams;
  int num_threads = 1;
#pragma omp parallel shared(num_threads)
  {
#pragma omp single nowait
    { num_threads = omp_get_num_threads(); }
  }
  _streams = new VSLStreamStatePtr[num_threads];
  for (int k = 0; k < num_threads; ++k) {
    int errcode =
        vslNewStream(&_streams[k], VSL_BRNG_MT19937,
                     VSL_BRNG_RDRAND); // VSL_BRNG_SFMT19937 simd version
    if (errcode == VSL_RNG_ERROR_NONDETERM_NOT_SUPPORTED) {
      std::cout << "CPU does not support non-deterministic generator \n";
      std::exit(EXIT_FAILURE);
    }
    //              vslSkipAheadStream( stream[k], nskip*k ); // nskip ???
  }
  // VSL_RNG_METHOD_GAUSSIAN_ICDF
  // The normal distribution with parameters a and s is transformed to the
  // random number y by scaling and the shift y = sx+a.
#else
  std::mt19937_64 generator(std::random_device{}());
  std::normal_distribution<float> distribution(0.0, 1.0);
  //
  // generator.seed(0); // for determistic tests
  distribution(generator); // init
#endif
  //
  auto W = Omega.getMatrix();
  auto nbCols = Omega.getNbCols();
  auto nbRows = Omega.getNbRows();
  ///
  /// Draw $r$ standards gaussian vectors w
#ifdef FMR_MKL_RANDOM
#pragma omp parallel
  {
    auto N = nbCols * nbRows;
    auto num_threads = omp_get_num_threads();
    int NBloc = N / num_threads;
    auto id_t = omp_get_thread_num();
    auto pos = NBloc * id_t;
    if (id_t == num_threads - 1) {
      NBloc = N - NBloc * id_t;
    }
    auto errcode = vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_ICDF, _streams[id_t],
                                 NBloc, &W[pos], 0.0, 1.0);
  }

  /* De-initialize */
  for (int i = 0; i < num_threads; i++) {
    auto errcode = vslDeleteStream(&_streams[i]);
  }
  //    Display::matrix(nbRows,nbCols,W," W(init) ",10);
#else
  for (auto i = 0; i < nbCols * nbRows; ++i) {
    W[i] = distribution(generator);
  }
#endif
}
#endif
#if defined(FMR_CHAMELEON)
template <typename INT, typename FReal_t>
[[deprecated("Use randGen.buildMatrix(Omega) instead.")]] void
setRandomizeMatrix(ChameleonDenseWrapper<INT, FReal_t> &Omega) {
  // Generate a random matrix
  Chameleon::plrnt(Omega.getMatrix(), rand() % 100 + 1);
}
#endif

} // namespace tools
} // namespace fmr

#endif // RANDOMMATRIX_HPP
