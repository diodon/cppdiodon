/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

// ===================================================================================
// Copyright ScalFmm 2016 INRIA, Olivier Coulaud, Bérenger Bramas,
// Matthias Messner olivier.coulaud@inria.fr, berenger.bramas@inria.fr
// This software is a computer program whose purpose is to compute the
// FMM.
//
// This software is governed by the CeCILL-C and LGPL licenses and
// abiding by the rules of distribution of free software.
// An extension to the license is given to allow static linking of scalfmm
// inside a proprietary application (no matter its license).
// See the main license file for more details.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public and CeCILL-C Licenses for more details.
// "http://www.cecill.info".
// "http://www.gnu.org/licenses".
// ===================================================================================
#ifndef ACCURATOR_HPP
#define ACCURATOR_HPP

#include <iostream>
#include <cinttypes>
/**
 * @authors Berenger Bramas (berenger.bramas@inria.fr)
 *          Olivier Coulaud (olivier.coulaud@inria.fr)
 * @class
 * Please read the license
 *

*/
namespace fmr  {

    /** A class to compute accuracy */
    template <class ValueType, class IndexType = std::int64_t>
    class Accurater {
    IndexType    nbElements;
    ValueType l2Dot;
    ValueType l2Diff;
    ValueType max;
    ValueType maxDiff;
public:
    Accurater() : nbElements(0),l2Dot(0.0), l2Diff(0.0), max(0.0), maxDiff(0.0) {
    }
    /** with inital values */
    Accurater(const ValueType inGood[], const ValueType inBad[], const IndexType nbValues)
        :  nbElements(0),l2Dot(0.0), l2Diff(0.0), max(0.0), maxDiff(0.0)  {
        add(inGood, inBad, nbValues);
    }


    /** Add value to the current list */
    void add(const ValueType inGood, const ValueType inBad){
        l2Diff          += (inBad - inGood) * (inBad - inGood);
        l2Dot          += inGood * inGood;
        max               = std::max(max , std::abs(inGood));
        maxDiff         = std::max(maxDiff, std::abs(inGood-inBad));
        nbElements += 1 ;
    }
    /** Add array of values */
    void add(const ValueType inGood[], const ValueType inBad[], const IndexType nbValues){
        for(IndexType idx = 0 ; idx < nbValues ; ++idx){
                this->add(inGood[idx],inBad[idx]);
            }
        nbElements += nbValues ;
    }

    /** Add an accurater*/
    void add(const Accurater& inAcc){
        l2Diff += inAcc.getl2Diff();
        l2Dot +=  inAcc.getl2Dot();
        max = std::max(max,inAcc.getmax());
        maxDiff = std::max(maxDiff,inAcc.getInfNorm());
        nbElements += inAcc.getNbElements();
    }

    ValueType getl2Diff() const{
        return l2Diff;
    }
    ValueType getl2Dot() const{
        return l2Dot;
    }
    ValueType getmax() const{
        return max;
    }
    IndexType getNbElements() const{
        return nbElements;
    }
    void  setNbElements(const IndexType & n) {
        nbElements = n;
    }

    /** Get the root mean squared error*/
    ValueType getL2Norm() const{
        return std::sqrt(l2Diff );
    }
    /** Get the L2 norm */
    ValueType getRMSError() const{
        return std::sqrt(l2Diff /static_cast<ValueType>(nbElements));
    }

    /** Get the inf norm */
    ValueType getInfNorm() const{
        return maxDiff;
    }
    /** Get the L2 norm */
    ValueType getRelativeL2Norm() const{
        return std::sqrt(l2Diff / l2Dot);
    }
    /** Get the inf norm */
    ValueType getRelativeInfNorm() const{
        return maxDiff / max;
    }
    /** Print */
    template <class StreamClass>
    friend StreamClass& operator<<(StreamClass& output, const Accurater& inAccurater){
        output << "[Error] Relative L2Norm = " << inAccurater.getRelativeL2Norm() << " \t RMS Norm = " << inAccurater.getRMSError() << " \t Relative Inf = " << inAccurater.getRelativeInfNorm();
        return output;
    }

    void reset()
    {
        l2Dot      = ValueType(0);
        l2Diff     = ValueType(0);;
        max        = ValueType(0);
        maxDiff    = ValueType(0);
        nbElements = 0 ;
    }
};

}

#endif //ACCURATOR_HPP
