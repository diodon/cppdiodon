/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <sstream>
#include <iostream>
#include <cstring>

#include <vector>

//#include "fmr/Containers/Vector.hpp"
template<typename T>
using Vector = std::vector<T> ;
#include "fmr/Utils/Assert.hpp"

/** This file proposes some methods
  * to work with user input parameters.
  */

namespace fmr{
namespace param{

        /** If it is not found */
        const static int NotFound = -1;
        /**
         * This function gives a parameter in a standart type
         * @param str string of chars to be converted from
         * @param defaultValue the value to be converted to
         * @return argv[inArg] in the template VariableType form
         * @warning VariableType need to work with istream >> operator
         * <code> const int argInt = userParemetersAt<int>(1,-1); </code>
         */
        template <class VariableType>
    inline const VariableType StrToOther(const char* const str, const VariableType& defaultValue = VariableType(), bool* hasWorked = nullptr){
                std::istringstream iss(str,std::istringstream::in);
        VariableType value = defaultValue;
                iss >> value;
        AssertLF(iss.eof());
        if(hasWorked) (*hasWorked) = bool(iss.eof());
                if( /*iss.tellg()*/ iss.eof() ) return value;
                return defaultValue;
        }

    /** To put a char into lower format
      *
      */
    inline char toLower(const char c){
        return char('A' <= c && c <= 'Z' ? (c - 'A') + 'a' : c);
    }

    /** To know if two char are equals
      *
      */
    inline bool areCharsEquals(const char c1, const char c2, const bool caseSensible = false){
        return (caseSensible && c1 == c2) || (!caseSensible && toLower(c1) == toLower(c2));
    }

    /** To know if two str are equals
      *
      */
    inline bool areStrEquals(const char* const inStr1, const char* const inStr2, const bool caseSensible = false){
        int idxStr = 0;
        while(inStr1[idxStr] != '\0' && inStr2[idxStr] != '\0'){
            if(!areCharsEquals(inStr1[idxStr] ,inStr2[idxStr],caseSensible)){
                return false;
            }
            ++idxStr;
        }
        return inStr1[idxStr] == inStr2[idxStr];
    }

    /** To find a parameters from user format char parameters
      *
      */
    inline int findParameter(const int argc, const char* const * const argv, const char* const inName, const bool caseSensible = false){
        for(int idxArg = 0; idxArg < argc ; ++idxArg){
            if(areStrEquals(inName, argv[idxArg], caseSensible)){
                return idxArg;
            }
        }
        return NotFound;
    }

    /** To know if a parameter exist from user format char parameters
      *
      */
    inline bool existParameter(const int argc, const char* const * const argv, const char* const inName, const bool caseSensible = false){
        return NotFound != findParameter( argc, argv, inName, caseSensible);
    }

    /** To get a value like :
      * getValue(argc,argv, "Toto", 0, false);
      * will return 55 if the command contains : -Toto 55
      * else 0
      */
    template <class VariableType>
    inline const VariableType getValue(const int argc, const char* const * const argv, const char* const inName, const VariableType& defaultValue = VariableType(), const bool caseSensible = false){
        const int position = findParameter(argc,argv,inName,caseSensible);
        AssertLF(position == NotFound || position != argc - 1);
        if(position == NotFound || position == argc - 1){
            return defaultValue;
        }
        return StrToOther(argv[position+1],defaultValue);
    }

    /** Get a str from argv
      */
    inline const char* getStr(const int argc, const char* const * const argv, const char* const inName, const char* const inDefault, const bool caseSensible = false){
        const int position = findParameter(argc,argv,inName,caseSensible);
        AssertLF(position == NotFound || position != argc - 1);
        if(position == NotFound || position == argc - 1){
            return inDefault;
        }
        return argv[position+1];
    }


    /** To find a parameters from user format char parameters
      *
      */
    inline int findParameter(const int argc, const char* const * const argv, const std::vector<const char*>& inNames, const bool caseSensible = false){
        for(const char* name : inNames){
            const int res = findParameter(argc, argv, name, caseSensible);
            if(res != NotFound){
                return res;
            }
        }
        return NotFound;
    }

    /** To know if a parameter exist from user format char parameters
      *
      */
    inline bool existParameter(const int argc, const char* const * const argv, const std::vector<const char*>& inNames, const bool caseSensible = false){
        for(const char* name : inNames){
            if(existParameter(argc, argv, name, caseSensible)){
                return true;
            }
        }
        return false;
    }

    /** To get a value like :
      * getValue(argc,argv, "Toto", 0, false);
      * will return 55 if the command contains : -Toto 55
      * else 0
      */
    template <class VariableType>
    inline const VariableType getValue(const int argc, const char* const * const argv, const std::vector<const char*>& inNames, const VariableType& defaultValue = VariableType(), const bool caseSensible = false){
        for(const char* name : inNames){
            const int position = findParameter(argc, argv, name, caseSensible);
            AssertLF(position == NotFound || position != argc - 1, "Could no find a value for argument: ",name, ". " );
            if(position != NotFound && position != argc - 1){
                return StrToOther(argv[position+1],defaultValue);
            }
        }
        return defaultValue;
    }

    /** Get a str from argv
      */
    inline const char* getStr(const int argc, const char* const * const argv, const std::vector<const char*>& inNames, const char* const inDefault, const bool caseSensible = false){
        for(const char* name : inNames){
            const int position = findParameter(argc, argv, name, caseSensible);
            AssertLF(position == NotFound || position != argc - 1, "Could no find a value for argument: ",name, ". ");
            if(position != NotFound && position != argc - 1){
                return argv[position+1];
            }
        }
        return inDefault;
    }


    template <class ValueType>
    inline Vector<ValueType> getListOfValues(const int argc, const char* const * const argv, const std::vector<const char*>& inNames, const char separator = ';'){
        const char* valuesStr = getStr( argc, argv, inNames, nullptr);
        if(valuesStr == nullptr){
            return Vector<ValueType>();
        }

        Vector<char> word;
        Vector<ValueType> res;
        int idxCharStart = 0;
        int idxCharEnd = 0;
        while(valuesStr[idxCharEnd] != '\0'){
            if(valuesStr[idxCharEnd] == separator){
                const int lengthWord = idxCharEnd-idxCharStart;
                if(lengthWord){
                    word.clear();                    word.resize(lengthWord);

                    std::memcpy(word.data(),&valuesStr[idxCharStart], lengthWord);
//                    word.memocopy(&valuesStr[idxCharStart], lengthWord);
                    word.push_back('\0');
                    bool hasWorked;
                    const ValueType val = StrToOther(word.data(), ValueType(), &hasWorked);
                    if(hasWorked){
                        res.push_back(val);
                    }
                }
                idxCharEnd  += 1;
                idxCharStart = idxCharEnd;
            }
            else{
                idxCharEnd  += 1;
            }
        }
        {
            const int lengthWord = idxCharEnd-idxCharStart;
            if(lengthWord){
//                word.clear();
//                word.memocopy(&valuesStr[idxCharStart], lengthWord);
//                word.push('\0');
                word.clear();  word.resize(lengthWord);

                std::memcpy(word.data(),&valuesStr[idxCharStart], lengthWord);
                //                    word.memocopy(&valuesStr[idxCharStart], lengthWord);
                word.push_back('\0');
                bool hasWorked;
                const ValueType val = StrToOther(word.data(), ValueType(), &hasWorked);
                if(hasWorked){
                    res.push_back(val);
                }
            }
        }

        return res;
    }

}} /* namespace fmr::param */

#endif // PARAMETERS_H
