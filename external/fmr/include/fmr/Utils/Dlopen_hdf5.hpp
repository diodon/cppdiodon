#ifndef FMR_DLOPEN_HDF5_HPP
#define FMR_DLOPEN_HDF5_HPP

#ifdef FMR_DLOPEN_HDF5

// The dlopen part
#include <dlfcn.h>
#include <unistd.h>

// Check error in a dl call
void check_dl_error(void* result)
{
  if(!result){
     char *errstr;
     errstr = dlerror();
     if (errstr != NULL)
     printf ("Dlopen error: (%s)\n", errstr);
     exit(1);
  }
}

// Call dlsym and check errors
void* dl_load_func(void* handle, const char* string)
{
  void* func = dlsym(handle, string);
  check_dl_error(func);
  return(func);
}

// libhdf5 function types that we are loading
typedef  herr_t (*dl_H5open_f)(void);
typedef  hid_t  (*dl_H5Fopen_f)(const char*, unsigned int, hid_t);
typedef  hid_t  (*dl_H5Dopen2_f)(hid_t, const char *, hid_t);
typedef  hid_t  (*dl_H5Dget_space_f)(hid_t);
typedef  int    (*dl_H5Sget_simple_extent_dims_f)(hid_t, hsize_t[], hsize_t[]);
typedef  hid_t  (*dl_H5Screate_simple_f)(int, const hsize_t[], const hsize_t[]);
typedef  herr_t (*dl_H5Sselect_hyperslab_f)(hid_t, H5S_seloper_t, const hsize_t[], const hsize_t[], const hsize_t[], const hsize_t[]);
typedef  herr_t (*dl_H5Dread_f)(hid_t, hid_t, hid_t, hid_t, hid_t, void *);
typedef  herr_t (*dl_H5Sclose_f)(hid_t);
typedef  herr_t (*dl_H5Dclose_f)(hid_t);
typedef  herr_t (*dl_H5Fclose_f)(hid_t);

// Struct to hold the functions ptrs and the lib file used
typedef struct hdf5_dyn_functions
{
  int id;
  char new_lib[64];
  void* dlmopen_ptr;

  // libhdf5 functions ptrs
  dl_H5open_f dl_H5open;
  dl_H5Fopen_f dl_H5Fopen;
  dl_H5Dopen2_f dl_H5Dopen2;
  dl_H5Dget_space_f dl_H5Dget_space;
  dl_H5Sget_simple_extent_dims_f dl_H5Sget_simple_extent_dims;
  dl_H5Screate_simple_f dl_H5Screate_simple;
  dl_H5Sselect_hyperslab_f dl_H5Sselect_hyperslab;
  hid_t dl_H5T_IEEE_F64LE_g;
  hid_t dl_H5T_IEEE_F32LE_g;
  dl_H5Dread_f dl_H5Dread;
  dl_H5Sclose_f dl_H5Sclose;
  dl_H5Dclose_f dl_H5Dclose;
  dl_H5Fclose_f dl_H5Fclose;

  int nfiles;
  hid_t *read_file;
  hid_t *read_dataset;
  hid_t *read_dataspace;
} hdf5_dyn_functions_t;

hdf5_dyn_functions_t* create_hdf5_instance(int mpirank, int id)
{
     hdf5_dyn_functions_t* ret = (hdf5_dyn_functions_t*)malloc(sizeof(hdf5_dyn_functions_t));
     // The copy will be placed here
     sprintf(ret->new_lib, "/tmp/libhdf5_%d_%d", mpirank, id);

     // Open the original lib (to get path) and make a copy
     if (access(ret->new_lib, F_OK) != 0) { // File do not exist
       void* lib = dlopen("libhdf5.so", RTLD_DEEPBIND | RTLD_LOCAL | RTLD_LAZY);
       Dl_info info;
       check_dl_error(lib);
       void* func_lib = dlsym(lib, "H5open");
       check_dl_error(func_lib);
       dladdr(func_lib, &info);
       char cmd[512];
       sprintf(cmd, "cp %s %s", info.dli_fname, ret->new_lib);
       system(cmd);
       dlclose(lib);
     }

     /* Now open it and get functions pointers
        RTLD_DEEPBIND is what enables linking fmr with libhdf5 normally, so we
        can have function calls using the library normally without the dlopen
        trick, but when using dlopen it do not get lost */

     ret->dlmopen_ptr = dlopen(ret->new_lib, RTLD_DEEPBIND | RTLD_LOCAL | RTLD_LAZY);
     check_dl_error(ret->dlmopen_ptr);
     ret->id = id;
     ret->nfiles = 0;
     ret->dl_H5open = (dl_H5open_f)dl_load_func(ret->dlmopen_ptr, "H5open");
     ret->dl_H5Fopen = (dl_H5Fopen_f)dl_load_func(ret->dlmopen_ptr, "H5Fopen");
     ret->dl_H5Dopen2 = (dl_H5Dopen2_f)dl_load_func(ret->dlmopen_ptr, "H5Dopen2");
     ret->dl_H5Dget_space = (dl_H5Dget_space_f)dl_load_func(ret->dlmopen_ptr, "H5Dget_space");
     ret->dl_H5Sget_simple_extent_dims = (dl_H5Sget_simple_extent_dims_f)dl_load_func(ret->dlmopen_ptr, "H5Sget_simple_extent_dims");
     ret->dl_H5Screate_simple = (dl_H5Screate_simple_f)dl_load_func(ret->dlmopen_ptr, "H5Screate_simple");
     ret->dl_H5Sselect_hyperslab = (dl_H5Sselect_hyperslab_f)dl_load_func(ret->dlmopen_ptr, "H5Sselect_hyperslab");
     ret->dl_H5T_IEEE_F64LE_g = *((hid_t*)dl_load_func(ret->dlmopen_ptr, "H5T_IEEE_F64LE_g"));
     ret->dl_H5T_IEEE_F32LE_g = *((hid_t*)dl_load_func(ret->dlmopen_ptr, "H5T_IEEE_F32LE_g"));
     ret->dl_H5Dread = (dl_H5Dread_f)dl_load_func(ret->dlmopen_ptr, "H5Dread");
     ret->dl_H5Sclose = (dl_H5Sclose_f)dl_load_func(ret->dlmopen_ptr, "H5Sclose");
     ret->dl_H5Dclose = (dl_H5Dclose_f)dl_load_func(ret->dlmopen_ptr, "H5Dclose");
     ret->dl_H5Fclose = (dl_H5Fclose_f)dl_load_func(ret->dlmopen_ptr, "H5Fclose");

     ret->read_file = NULL;
     ret->read_dataset = NULL;
     ret->read_dataspace = NULL;

     return(ret);
}

// Functions to make HDF5 include coop and call the correct functions
static herr_t _dl_H5open(hdf5_dyn_functions_t* dl_handle){
  return dl_handle->dl_H5open();
}

static herr_t _dl_H5check(hdf5_dyn_functions_t* dl_handle){//TODO
  (void)dl_handle;
  return (herr_t)0;
}

static hid_t _dl_H5T_IEEE_F64LE_g(hdf5_dyn_functions_t* dl_handle){
  dl_handle->dl_H5T_IEEE_F64LE_g = *((hid_t*)dl_load_func(dl_handle->dlmopen_ptr, "H5T_IEEE_F64LE_g"));
  return dl_handle->dl_H5T_IEEE_F64LE_g;
}

static hid_t _dl_H5T_IEEE_F32LE_g(hdf5_dyn_functions_t* dl_handle){
  dl_handle->dl_H5T_IEEE_F32LE_g = *((hid_t*)dl_load_func(dl_handle->dlmopen_ptr, "H5T_IEEE_F32LE_g"));
  return dl_handle->dl_H5T_IEEE_F32LE_g;
}

/* Redefine some macros that call functions to use the dlopen ones
   The macros need to pass the dl_handle so it can call the correct function */
#define DL5_H5OPEN(dl_handle)          _dl_H5open(dl_handle),
#define DL5_H5CHECK(dl_handle)         _dl_H5check(dl_handle),

#define DL5_H5F_ACC_RDONLY(dl_handle)    (DL5_H5CHECK(dl_handle) DL5_H5OPEN(dl_handle) 0x0000u)
#define DL5_H5T_IEEE_F64LE(dl_handle)    (DL5_H5OPEN(dl_handle) _dl_H5T_IEEE_F64LE_g(dl_handle))
#define DL5_H5T_IEEE_F32LE(dl_handle)    (DL5_H5OPEN(dl_handle) _dl_H5T_IEEE_F32LE_g(dl_handle))


// Auxiliary functions to open files easily
void dl_open_file_read(int files, hdf5_dyn_functions_t* dl_handle, std::vector<std::string> filenames, std::vector<std::string> datasetnames){
  dl_handle->nfiles = files;
  dl_handle->read_file = (hid_t*)calloc(files, sizeof(hid_t));
  dl_handle->read_dataset = (hid_t*)calloc(files, sizeof(hid_t));
  dl_handle->read_dataspace = (hid_t*)calloc(files, sizeof(hid_t));
  for(int i=0; i<files; i++){
    dl_handle->read_file[i] = dl_handle->dl_H5Fopen(filenames[i].c_str(), DL5_H5F_ACC_RDONLY(dl_handle), H5P_DEFAULT);
    dl_handle->read_dataset[i] = dl_handle->dl_H5Dopen2(dl_handle->read_file[i], datasetnames[i].c_str(), H5P_DEFAULT);
    dl_handle->read_dataspace[i] = dl_handle->dl_H5Dget_space(dl_handle->read_dataset[i]);
  }

}

void close_hdf5_instance(hdf5_dyn_functions_t* dl_handle)
{
  for(int i=0; i < dl_handle->nfiles; i++){
    dl_handle->dl_H5Sclose(dl_handle->read_dataspace[i]);
    dl_handle->dl_H5Dclose(dl_handle->read_dataset[i]);
    dl_handle->dl_H5Fclose(dl_handle->read_file[i]);
  }
  if(dl_handle->nfiles>0){
    free(dl_handle->read_file);
    free(dl_handle->read_dataset);
    free(dl_handle->read_dataspace);
  }
  if(dl_handle->dlmopen_ptr!=NULL){
      // TODO: Close lib
      /*int st = dlclose(dl_handle->dlmopen_ptr);
      if(st!=0){
        printf("Error dlclose\n");
      }*/
  }

  dl_handle->dlmopen_ptr = NULL;
  free(dl_handle);
  // TODO: Delete file in tmp?
}

#endif //FMR_DLOPEN_HDF5

#endif //HDF5_DLOPEN_POST_HPP
