/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef STARPU_HPP
#define STARPU_HPP

#include <algorithm>
#include "fmr/Utils/Chameleon.hpp"
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#include <starpu_mpi.h>
#else
#include <starpu.h>
#endif
#if defined(FMR_HDF5)
#include "hdf5.h"
#ifdef FMR_DLOPEN_HDF5
#include "fmr/Utils/Dlopen_hdf5.hpp"
#endif
#endif

/* This file defines our starpu objects and functions. This is used to read H5
files in parallel in an optimized way. */

template <typename FReal>
starpu_data_handle_t get_starpu_handle( int id_handles, starpu_data_handle_t *handles, int id_handle, FReal *user_buffer, unsigned long long sizeblock, int myrank, int owner )
{
    starpu_data_handle_t *tile_handle = handles + id_handle;

    /* If the starpu_data_handle_t is NULL, we need to register the data */
    if ( *tile_handle == NULL ) {
        int home_node = -1;
        void *user_ptr = NULL;
        if ( myrank == owner ) {
            user_ptr = user_buffer;
            if ( user_buffer != NULL ) {
                home_node = STARPU_MAIN_RAM;
                starpu_memory_allocate(STARPU_MAIN_RAM, sizeblock * sizeof( FReal ), STARPU_MEMORY_WAIT);
            }
        }

        starpu_vector_data_register( tile_handle, home_node, (uintptr_t)user_ptr, (uint32_t)sizeblock, sizeof( FReal ));

#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        /**
         * We need to give a unique tag to each data
         * Be careful to take into account the multiple decriptors that can be used in parallel.
         */
        {
            /* TODO */
            int64_t tag = ((int64_t)id_handles << 32) | id_handle;
            starpu_mpi_data_register( *tile_handle, tag, owner );
        }
#endif /* defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI) */
    }

    return *tile_handle;
}

void free_unregister_starpu_handle_cb(void *arg)
{
    starpu_data_handle_t handle = (starpu_data_handle_t)arg;
    size_t sizeblock = starpu_data_get_size(handle);
    void* ptr = (void*)starpu_vector_get_local_ptr(handle);
    starpu_data_release_on_node(handle, STARPU_ACQUIRE_NO_NODE_LOCK_ALL);
    starpu_data_unregister_submit(handle);
    free(ptr);
    starpu_memory_deallocate(STARPU_MAIN_RAM, sizeblock);
}

void free_unregister_starpu_handle( int nb, starpu_data_handle_t *handles )
{
    int i;
    for (i=0; i<nb; i++, handles++) {
        if (*handles != NULL) {
            starpu_data_acquire_on_node_cb(*handles, STARPU_ACQUIRE_NO_NODE_LOCK_ALL, STARPU_RW, free_unregister_starpu_handle_cb, *handles);
        }
    }
}

void unregister_starpu_handle( int nb, starpu_data_handle_t *handles )
{
    int i;
    for (i=0; i<nb; i++, handles++) {
        if (*handles != NULL) {
            starpu_data_unregister_submit( *handles );
        }
    }
}

/**
 * @brief Modify the insert call if MPI is used
 */
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#undef starpu_insert_task
#define starpu_insert_task(...) starpu_mpi_insert_task(MPI_COMM_WORLD, __VA_ARGS__)
#define starpu_mpi_codelet(_codelet_) _codelet_
#else
#define starpu_mpi_codelet(_codelet_) _codelet_
#endif

#if defined(FMR_HDF5)
/**
 * @brief Structure to gather static parameters of the kernel
 */
typedef struct cl_readh5_arg_s {
    hid_t               dset;   // H5 dataset id
    hid_t               dspace; // H5 dataspace id
    hsize_t             nrows;  // number of rows to read
    hsize_t             ncols;  // number of columns to read
    hsize_t             orows;   // offset rows in the H5 dataset
    hsize_t             ocols;   // offset columns in the H5 dataset
} cl_readh5_arg_t;

template <typename FReal>
static void cl_readh5_cpu_func( void *buffer[], void *cl_arg )
{
    /* CPU copy of the vector pointer */
    FReal *panel = (FReal *)STARPU_VECTOR_GET_PTR(buffer[0]);

    cl_readh5_arg_t args;
    starpu_codelet_unpack_args( cl_arg, &args );

    hsize_t start[2] = {args.orows, args.ocols};
    hsize_t count[2] = {args.nrows, args.ncols};
    hsize_t stride[2] = {1, 1};
    hsize_t block[2]  = {1, 1};

    /* create memspace */
    hid_t memspace = H5Screate_simple (2, count, NULL);
    /* select subset */
    H5Sselect_hyperslab(args.dspace, H5S_SELECT_SET, start, stride, count, block);

    /* Read dataset's values */
    const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    H5Dread(args.dset, H5_TYPE, memspace, args.dspace, H5P_DEFAULT, panel);

    H5Sclose(memspace);
}

void insert_readh5( starpu_codelet *cl,
                    hid_t dset,
                    hid_t dspace,
                    hsize_t nrows,
                    hsize_t ncols,
                    hsize_t orows,
                    hsize_t ocols,
                    starpu_data_handle_t panel )
{
    cl_readh5_arg_t args;
    args.dset = dset;
    args.dspace = dspace;
    args.nrows = nrows;
    args.ncols = ncols;
    args.orows = orows;
    args.ocols = ocols;

    /* hdf5 read is not threadsafe so that we force executiion on a specific
    worker id. starpu numbered cuda and opencl workers first. Thus the first
    worker id of type cpu is: */
    int workerid = starpu_cuda_worker_get_count()+starpu_opencl_worker_get_count();

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_readh5_arg_t),
        STARPU_W, panel,
        STARPU_EXECUTE_ON_WORKER, workerid,
        0);
}

#ifdef FMR_DLOPEN_HDF5

int dl_diodon_nworkers_read(){
  // Assuming we have at least two CPU workers
  unsigned int nworker_read = std::min((unsigned)4, starpu_cpu_worker_get_count() - 1);
  const char* nworker_read_env = getenv("FMR_CHAMELEON_NWORKER_READ");
  if ( nworker_read_env != NULL ){
    try
    {
      nworker_read = std::stoi(nworker_read_env);
      if(nworker_read >= starpu_cpu_worker_get_count()){
        fprintf(stderr, "[fmr] NWORKER_READ needs to be at maximum NCPU workers - 1, asked for %u, changing to %u\n", nworker_read, starpu_cpu_worker_get_count() - 1);
        nworker_read = starpu_cpu_worker_get_count() - 1;
      }
      if(nworker_read <= 0){
        fprintf(stderr, "[fmr] NWORKER_READ needs to be positive\n");
        exit(EXIT_FAILURE);
      }
    }
    catch(std::invalid_argument const& ex)
    {
      std::cout << "std::invalid_argument::what(): " << ex.what() << '\n';
    }
  }
  return(nworker_read);
}

typedef struct cl_readh5dl_arg_s {
    hsize_t             nrows;  // number of rows to read
    hsize_t             ncols;  // number of columns to read
    hsize_t             orows;   // offset rows in the H5 dataset
    hsize_t             ocols;   // offset columns in the H5 dataset
    int                 file; //the file id to select in the struct
} cl_readh5dl_arg_t;

template <typename FReal>
static void cl_readh5dl_cpu_func( void *buffer[], void *cl_arg )
{
    /* CPU copy of the vector pointer */
    FReal *panel = (FReal *)STARPU_VECTOR_GET_PTR(buffer[0]);
    hdf5_dyn_functions_t *dl_handler = (hdf5_dyn_functions_t*)STARPU_VECTOR_GET_PTR(buffer[1]);

    cl_readh5dl_arg_t args;
    starpu_codelet_unpack_args( cl_arg, &args );

    hsize_t start[2] = {args.orows, args.ocols};
    hsize_t count[2] = {args.nrows, args.ncols};
    hsize_t stride[2] = {1, 1};
    hsize_t block[2]  = {1, 1};

    /* create memspace */
    hid_t memspace = dl_handler->dl_H5Screate_simple(2, count, NULL);
    /* select subset */
    dl_handler->dl_H5Sselect_hyperslab(dl_handler->read_dataspace[args.file], H5S_SELECT_SET, start, stride, count, block);

    /* Read dataset's values */
    const auto H5_TYPE = (sizeof(FReal) == 8) ? DL5_H5T_IEEE_F64LE(dl_handler) : DL5_H5T_IEEE_F32LE(dl_handler) ;
    dl_handler->dl_H5Dread(dl_handler->read_dataset[args.file], H5_TYPE, memspace, dl_handler->read_dataspace[args.file], H5P_DEFAULT, panel);

    dl_handler->dl_H5Sclose(memspace);
}

static void cl_endhdf5_cpu_func( void *buffer[], void *cl_arg )
{
    (void)cl_arg;
    hdf5_dyn_functions_t *dl_handler = (hdf5_dyn_functions_t*)STARPU_VECTOR_GET_PTR(buffer[0]);
    close_hdf5_instance(dl_handler);
}

void insert_readh5dl( starpu_codelet *cl,
                    hsize_t nrows,
                    hsize_t ncols,
                    hsize_t orows,
                    hsize_t ocols,
                    int file,
                    starpu_data_handle_t panel,
                    starpu_data_handle_t dl_handler,
                    int row,
                    int col)
{
    cl_readh5dl_arg_t args;
    args.nrows = nrows;
    args.ncols = ncols;
    args.orows = orows;
    args.ocols = ocols;
    args.file = file;

    int prio = 0;

    starpu_data_set_coordinates(panel, 2, row, col);

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_readh5dl_arg_t),
        STARPU_W, panel,
        STARPU_RW, dl_handler,
        STARPU_PRIORITY, prio,
        0);

}

void insert_closeh5dl( starpu_codelet *cl,
                       starpu_data_handle_t dl_handler)
{
    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_RW, dl_handler,
        0);
}
#endif

#endif

struct starpu_cham_tile_interface_s;
typedef struct starpu_cham_tile_interface_s starpu_cham_tile_interface_t;

/**
 * @brief Chameleon tile interface
 */
struct starpu_cham_tile_interface_s
{
    enum starpu_data_interface_id id; /**< Identifier of the interface           */
    uintptr_t      dev_handle;        /**< device handle of the matrix           */
    cham_flttype_t flttype;           /**< Type of the elements of the matrix    */
    size_t         allocsize;         /**< size actually currently allocated     */
    size_t         tilesize;          /**< size of the elements of the matrix    */
    CHAM_tile_t    tile;              /**< Internal tile structure used to store
                                           information on non memory home_node   */
};

/**
 * @brief Returns the chameleon tile object
 */
static inline CHAM_tile_t *
cti_interface_get( starpu_cham_tile_interface_t *interface )
{
    return &(interface->tile);
}

#if defined(FMR_HDF5)
/**
 * @brief Structure to gather static parameters of the kernel
 */
typedef struct cl_copypaneltotile_arg_s {
    hsize_t panel_rows_beg; // beginning index rows in the global matrix of the H5 block
    hsize_t panel_rows_end; // end index rows in the global matrix of the H5 block
    hsize_t panel_cols_beg; // beginning index rows in the global matrix of the H5 block
    hsize_t panel_cols_end; // end index rows in the global matrix of the H5 block
    cham_uplo_t cham_uplo;   // format of global matrix, 0 if Upper/Lower, 1 if Lower, 2 if Upper
    CHAM_desc_t *cham_desc;  // chameleon description of the global matrix
    int     cham_it;     // tile index on rows in the global matrix
    int     cham_jt;     // tile index on cols in the global matrix
} cl_copypaneltotile_arg_t;

/**
 * @brief Codelet Copy H5 panel (block of rows) in one tile CPU function
 */
template <typename FReal>
static void cl_copypaneltotile_cpu_func( void *buffer[], void *cl_arg )
{
    /* CPU copy of the vector pointer */
    FReal *panel = (FReal *)STARPU_VECTOR_GET_PTR(buffer[0]);
    CHAM_tile_t *cham_tile = cti_interface_get((starpu_cham_tile_interface_t *)buffer[1]);
    FReal *tile = (FReal *)cham_tile->mat;

    cl_copypaneltotile_arg_t args;
    starpu_codelet_unpack_args( cl_arg, &args );

    // for specific operation on the diagonal (symmetry)
    cham_uplo_t uplo = args.cham_uplo;
    CHAM_desc_t *cham_desc = args.cham_desc;

    // parameters of the global matrix
    int m = cham_desc->m;
    int n = cham_desc->n;
    int ts = cham_desc->mb;
    int mt = cham_desc->mt ;
    int nt = cham_desc->nt ;
    // tile position in the matrix
    int it = args.cham_it;
    int jt = args.cham_jt;

    // uplot is meaningfull only for tiles on the diagonal, else we need to
    // copy the entire tile.
    cham_uplo_t uplot = ( it == jt ) ? uplo : ChamUpperLower;

    // starting row index of the tile in the global matrix
    int tile_row_min = it * ts;
    // starting column index of the tile in the global matrix
    int tile_col_min = jt * ts;
    // ending row index of the tile in the global matrix
    int tile_row_max = (it == (mt-1)) ? m : (it+1)*ts;
    // ending column index of the tile in the global matrix
    int tile_col_max = (jt == (nt-1)) ? n : (jt+1)*ts;
    // leading dimension of the tile
    int tile_ldam = cham_tile->ld;

    // global indexes of corners of this block (top left and bottom right)
    int blk_row_min = args.panel_rows_beg;
    int blk_row_max = args.panel_rows_end+1;
    int blk_col_min = args.panel_cols_beg;
    int blk_col_max = args.panel_cols_end+1;
    int blk_ncols = blk_col_max - blk_col_min;
    // global indexes of the intersection between the block and the tile
    int int_row_min=-1, int_row_max=-1, int_col_min=-1, int_col_max=-1;
    if ( tile_row_max > blk_row_min && tile_row_min < blk_row_max){
        int_row_min = std::max(tile_row_min, blk_row_min);
        int_row_max = std::min(tile_row_max, blk_row_max);
    }
    if ( tile_col_max > blk_col_min && tile_col_min < blk_col_max){
        int_col_min = std::max(tile_col_min, blk_col_min);
        int_col_max = std::min(tile_col_max, blk_col_max);
    }

    // calculate size of the intersection to detect croped tile
    int int_row_size = int_row_max - int_row_min;
    int int_col_size = int_col_max - int_col_min;
    bool tile_croped = false;
    if ( int_row_size < cham_tile->m || int_col_size < cham_tile->n ) {
        tile_croped = true;
    }

    // is the intersection non empty
    int int_min = std::min( {int_row_min, int_row_max, int_col_min, int_col_max} );
    if ( uplot == ChamLower && int_col_min >= int_row_max ){
        int_min=-1;
    }
    if ( uplot == ChamUpper && int_row_min >= int_col_max ){
        int_min=-1;
    }
    // if int_min==-1 then it is empty, else read and copy the intersection
    if ( int_min >= 0 ){
        // compute the starting index of the intersection relative to
        // the block, consider the transpose (optimization)
        int intblk_row_min = int_row_min - blk_row_min;
        int intblk_col_min = int_col_min - blk_col_min;
        // size of the intersection
        int intblk_nrows = int_row_max - int_row_min;
        int intblk_ncols = int_col_max - int_col_min;
        // compute the indexes where to start in the chameleon tile in
        // order to copy the data in the proper part which represents the intersection
        int zero = 0;
        int inttile_row_min = std::max(zero, blk_row_min - tile_row_min);
        int inttile_col_min = std::max(zero, blk_col_min - tile_col_min);
        // copy in the chameleon tile
        for (int j = inttile_col_min; j < inttile_col_min+intblk_ncols; ++j)
        {
            int imin = ( uplot == ChamLower ) ? std::max(j, inttile_row_min) : inttile_row_min;
            int imax = ( uplot == ChamUpper ) ? std::min(j+1, inttile_row_min+intblk_nrows) : inttile_row_min+intblk_nrows;
            for (int i = imin; i < imax; ++i)
            {
                int index_cham = j*tile_ldam + i;
                int index_blk  = (i-inttile_row_min)*blk_ncols + (j-inttile_col_min)
                                    + intblk_row_min*blk_ncols + intblk_col_min;
                tile[ index_cham ] = panel[ index_blk ];
                //printf("%d: %lld %lld %f\n", mpirank, index_cham, index_blk, panel[ index_blk ]);
            }
        }
    }

    // consider the symmetric part if blocks give the global matrix in UPPER/LOWER mode
    // global indexes of the intersection between the block and the tile
    int_row_min=-1;
    int_row_max=-1;
    int_col_min=-1;
    int_col_max=-1;
    if ( tile_col_max > blk_row_min && tile_col_min < blk_row_max){
        int_row_min = std::max(tile_col_min, blk_row_min);
        int_row_max = std::min(tile_col_max, blk_row_max);
    }
    if ( tile_row_max > blk_col_min && tile_row_min < blk_col_max){
        int_col_min = std::max(tile_row_min, blk_col_min);
        int_col_max = std::min(tile_row_max, blk_col_max);
    }
    // is the intersection non empty, check it only if intersection is
    // empty in the previous step
    if ( int_min == -1 || (uplo == ChamUpperLower && it == jt && tile_croped) ){
        int_min = std::min( {int_row_min, int_row_max, int_col_min, int_col_max} );
    } else {
        int_min = -1;
    }
    // if int_min==-1 then it is empty, else read and copy the intersection
    if ( int_min>=0 ){
        // compute the starting index of the intersection relative to the HDF5 block
        int intblk_row_min = int_row_min - blk_row_min;
        int intblk_col_min = int_col_min - blk_col_min;
        // size of the intersection
        int intblk_nrows = int_row_max - int_row_min;
        int intblk_ncols = int_col_max - int_col_min;
        // compute the indexes where to start in the chameleon tile in
        // order to copy the data in the proper part which represents the intersection
        int zero = 0;
        int inttile_row_min = std::max(zero, blk_col_min - tile_row_min);
        int inttile_col_min = std::max(zero, blk_row_min - tile_col_min);
        // copy in the chameleon tile and turn in column major
        for (int j = inttile_col_min; j < inttile_col_min+intblk_nrows; ++j)
        {
            int imin = ( uplot == ChamLower ) ? std::max(j, inttile_row_min) : inttile_row_min;
            int imax = ( uplot == ChamUpper ) ? std::min(j+1, inttile_row_min+intblk_ncols) : inttile_row_min+intblk_ncols;
            for (int i = imin; i < imax; ++i)
            {
                int index_cham = j*tile_ldam + i;
                int index_blk = (j-inttile_col_min)*blk_ncols + (i-inttile_row_min)
                                    + intblk_row_min*blk_ncols + intblk_col_min;
                tile[ index_cham ] = panel[ index_blk ];
            }
        }
    }
}

/**
 * @brief Insert task funtion
 */
void insert_copypaneltotile( starpu_codelet *cl,
                    hsize_t panel_rows_beg,
                    hsize_t panel_rows_end,
                    hsize_t panel_cols_beg,
                    hsize_t panel_cols_end,
                    starpu_data_handle_t panel,
                    cham_uplo_t cham_uplo,
                    CHAM_desc_t *cham_desc,
                    int     cham_it,
                    int     cham_jt,
                    starpu_data_handle_t tile)
{
    cl_copypaneltotile_arg_t args;
    args.panel_rows_beg = panel_rows_beg;
    args.panel_rows_end = panel_rows_end;
    args.panel_cols_beg = panel_cols_beg;
    args.panel_cols_end = panel_cols_end;
    args.cham_uplo = cham_uplo;
    args.cham_desc = cham_desc;
    args.cham_it = cham_it;
    args.cham_jt = cham_jt;

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_copypaneltotile_arg_t),
        STARPU_R, panel,
        STARPU_W, tile,
        0);
}

/**
 * @brief Codelet Init one tile CPU function (to avoid a bug where
 * copypaneltotile start only after all readh5 tasks on worker 0)
 */
template <typename FReal>
static void cl_inittile_cpu_func( void *buffer[], void *cl_arg ){
    (void)buffer;
    (void)cl_arg;
}

/**
 * @brief Insert task funtion
 */
void insert_inittile( starpu_codelet *cl,
                      starpu_data_handle_t tile)
{
    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_W, tile,
        0);
}

/**
 * @brief Structure to gather static parameters of the kernel
 */
typedef struct cl_copytiletopanel_arg_s {
    cham_uplo_t cham_uplo;   // format of global matrix, 0 if Upper/Lower, 1 if Lower, 2 if Upper
    CHAM_desc_t *cham_desc;  // chameleon description of the global matrix
    int     cham_it;     // tile index on rows in the global matrix
    int     cham_jt;     // tile index on cols in the global matrix
    hsize_t panel_rows_beg; // beginning index rows in the global matrix of the block
    hsize_t panel_rows_end; // end index rows in the global matrix of the block
    hsize_t panel_cols_beg; // beginning index rows in the global matrix of the block
    hsize_t panel_cols_end; // end index rows in the global matrix of the block
} cl_copytiletopanel_arg_t;

/**
 * @brief Codelet Copy H5 panel (block of rows) in one tile CPU function
 */
template <typename FReal>
static void cl_copytiletopanel_cpu_func( void *buffer[], void *cl_arg )
{
    CHAM_tile_t *cham_tile = cti_interface_get((starpu_cham_tile_interface_t *)buffer[0]);
    FReal *tile = (FReal *)cham_tile->mat;
    FReal *panel = (FReal *)STARPU_VECTOR_GET_PTR(buffer[1]);

    cl_copytiletopanel_arg_t args;
    starpu_codelet_unpack_args( cl_arg, &args );

    // for specific operation on the diagonal (symmetry)
    cham_uplo_t uplo = args.cham_uplo;
    CHAM_desc_t *cham_desc = args.cham_desc;

    // parameters of the global matrix
    int m = cham_desc->m;
    int n = cham_desc->n;
    int ts = cham_desc->mb;
    int mt = cham_desc->mt ;
    int nt = cham_desc->nt ;
    // tile position in the matrix
    int it = args.cham_it;
    int jt = args.cham_jt;

    // uplot is meaningfull only for tiles on the diagonal, else we need to
    // copy the entire tile.
    cham_uplo_t uplot = ( it == jt ) ? uplo : ChamUpperLower;

    // starting row index of the tile in the global matrix
    int tile_row_min = it * ts;
    // starting column index of the tile in the global matrix
    int tile_col_min = jt * ts;
    // ending row index of the tile in the global matrix
    int tile_row_max = (it == (mt-1)) ? m : (it+1)*ts;
    // ending column index of the tile in the global matrix
    int tile_col_max = (jt == (nt-1)) ? n : (jt+1)*ts;
    // leading dimension of the tile
    int tile_ldam = cham_tile->ld;

    // global indexes of corners of this block (top left and bottom right)
    int blk_row_min = args.panel_rows_beg;
    int blk_row_max = args.panel_rows_end+1;
    int blk_col_min = args.panel_cols_beg;
    int blk_col_max = args.panel_cols_end+1;
    int blk_ncols = blk_col_max - blk_col_min;
    // global indexes of the intersection between the block and the tile
    int int_row_min=-1, int_row_max=-1, int_col_min=-1, int_col_max=-1;
    if ( tile_row_max > blk_row_min && tile_row_min < blk_row_max){
        int_row_min = std::max(tile_row_min, blk_row_min);
        int_row_max = std::min(tile_row_max, blk_row_max);
    }
    if ( tile_col_max > blk_col_min && tile_col_min < blk_col_max){
        int_col_min = std::max(tile_col_min, blk_col_min);
        int_col_max = std::min(tile_col_max, blk_col_max);
    }

    // is the intersection non empty
    int int_min = std::min( {int_row_min, int_row_max, int_col_min, int_col_max} );
    if ( uplot == ChamLower && int_col_min >= int_row_max ){
        int_min=-1;
    }
    if ( uplot == ChamUpper && int_row_min >= int_col_max ){
        int_min=-1;
    }
    // if int_min==-1 then it is empty, else read and copy the intersection
    if ( int_min >= 0 ){
        // compute the starting index of the intersection relative to
        // the block, consider the transpose (optimization)
        int intblk_row_min = int_row_min - blk_row_min;
        int intblk_col_min = int_col_min - blk_col_min;
        // size of the intersection
        int intblk_nrows = int_row_max - int_row_min;
        int intblk_ncols = int_col_max - int_col_min;
        // compute the indexes where to start in the chameleon tile in
        // order to copy the data in the proper part which represents the intersection
        int zero = 0;
        int inttile_row_min = std::max(zero, blk_row_min - tile_row_min);
        int inttile_col_min = std::max(zero, blk_col_min - tile_col_min);
        // copy from the chameleon tile into the panel
        for (int j = inttile_col_min; j < inttile_col_min+intblk_ncols; ++j)
        {
            int imin = ( uplot == ChamLower ) ? std::max(j, inttile_row_min) : inttile_row_min;
            int imax = ( uplot == ChamUpper ) ? std::min(j+1, inttile_row_min+intblk_nrows) : inttile_row_min+intblk_nrows;
            for (int i = imin; i < imax; ++i)
            {
                int index_cham = j*tile_ldam + i;
                int index_blk  = (i-inttile_row_min)*blk_ncols + (j-inttile_col_min)
                                    + intblk_row_min*blk_ncols + intblk_col_min;
                panel[ index_blk ] = tile[ index_cham ];
                //printf("%d: %lld %lld %f\n", mpirank, index_cham, index_blk, panel[ index_blk ]);
            }
        }
    }
}

/**
 * @brief Insert task funtion
 */
void insert_copytiletopanel( starpu_codelet *cl,
                             cham_uplo_t cham_uplo,
                             CHAM_desc_t *cham_desc,
                             int     cham_it,
                             int     cham_jt,
                             starpu_data_handle_t tile,
                             hsize_t panel_rows_beg,
                             hsize_t panel_rows_end,
                             hsize_t panel_cols_beg,
                             hsize_t panel_cols_end,
                             starpu_data_handle_t panel)
{
    cl_copytiletopanel_arg_t args;
    args.cham_uplo = cham_uplo;
    args.cham_desc = cham_desc;
    args.cham_it = cham_it;
    args.cham_jt = cham_jt;
    args.panel_rows_beg = panel_rows_beg;
    args.panel_rows_end = panel_rows_end;
    args.panel_cols_beg = panel_cols_beg;
    args.panel_cols_end = panel_cols_end;

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_copytiletopanel_arg_t),
        STARPU_R, tile,
        STARPU_W, panel,
        0);
}
#endif

/**
 * @brief Structure to gather static parameters of the kernel
 */
template <typename FSize>
struct cl_copycols_arg_t {
    FSize *cols;
    cham_uplo_t cham_uplo; // format of global matrix, 0 if Upper/Lower, 1 if Lower, 2 if Upper
    CHAM_desc_t *cham_desc_r; // chameleon description of the matrix to read
    int itr; // tile index on rows in the matrix to read
    int jtr; // tile index on cols in the matrix to read
    CHAM_desc_t *cham_desc_w;  // chameleon description of the matrix to write
    int jtw; // 1st tile index on cols in the matrix to write
    int ofw; // tile offset in the matrix to write
};

/**
 * @brief Codelet Copy subset of columns in tiles CPU function
 */
template <typename FSize, typename FReal>
static void cl_copycols1_cpu_func( void *buffer[], void *cl_arg )
{
    CHAM_tile_t *cham_tile_r = cti_interface_get((starpu_cham_tile_interface_t *)buffer[0]);
    FReal *tile_r = (FReal *)cham_tile_r->mat;
    CHAM_tile_t *cham_tile_w1 = cti_interface_get((starpu_cham_tile_interface_t *)buffer[1]);
    FReal *tile_w = (FReal *)cham_tile_w1->mat;

    cl_copycols_arg_t<FSize> args;
    starpu_codelet_unpack_args( cl_arg, &args );

    // columns indexes to copy
    FSize *cols = args.cols;

    CHAM_desc_t *cham_desc_r = args.cham_desc_r;
    CHAM_desc_t *cham_desc_w = args.cham_desc_w;

    // parameters of the global matrix
    int m = cham_desc_r->m;
    int n = cham_desc_r->n;
    int ts = cham_desc_r->mb;
    int mt = cham_desc_r->mt ;
    int nt = cham_desc_r->nt ;
    // tile position in the matrix
    int itr = args.itr;
    int jtr = args.jtr;

    // starting column index of the tile to read in the global matrix
    int tile_r_col_min = jtr * ts;
    // ending column index of the tile to read in the global matrix
    int tile_r_col_max = (jtr == (nt-1)) ? n : (jtr+1)*ts;
    // number of rows in the tile
    int nrows = (itr == (mt-1)) ? m - itr*ts : ts;
    // leading dimension of the tile
    int ldam = cham_tile_r->ld;

    int n_w = cham_desc_w->n;
    // tile position in the matrix
    int jtw1 = args.jtw;

    // column index in matrix to write
    int tile_w_col = jtw1 * ts + args.ofw;

    // column index in tile to write
    int jw = args.ofw;

    /* main loop to select and copy columns */
    while ( (tile_w_col < n_w ) && (cols[tile_w_col] < (FSize)tile_r_col_max) ){

        // column index relative to the tile to read
        int jr = cols[tile_w_col] - tile_r_col_min;

        for (int i=0; i < nrows; i++){
            tile_w[ jw * ldam + i ] = tile_r[ jr * ldam + i ];
        }

        tile_w_col++;
        jw++;
    }
}

template <typename FSize>
void insert_copycols( starpu_codelet *cl,
                      FSize *cols,
                      cham_uplo_t cham_uplo,
                      CHAM_desc_t *cham_desc_r,
                      int itr,
                      int jtr,
                      starpu_data_handle_t tile_r,
                      CHAM_desc_t *cham_desc_w,
                      int jtw,
                      int ofw,
                      starpu_data_handle_t tile_w)
{
    cl_copycols_arg_t<FSize> args;
    args.cols = (FSize *)cols;
    args.cham_uplo = cham_uplo;
    args.cham_desc_r = cham_desc_r;
    args.itr = itr;
    args.jtr = jtr;
    args.cham_desc_w = cham_desc_w;
    args.jtw = jtw;
    args.ofw = ofw;

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_copycols_arg_t<FSize>),
        STARPU_R, tile_r,
        STARPU_RW, tile_w,
        0);
}

/**
 * @brief Codelet Copy subset of columns in tiles CPU function
 */
template <typename FSize, typename FReal>
static void cl_copycols2_cpu_func( void *buffer[], void *cl_arg )
{
    CHAM_tile_t *cham_tile_r = cti_interface_get((starpu_cham_tile_interface_t *)buffer[0]);
    FReal *tile_r = (FReal *)cham_tile_r->mat;
    CHAM_tile_t *cham_tile_w1 = cti_interface_get((starpu_cham_tile_interface_t *)buffer[1]);
    FReal *tile_w1 = (FReal *)cham_tile_w1->mat;
    CHAM_tile_t *cham_tile_w2 = cti_interface_get((starpu_cham_tile_interface_t *)buffer[2]);
    FReal *tile_w2 = (FReal *)cham_tile_w2->mat;
    FReal *tile_w = tile_w1;

    cl_copycols_arg_t<FSize> args;
    starpu_codelet_unpack_args( cl_arg, &args );

    // columns indexes to copy
    FSize *cols = args.cols;

    CHAM_desc_t *cham_desc_r = args.cham_desc_r;
    CHAM_desc_t *cham_desc_w = args.cham_desc_w;

    // parameters of the global matrix
    int m = cham_desc_r->m;
    int n = cham_desc_r->n;
    int ts = cham_desc_r->mb;
    int mt = cham_desc_r->mt ;
    int nt = cham_desc_r->nt ;
    // tile position in the matrix
    int itr = args.itr;
    int jtr = args.jtr;

    // starting column index of the tile to read in the global matrix
    int tile_r_col_min = jtr * ts;
    // ending column index of the tile to read in the global matrix
    int tile_r_col_max = (jtr == (nt-1)) ? n : (jtr+1)*ts;
    // number of rows in the tile
    int nrows = (itr == (mt-1)) ? m - itr*ts : ts;
    // leading dimension of the tile
    int ldam = cham_tile_r->ld;

    // tile position in the matrix
    int jtw1 = args.jtw;

    // column index in matrix to write
    int tile_w_col = jtw1 * ts + args.ofw;

    int n_w = cham_desc_w->n;
    // column index in tile to write
    int jw = args.ofw;

    /* main loop to select and copy columns */
    while ( (tile_w_col < n_w ) && (cols[tile_w_col] < (FSize)tile_r_col_max) ){

        // column index relative to the tile to read
        int jr = cols[tile_w_col] - tile_r_col_min;

        for (int i=0; i < nrows; i++){
            tile_w[ jw * ldam + i ] = tile_r[ jr * ldam + i ];
        }

        // if first to write tile is full continue with the next tile
        tile_w_col++;
        if (jw < ts-1){
            jw++;
        } else {
            jw=0;
            tile_w = tile_w2;
        }

    }
}

/**
 * @brief Insert task funtion
 */
template <typename FSize>
void insert_copycols( starpu_codelet *cl,
                      FSize *cols,
                      cham_uplo_t cham_uplo,
                      CHAM_desc_t *cham_desc_r,
                      int itr,
                      int jtr,
                      starpu_data_handle_t tile_r,
                      CHAM_desc_t *cham_desc_w,
                      int jtw1,
                      int ofw1,
                      starpu_data_handle_t tile_w1,
                      starpu_data_handle_t tile_w2)
{
    cl_copycols_arg_t<FSize> args;
    args.cols = (FSize *)cols;
    args.cham_uplo = cham_uplo;
    args.cham_desc_r = cham_desc_r;
    args.itr = itr;
    args.jtr = jtr;
    args.cham_desc_w = cham_desc_w;
    args.jtw = jtw1;
    args.ofw = ofw1;

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_copycols_arg_t<FSize>),
        STARPU_R, tile_r,
        STARPU_RW, tile_w1,
        STARPU_RW, tile_w2,
        0);
}

/**
 * @brief Structure to gather static parameters of the kernel
 */
typedef struct cl_transpose_arg_s {
    int it; // tile index on rows of the entry matrix
    int jt; // tile index on cols of the entry matrix
    CHAM_desc_t *chamin; // chameleon description of the entry matrix
    CHAM_desc_t *chamout; // chameleon description of the output matrix
} cl_transpose_arg_t;

/**
 * @brief Codelet transpose tiles of a matrix
 */
template <typename FReal>
static void cl_transpose_cpu_func( void *buffer[], void *cl_arg )
{
    CHAM_tile_t *matin = cti_interface_get((starpu_cham_tile_interface_t *)buffer[0]);
    CHAM_tile_t *matout = cti_interface_get((starpu_cham_tile_interface_t *)buffer[1]);
    FReal *tilein = (FReal *)matin->mat;
    FReal *tileout = (FReal *)matout->mat;

    cl_transpose_arg_t args;
    starpu_codelet_unpack_args( cl_arg, &args );

    // parameters of the input tile
    int it = args.it;
    int jt = args.jt;
    CHAM_desc_t *descin = args.chamin;
    int m = it == descin->mt-1 ? descin->m - it * descin->mb : descin->mb;
    int n = jt == descin->nt-1 ? descin->n - jt * descin->nb : descin->nb;

    // transpose tile
    for (int i = 0; i < m ; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            tileout[ j + i*n ] = tilein[ i + j*m ];
            //printf("%d: %lld %lld %f\n", mpirank, i + j*m, j + i*n, tileout[ j + i*n ]);
        }
    }
}

/**
 * @brief Insert task funtion
 */
void insert_transpose( starpu_codelet *cl,
                       int it,
                       int jt,
                       CHAM_desc_t *chamin,
                       CHAM_desc_t *chamout,
                       starpu_data_handle_t tilein,
                       starpu_data_handle_t tileout)
{
    cl_transpose_arg_t args;
    args.it = it;
    args.jt = jt;
    args.chamin = chamin;
    args.chamout = chamout;

    starpu_insert_task(
        starpu_mpi_codelet(cl),
        STARPU_VALUE, &args, sizeof(cl_transpose_arg_t),
        STARPU_R, tilein,
        STARPU_W, tileout,
        0);
}

#endif //STARPU_HPP
