/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

// ===================================================================================
// Copyright ScalFmm 2016 INRIA, Olivier Coulaud, Bérenger Bramas,
// Matthias Messner olivier.coulaud@inria.fr, berenger.bramas@inria.fr
// This software is a computer program whose purpose is to compute the
// FMM.
//
// This software is governed by the CeCILL-C and LGPL licenses and
// abiding by the rules of distribution of free software.
// An extension to the license is given to allow static linking of scalfmm
// inside a proprietary application (no matter its license).
// See the main license file for more details.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public and CeCILL-C Licenses for more details.
// "http://www.cecill.info".
// "http://www.gnu.org/licenses".
// ===================================================================================
#ifndef MATH_HPP
#define MATH_HPP

#include <cmath>
#include <limits>

#include "fmr/Utils/Global.hpp"
#warning("fmr/Utils/Math.hpp DO NOT USE")

namespace fmr { namespace math {
/**
 * @author Berenger Bramas (berenger.bramas@inria.fr)
 * @class
 * Please read the license
 *
 * Propose basic math functions or indirections to std math.
 */
[[depracated]] struct Math {
  template <class ValueType> constexpr static ValueType Pi() {
    return ValueType(M_PI);
  }
  template <class ValueType> constexpr static ValueType TwoPi() {
    return ValueType(2.0 * M_PI);
  }
  template <class ValueType> constexpr static ValueType PiDiv2() {
    return ValueType(M_PI_2);
  }
  template <class ValueType> constexpr static ValueType Epsilon() {
    return ValueType(0.00000000000000000001);
  }

  /** To get absolute value */
  template <class NumType> static NumType Abs(const NumType inV) {
    return (inV < 0 ? -inV : inV);
  }

  /** To get max between 2 values */
  template <class NumType>
  static NumType Max(const NumType inV1, const NumType inV2) {
    return (inV1 > inV2 ? inV1 : inV2);
  }

  /** To get min between 2 values */
  template <class NumType>
  static NumType Min(const NumType inV1, const NumType inV2) {
    return (inV1 < inV2 ? inV1 : inV2);
  }

  /** To know if 2 values seems to be equal */
  template <class NumType>
  static bool LookEqual(const NumType inV1, const NumType inV2) {
    return (Abs(inV1 - inV2) < std::numeric_limits<NumType>::epsilon());
    // const ValueType relTol = ValueType(0.00001);
    // const ValueType absTol = ValueType(0.00001);
    // return (Abs(inV1 - inV2) <= Max(absTol, relTol * Max(Abs(inV1),
    // Abs(inV2))));
  }

  /** To know if 2 values seems to be equal */
  template <class NumType>
  static NumType RelatifDiff(const NumType inV1, const NumType inV2) {
    return Abs(inV1 - inV2) * Abs(inV1 - inV2) /
           Max(Abs(inV1 * inV1), Abs(inV2 * inV2));
  }

  /** To get floor of a ValueType */
  static float dfloor(const float inValue) { return floorf(inValue); }
  static double dfloor(const double inValue) { return floor(inValue); }

  /** To get ceil of a ValueType */
  static float Ceil(const float inValue) { return ceilf(inValue); }
  static double Ceil(const double inValue) { return ceil(inValue); }

  /** To get pow */
  static double pow(double x, double y) { return ::pow(x, y); }
  static float pow(float x, float y) { return ::powf(x, y); }
  template <class NumType>
  static NumType pow(const NumType inValue, int power) {
    NumType result = 1;
    while (power-- > 0)
      result *= inValue;
    return result;
  }

  /** To get pow of 2 */
  static int pow2(const int power) { return (1 << power); }

  /** To get factorial */
  template <class NumType> static double factorial(int inValue) {
    if (inValue == 0)
      return NumType(1.);
    else {
      NumType result = NumType(inValue);
      while (--inValue > 1)
        result *= NumType(inValue);
      return result;
    }
  }

  /** To get exponential */
  static double Exp(double x) { return ::exp(x); }
  static float Exp(float x) { return ::expf(x); }

  /** To know if a value is between two others */
  template <class NumType>
  static bool Between(const NumType inValue, const NumType inMin,
                      const NumType inMax) {
    return (inMin <= inValue && inValue < inMax);
  }
  /** To compute fmadd operations **/
  template <class NumType>
  static NumType FMAdd(const NumType a, const NumType b, const NumType c) {
    return a * b + c;
  }

  /** To get sqrt of a ValueType */
  static float Sqrt(const float inValue) { return sqrtf(inValue); }
  static double Sqrt(const double inValue) { return sqrt(inValue); }
  static float Rsqrt(const float inValue) {
    return float(1.0) / sqrtf(inValue);
  }
  static double Rsqrt(const double inValue) { return 1.0 / sqrt(inValue); }

  /** To get Log of a ValueType */
  static float Log(const float inValue) { return logf(inValue); }
  static double Log(const double inValue) { return log(inValue); }

  /** To get Log2 of a ValueType */
  static float Log2(const float inValue) { return log2f(inValue); }
  static double Log2(const double inValue) { return log2(inValue); }

  /** To get atan2 of a 2 ValueType,  The return value is given in radians and
    is in the range -pi to pi, inclusive.  */
  static float Atan2(const float inValue1, const float inValue2) {
    return atan2f(inValue1, inValue2);
  }
  static double Atan2(const double inValue1, const double inValue2) {
    return atan2(inValue1, inValue2);
  }

  /** To get sin of a ValueType */
  static float Sin(const float inValue) { return sinf(inValue); }
  static double Sin(const double inValue) { return sin(inValue); }

  /** To get asinf of a float. The result is in the range [0, pi]*/
  static float ASin(const float inValue) { return asinf(inValue); }
  /** To get asinf of a double. The result is in the range [0, pi]*/
  static double ASin(const double inValue) { return asin(inValue); }

  /** To get cos of a ValueType */
  static float Cos(const float inValue) { return cosf(inValue); }
  static double Cos(const double inValue) { return cos(inValue); }

  /** To get arccos of a float. The result is in the range [0, pi]*/
  static float ACos(const float inValue) { return acosf(inValue); }
  /** To get arccos of a double. The result is in the range [0, pi]*/
  static double ACos(const double inValue) { return acos(inValue); }

  /** To get atan2 of a 2 ValueType */
  static float Fmod(const float inValue1, const float inValue2) {
    return fmodf(inValue1, inValue2);
  }
  /** return the floating-point remainder of inValue1  / inValue2 */
  static double Fmod(const double inValue1, const double inValue2) {
    return fmod(inValue1, inValue2);
  }

  /** To know if a variable is nan, based on the C++0x */
  template <class TestClass> static bool IsNan(const TestClass &value) {
    // volatile const TestClass* const pvalue = &value;
    // return (*pvalue) != value;
    return std::isnan(value);
  }

  /** To know if a variable is not inf, based on the C++0x */
  template <class TestClass> static bool IsFinite(const TestClass &value) {
    // return !(value <= std::numeric_limits<T>::min()) &&
    // !(std::numeric_limits<T>::max() <= value);
    return std::isfinite(value);
  }

  template <class NumType> static NumType Zero();

  template <class NumType> static NumType One();

  template <class DestType, class SrcType>
  static DestType ConvertTo(const SrcType val);

#ifdef DEV_OLD
  /** A class to compute accuracy */
  template <class ValueType, class IndexType = Size> class Accurater {
    IndexType nbElements;
    ValueType l2Dot;
    ValueType l2Diff;
    ValueType max;
    ValueType maxDiff;

  public:
    Accurater()
        : nbElements(0), l2Dot(0.0), l2Diff(0.0), max(0.0), maxDiff(0.0) {}
    /** with inital values */
    Accurater(const ValueType inGood[], const ValueType inBad[],
              const IndexType nbValues)
        : nbElements(0), l2Dot(0.0), l2Diff(0.0), max(0.0), maxDiff(0.0) {
      add(inGood, inBad, nbValues);
    }

    /** Add value to the current list */
    void add(const ValueType inGood, const ValueType inBad) {
      l2Diff += (inBad - inGood) * (inBad - inGood);
      l2Dot += inGood * inGood;
      max = Max(max, Abs(inGood));
      maxDiff = Max(maxDiff, Abs(inGood - inBad));
      nbElements += 1;
    }
    /** Add array of values */
    void add(const ValueType inGood[], const ValueType inBad[],
             const IndexType nbValues) {
      for (IndexType idx = 0; idx < nbValues; ++idx) {
        add(inGood[idx], inBad[idx]);
      }
      nbElements += nbValues;
    }

    /** Add an accurater*/
    void add(const Accurater &inAcc) {
      l2Diff += inAcc.getl2Diff();
      l2Dot += inAcc.getl2Dot();
      max = Max(max, inAcc.getmax());
      maxDiff = Max(maxDiff, inAcc.getInfNorm());
      nbElements += inAcc.getNbElements();
    }

    ValueType getl2Diff() const { return l2Diff; }
    ValueType getl2Dot() const { return l2Dot; }
    ValueType getmax() const { return max; }
    IndexType getNbElements() const { return nbElements; }
    void setNbElements(const IndexType &n) { nbElements = n; }

    /** Get the root mean squared error*/
    ValueType getL2Norm() const { return Sqrt(l2Diff); }
    /** Get the L2 norm */
    ValueType getRMSError() const {
      return Sqrt(l2Diff / static_cast<ValueType>(nbElements));
    }

    /** Get the inf norm */
    ValueType getInfNorm() const { return maxDiff; }
    /** Get the L2 norm */
    ValueType getRelativeL2Norm() const { return Sqrt(l2Diff / l2Dot); }
    /** Get the inf norm */
    ValueType getRelativeInfNorm() const { return maxDiff / max; }
    /** Print */
    template <class StreamClass>
    friend StreamClass &operator<<(StreamClass &output,
                                   const Accurater &inAccurater) {
      output << "[Error] Relative L2Norm = " << inAccurater.getRelativeL2Norm()
             << " \t RMS Norm = " << inAccurater.getRMSError()
             << " \t Relative Inf = " << inAccurater.getRelativeInfNorm();
      return output;
    }

    void reset() {
      l2Dot = ValueType(0);
      l2Diff = ValueType(0);
      ;
      max = ValueType(0);
      maxDiff = ValueType(0);
      nbElements = 0;
    }
  };
#endif
};

template <> inline float Math::Zero<float>() { return float(0.0); }

template <> inline double Math::Zero<double>() { return double(0.0); }

template <> inline float Math::One<float>() { return float(1.0); }

template <> inline double Math::One<double>() { return double(1.0); }

template <> inline float Math::ConvertTo<float, float>(const float val) {
  return val;
}

template <> inline double Math::ConvertTo<double, double>(const double val) {
  return val;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN PB: The following function need only be implemented to allow the use of
// functions in correlation.hpp Convert from double to float
template <> inline float Math::ConvertTo<float, double>(const double val) {
  return float(val);
}
// Convert from float to double
template <> inline double Math::ConvertTo<double, float>(const float val) {
  return double(val);
}
// END PB
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <>
inline float Math::ConvertTo<float, const float *>(const float *val) {
  return *val;
}

template <>
inline double Math::ConvertTo<double, const double *>(const double *val) {
  return *val;
}

//#ifdef SCALFMM_USE_SSE
//    template <>
//    inline __m128 Math::One<__m128>(){
//        return _mm_set_ps1(1.0);
//    }
//
//    template <>
//    inline __m128d Math::One<__m128d>(){
//        return _mm_set_pd1(1.0);
//    }
//
//    template <>
//    inline __m128 Math::Zero<__m128>(){
//        return _mm_setzero_ps();
//    }
//
//    template <>
//    inline __m128d Math::Zero<__m128d>(){
//        return _mm_setzero_pd();
//    }
//
//    template <>
//    inline __m128 Math::ConvertTo<__m128,float>(const float val){
//        return _mm_set_ps1(val);
//    }
//
//    template <>
//    inline __m128d Math::ConvertTo<__m128d,double>(const double val){
//        return _mm_set_pd1(val);
//    }
//
//    template <>
//    inline __m128 Math::ConvertTo<__m128,const float*>(const float* val){
//        return _mm_load1_ps(val);
//    }
//
//    template <>
//    inline __m128d Math::ConvertTo<__m128d,const double*>(const double* val){
//        return _mm_load1_pd(val);
//    }
//
//    template <>
//    inline float Math::ConvertTo<float,__m128>(const __m128 val){
//        __attribute__((aligned(16))) float buffer[4];
//        _mm_store_ps(buffer, val);
//        return buffer[0] + buffer[1] + buffer[2] + buffer[3];
//    }
//
//    template <>
//    inline double Math::ConvertTo<double,__m128d>(const __m128d val){
//        __attribute__((aligned(16))) double buffer[2];
//        _mm_store_pd(buffer, val);
//        return buffer[0] + buffer[1];
//    }
//#endif
//#ifdef SCALFMM_USE_AVX
//    template <>
//    inline __m256 Math::One<__m256>(){
//        return _mm256_set1_ps(1.0);
//    }
//
//    template <>
//    inline __m256d Math::One<__m256d>(){
//        return _mm256_set1_pd(1.0);
//    }
//
//    template <>
//    inline __m256 Math::Zero<__m256>(){
//        return _mm256_setzero_ps();
//    }
//
//    template <>
//    inline __m256d Math::Zero<__m256d>(){
//        return _mm256_setzero_pd();
//    }
//
//    template <>
//    inline __m256 Math::ConvertTo<__m256,float>(const float val){
//        return _mm256_set1_ps(val);
//    }
//
//    template <>
//    inline __m256d Math::ConvertTo<__m256d,double>(const double val){
//        return _mm256_set1_pd(val);
//    }
//
//    template <>
//    inline __m256 Math::ConvertTo<__m256,const float*>(const float* val){
//        return _mm256_broadcast_ss(val);
//    }
//
//    template <>
//    inline __m256d Math::ConvertTo<__m256d,const double*>(const double* val){
//        return _mm256_broadcast_sd(val);
//    }
//
//    template <>
//    inline float Math::ConvertTo<float,__m256>(const __m256 val){
//        __attribute__((aligned(32))) float buffer[8];
//        _mm256_store_ps(buffer, val);
//        return buffer[0] + buffer[1] + buffer[2] + buffer[3] + buffer[4] +
//        buffer[5] + buffer[6] + buffer[7];
//    }
//
//    template <>
//    inline double Math::ConvertTo<double,__m256d>(const __m256d val){
//        __attribute__((aligned(32))) double buffer[4];
//        _mm256_store_pd(buffer, val);
//        return buffer[0] + buffer[1] + buffer[2] + buffer[3];
//    }
//#endif
//#ifdef SCALFMM_USE_AVX2
//#ifdef __MIC__
//    template <>
//    inline __m512 Math::One<__m512>(){
//        return _mm512_set1_ps(1.0);
//    }
//
//    template <>
//    inline __m512d Math::One<__m512d>(){
//        return _mm512_set1_pd(1.0);
//    }
//
//    template <>
//    inline __m512 Math::Zero<__m512>(){
//        return _mm512_setzero_ps();
//    }
//
//    template <>
//    inline __m512d Math::Zero<__m512d>(){
//        return _mm512_setzero_pd();
//    }
//
//    template <>
//    inline __m512 Math::ConvertTo<__m512,__attribute__((aligned(64)))
//    float>(const float val){
//        return _mm512_set1_ps(val);
//    }
//
//    template <>
//    inline __m512d Math::ConvertTo<__m512d,__attribute__((aligned(64)))
//    double>(const double val){
//        return _mm512_set1_pd(val);
//    }
//
//    template <>
//    inline __m512 Math::ConvertTo<__m512,const __attribute__((aligned(64)))
//    float*>(const float* val){
//        return _mm512_set1_ps(val[0]);
//    }
//
//    template <>
//    inline __m512d Math::ConvertTo<__m512d,const __attribute__((aligned(64)))
//    double*>(const double* val){
//        return _mm512_set1_pd(val[0]);
//    }
//
//    template <>
//    inline float Math::ConvertTo<float,__m512>(const __m512 val){
//        __attribute__((aligned(64))) float buffer[16];
//        _mm512_store_ps(buffer, val);
//        return buffer[0] + buffer[1] + buffer[2] + buffer[3] + buffer[4] +
//        buffer[5] + buffer[6] + buffer[7] + buffer[8] + buffer[9] + buffer[10]
//        + buffer[11] + buffer[12] + buffer[13] + buffer[14] + buffer[15];
//    }
//
//    template <>
//    inline double Math::ConvertTo<double,__m512d>(const __m512d val){
//        __attribute__((aligned(64))) double buffer[8];
//        _mm512_store_pd(buffer, val);
//        return buffer[0] + buffer[1] + buffer[2] + buffer[3] + buffer[4] +
//        buffer[5] + buffer[6] + buffer[7];
//    }
//#endif
//#endif

}}

#endif // MATH_HPP
