/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXNORMS_HPP
#define MATRIXNORMS_HPP

// Utilities
#include <cmath>

// FMR includes
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/StandardLRA/SVD.hpp"

namespace fmr {
/**
 * @brief The Matrix Norms class
 */
template <class Real, class Size> class MatrixNorms {

public:
  /*
   * Computes and returns Spectral norm of $A$, using expression in terms of
   * largest singular values. \param A contains input square matrix \param size
   * contains number of col/rows of A
   */
  static Real computeSpectral(const Size size, const Real *A) {

    // Compute SVD of A
    Real *const S = new Real[size];
    SVD<Real>::computeSVD(size, size, A, S);

    return S[0];
  }

  /*
   * Computes and returns Frobenius norm of $A$, using expression in terms of
   * entrywise norm by default. Expression in terms of singular values is also
   * available (flagSV = true). \param A contains input square matrix \param
   * size contains number of col/rows of A
   */
  static Real computeFrobenius(const Size size, const Real *A,
                               const bool flagSV = false) {

    Real norm = 0.;

    if (!flagSV) {

      // Compute norm
      for (Size i = 0; i < size; ++i) {
        for (Size j = 0; j < size; ++j) {
          norm += A[i * size + j] * A[i * size + j];
        }
      }

    } else {

      // Compute SVD of A
      Real *const S = new Real[size];
      SVD<Real>::computeSVD(size, size, A, S);

      // Compute norm
      for (Size i = 0; i < size; ++i)
        norm += S[i] * S[i];
    }

    return std::sqrt(norm);
  }
};

template <class Real> class FrobeniusError {

  Real error;
  Real norm;

public:
  explicit FrobeniusError() : error(Real(0.)), norm(Real(0.)) {}

  void add(const Real A_ref, const Real A = Real(0.)) {
    error += (A - A_ref) * (A - A_ref);
  }

  void addRel(const Real A_ref, const Real A = Real(0.)) {
    error += (A - A_ref) * (A - A_ref);
    norm += A_ref * A_ref;
  }

  Real getNorm() { return std::sqrt(error); }

  Real getRelativeNorm() { return std::sqrt(error / norm); }

  Real getReferenceNorm2() { return norm; }
};

/**
 * @brief Computes the frobenius norm of the matrix
 *
 * @param[in] matrix input dense wrapper for which we compute the norm
 * @return the frobenius norm
 */
template<class FSize, class FReal>
FReal computeFrobenius(BlasDenseWrapper<FSize, FReal> &matrix) {
  FReal energy = 0.0;
  FSize nbrows = matrix.getNbRows();
  FSize nbcols = matrix.getNbCols();
  FReal *mat = matrix.getMatrix();
#pragma omp parallel for simd reduction(+ : energy)
  for (FSize k = 0; k < nbrows * nbcols; ++k) {
    energy += mat[k] * mat[k];
  }
  return std::sqrt(energy);
}

#if defined(FMR_CHAMELEON)
/**
 * @brief Computes the frobenius norm of the matrix
 * @param[in] matrix input dense wrapper for which we compute the norm
 * @return the frobenius norm
 */
template<class FSize, class FReal>
FReal computeFrobenius(ChameleonDenseWrapper<FSize, FReal> &matrix) {
  if (matrix.isSymmetric()){
    return Chameleon::lansy<FReal>( ChamFrobeniusNorm, matrix.getUpperLower(), matrix.getMatrix() );
  } else {
    return Chameleon::lange<FReal>( ChamFrobeniusNorm, matrix.getMatrix() );
  }
}
#endif

} /* namespace fmr */

#endif // MATRIXNORMS_HPP
