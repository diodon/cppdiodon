/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_ALGORITHME_TRUNCATED_SVD_HPP
#define FMR_ALGORITHME_TRUNCATED_SVD_HPP

#include "fmr/StandardLRA/SVD.hpp"
namespace fmr {
///
/// \brief Compute a truncated or full SVD with prescribed rank \f$A_r = U /Sigma VT \f$
///
/// We compute and approximation of A at rank r
/// \f$ A_r \$
/// \param[in] rank rank used to compute the SVD
/// \param[in] A a matrix with nb_rows and nb_cols
/// \param[out] sigma the singular values vector of size rank
/// \param[out] U orthogonal matrix of size  nb_rows x rank
/// \param[out] VT  the transposed orthogonal matrix of size  nb_cols x rank
///
template < class REAL_T, class MATRIX_T >
auto inline truncatedSVD(const typename MATRIX_T::Int_type rank,
                         MATRIX_T& A, std::vector< REAL_T >& sigma, MATRIX_T*& U,
                         MATRIX_T*& VT) -> void {
    REAL_T* ApproxSingularValues =
        new REAL_T[std::min(A.getNbCols(), A.getNbRows())];

    MATRIX_T* U2                          = nullptr;
    MATRIX_T* VT2                         = nullptr; // If we end up needing to truncate the SVD

    SVD< REAL_T >::computeSVD(A, ApproxSingularValues, U, VT);
    if(rank <= 0) {
        // We just want to make the full SVD
    } else if(rank > 0 && rank <= U->getNbCols()) { // We want to do a truncated
        // SVD at the specified rank
        U2 = new MATRIX_T(U->getNbRows(), rank);
        U2->init(U->getBlasMatrix());
        VT2 = new MATRIX_T(VT->getNbRows(), rank);
        VT2->init(VT->getBlasMatrix());
        U->free();
        VT->free();
        U  = U2;
        VT = VT2;
    }
    sigma.assign(ApproxSingularValues, ApproxSingularValues + rank);
    // Do something to extract U with the asked rank.
}
///
/// \brief tCompute a truncated or full SVD with prescribed accuracy \f$A_r =U /Sigma VT \f$
///
/// We compute \f$ A_r\f$ an approxiation of A such that
/// We compute the rank such that \f$||A - A_r|| < epsilon ||A||
/// \param[in] rank rank used to compute the SVD
/// \param[in] A a matrix with nb_rows and nb_cols
/// \param[out] sigma the singular values vector of size rank
/// \param[out] U orthogonal matrix of size  nb_rows x rank
/// \param[out] VT  the transposed orthogonal matrix of size  nb_cols x rank
///
/// \return the rank of the approximation
///
template < class REAL_T, class MATRIX_T >
auto inline truncatedSVD(const REAL_T epsilon,
                         MATRIX_T& A, std::vector< REAL_T >& sigma, MATRIX_T*& U,
                         MATRIX_T*& VT) -> typename MATRIX_T::Int_type {
    REAL_T* ApproxSingularValues =
        new REAL_T[std::min(A.getNbCols(), A.getNbRows())];

    typename MATRIX_T::Int_type real_rank = 0; // Because rank can be negative
    MATRIX_T* U2                          = nullptr;
    MATRIX_T* VT2                         = nullptr; // If we end up needing to truncate the SVD

    SVD< REAL_T >::computeSVD(A, ApproxSingularValues, U, VT);

    // We want a specified precision
    // FIXME Why is there an error if epsilon is too small ?
    REAL_T mu2 = A.frobenius();
    mu2 *= mu2 * epsilon * epsilon; // µ^2 = eps^2||A||^2
    int computedRank = U->getNbCols();
    REAL_T sumSigma  = 0;
    int i(0);
    do {
        sumSigma = 0;
        for(int j(0); j < computedRank - i; ++j) {
            sumSigma += ApproxSingularValues[computedRank - 1 -
                                             j]; // TODO: is it sigma or sigma^2
        }
        ++i;
    } while(sumSigma > mu2);
    real_rank = i;
    std::cout << "[fmr] sumSigma = " << sumSigma << std::endl;
    std::cout << "[fmr] i = " << i << std::endl;

    U2 = new MATRIX_T(U->getNbRows(), real_rank);
    U2->init(U->getBlasMatrix());
    VT2 = new MATRIX_T(VT->getNbRows(), real_rank);
    VT2->init(VT->getBlasMatrix());
    U->free();
    VT->free();
    U  = U2;
    VT = VT2;

    sigma.assign(ApproxSingularValues, ApproxSingularValues + real_rank);
    // Do something to extract U with the asked rank.
    return real_rank;
}
} // namespace fmr
#endif
