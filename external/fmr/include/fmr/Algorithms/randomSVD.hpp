/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMRAPI_HPP
#define FMRAPI_HPP

/**
 * @author Olivier Coulaud  (olivier.coulaud@inria.fr)
 * @date November 27th, 2018
 *
 */
#include <cstdlib>
#include <iostream>
#include <vector>

#include "fmr/RandomizedLRA/AdaptiveRandomizedRangeFinder.hpp"
#include "fmr/RandomizedLRA/RandSVD.hpp"
#include "fmr/RandomizedLRA/RandomizedRangeFinder.hpp"
#include "fmr/StandardLRA/SVD.hpp"

namespace fmr {
#ifdef NOT_TO_REMOVE

//////
///
/// \brief randomSVD perform a truncated random singular value decomposition
/// with prescribed rank
///
/// Perform  A ~ U S V^T
///
/// \param[in] prescribed_rank rank for the truncated svd
/// \param[in] oversampling oversanpling parameter to obtain better results
/// \param[in] nbPowerIterations Number of iterations of the power method
/// \param[out]  A The data
/// \param[out]  ApproxSingularValues The approximate singular values of A
///            (vecteur of size prescribed_rank)
/// \param U The orthogonal matrix
/// \param[out]  energy2 is the squared norm of the approximation matrix which
/// is given by the sum of the rank first squared singular values
///
/// \param[out] the estimator is the first singular value that we truncate i.e.
///              ApproxSingularValues[rank]
///
/// \param[in] verbose [optional] parameter 0 (quiet) integer > 0 (verbose
///                   mode)
///
template <class REAL_T, class MATRIX_T>
[[deprecated]] void randomSVD(const int &prescribed_rank, const int &oversampling,
               const int &nbPowerIterations, MATRIX_T &A,
               REAL_T *&ApproxSingularValues, MATRIX_T *&U, REAL_T &energy2,
               REAL_T &estimator, const int verbose = 0) {
  std::clog <<" COUCOU 1\n";
  if (verbose > 0) {
    std::cout << "[fmr] Prescribed rank: " << prescribed_rank << std::endl;
    std::cout << "[fmr] Oversampling:    " << oversampling << std::endl;
  }
  if ((prescribed_rank + oversampling) > A.getNbCols())
    throw std::runtime_error(
        "[fmr] Oversampled rank should be lower than full rank!");
  std::cout << "[fmr] Nb of subspace iterations: " << nbPowerIterations
            << std::endl;

  //
  // Fixed rank RRF
  using Dense_RRFClass = RandomizedRangeFinder<REAL_T, MATRIX_T>;

  // Classic Randomized Range Finder
  // RRF
  Dense_RRFClass RandRangeFinder(&A, prescribed_rank, oversampling,
                                 nbPowerIterations);
  //
  // RandSVD
  RandSVD<REAL_T, MATRIX_T, Dense_RRFClass> RSVD(A.getNbRows(), A.getNbCols(),
                                                 &RandRangeFinder);
  auto rank = RSVD.factorize(verbose);
  std::clog << " end  RSVD.factorize(verbose)\n";
  //
  // Get approx. singular values
  ApproxSingularValues = RSVD.getApproxSingularValues();

  // Get approx. singular vectors
  //  U is colomn major [n l > rank]

  U = RSVD.getApproxU();

  // Get some parameters
  energy2 = RSVD.getEnergy2();
  estimator = RSVD.getApproxBaselineSpec();
  //
  if (verbose > 0) {
    std::cout << "[fmr] energy2: " << energy2 << "  estimator " << estimator
              << std::endl;
  }
}
#endif
///
/// \brief Compute a random projection SVD for a given rank
///
/// \param[in] prescribed_rank the number of singulat values required
/// \param[in] oversampling  Oversampling using in the random projection
/// \param[in] nbPowerIterations number of iteration of the power iteration
/// \param[in] A a matrix with nb_rows and nb_cols
/// \param[out] sigma the singular values vector of size prescribed_rank
/// \param[out] U orthogonal matrix of size  nb_rows x prescribed_rank
/// \param[out] VT  the transposed orthogonal matrix of size  nb_cols x
///              prescribed_rank
/// \param[out] energy2 is the sum of the squared singular eigenvalues. This corresponds to an estimate of the squared norm of the matrix A
/// \param[out] estimator is the first truncated singular eigenvalues (if oversampling > 0)
///
template <typename REAL_T, class MATRIX_T>
void randomSVD(const int &prescribed_rank, const int &oversampling,
               const int &nbPowerIterations, MATRIX_T &A,
               std::vector<REAL_T> &sigma, MATRIX_T &U, MATRIX_T &VT,
               REAL_T &energy2, REAL_T &estimator, const int verbose = 0) {

  REAL_T *ApproxSingularValues = nullptr;
  using SIZE_T = typename MATRIX_T::int_type;

  if (verbose > 0) {
    std::cout << "[fmr] Prescribed rank: " << prescribed_rank << std::endl;
    std::cout << "[fmr] Oversampling:    " << oversampling << std::endl;
    std::cout << "[fmr] Nb of subspace iterations: " << nbPowerIterations
              << std::endl;
  }
  if ((SIZE_T)(prescribed_rank + oversampling) > A.getNbCols())
    throw std::runtime_error(
        "[fmr] Oversampled rank should be lower than full rank!");
  //
  // Fixed rank RRF
  using Dense_RRFClass = RandomizedRangeFinder<REAL_T, MATRIX_T>;
  //
  // Classic Randomized Range Finder
  // RRF
  Dense_RRFClass RandRangeFinder(&A, prescribed_rank, oversampling,
                                 nbPowerIterations);
  //
  // RandSVD
  RandSVD<REAL_T, MATRIX_T, Dense_RRFClass> RSVD(A.getNbRows(), A.getNbCols(),
                                                 &RandRangeFinder);

  auto rank = RSVD.factorize(verbose);
  //
  // Get approx. singular values
  ApproxSingularValues = RSVD.getApproxSingularValues();
  sigma.assign(ApproxSingularValues, ApproxSingularValues + rank);

  // Get approx. singular vectors. Reshape the matrix to ignore columns related
  // to oversampling
  MATRIX_T* Uover = RSVD.getApproxU();
  U.init(A.getNbRows(), rank, *Uover);

  MATRIX_T* VTover = RSVD.getApproxVT();
  VT.init(rank, A.getNbCols(), *VTover);

  const auto shapeU = RSVD.getShapeU();
  // Get some parameters
  energy2 = RSVD.getEnergy2();
  estimator = RSVD.getApproxBaselineSpec();
  //
  if (verbose > 0) {
    std::cout << "[fmr] U: " << shapeU[0] << " x " << shapeU[1] << "  "
              << A.getNbRows() << "  " << rank << std::endl;
    std::cout << "[fmr] energy2: " << energy2 << "  estimator " << estimator
              << std::endl;
  }
  // auto rank = prescribed_rank;
}
/**
 *
 */
///
/// \brief compute a random SVD with prescribed accuracy
/// \param[in] prescribed_accuracy the targueted accuracy for teh SVD approximation
/// \param BlockSize the size of the block to increase the search
/// \param nbPowerIterations the number of the subspace iteration method
/// \param maxRank the maximal rank allowed
/// \param A
/// \param ApproxSingularValues
/// \param X
/// \param energy2
/// \param estimator
///
template <class REAL_T, class MATRIX_T>
void randomSVD(const REAL_T &prescribed_accuracy, const int &blockSize,
               const int &nbPowerIterations, const int &maxRank, MATRIX_T &A,
               std::vector<REAL_T> &sigma, MATRIX_T &U, MATRIX_T &VT,
               REAL_T &energy2, REAL_T &estimator, const int verbose = 1) {
  std::clog<< " COUCOU 3\n";

  if (verbose > 0) {

    std::cout << "[fmr] Prescribed accuracy:    " << prescribed_accuracy
              << std::endl;
    std::cout << "[fmr] BlockSize:              " << blockSize
              << std::endl;
    std::cout << "[fmr] Subspace iterations num: " << nbPowerIterations
              << std::endl;

    std::cerr << "[fmr] NOT FULLY IMPLEMENTED" << std::endl;
  }
  using Dense_ARRFClass = AdaptiveRRF<REAL_T, MATRIX_T>;
  REAL_T *ApproxSingularValues = nullptr;
  REAL_T *X = nullptr;

  /// we compute the orthogonal matrix and the rank
  Dense_ARRFClass AdaptRandRangeFinder(
      &A, prescribed_accuracy, blockSize, maxRank, nbPowerIterations);
  //
  // perform the RandSVD
  RandSVD<REAL_T, MATRIX_T, Dense_ARRFClass> RSVD(A.getNbRows(), A.getNbCols(),
                                                  &AdaptRandRangeFinder);


  // perform the RandSVD
  const auto rank = RSVD.factorize(verbose);

  //
  // Get approx. singular values
  ApproxSingularValues = RSVD.getApproxSingularValues();

  // Get approx. singular vectors
  //  U is colomn major [n l > rank]
  U = RSVD.getApproxU();
  //
  energy2 = RSVD.getEnergy2();
  estimator = RSVD.getApproxBaselineSpec();
#ifdef TOTO
#endif
}

} // namespace fmr

#endif
