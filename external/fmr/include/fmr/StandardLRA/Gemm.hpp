/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef DEF_FMR_GEMM
#define DEF_FMR_GEMM

#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include <iostream>
#include <string>
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/Utils/Blas.hpp"
#include "fmr/Utils/Display.hpp"

namespace fmr {
///
/// \brief Perform C = A B
/// \param A
/// \param B
/// \param C
/// \return
///
template <typename MatrixWrapper>
int fmrGemm(MatrixWrapper &A, MatrixWrapper &B, MatrixWrapper &C) {
  fmrGemm("N", "N", 1.0, A, B, 0.0, C);
  return 0;
}

///
/// \brief  Perform C = alpha A^TransA B^TransB + beta C
/// \param TransA
/// \param TransB
/// \param alpha
/// \param A
/// \param B
/// \param beta
/// \param C
/// \return
///
template <typename FSize, typename FReal>
int fmrGemm(const std::string& TransA, const std::string& TransB, FReal alpha,
            BlasDenseWrapper<FSize, FReal> &A,
            BlasDenseWrapper<FSize, FReal> &B, FReal beta,
            BlasDenseWrapper<FSize, FReal> &C) {
  (void)beta;
  if ((TransA == "N" || TransA == "n") && (TransB == "N" || TransB == "n")) {
    Blas::gemm(A.getNbRows(), A.getNbCols(), B.getNbCols(), alpha,
               A.getMatrix(), A.getNbRows(), B.getMatrix(), B.getNbRows(),
               beta, C.getMatrix(), C.getNbRows());
  }
  if ((TransA == "T" || TransA == "t") && (TransB == "N" || TransB == "n")) {
    Blas::gemtm(A.getNbRows(), A.getNbCols(), B.getNbCols(), alpha,
                A.getMatrix(), A.getNbRows(), B.getMatrix(), B.getNbRows(),
                beta, C.getMatrix(), C.getNbRows());
  }
  if ((TransA == "N" || TransA == "n") && (TransB == "T" || TransB == "t")) {
    Blas::gemmt(A.getNbRows(), A.getNbCols(), B.getNbRows(), alpha,
                A.getMatrix(), A.getNbRows(), B.getMatrix(), B.getNbRows(),
                beta, C.getMatrix(), C.getNbRows());
  }
  if ((TransA == "T" || TransA == "t") && (TransB == "T" || TransB == "t")) {
    Blas::gemmtt(A.getNbRows(), A.getNbCols(), B.getNbRows(), alpha,
                 A.getMatrix(), A.getNbRows(), B.getMatrix(), B.getNbRows(),
                 beta, C.getMatrix(), C.getNbRows());
  }
  return 0;
}
///
/// \brief fmrGemm
/// \param A
/// \param B
/// \param C
/// \return
///
template <typename FSize, typename FReal>
int fmrGemm(BlasDenseWrapper<FSize, FReal> &A,
            BlasDenseWrapper<FSize, FReal> &B,
            BlasDenseWrapper<FSize, FReal> &C) {
  fmrGemm<FSize, FReal>("N", "N", 1, A, B, 0, C);
  return 0;
}

#if defined(FMR_CHAMELEON)
template <typename FSize, typename FReal>
int fmrGemm(const std::string& TransA, const std::string& TransB, FReal alpha,
            ChameleonDenseWrapper<FSize, FReal> &A,
            ChameleonDenseWrapper<FSize, FReal> &B, FReal beta,
            ChameleonDenseWrapper<FSize, FReal> &C) {
  cham_trans_t TA, TB;
  // Check TransA
  if (TransA == "N" || TransA == "n") {
    TA = ChamNoTrans;
  } else if (TransA == "T" || TransA == "t") {
    TA = ChamTrans;
  } else if (TransA == "C" || TransA == "c") {
    TA = ChamConjTrans;
  } else {
    return -1;
  }

  // Check TransB
  if (TransB == "N" || TransB == "n") {
    TB = ChamNoTrans;
  } else if (TransB == "T" || TransB == "t") {
    TB = ChamTrans;
  } else if (TransB == "C" || TransB == "c") {
    TB = ChamConjTrans;
  } else {
    return -2;
  }

  /* Enforce Chameleon GEMM/SYMM algorithm, because chameleon's defaut choice is
  not satisfactory, e.g. when sizeA=sizeB -> the generic gemm algorithm is
  chosen in chameleon */
  FSize sizeA, sizeB, sizeC;
  FReal ratio = 1.5; /* Arbitrary ratio to give more weight to writes wrt reads. */
  /* Compute the average array per node for each matrix */
  int p = chameleon_desc_datadist_get_iparam(A.getMatrix(), 0);
  int q = chameleon_desc_datadist_get_iparam(A.getMatrix(), 1);
  sizeA = ((FReal)A.getMatrix()->m * (FReal)A.getMatrix()->n) / (FReal)(p*q);
  p = chameleon_desc_datadist_get_iparam(B.getMatrix(), 0);
  q = chameleon_desc_datadist_get_iparam(B.getMatrix(), 1);
  sizeB = ((FReal)B.getMatrix()->m * (FReal)B.getMatrix()->n) / (FReal)(p*q);
  p = chameleon_desc_datadist_get_iparam(C.getMatrix(), 0);
  q = chameleon_desc_datadist_get_iparam(C.getMatrix(), 1);
  sizeC = ((FReal)C.getMatrix()->m * (FReal)C.getMatrix()->n) / (FReal)(p*q) * ratio;
  if ( sizeA >= sizeB && sizeA > sizeC ) {
    setenv( "CHAMELEON_GEMM_ALGO", "SUMMA_A", 1);
  } else {
    setenv( "CHAMELEON_GEMM_ALGO", "AUTO", 1);
  }

  if (A.isSymmetric()) {
    Chameleon::symm(ChamLeft, A.getUpperLower(), alpha, A.getMatrix(),
                    B.getMatrix(), beta, C.getMatrix(),
                    C.getMatrixWorkspaceSymm(A.getMatrix(), B.getMatrix()));
  } else {
    Chameleon::gemm(TA, TB, alpha, A.getMatrix(), B.getMatrix(), beta,
                    C.getMatrix(), C.getMatrixWorkspaceGemm(A.getMatrix(),
                                                            B.getMatrix()));
  }

  return 0;
}

template <typename FSize, typename FReal>
int fmrGemm(ChameleonDenseWrapper<FSize, FReal> &A,
            ChameleonDenseWrapper<FSize, FReal> &B,
            ChameleonDenseWrapper<FSize, FReal> &C) {
  fmrGemm<FSize, FReal>("N", "N", 1, A, B, 0, C);
  return 0;
}
#endif // FMR_CHAMELEON

#define FMULS_GEMM(__m, __n, __k)                                              \
  ((double)(__m) * (double)(__n) * (double)(__k))
#define FADDS_GEMM(__m, __n, __k)                                              \
  ((double)(__m) * (double)(__n) * (double)(__k))
#define FLOPS_GEMM(__m, __n, __k)                                              \
  (FMULS_GEMM(__m, __n, __k) + FADDS_GEMM(__m, __n, __k))

} /* namespace fmr */

#endif // DEF_FMR_GEMM
