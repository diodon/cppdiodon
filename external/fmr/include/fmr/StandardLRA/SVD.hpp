/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef SVD_HPP
#define SVD_HPP
#include <tuple>

// includes
#include "fmr/Utils/Blas.hpp" // for SVD (via Blas::gesvd)
#include "fmr/Utils/Global.hpp"
#include "fmr/Utils/Parameters.hpp"
//#include "fmr/Utils/Math.hpp"

// Utils
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/Utils/Display.hpp"
#include <cmath>
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace fmr {
/**
 * @brief The Singular Value Decomposition class
 *
 */
template <class Real, typename Int_type = int> class SVD {
public:
  /**
   * @brief The SVD namespace
   */
  static Int_type getNumericalRank(const Real *S, const Int_type size,
                                   const Real eps) {
    const Real nrm2 = Blas::scpr(int(size), S, S);
    Real nrm2k(0.);
    for (auto k = size; k > 0; --k) {
      nrm2k += S[k - 1] * S[k - 1];
      if (nrm2k > eps * eps * nrm2)
        return k;
    }
    throw std::runtime_error("rank cannot be larger than size");
    return 0;
  }

  // Beware! Does not necessarily mean that A=USU' with S non-negative! Use
  // has_sqrt()
  static bool is_nonnegative(const Int_type size, const Real *A) {
    // allocate spectrum
    Real *S = new Real[size];
    // compute SVD
    computeSVD(size, size, A, S);
    // test non-negativeness
    for (int i = 0; i < size; ++i)
      if (S[i] <= -1e-15)
        return false;
    return true;
  }

  //
  static bool has_sqrt(const Int_type size, const Real *A) {
    // allocate spectrum
    Real *S = new Real[size];
    // compute SVD
    computeSYMSVD(size, A, S);
    // test non-negativeness
    for (int i = 0; i < size; ++i)
      if (S[i] <= -1e-15)
        return false;
    return true;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // SVD wrappers
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // list of functions:
  // * static void computeSYMSVD(const Size size, const Real* A, Real* S, Real*
  // C)
  // * static void computeSYMSVD(const Size size, const Real* A, Real* S)
  // * static void computeSVD(const Size size, const Real* A, Real* S, Real* C,
  // Real* Q)
  // * static void computeSVD(const Size size, const Real* A, Real* S, Real* C)
  // * static void computeSVD(const Size size, const Real* A, Real* S)

  //
  static void ensureUequalV(const Int_type size, /*const*/ Real *U,
                            /*const*/ Real *Vt, Real *&S) {

    // Compute VtU=V'*U
    Real *VtU = new Real[size * size];
    Blas::gemm(int(size), int(size), int(size), Real(1.), Vt, int(size), U,
               int(size), VtU, int(size));

    // Display diag(VtU)
    Real *diagVtU = new Real[size];
    for (int i = 0; i < size; ++i)
      diagVtU[i] = VtU[i + i * size];
    delete[] VtU;
    Display::vector(size, diagVtU, "diag(VtU)", 10, 1);

    std::cout << "Matrix is non-negative? ";
    bool isSPSD = true;
    for (Int_type i = 0; i < size; ++i) {
      if (diagVtU[i] < Real(-0.999)) {

        // std::cout << "VtU["<<i<<","<<i<<"]=" << diagVtU[i] << std::endl;
        ////
        // std::cout << "Change sign of " << i << "-th singular value." <<
        // std::endl;
        //
        S[i] = -S[i];
        //
        // indicate that matrix is not SPSD
        if (S[i] < -1.e-14)
          isSPSD = false;
      }
    }
    if (isSPSD)
      std::cout << "YES!" << std::endl;
    else
      std::cout << "NO, matrix has negative singular values!" << std::endl;

    // free memory
    delete[] diagVtU;
  }

  /*
   * Compute SVD $A=USU'$ and return $U$ and $S$.
   * If matrix not non-negative then S contains negative values !
   * \param A contains input square matrix to be decomposed
   * \param S contains singular values $S$
   * \param C contains $U$
   */
  static void computeSYMSVD(const Int_type size, const Real *A, Real *S,
                            Real *C) {
    // copy A
    is_int(size * size);
    Blas::copy(int(size * size), A, C);
    // init SVD
    const Int_type LWORK = 2 * 4 * size;
    Real *const WORK = new Real[LWORK];
    // singular value decomposition
    Real *const Q = new Real[size * size];

    // SO means that first min(m,n) lines of U overwritten on Q and V' on C
    // (A=QSC) AA means all lines nothing means OS (the opposite of SO, A=CSQ)
    is_int(size);
    is_int(LWORK);
    const unsigned int INFOSVD =
        Blas::gesvd(int(size), int(size), C, S, Q, int(size), int(LWORK), WORK);

    if (INFOSVD != 0) {
      std::cout << "[fmr] Blas::gesvd error" << std::endl;
    }

    // Ensure U=V by checkin diagonal of V'U
    ensureUequalV(size, C, Q, S);

    // free memory
    delete[] WORK;
    delete[] Q;
  }

  /*
   * Compute SVD $A=USU'$ and return $S$.
   * If matrix not non-negative then S contains negative values !
   * \param A contains input square matrix to be decomposed
   * \param S contains singular values $S$
   */
  static void computeSYMSVD(const Int_type size, const Real *A, Real *S) {
    // copy A
    Real *C = new Real[size * size];
    is_int(size * size);
    Blas::copy(int(size * size), A, C);
    // init SVD
    const Int_type LWORK = 2 * 4 * size;
    Real *const WORK = new Real[LWORK];
    // singular value decomposition
    Real *const Q = new Real[size * size];

    // SO means that first min(m,n) lines of U overwritten on Q and V' on C
    // (A=QSC) AA means all lines nothing means OS (the opposite of SO, A=CSQ)
    is_int(size);
    is_int(LWORK);
    const unsigned int INFOSVD =
        Blas::gesvd(int(size), int(size), C, S, Q, int(size), int(LWORK), WORK);

    if (INFOSVD != 0) {
      std::cout << "[fmr] Blas::gesvd error" << std::endl;
    }

    // Ensure U=V by checkin diagonal of V'U
    ensureUequalV(size, C, Q, S);

    // free memory
    delete[] WORK;
    delete[] Q;
  }

  /*
   * Compute SVD $A=USV'$ and return $V'$, $S$ and $U$.
   * \param A contains input m-by-n matrix to be decomposed
   * \param S contains output min(m,n) singular values $S$
   * \param U contains output m-by-min(m,n) matrix $U$
   * \param VT contains output min(m,n)-by-n matrix $V'$
   */
  static void computeSVD(const Int_type m, const Int_type n, const Real *A,
                         Real *S, Real *U, Real *VT) {
    // Left and Right singular vectors
    // Please allocate U[m x min(m,n)] VT[min(m,n) x n]

    // copy A
    is_int(m * n);
    if (m >= n) {
      Blas::copy(int(m * n), A, U);
    } else {
      Blas::copy(int(m * n), A, VT);
    }

    // init SVD
    const Int_type minMN = std::min(m, n);
    const Int_type maxMN = std::max(m, n);
    const Int_type LWORK = 2 * std::max(3 * minMN + maxMN, 5 * minMN);
    // const Size LWORK = 2*4*size;
    Real *const WORK = new Real[LWORK];
    // singular value decomposition

    // How does gesvdXX function?
    // gesvdXX(m,n,B,S,C,ldC)
    // SO means that first min(m,n) lines of U overwritten on 3rd array and V'
    // on 1st array (i.e. A=VT*S*U) nothing means OS (the opposite of SO, i.e.
    // A=U*S*VT) AA means all lines Should be used if we want to overwrite U on
    // A (m>n) or VT on A (m<n). This is not the case in general for this
    // library.
    is_int(m);
    is_int(n);
    is_int(LWORK);
    unsigned int INFOSVD;
    if (m >= n) {
      INFOSVD = Blas::gesvd(int(m), int(n), U, S, VT, int(n), int(LWORK), WORK);
    } else // if (m<n)
    {
      INFOSVD =

          Blas::gesvdSO(int(m), int(n), VT, S, U, int(m), int(LWORK), WORK);
    }
    // else
    //    INFOSVD  = Blas::gesvdAA(int(m), int(n), U, S, VT, int(minMN),
    //    int(LWORK), WORK);
    if (INFOSVD != 0) {
      std::cout << "[fmr] Blas::gesvd error number " << INFOSVD << std::endl;
    }

    // free memory
    delete[] WORK;
  }

  /*
   * Compute SVD $A=USV'$ and return $V'$ and $S$.
   * \param A contains input m-by-n matrix to be decomposed
   * \param S contains singular values $S$
   * \param U contains output m-by-min(m,n) matrix $U$
   */
  static void computeSVD(const Int_type m, const Int_type n, const Real *A,
                         Real *S, Real *U) {
    //
    const Int_type minMN = std::min(m, n);
    // Allocate memory
    Real *const VT = new Real[minMN * n];
    // Compute SVD
    computeSVD(m, n, A, S, U, VT);

    // free memory
    delete[] VT;
  }

  /*
   * Compute SVD $A=USV'$ and return $S$.
   * \param A contains input m-by-n matrix to be decomposed
   * \param S contains singular values $S$
   */
  static void computeSVD(const Int_type m, const Int_type n, const Real *A,
                         Real *S) {
    //
    const Int_type minMN = std::min(m, n);
    // Allocate memory
    Real *const U = new Real[m * minMN];
    Real *const VT = new Real[minMN * n];

    // Compute SVD
    computeSVD(m, n, A, S, U, VT);

    // free memory
    delete[] U;
    delete[] VT;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // SVD::SQRT wrappers
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // list of functions:
  // * static void computeSQRT(const Size size, const Real* A, Real* sqrtA)
  // * static void computeSYMSQRT(const Size size, const Real* A, Real* sqrtA)
  // * static void computeSYMSQRT_withSV(const Size size, const Real* A, Real*
  // sqrtA, Real* S)
  // * static void computeSQRT_withSV(const Size size, const Real* A, Real*
  // sqrtA, Real* S)
  // * static void computeSQRTINV_withSV(const Size size, const Real* A, Real*
  // sqrtA, Real* S)

  static void computeSQRT(const Int_type size, const Real *A, Real *sqrtA) {

    // init SVD
    Real *const S = new Real[size];

    // Perform SVD
    computeSYMSVD(size, A, S, sqrtA);

    // Tolerance for square root computation
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = machine_precision;

    // Compute square root of S
    for (Int_type i = 0; i < size; ++i) {

      // if(S[i]<=-1e-15)
      //  throw std::runtime_error("A has negative singular values! Cannot take
      //  square root!");
      // else
      //  S[i]=std::sqrt(S[i]);
      if (std::abs(S[i]) <= tol)
        S[i] = 0;
      else if (std::abs(S[i]) > tol && S[i] >= 0.)
        S[i] = std::sqrt(S[i]);
      else
        throw std::runtime_error(
            "A has negative singular values! Cannot take square root!");
    }

    // form US^{1/2}
    is_int(size);
    for (Int_type j = 0; j < size; ++j)
      Blas::scal(int(size), S[j], sqrtA + j * size);

    // free memory
    delete[] S;
  }

  static void computeSYMSQRT(const Int_type size, const Real *A, Real *sqrtA) {

    // init SVD
    Real *const S = new Real[size];
    Real *const U = new Real[size * size]; // in col major

    // Perform SVD
    computeSYMSVD(size, A, S, U);

    // Tolerance for square root computation
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = machine_precision;

    // Compute square root of S
    for (int i = 0; i < size; ++i) {

      if (std::abs(S[i]) <= tol)
        S[i] = 0;
      else if (std::abs(S[i]) > tol && S[i] >= 0.)
        S[i] = std::sqrt(S[i]);
      else
        throw std::runtime_error(
            "A has negative singular values! Cannot take square root!");
    }

    // form B=US^{1/2}
    Real *const B = new Real[size * size]; // in col major
    for (Int_type i = 0; i < size; ++i)
      for (Int_type j = 0; j < size; ++j)
        B[i + j * size] = U[i + j * size] * S[j];

    // form BU^t
    is_int(size);
    Blas::gemmt(int(size), int(size), int(size), Real(1.), B, int(size), U,
                int(size), sqrtA, int(size));

    // free memory
    delete[] S;
    delete[] U;
    delete[] B;
  }

  static void computeSYMSQRT_withSV(const Int_type size, const Real *A,
                                    Real *sqrtA, Real *S) {

    // init SVD
    Real *const U = new Real[size * size]; // in col major

    // Perform SVD
    computeSYMSVD(size, A, S, U);

    // Tolerance for square root computation
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = machine_precision;

    // Compute square root of S
    Real *const sqrtS = new Real[size];
    for (Int_type i = 0; i < size; ++i) {

      if (std::abs(S[i]) <= tol)
        sqrtS[i] = 0;
      else if (std::abs(S[i]) > tol && S[i] >= 0.)
        sqrtS[i] = std::sqrt(S[i]);
      else
        throw std::runtime_error(
            "A has negative singular values! Cannot take square root!");
    }

    // form B=US^{1/2}
    Real *const B = new Real[size * size]; // in col major
    for (Int_type i = 0; i < size; ++i)
      for (Int_type j = 0; j < size; ++j)
        B[i + j * size] = U[i + j * size] * sqrtS[j];

    // form BU^t
    is_int(size);
    Blas::gemmt(int(size), int(size), int(size), Real(1.), B, int(size), U,
                int(size), sqrtA, int(size));

    // free memory
    delete[] sqrtS;
    delete[] U;
    delete[] B;
  }

  static void computeSQRT_withSV(const Int_type size, const Real *A,
                                 Real *sqrtA, Real *S) {

    // Perform SVD
    computeSYMSVD(size, A, S, sqrtA);

    // Tolerance for square root computation
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = machine_precision;

    // Compute square root of S
    Real *const sqrtS = new Real[size];
    for (Int_type i = 0; i < size; ++i) {

      if (std::abs(S[i]) <= tol)
        sqrtS[i] = 0;
      else if (std::abs(S[i]) > tol && S[i] >= 0.)
        sqrtS[i] = std::sqrt(S[i]);
      else
        throw std::runtime_error(
            "A has negative singular values! Cannot take square root!");
    }

    // form B=US^{1/2}
    is_int(size);
    for (Int_type j = 0; j < size; ++j)
      Blas::scal(int(size), sqrtS[j], sqrtA + j * size);

    // free memory
    delete[] sqrtS;
  }

  static Int_type computeSQRTINV_withSV(const Int_type size, const Real *A,
                                        Real *sqrtA, Real *S) {

    // Perform SVD
    computeSYMSVD(
        size, A, S,
        sqrtA); // calls ensureUequalsV() => sign of S might be changed

    //// Display S
    // std::cout << "S=[" ;
    // for (int i=0; i<size; ++i)
    //    std::cout << S[i] << " " ;
    // std::cout << "]" << std::endl;

    // Tolerance for pseudo inverse spectrum
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = Real(size) * machine_precision * S[0];
    // Init final rank for truncation to tolerance
    Int_type rank = -1;

    // Compute square root of S
    Real *const sqrtinvS = new Real[size];
    for (Int_type i = 0; i < size; ++i) {

      if (std::abs(S[i]) <= tol) {
        sqrtinvS[i] = 0;
        if (rank == -1)
          rank = i;
      } else if (std::abs(S[i]) > tol && S[i] > 0.)
        sqrtinvS[i] = std::sqrt(Real(1.) / S[i]);
      else
        throw std::runtime_error(
            "A has negative singular values! Cannot take square root!");
    }

    if (rank == -1)
      rank = size;

    // Display S^{-1/2}
    std::cout << "S^{-1/2}=[";
    for (int i = 0; i < size; ++i)
      std::cout << sqrtinvS[i] << " ";
    std::cout << "]" << std::endl;

    // form B=US^{1/2}
    is_int(rank);
    for (Int_type j = 0; j < rank; ++j)
      Blas::scal(int(size), sqrtinvS[j], sqrtA + j * size);

    // free memory
    delete[] sqrtinvS;

    // Return nb of non zero values in S^{-1/2}
    return rank;
  }

  static Int_type invertSV(const Int_type size, const Real *S, Real *invS) {

    // Display S
    std::cout << "S=[";
    for (int i = 0; i < size; ++i)
      std::cout << S[i] << " ";
    std::cout << "]" << std::endl;

    // Tolerance for pseudo inverse spectrum
    Real machine_precision;
    if (sizeof(Real) == 4)
      machine_precision = Real(1.e-7);
    else if (sizeof(Real) == 8)
      machine_precision = Real(1.e-14);
    else
      throw std::runtime_error("Wrong value type!");
    const Real tol = Real(size) * machine_precision * S[0];
    // Init final rank for truncation to tolerance
    Int_type rank = -1;

    // Compute inverse of S
    for (Int_type i = 0; i < size; ++i) {

      if (std::abs(S[i]) <= tol) {
        invS[i] = 0;
        if (rank == -1)
          rank = i;
      } else if (std::abs(S[i]) > tol)
        invS[i] = Real(1.) / S[i];
      else
        throw std::runtime_error("Case not accounted for!");
    }

    if (rank == -1)
      rank = size;

    // Display S^{+}
    std::cout << "S^{+}=[";
    for (int i = 0; i < size; ++i)
      std::cout << invS[i] << " ";
    std::cout << "]" << std::endl;

    // Return nb of non zero values in S^{+}
    return rank;
  }

  static Int_type computeFACT_withSVandSVp(const Int_type size, const Real *A,
                                           Real *U, Real *S, Real *invS) {

    // Perform SVD
    computeSYMSVD(size, A, S,
                  U); // calls ensureUequalsV() => sign of S might be changed

    // Invert singular values
    const Int_type rank = invertSV(size, S, invS);

    // Return nb of non zero values in S^{+}
    return rank;
  }

  static void computeSVD(BlasDenseWrapper<Int_type, Real> &A, Real *S,
                         BlasDenseWrapper<Int_type, Real> &U,
                         BlasDenseWrapper<Int_type, Real> &VT) {
    // Left and Right singular vectors
    // Please allocate U[m x min(m,n)] VT[min(m,n) x n]
    Int_type m = A.getNbRows();
    Int_type n = A.getNbCols();
    if ( !(U.isAllocated()) ) {
      U.reset(m, std::min(m, n));
      U.allocate();
    }
    if ( !(VT.isAllocated()) ) {
      VT.reset(std::min(m, n), n);
      VT.allocate();
    }
    computeSVD(m, n, A.getBlasMatrix(), S, U.getBlasMatrix(),
               VT.getBlasMatrix());
  }

  static void computeSVD(BlasDenseWrapper<Int_type, Real> &A, Real *&S,
                         BlasDenseWrapper<Int_type, Real> *&U,
                         BlasDenseWrapper<Int_type, Real> *&VT) {
    // Left and Right singular vectors
    // Please allocate U[m x min(m,n)] VT[min(m,n) x n]
    Int_type m = A.getNbRows();
    Int_type n = A.getNbCols();

    Real *UBlas;
    Real *VTBlas;
    if (S == nullptr) {
      S = new Real[std::min(m, n)];
    }
    if (U == nullptr) {
      UBlas = new Real[m * std::min(m, n)];
    } else {
      UBlas = U->getBlasMatrix();
    }
    if (VT == nullptr) {
      VTBlas = new Real[n * std::min(m, n)];
    } else {
      VTBlas = VT->getBlasMatrix();
    }
    computeSVD(m, n, A.getBlasMatrix(), S, UBlas, VTBlas);

    if (U == nullptr) {
      U = new BlasDenseWrapper<Int_type, Real>(m, std::min(m, n), UBlas);
    }
    if (VT == nullptr) {
      VT = new BlasDenseWrapper<Int_type, Real>( std::min(m, n), n, VTBlas);
    }
  }

  static auto computeSVD(BlasDenseWrapper<Int_type, Real> &A) {
    // Left and Right singular vectors
    // Please allocate U[m x min(m,n)] VT[min(m,n) x n]
    Int_type m = A.getNbRows();
    Int_type n = A.getNbCols();
    Real *UBlas = new Real[m * std::min(m, n)];
    Real *VTBlas = new Real[n * std::min(m, n)];
    Real *S = new Real[std::min(m, n)];

    computeSVD(m, n, A.getBlasMatrix(), S, UBlas, VTBlas);

    return std::make_tuple(
        S, BlasDenseWrapper<Int_type, Real>(m, std::min(m, n), UBlas),
        BlasDenseWrapper<Int_type, Real>(std::min(m, n), n, VTBlas));
  }

#ifdef FMR_CHAMELEON
  static void computeSVD(ChameleonDenseWrapper<Int_type, Real> &A, Real *S,
                         ChameleonDenseWrapper<Int_type, Real> &U,
                         ChameleonDenseWrapper<Int_type, Real> &VT) {

    Int_type m = A.getNbRows();
    Int_type n = A.getNbCols();
    Int_type k = std::min(m, n);

    Real *ABlas = A.getBlasMatrix();
    Real *UBlas;
    Real *VTBlas;
    bool uallocated;
    bool vallocated;

    if (S == nullptr) {
      S = new Real[k];
    }
    if ( U.isAllocated() ) {
      UBlas = U.getBlasMatrix();
      uallocated = false;
    } else {
      UBlas = new Real[m * std::min(m, n)];
      uallocated = true;
    }
    if ( VT.isAllocated() ) {
      VTBlas = VT.getBlasMatrix();
      vallocated = false;
    } else {
      VTBlas = new Real[std::min(m, n) * n];
      vallocated = true;
    }

    Chameleon::pause();

    computeSVD(m, n, ABlas, S, UBlas, VTBlas);

    Chameleon::resume();

    if ( uallocated ) {
      U.init(m, std::min(m, n), UBlas);
      delete[] UBlas;
    } else {
      U.init(UBlas);
    }
    if ( vallocated ) {
      VT.init(std::min(m, n), n, VTBlas);
      delete[] VTBlas;
    } else {
      VT.init(VTBlas);
    }

  }

  static void computeSVD(ChameleonDenseWrapper<Int_type, Real> &A, Real *&S,
                         ChameleonDenseWrapper<Int_type, Real> *&U,
                         ChameleonDenseWrapper<Int_type, Real> *&VT) {

    Int_type m = A.getNbRows();
    Int_type n = A.getNbCols();
    Int_type k = std::min(m, n);

    Real *ABlas = A.getBlasMatrix();

    if (S == nullptr) {
      S = new Real[k];
    }

    Chameleon::pause();

    Real *UBlas = new Real[m * k];
    Real *VTBlas = new Real[k * n];
    computeSVD(m, n, ABlas, S, UBlas, VTBlas);

    Chameleon::resume();

    // Allocate U[m x min(m,n)] VT[min(m,n) x n] if needed
    if (U == nullptr) {
      U = new ChameleonDenseWrapper<Int_type, Real>(m, k);
    }
    U->init(UBlas);
    if (VT == nullptr) {
      VT = new ChameleonDenseWrapper<Int_type, Real>(k, n);
    }
    VT->init(VTBlas);

    delete[] UBlas;
    delete[] VTBlas;
  }
#endif

  void allignFactorsWithMatrix(const BlasDenseWrapper<Int_type, Real> &A,
                               BlasDenseWrapper<Int_type, Real> *&U,
                               BlasDenseWrapper<Int_type, Real> *&VT) {
    //
  }
};

} /* namespace fmr */

#endif // SVD_HPP

//  LocalWords:  cmath
