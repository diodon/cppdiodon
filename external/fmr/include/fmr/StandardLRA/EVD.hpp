/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef EVD_HPP
#define EVD_HPP

#include "fmr/Utils/Blas.hpp" // for EVD (via Blas::syevd)

// Utils
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/Utils/Display.hpp"
#include <cmath>
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace fmr {

  template <class Real, typename Int_type = int> class EVD {
  public:

    static void computeEVD(const Int_type n, const Real *A,
                           Real *E, Real *U, bool sortDescending=false){
      is_int(n * n);
      Blas::copy(int(n * n), A, U);
      /* The length of the array WORK.
       *  LDWORK > = max(1,3*N-1).
       * For optimal efficiency, LDWORK > = (NB+2)*N,
       * where NB is the blocksize for SSYTRD returned by ILAENV.
       */
      const Int_type LWORK = 3 * n - 1;
      Real *const WORK = new Real[LWORK];

      is_int(n);
      is_int(LWORK);
      unsigned int INFOEVD;
      INFOEVD = Blas::syev(int(n), U, E, int(LWORK), WORK);

      if (INFOEVD != 0) {
        std::cout << "[fmr] Blas::syev error number " << INFOEVD << std::endl;
      }

      delete[] WORK;

      // Sorting EVal and EVect in decreasing order
      if (sortDescending){
        Real * tmp = new Real[n];
        Real ttmp(0);
        for(Int_type i(0); i < n/2; i++){
          std::memcpy(tmp, U + i*n, n*sizeof(Real));
          std::memcpy(U + i*n, U + (n-i-1)*n, n*sizeof(Real));
          std::memcpy(U + (n-i-1)*n, tmp, n*sizeof(Real));
          ttmp = E[i];
          E[i] = E[n-i-1];
          E[n-i-1] = ttmp;
        }
      }

    }

    // Only for syevd
    static void computeEVD(BlasDenseWrapper<Int_type, Real> &A, Real *L,
                           BlasDenseWrapper<Int_type, Real> &U,
                           bool sortDescending=false){

      // No need for m as we only compute symmetric case
      Int_type n = A.getNbRows();

      Real *ABlas = A.getBlasMatrix();

      if (L == nullptr){
        L = new Real[n];
      }

      if ( !(U.isAllocated()) ) {
        U.reset(n, n);
        U.allocate();
      }
      computeEVD(n, ABlas, L, U.getBlasMatrix(), sortDescending);
    }

#ifdef FMR_CHAMELEON
    static void computeEVD(ChameleonDenseWrapper<Int_type, Real> &A, Real *L,
                           ChameleonDenseWrapper<Int_type, Real> &U,
                           bool sortDescending=false){

      // No need for m as we only compute symmetric case
      Int_type n = A.getNbRows();
      Real *ABlas = A.getBlasMatrix();
      Real *UBlas;
      bool uallocated;

      if (L == nullptr){
        L = new Real[n];
      }
      if ( U.isAllocated() ) {
        UBlas = U.getBlasMatrix();
        uallocated = false;
      } else {
        UBlas = new Real[n * n];
        uallocated = true;
      }

      Chameleon::pause();

      computeEVD(n, ABlas, L, UBlas, sortDescending);

      Chameleon::resume();

      if ( uallocated ) {
        U.init(n, n, UBlas);
        delete[] UBlas;
      } else {
        U.init(UBlas);
      }

    }
#endif

  };

} /* namespace fmr */

#endif
