/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXWRAPPER_BLASDENSEWRAPPER_HPP
#define MATRIXWRAPPER_BLASDENSEWRAPPER_HPP

// standard includes
#include <array>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <type_traits>
#include <vector>

// Utilities
#include "fmr/MatrixWrappers/MatrixWrapperTraits.hpp"
#include "fmr/Utils/Assert.hpp"
#include "fmr/Utils/Blas.hpp"
/**
 * @author Pierre Blanchard (pierre.blanchard@inria.fr)
 * @date December 29th, 2015
 *
 * Modifications O. Coulaud
 *        2018 rewrite the class
 *        2019 add cmath
 *        2020-2021 improvements and add functionnalities
 */

namespace fmr {

/**
 * @brief Blas Dense Wrapper class
 *
 * The matrix is stored in a 1d array column by comlum (Fortran convention)
 *
 * [TODO] Define wrapper for kernel matrices in different wrapper, eg,
 * KernelWrapper [TODO] Define subsampling in this/each wrapper?
 *
 */
template <class FSize, class FReal> class BlasDenseWrapper {

protected:
  // Matrix Dimensions
 std::array<FSize, 2> _shape; ///< The shape of the matrix

  // Symmetric square matrix?
  bool _matrixIsSymmetric;
  std::string _UpperLower;
  //
  FReal *_matrix;
  bool _isAllocated; ///< true if the wraper allocate the matrix or not

public:
  using value_type = FReal;
  using int_type = FSize;
  using Int_type = FSize;
  using matrix_type = FReal;
  using Tau_type = FReal *;
  using Tree_type = FReal *;

  /*
   * @brief allocate
   */
  ///
  /// @brief Constructor
  ///
  /// Constructor to wrap a dense matrix by copy. We allocate the place in a 1d
  ///   array then you should init the matrix throught the method init(matrix)
  ///
  /// @param[in] inNbRows      number of line
  /// @param[in] inNbCols      number of column
  /// @param[in] inMatrixIsSymmetric if the matrix is symmetric or not (false
  /// by default)
  explicit BlasDenseWrapper(
      const FSize inNbRows = 0,
      const FSize inNbCols = 0 /*, const FReal* inMatrix = nullptr*/,
      const bool inMatrixIsSymmetric = false)
      :  _shape({inNbRows, inNbCols}), _matrixIsSymmetric(inMatrixIsSymmetric), _matrix(nullptr),
         _isAllocated(false) {
    this->allocate();
  }
  ///
  /// @brief Constructor
  ///
  /// Constructor to wrap a dense matrix. We allocate the memory in a 1d
  /// array, then initialize the matrix with the value given by user.
  ///
  /// @param[in] inNbRows number of line
  /// @param[in] inNbCols number of column
  /// @param[in] inValue floating point value given to initialize all the matrix
  /// @param[in] inMatrixIsSymmetric if the matrix is symmetric or not (false
  /// by default)
  explicit BlasDenseWrapper(const FSize inNbRows, const FSize inNbCols,
                            FReal inValue,
                            const bool inMatrixIsSymmetric = false)
      :  _shape({inNbRows, inNbCols}),_matrixIsSymmetric(inMatrixIsSymmetric), _matrix(nullptr),
        _isAllocated(false) {
    init(inValue);
  }
  ///
  /// @brief Constructor
  ///
  /// Constructor to wrap a dense matrix by copy. We allocate the place then you
  ///   should init the matrix throught the methode init(matrix)
  ///
  /// @param[out] inNbRows      number of line
  /// @param[out] inNbCols      number of column
  /// @param[out] inMatrix      a pointer on the matrix in a 1d array.
  /// @param[out] inMatrixIsSymmetric if the matrix is symmetric or not (false
  /// by default)
  explicit BlasDenseWrapper(const FSize inNbRows, const FSize inNbCols,
                            FReal *inMatrix,
                            const bool inMatrixIsSymmetric = false)
      : _shape({inNbRows, inNbCols}), _matrixIsSymmetric(inMatrixIsSymmetric), _matrix(inMatrix),
        _isAllocated(false)  {
    // std::cout << "BlasDenseWrapper constructor pointer " << std::endl;
  }

  /// @brief an explicit copy constructor
  ///
  /// Constructor to wrap a dense matrix by copy. We allocate the place in a 1d
  ///   array then you should init the matrix throught the methode init(matrix)
  ///
  /// @param[out] other a BlasDenseWrapper matrix to copy
  explicit BlasDenseWrapper(const BlasDenseWrapper &other)
      : _shape(other._shape), _matrixIsSymmetric(other._matrixIsSymmetric),
        _isAllocated(other._isAllocated) {

    _matrix = new FReal[_shape[0] * _shape[1]];

    memcpy(_matrix, other._matrix, _shape[0] * _shape[1] * sizeof(FReal));
  }

  /*
   * Dtor
   */
  ~BlasDenseWrapper() {
    // std::clog << "~BlasDenseWrapper() " << _matrix << std::endl;
    if (_isAllocated) {
      delete[] _matrix;
    }
    _matrix = nullptr;
    _isAllocated = false;
  }

public:
  /*
   * Get matrix size
   *
   */
  FSize getLeadingDim() const {
    return _shape[0]; // since blas uses column major convention
  }
  /*
   * @brief Get matrix number of rows
   */
  FSize getNbRows() const { return _shape[0]; }

  /*
   * @brief Get matrix number of columns
   */
  FSize getNbCols() const { return _shape[1]; }

  /**
   * @brief return the shape of the matrix
   */
  auto getShape() const { return _shape; }
  /**
   * @brief isAllocated check if the matrix is allocated
   * @return true if the matrix is allocated
   */
  bool isAllocated() const { return _isAllocated; }
  /**
   * @brief allocate allocate the matrix
   */
  void allocate() {
    //    std::clog << "allocate" << _matrix << " " << _shape[0] << " " <<
    //    _shape[1]
    //              << std::endl;
    if (_isAllocated) {
      delete[] _matrix;
    }
    if (_shape[0] * _shape[1] > 0) {
      _matrix = new FReal[_shape[0] * _shape[1]]{};
      _isAllocated = true;
    }
  }
  /**
   * @brief allocate the matrix with given shape
   *
   * @param[in] nbRows the number of rows
   * @param[in] nbCols the number of columns
   * @param[in] isSymmetric (not used)
   */
  void initAndAllocate(int_type nbRows, int_type nbCols,
                       bool isSymmetric = false) {
    _shape[0] = nbRows;
    _shape[1] = nbCols;
    _matrixIsSymmetric = isSymmetric;
    if (_isAllocated) {
      delete[] _matrix;
    }
    if (_shape[0] * _shape[1] > 0) {
      _matrix = new FReal[_shape[0] * _shape[1]]{};
      _isAllocated = true;
    }
  }
  /**
   * @brief free deallocate the matrix (free the memory)
   */
  void free() {
    //    std::clog << "BlasDenseWrapper::free this " << this << " _matrix "
    //              << _matrix << std::boolalpha << "  _isAllocated " <<
    //              _isAllocated
    //              << std::endl;
    if (_isAllocated) {
      delete[] _matrix;
      _matrix = nullptr;
      _isAllocated = false;
    }
  }

  /**
   * @brief return a pointer on the array of elements.
   *
   * It should be used only with the classes fully compatible with
   * BlasDenseWrapper
   *
   */
  FReal *getMatrix() const { return _matrix; }
  /**
   * @brief return a pointer on the array of elements.
   *
   * It should be used only with the classes fully compatible with
   * BlasDenseWrapper
   *
   */
  FReal *&getMatrix() { return _matrix; }
  /**
   * @brief return a pointer on a centralized 1D array column major
   * (blas/lapack data type) containing up-to-date data.
   */
  FReal *getBlasMatrix() const { return _matrix; }
  /**
   * @brief return a pointer on a centralized 1D array column major
   * (blas/lapack data type) containing up-to-date data.
   */
  FReal *&getBlasMatrix() { return _matrix; }

  /**
   *  @brief copy the matrix in an array whose memory is handle by the caller
   *
   */
  int copyMatrix(FReal *outputMatrix) {
    if (!_isAllocated) {
      return -1;
    }
    memcpy(outputMatrix, _matrix, _shape[1] * _shape[0] * sizeof(FReal));
    return 0;
  }
  /*
   * Start briging the gap between dense matrix wrapper from HMAT and the ones
   * in Wrappers/
   *
   */
  const FReal &getVal(const FSize idxRow, const FSize idxCol) const {
    return _matrix[idxCol * _shape[0] + idxRow];
  }

  FReal &getVal(const FSize idxRow, const FSize idxCol) {
    return _matrix[idxCol * _shape[0] + idxRow];
  }

  void setVal(const FSize idxRow, const FSize idxCol, FReal val) {
    _matrix[idxCol * _shape[0] + idxRow] = val;
  }

  void extractCol(FSize colNb, std::vector<FReal> &out) const {
    if (colNb > _shape[1]) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colNb
                << " from a matrix with " << _shape[1] << " columns" << std::endl;
      exit(EXIT_FAILURE);
    }
    out.insert(out.end(), &_matrix[colNb * _shape[0]],
               &_matrix[(colNb + 1) * _shape[0]]);
  }
  void extractCols(std::vector<FSize> listCols, std::vector<FReal> &out) const {
    for (typename std::vector<FSize>::iterator it = listCols.begin();
         it != listCols.end(); ++it) {
      extractCol(*it, out);
    }
  }

  void extractCol(FSize colIdxIn, FSize colIdxOut,
                  BlasDenseWrapper &columns) const {
    if (colIdxIn > _shape[1]) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colIdxIn
                << " from a matrix with " << _shape[1] << " columns" << std::endl;
      exit(EXIT_FAILURE);
    }
    if (colIdxOut > columns.getNbCols()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colIdxOut
                << " in a matrix with " << columns.getNbCols() << " columns"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    if (_shape[0] > columns.getNbRows()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy a column of " << _shape[0]
                << " values in a matrix with " << columns.getNbRows() << " rows"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    FReal *outmatrix = columns.getMatrix();
    std::memcpy(&outmatrix[colIdxOut * columns.getNbRows()],
                &_matrix[colIdxIn * _shape[0]], _shape[0] * sizeof(FReal));
  }
  void extractCols(std::vector<FSize> listCols,
                   BlasDenseWrapper &columns) const {
    if (!columns.isAllocated()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Output matrix is not allocated" << std::endl;
      exit(EXIT_FAILURE);
    }
    FSize idxCols = 0;
    for (typename std::vector<FSize>::iterator it = listCols.begin();
         it != listCols.end(); ++it) {
      extractCol(*it, idxCols, columns);
      idxCols++;
    }
  }

  void getSubMatrix(const FSize idxRowStart, const FSize idxColStart,
                    const FSize nrows, const FSize ncols,
                    FReal *submatrix) const {
    for (FSize j(0); j < ncols; ++j) {
      for (FSize i(0); i < nrows; ++i) {
        submatrix[j * nrows + i] =
            _matrix[(idxColStart + j) * _shape[0] + (idxRowStart + i)];
      }
    }
  }

  void setSubMatrix(const FSize idxRowStart, const FSize idxColStart,
                    const FSize nrows, const FSize ncols, FReal *submatrix,
                    const std::string &transpose = "N") {
    if (transpose[0] == std::string("T")[0]) {
      for (FSize j(0); j < ncols; ++j) {
        for (FSize i(0); i < nrows; ++i) {
          _matrix[(idxColStart + j) * _shape[0] + (idxRowStart + i)] =
              submatrix[i * ncols + j];
        }
      }
    } else {
      for (FSize j(0); j < ncols; ++j) {
        for (FSize i(0); i < nrows; ++i) {
          _matrix[(idxColStart + j) * _shape[0] + (idxRowStart + i)] =
              submatrix[j * nrows + i];
        }
      }
    }
  }

  /*
   * Get wrapper ID
   *
   */
  int getID() { return 1; }

  /*
   * Get symmetric flag
   *
   */
  bool isSymmetric() const { return _matrixIsSymmetric; }

  /*
   * Set symmetric flag
   *
   */
  void setSymmetric(bool sym) { _matrixIsSymmetric = sym; }

  /**
   * @brief init set by copy the element of the matrix
   * @param[in] inMatrix
   */
  void init(const FReal *inMatrix) {
    // Copy values from matrix beware nbRows*nbCols might not be integer!!
    // Hence do not use Blas to copy all at once
    // is_int(nbRows*nbCols);
    if (!_matrixIsSymmetric) {
      if (!_isAllocated) {
        _matrix = new FReal[_shape[0] * _shape[1]];
        _isAllocated = true;
      }
      memcpy(_matrix, inMatrix, _shape[0] * _shape[1] * sizeof(FReal));
    }
    else {
      // Here we assume that the matrix is always in Lower form
      if (!_isAllocated) {
        _matrix = new FReal[(_shape[0] * (_shape[0]+1)) / 2];
        _isAllocated = true;
      }
      FSize copied = 0;
      for (FSize i(0); i < _shape[0]; i++){
        memcpy(_matrix + copied, inMatrix + (i * _shape[0]) + i,
               (_shape[0] - i) * sizeof(FReal));
        copied += _shape[0] - i;
      }
    }
  }
  /**
   * @brief init reshape the matrix and copy the element of the matrix.
   * @param[in] inMatrix
   */
  void init(const FSize &nbRows, const FSize &nbCols, const FReal *inMatrix) {
    if (inMatrix != _matrix) {
      reset(nbRows, nbCols);
      init(inMatrix);
    } else {
      _shape[0] = nbRows;
      _shape[1] = nbCols;
    }
  }
  /**
   * @brief init reshape the matrix and copy the element of the matrix.
   * @param[in] inMatrix
   */
  void init(const FSize &nbRows, const FSize &nbCols, const BlasDenseWrapper &inMatrix) {
    if (inMatrix.getMatrix() != _matrix) {
      reset(nbRows, nbCols);
      if (nbRows == inMatrix.getNbRows() && nbCols == inMatrix.getNbCols()) {
        init(inMatrix.getMatrix());
      } else if (nbRows > inMatrix.getNbRows() || nbCols > inMatrix.getNbCols() ) {
          std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                    << std::endl;
          std::cerr << "[fmr] The matrix to copy must not be larger than the "
                       "input matrix"
                    << std::endl;
          exit(EXIT_FAILURE);
      } else {
        allocate();
        FReal *submatrix = inMatrix.getMatrix();
        for (FSize j(0); j < nbCols; ++j) {
          for (FSize i(0); i < nbRows; ++i) {
            _matrix[j*nbRows + i] = submatrix[j*inMatrix.getNbRows() + i];
          }
        }
      }
    } else {
      _shape[0] = nbRows;
      _shape[1] = nbCols;
    }
  }
  /**
   * @brief init set a specific value in all the matrix
   * @param[in] inValue
   */
  void init(const FReal inValue) {
    // Copy values from matrix beware nbRows*nbCols might not be integer!!
    // Hence do not use Blas to copy all at once
    // is_int(nbRows*nbCols);
    if (!_isAllocated) {
      _matrix = new FReal[_shape[0] * _shape[1]];
      _isAllocated = true;
    }
    for (FSize i = 0; i < _shape[0] * _shape[1]; ++i) {
      _matrix[i] = inValue;
    }
  }
  /**
   * @brief Reshape the internal matrix, previous values are lost.
   * @param[in] nRows new number of rows
   * @param[in] nCols new number of columns
   * @param[in] in value to copy everywhere
   * @param[in] inMatrixIsSymmetric if new matrix is considered as symmetric
   */
  void reset(FSize nRows, FSize nCols, const bool inMatrixIsSymmetric = false) {
    _shape[0] = nRows;
    _shape[1] = nCols;
    _matrixIsSymmetric = inMatrixIsSymmetric;
    if (_matrix != nullptr) {
      delete[] _matrix;
    }
    _isAllocated = false;
    _matrix = nullptr;
  }
  /**
   * @brief Reshape the internal matrix and copy value in everywhere.
   * @param[in] nRows new number of rows
   * @param[in] nCols new number of columns
   * @param[in] in value to copy everywhere
   * @param[in] inMatrixIsSymmetric if new matrix is considered as symmetric
   */
  template <typename InitType>
  void reset(FSize nRows, FSize nCols, InitType in,
             const bool inMatrixIsSymmetric = false) {
    reset(nRows, nCols, inMatrixIsSymmetric);
    init(in);
  }
  /**
   * @brief Reshape the internal matrix and copy the matrix Mat in the class.
   * @param[in] Mat the BlasDenseWrapper matrix to copy in the class
   */
  int reset(const BlasDenseWrapper &Mat) {
    _shape[0] = Mat.getNbRows();
    _shape[1] = Mat.getNbCols();
    _matrixIsSymmetric = Mat.isSymmetric();
    allocate();
    memcpy(_matrix, Mat.getBlasMatrix(), _shape[0] * _shape[1] * sizeof(FReal));
    return 0;
  }
  /**
   * @brief Scale matrix columns with a value for each column picked in
   * scaleValues
   * @param[in] nbColsToScale number of columns to scale
   * @param[in] scaleValues scaling factors
   */
  void scaleCols(const FSize &nbColsToScale, FReal* scaleValues) {

#pragma omp parallel
    for (FSize j = 0; j < nbColsToScale; ++j) {
#pragma omp for simd
      for (FSize i = 0; i < _shape[0]; ++i) {
        _matrix[i + j * _shape[0]] *= scaleValues[j];
      }
    }
  }
  /**
   * @brief Return the Frobenius norm of the matrix
   */
  value_type getFrobeniusNorm() {
    value_type norm = 0;
#pragma omp parallel for simd shared(_shape, _matrix) reduction(+:norm)
    for (FSize j = 0; j < _shape[0] * _shape[1]; ++j) {
      norm += _matrix[j] * _matrix[j];
    }
    return std::sqrt(norm);
  }

  /**
   * @brief Scale matrix rows with a value for each column picked in
   * scaleValues
   * @param[in] nbRowsToScale number of rows to scale
   * @param[in] scaleValues scaling factors
   */
  void scaleRows(const FSize &nbRowsToScale, FReal* scaleValues) {

#pragma omp parallel
    for (FSize j = 0; j < _shape[1]; ++j) {
#pragma omp for simd
      for (FSize i = 0; i < nbRowsToScale; ++i) {
        _matrix[i + j * _shape[0]] *= scaleValues[i];
      }
    }
  }

}; // end class


namespace traits {
/// Define the blas wrapper traits
template <typename VALUE_T, typename INT_T>
struct is_blas_wrapper<BlasDenseWrapper<INT_T, VALUE_T>> : std::true_type {};
} // namespace traits
} /* namespace fmr */

#endif /* BLAS_DENSEWRAPPER_HPP */
