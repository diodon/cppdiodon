#!/bin/bash
set -ex

# configure
export CC=gcc
export CXX=g++
export FC=gfortran
export CXXFLAGS="-O0 -g -fPIC --coverage -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wcomment -pedantic -fdiagnostics-show-option -fno-inline -fno-omit-frame-pointer"
[[ -d build ]] && rm build -rf
cmake -B build \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
      -DCMAKE_INSTALL_PREFIX=$PWD/build/install \
      -DCMAKE_BUILD_TYPE=Debug \
      -DFMR_BUILD_TESTS=ON -DFMR_USE_HDF5=ON -DFMR_USE_CHAMELEON=ON -DFMR_USE_MKL_AS_BLAS=ON

# make (+clang analyzer)
scan-build -plist --exclude CMakeFiles -o analyzer_reports cmake --build build 2>&1 | tee build.log

# test
ctest --test-dir build --output-on-failure --no-compress-output --output-junit ../junit.xml

# coverage
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .

# clang-tidy : too long for now
#run-clang-tidy -checks='*' -header-filter=. -p build -j2 > clang-tidy-report

# cppcheck
cppcheck -v --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem --project=build/compile_commands.json . 2> cppcheck.xml

# print resulting file
cat sonar-project.properties

sonar-scanner --debug &> sonar.log
