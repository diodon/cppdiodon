FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y
RUN apt-get install git build-essential wget tar curl devscripts dh-make quilt pbuilder sbuild lintian svn-buildpackage git-buildpackage jo -y
RUN apt-get install gfortran cmake python-is-python3 pkg-config zlib1g-dev libbz2-dev libmkl-dev libhdf5-dev -y

ARG CI_COMMIT_TAG
ARG CI_PROJECT_ID
ARG CI_JOB_TOKEN
ARG DEBIANDIST=ubuntu_22.04
ARG DEBIANARCH="1_amd64"
ARG RELEASEURL=https://gitlab.inria.fr/compose/legacystack/fmr.git

# download fmr on the CI_COMMIT_TAG version
RUN git clone --recursive --branch $CI_COMMIT_TAG $RELEASEURL fmr-$CI_COMMIT_TAG

# make a source archive (without git files)
RUN wget https://raw.githubusercontent.com/Kentzo/git-archive-all/master/git_archive_all.py
RUN python3 git_archive_all.py -C fmr-$CI_COMMIT_TAG --force-submodules fmr-$CI_COMMIT_TAG.tar.gz

# upload the source package on gitlab
RUN curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./fmr-$CI_COMMIT_TAG.tar.gz "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/fmr-$CI_COMMIT_TAG.tar.gz"

# try to remove the release if it already exists
RUN curl --request DELETE --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG"

# generate the ubuntu package
RUN cp fmr-$CI_COMMIT_TAG.tar.gz fmr_$CI_COMMIT_TAG.orig.tar.gz
ENV LOGNAME=root
ENV DEBEMAIL=florent.pruvost@inria.fr
ENV DEB_BUILD_OPTIONS='nocheck'
RUN cd fmr-$CI_COMMIT_TAG/ && dh_make --library --yes
RUN cp fmr-$CI_COMMIT_TAG/Scripts/distrib/debian/changelog fmr-$CI_COMMIT_TAG/debian/
RUN cp fmr-$CI_COMMIT_TAG/Scripts/distrib/debian/control fmr-$CI_COMMIT_TAG/debian/
RUN cp fmr-$CI_COMMIT_TAG/Scripts/distrib/debian/copyright fmr-$CI_COMMIT_TAG/debian/
RUN cp fmr-$CI_COMMIT_TAG/Scripts/distrib/debian/rules fmr-$CI_COMMIT_TAG/debian/
RUN cd fmr-$CI_COMMIT_TAG/ && debuild -us -uc

# check the ubuntu package installation
RUN apt-get install -y ./fmr_$CI_COMMIT_TAG-$DEBIANARCH.deb

# upload on gitlab the ubuntu package
RUN curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./fmr_$CI_COMMIT_TAG-$DEBIANARCH.deb "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/$DEBIANDIST/$CI_COMMIT_TAG/fmr_$CI_COMMIT_TAG-$DEBIANARCH.deb"

# create the release and the associated tag
RUN CHANGELOG=`cat fmr-$CI_COMMIT_TAG/Scripts/distrib/debian/changeloglatest` && \
    ASSETSDATA="{ \"links\": [{ \"name\": \"fmr_$CI_COMMIT_TAG-$DEBIANARCH.deb\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/$DEBIANDIST/$CI_COMMIT_TAG/fmr_$CI_COMMIT_TAG-$DEBIANARCH.deb\" }, \
                              { \"name\": \"fmr-$CI_COMMIT_TAG.tar.gz\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/fmr-$CI_COMMIT_TAG.tar.gz\" }]}" && \
    CURLDATA=$(jo tag_name="$CI_COMMIT_TAG" description="$CHANGELOG" assets="$ASSETSDATA") && \
    echo $CURLDATA && \
    curl --header 'Content-Type: application/json' \
         --header "JOB-TOKEN: $CI_JOB_TOKEN" \
         --data "$CURLDATA" \
         --request POST "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases"
