#!/bin/bash
set -ex
[[ -d build ]] && rm build -rf
export CXXFLAGS="-O0 -g -fPIC --coverage -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wcomment -pedantic -fdiagnostics-show-option -fno-inline -fno-omit-frame-pointer"
cmake -B build \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=$PWD/install \
    -DCMAKE_BUILD_TYPE=Debug \
    -DFMR_BUILD_TESTS=ON \
    -DFMR_USE_HDF5=ON \
    -DFMR_USE_CHAMELEON=$1 \
    -DFMR_USE_MKL_AS_BLAS=$2 \
    -DFMR_USE_OPENBLAS_AS_BLAS=$3
cmake --build build > /dev/null
ctest --test-dir build --output-on-failure --no-compress-output --output-junit ../junit.xml
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .
