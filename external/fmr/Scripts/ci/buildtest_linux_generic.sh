#!/bin/bash
set -ex

# to avoid a lock during fetching branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

if [ "$BUILDNAME" = "lapack-mkl" ]; then
  guix time-machine --channels=./Scripts/ci/guix-channels.scm -- shell --pure --preserve=^CI -D fmr python-gcovr \
  -- /bin/bash --norc ./Scripts/ci/buildtest_linux.sh OFF ON OFF
elif [ "$BUILDNAME" = "lapack-openblas" ]; then
  guix time-machine --channels=./Scripts/ci/guix-channels.scm -- shell --pure --preserve=^CI -D fmr --with-input=intel-oneapi-mkl=openblas python-gcovr \
  -- /bin/bash --norc ./Scripts/ci/buildtest_linux.sh OFF OFF ON
elif [ "$BUILDNAME" = "chameleon-mkl-mpi" ]; then
  guix time-machine --channels=./Scripts/ci/guix-channels.scm -- shell --pure --preserve=^CI -D fmr-mpi --without-tests=hdf5-parallel-openmpi openssh python-gcovr \
  -- /bin/bash --norc ./Scripts/ci/buildtest_linux.sh ON ON OFF
elif [ "$BUILDNAME" = "chameleon-openblas-mpi" ]; then
  guix time-machine --channels=./Scripts/ci/guix-channels.scm -- shell --pure --preserve=^CI -D fmr-mpi --with-input=chameleon-mkl-mt=chameleon --with-input=intel-oneapi-mkl=openblas python-gcovr --without-tests=hdf5-parallel-openmpi openssh \
  -- /bin/bash --norc ./Scripts/ci/buildtest_linux.sh ON OFF ON
fi

# clean tmp
rm -rf /tmp/guix-$$
