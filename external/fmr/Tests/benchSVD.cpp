/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <fstream>
#include <iostream>
#ifdef _OPENMP
#include <omp.h>
#endif
// Includes
#include "fmr/Utils/ParameterNames.hpp"
#include "fmr/Utils/Parameters.hpp"
// for full SVD
//#include "fmr/StandardLRA/fullSVD.hpp"
#include "fmr/StandardLRA/SVD.hpp"

// for generating random matrices
#include "fmr/Utils/RandomMatrix.hpp"
// Wrapper for matrix
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"

////  GNU compiler
///  g++-9 -I ../include -DBLAS_NOCHANGE -DFMR_MKL_RANDOM  -fopenmp benchSVD.cpp
///  -o benchSVD -L${MKLROOT}/lib -lmkl_sequential -lmkl_intel_lp64 -lmkl_core
//
////  intel compiler
///  icpc  -I ../include -DBLAS_NOCHANGE -DFMR_MKL_RANDOM  -fopenmp benchSVD.cpp
///  -o benchSVD -L${MKLROOT}/lib -lmkl_sequential -lmkl_intel_lp64 -lmkl_core

/// ./benchSVD -nbRows 100 -nbCols 30 -t 2
//
static const ParameterNames NbRepetition = {
    {"-nbr", "-nbrepetitions"}, "Number of times we perform the bench."};
static const ParameterNames OutputFile = {
    {"-fout", "--output-filename"},
    "To give the output filename to add times."};

std::string get_binding_str(int type) {
  switch (type) {
  case 0:
    return std::string("false");
  case 1:
    return std::string("true");
  case 2:
    return std::string("master");
  case 3:
    return std::string("close");
  case 4:
    return std::string("spread");
  }
}

int main(int argc, char *argv[]) {
  HelpDescribeAndExit(
      argc, argv, "Test the randomized SVD on a matrix defined by blocks.",
      fmr::param::EnabledVerbose, fmr::param::NbRows, fmr::param::NbCols,
      fmr::param::NbThreads, OutputFile, NbRepetition);
  // Define type
  using value_type = float;
  // using  DenseMatrix_type = BlasDenseWrapper<std::int64_t,value_type> ;
  using DenseMatrix_type = fmr::BlasDenseWrapper<int, value_type>;
  //

  //
  const int nb_rows =
      fmr::param::getValue(argc, argv, fmr::param::NbRows.options, 10);
  const int nb_cols = fmr::param::getValue(argc, argv, fmr::param::
                                               : NbCols.options, 10);
  const int nb_rep = fmr::param::getValue(argc, argv, NbRepetition.options, 2);
  int nb_threads =
      fmr::param::getValue(argc, argv, fmr::para::NbThreads.options, 1);

  std::cout << "Matrix configuration :\n";
  std::cout << "     number of rows: " << nb_rows << "\n";
  std::cout << "     number of col:  " << nb_cols << "\n";
  std::cout << "Parallel configuration :\n";
#ifdef _OPENMP
  omp_set_num_threads(nb_threads);
  auto binding = omp_get_proc_bind();
///
#pragma omp parallel
#pragma omp single
  nb_threads = omp_get_num_threads();
#else
  nb_threads = 1;
  integer binding = 0;
#endif
  std::cout << "     number of threads:  " << nb_threads << "\n";

  Tic timer, timeRand;
  DenseMatrix_type matrix(nb_rows, nb_cols);
  //
  std::cout << "Generate random matrix  ... ";
  timeRand.tic();
  fmr::tools::RandomClass<value_type> randGen;
  randGen.buildMatrix(matrix);
  timeRand.tac();
  std::cout << " time: " << timeRand.elapsed() << " sec. " << std::endl;
  auto min_size = std::min(nb_rows, nb_cols);
  value_type *ApproxSingularValues = new value_type[min_size];

  DenseMatrix_type *U = nullptr;
  DenseMatrix_type *VT = nullptr; // If we end up needing to truncate the SVD
  double time_min = 1000000.0;
  double time_max = 0.0;
  double tmp = 0.0;

  // The first iteration allocates U and VT !!!
  for (int iter = 0; iter < nb_rep; ++iter) {
    std::cout << "Compute the svd factorization " << iter << " ..."; // \n";
    timer.tic();
    SVD<value_type, int>::computeSVD(matrix, ApproxSingularValues, U, VT);
    timer.tac();
    tmp = timer.elapsed();
    time_max = std::max(time_max, tmp);
    time_min = std::min(time_min, tmp);
    std::cout << " time: " << tmp << " sec. " << std::endl;
  }
  // std::cout <<"Time for the svd: " << timer.elapsed() << " sec\n";
  std::cout << "times (min) " << time_min << " (max) " << time_max
            << " average: " << timer.cumulated() / nb_rep << std::endl;
  time_max = std::max(time_max, tmp);

  const std::string out_file_name =
      Parameters::getStr(argc, argv, OutputFile.options, "time_svd.txt");
  std::fstream out(out_file_name, std::ios_base::out | std::ios_base::app);
  // out.open("time_svd.txt", std::ios_base::app)
  out << nb_rows << ", " << nb_cols << ", " << nb_threads << ", "
      << get_binding_str(binding) << ", " << time_min << ", " << time_max
      << ", " << timer.cumulated() / nb_rep << std::endl;

  return EXIT_SUCCESS;
}
