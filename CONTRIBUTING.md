# How to contribute

If you need new feature or bugs fixing you can create new issues.
An account on Gitlab is required to access to the repository and edit issues.
If you are not an Inria member please ask to an Inria colleague to create an account for you on this
[portal](https://external-account.inria.fr/).

If you want to directly contribute to the source code you can do it through [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) by forking the project.
Notice that if you are not an Inria member you are considered as an external user and you can't create project and thus unable to fork.
