# DIODON

Parallel C++ library for Multivariate Data Analysis of large datasets.
- Contains methods to compute Singular Value Decomposition (SVD), Randomized
  SVD/EVD, Principal Component Analysis (PCA), Multidimensional Scaling (MDS)
  and Correspondence Analysis (CoA).
- Handles text and hdf5 files.
- Parallel (mpi, threads, cuqda) randomized SVD and EVD (for symmetric matrices)
  provided by [FMR](https://gitlab.inria.fr/compose/legacystack/fmr).
- Use multithreaded Lapack or
  [Chameleon](https://gitlab.inria.fr/solverstack/chameleon) (distributed
  systems + GPUs).

Main documentation is available online here: [doxygen
documentation](https://diodon.gitlabpages.inria.fr/cppdiodon/).

A [python
wrapper](https://gitlab.inria.fr/diodon/cppdiodon/-/blob/master/python/README.md)
is also furnished for the single-node version (without MPI).

## Install

Detailed instructions are given in the
[documentation](https://diodon.gitlabpages.inria.fr/cppdiodon/index.html#installation).

Here is an example on Ubuntu 24.04 LTS by downloading the latest release binary
package
```sh
sudo apt update -y
sudo apt install wget -y
wget https://gitlab.inria.fr/api/v4/projects/27035/packages/generic/ubuntu_24.04/0.4/cppdiodon_0.4-1_amd64.deb
sudo apt-get install -y ./cppdiodon_0.4-1_amd64.deb

# you can see all whate have been installed with
dpkg -L cppdiodon

# use mds executable
mds -if ~/atlas_guyane_trnH.h5 -ids 'distance' -of -ncs 3 -svdm rsvd -r 1000

# to uninstall
sudo apt remove cppdiodon
```

To build the last version from the git repository follow these steps
```sh
sudo apt update -y
sudo apt install -y git cmake build-essential zlib1g-dev libbz2-dev gfortran pkg-config python-is-python3 libmkl-dev libhdf5-dev

# configure, build and install with cmake

git clone --recursive https://gitlab.inria.fr/diodon/cppdiodon.git
cd cppdiodon
mkdir -p build && cd build
cmake .. -DDIODON_USE_MKL_AS_BLAS=ON
make -j5
sudo make install
```

## Using

See detailed instructions in the
[documentation](https://diodon.gitlabpages.inria.fr/cppdiodon/index.html#usage_executables).

```sh
# some data for testing
git clone https://gitlab.inria.fr/diodon/data4tests.git

# compute a pca
# -if to set the input file (hdf5 format by default)
# -ids the dataset to read
# -of is used to save the results in a file
pca -if data4tests/pca_template_nonames.h5 -ids values -of

# compute a mds using a randomized svd (rsvd) with prescribed rank (-r) 500
# -t to set the number of threads
mds -if data4tests/atlas_guyane_trnH.h5 -ids distance -of -svdm rsvd -r 500 -t 4

# see also svd, coa executables, cf. the documentation
```
