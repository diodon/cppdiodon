/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file mds.cpp
 *
 * In this file we load a (symmetric) matrix from a file. Then we compute the
 * (approximate) eigenvalues and eigenvectors of C using a randomized SVD.
 *
 * Authors:
 *     Olivier Coulaud (olivier.coulaud@inria.fr)
 *     Florent Pruvost (florent.pruvost@inria.fr)
 *
 * Date created: April 9th, 2017
 *
 * Example : Points are stored in hdf5 format src/mds  -if
 *   ../data/atlas_guyane_trnH.h5 -rank 20 -of
 *
 */
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
#include <fmr/Utils/openblas_cblas.h>
#endif
#include <omp.h>
////////////////////////////////////////////////////////////////////
///            FMR Includes
// Definitions
#include "fmr/Algorithms/randomSVD.hpp"
// Matrix wrapper
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
//
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
#include "fmr/Utils/Parameters.hpp"
#include "fmr/Utils/Tic.hpp"
#include "fmr/Utils/MatrixIO.hpp"
///
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
///               Diodon includes
#include "diodon.hpp"
///
////////////////////////////////////////////////////////////////////

#define FMR_API

static const fmr::param::ParameterNames InputFileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file."
};
static const fmr::param::ParameterNames InputFileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames InputFileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames InputDataSetName = {
    {"-ids", "--input-dataset-name", "-idasetname"},
    "To give a specific (default is 'distance') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames ColTags = {
    {"-ict", "--input-coltags", "-icoltags"},
    "Use it to enable read columns tags in the file."
};
static const fmr::param::ParameterNames RowTags = {
    {"-irt", "--input-rowtags", "-irowtags"},
    "Use it to enable read rows tags in the file."
};
static const fmr::param::ParameterNames InputColTagsDataSetName = {
    {"-ictds", "--input-column-tags-dataset-name", "-ictdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to read."
};
static const fmr::param::ParameterNames InputRowTagsDataSetName = {
    {"-irtds", "--input-row-tags-dataset-name", "-irtdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to read."
};
static const fmr::param::ParameterNames OutputFile = {
    {"-of", "--output-file", "-ocfile"},
    "Use it to enable MDS results saving in file(s) (default is inputfilename.mds.h5), else use options -ocf, -oef, -off, etc."
};
static const fmr::param::ParameterNames OutputCloudFileName = {
    {"-ocf", "--output-cloud-filename", "-ocfilename"},
    "To give the output filename for the points cloud (default is mds.h5)."
};
static const fmr::param::ParameterNames OutputEigenFileName = {
    {"-oef", "--output-eigen-filename", "-oefilename"},
    "To give the output filename for eigen values (default is mds.h5)."
};
static const fmr::param::ParameterNames OutputFileFormat = {
    {"-off", "--output-fileformat", "-ofileformat"},
    "Format of the file, can be h5, csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames OutputFileDelim = {
    {"-ofd", "--output-filedelim", "-ofiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames OutputCloudDataSetName = {
    {"-ocds", "--output-cloud-dataset-name", "-ocdasetname"},
    "To give a specific (default is 'points') dataset name to write points cloud to the hdf5 file (H5)."
};
static const fmr::param::ParameterNames OutputEigenDataSetName = {
    {"-oeds", "--output-eigen-dataset-name", "-oedasetname"},
    "To give a specific (default is 'eigen') dataset name to write eigen values to the hdf5 file (H5)."
};
static const fmr::param::ParameterNames OutputColTagsDataSetName = {
    {"-octds", "--output-column-tags-dataset-name", "-octdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to write."
};
static const fmr::param::ParameterNames OutputRowTagsDataSetName = {
    {"-ortds", "--output-row-tags-dataset-name", "-ortdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to write."
};
static const fmr::param::ParameterNames NColsToSave = {
    {"-ncs", "--number-cols-to-save", "-ncolstosave"},
    "To restrict the number of matrix columns to save."
};
static const fmr::param::ParameterNames NDigitsToSave = {
    {"-nds", "--number-digits-to-save", "-ndigitstosave"},
    "To restrict the number of digits to save."
};
static const fmr::param::ParameterNames GenerateRandomDataset = {
    {"-gen", "--generate-random"},
    "To generate a random distance matrix instead of reading the data from file."
};
static const fmr::param::ParameterNames RandomDatasetSize = {
    {"-rs", "--random-size"},
    "Given size of the random matrix to generate."
};
static const fmr::param::ParameterNames RandomDatasetRank = {
    {"-rk", "--random-rank"},
    "Given rank of the random matrix to generate."
};
static const fmr::param::ParameterNames SVDMethod = {
    {"-svdm", "--svd-method"}, "Define the method to use for the SVD: rsvd (RandomSVD), svd (SVD)"
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"
};
static const fmr::param::ParameterNames UseBlas = {
    {"-b", "--blas"}, "Use BlasDenseWrapper instead of ChameleonDenseWrapper"
};
static const fmr::param::ParameterNames NbGPUs = {
    {"-g", "--nbgpus"} ,
     "To choose the number of CUDA devices (GPUs), 0 by default. Only available with Chameleon+StarPU."
};
static const fmr::param::ParameterNames IsSym = {
    {"-s", "--sym"} ,
     "To store the input matrix as symmetric."
};

/**
 * @brief Compute the MDS and save the matrix of coordinates and singular values
 * in a file
 *
 * Compute the MDS from a distance matrix, which inlcude building the Gram
 * matrix, computing the actual mds with the specified method, and building the
 * matrix of coordinates.
 *
 * Examples : ./src/mds -if ../data/atlas_guyane_trnH.h5 -r 200

 * To save the points and the eigenvalues in H5 format add to the previous
 *     command -of filename -off fileformat ./src/mds -if
 *     ../data/atlas_guyane_trnH.h5 -r 200 -of
 *
 * @param[in] argc number of arguments of the main program
 * @param[in] argv arguments of the main program
 *
 */
template<class DenseWrapperClass>
int mdsMain(int argc, char *argv[]) {

    ////////////////////////////////////////////////////////////////////
    ///
    /// Input Matrix parameters
    ///
    ////////////////////////////////////////////////////////////////////

    using Real_t = typename DenseWrapperClass::value_type;
    using Size_t = typename DenseWrapperClass::int_type;

#ifdef DIODON_SPECIFIC_ARGS
    /* choose to generate a random distance matrix of given rank k */
    const bool generateDataset = fmr::param::getValue(argc, argv, GenerateRandomDataset.options, false);
    const unsigned int RandomDatasetDim = fmr::param::getValue(argc, argv, RandomDatasetSize.options, 1000);
    const unsigned int RandomDatasetRk = fmr::param::getValue(argc, argv, RandomDatasetRank.options, RandomDatasetDim / 2);
#else
    const bool generateDataset = false;
    const unsigned int RandomDatasetDim = 0;
    const unsigned int RandomDatasetRk = 0;
#endif
    /* input matrix filename and datasetname */
    const std::string infilename = fmr::param::getStr(argc, argv, InputFileName.options, "");
    const std::string infileformat = fmr::param::getStr( argc, argv, InputFileFormat.options, "h5");
    const std::string infiledelim = fmr::param::getStr( argc, argv, InputFileDelim.options, " ");
    const std::string indatasetname = fmr::param::getStr(argc, argv, InputDataSetName.options, "distance");
    const std::string incolsdatasetname = fmr::param::getStr(argc, argv, InputColTagsDataSetName.options, "colnames");
    const bool coltags = fmr::param::existParameter(argc, argv, ColTags.options);
    const std::string inrowsdatasetname = fmr::param::getStr(argc, argv, InputRowTagsDataSetName.options, "rownames");
    const bool rowtags = fmr::param::existParameter(argc, argv, RowTags.options);

    /* column tags to read in file if required (see coltags and rowtags) */
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;

    /* check arguments coherency: either read data from a file or generate a
    random distance matrix */
    if ( !generateDataset ) {
        if ( infilename.size() == 0 ) {
            std::cerr << "[diodon] The filename is empty, please give a "
            "filename see -if flag or use -gen with the testmds "
            "executable to let the program generate a random dataset." << std::endl;
            return EXIT_FAILURE;

        };
    }

    /* number of threads used for chameleon and multi-threaded blas */
    const unsigned int nbThreads = fmr::param::getValue(argc, argv, fmr::param::NbThreads.options, 2);

    /* number of cuda devices (gpus) used for chameleon */
    const unsigned int nbGPUs = fmr::param::getValue(argc, argv, NbGPUs.options, 0);

    /* consider the matrix as symmetric */
    const unsigned int symInt = fmr::param::getValue(argc, argv, IsSym.options, 0);
    bool isSym;
    if ( symInt == 0 ){
        isSym = false;
    } else {
        isSym = true;
    }

    diodon::method::attributes parameters;
    parameters.setDataInputName(infilename);

    /* Set number of OMP threads */
    omp_set_num_threads(nbThreads);
#ifdef DIODON_USE_MKL_AS_BLAS
    mkl_set_num_threads(nbThreads);
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
    openblas_set_num_threads(nbThreads);
#endif

    /* Decide to time or not, synchronisations with MPI to get fair timing */
    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    /* Rank of MPI process, set to 0 if MPI not used */
    int mpiRank=0;
#ifdef DIODON_USE_CHAMELEON
    Chameleon::Chameleon chameleon(nbThreads, nbGPUs);
    mpiRank = chameleon.getMpiRank();
#endif
    bool masterIO = mpiRank==0;

    fmr::tools::Tic timeMDS;
    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeMDS.tic();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                Read The data set and store the distance matrix in
    ///                distanceMat
    ///
    ///
    //////////////////////////////////////////////////////////////////////////////////////////////////

    fmr::tools::Tic timeRead;
    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeRead.tic();
    }

    /* Declare distanceMat */
    DenseWrapperClass distanceMat(0, 0, isSym);

    /* Fill distanceMat with random values or reading one or several hdf5 files */
    if (generateDataset){
        diodon::io::genRandomMatrixRank(distanceMat, RandomDatasetDim, RandomDatasetDim, RandomDatasetRk);
    } else {
        diodon::io::loadFile(distanceMat, infilename, infileformat, infiledelim, indatasetname,
                             coltags, colnames, incolsdatasetname,
                             rowtags, rownames, inrowsdatasetname);
    }
    Size_t nbRows = distanceMat.getNbRows();
    Size_t nbCols = distanceMat.getNbRows();

    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timeRead.tac();
        if (masterIO) std::cout << "[diodon] TIME:MDS:BUILDMAT=" << timeRead.elapsed() << " s" << "\n";
    }
    const int verbose = fmr::param::getValue(argc, argv,
      fmr::param::EnabledVerbose.options, 0);
    if (masterIO && verbose) {
        std::cout << "[diodon] End of loading dataset" << std::endl;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///   MDS BEGIN
    ///
    /////////////////////////////////////////////////////////////////////////////////////////////////


    /* Get the FMR parameters */
    const Size_t gridSize = nbRows;
    /* prescribed rank */
    //int default_rank = generateDataset ? RandomDatasetRk : gridSize / 10;
    const string svdMethod = fmr::param::getStr(argc, argv, SVDMethod.options, "svd");
    int prescribed_rank = generateDataset ?
        RandomDatasetRk :
        fmr::param::getValue(argc, argv,
                             fmr::param::MatrixRank.options, nbCols/10);
#ifdef DIODON_SPECIFIC_ARGS
    // randomized range finder (0: fixed rank; 1: fixed accuracy)
    //const int rangefindermethod = fmr::param::getValue(
    //    argc, argv, fmr::param::RangeFinderMethod.options, 0);
    int oversampling = fmr::param::getValue(
        argc, argv, fmr::param::OverSampling.options, 5);
    const int qRSI = fmr::param::getValue(
        argc, argv, fmr::param::PowerIterations.options, 0);
    /* prescribed accuracy and balance parameter */
    const Real_t prescribed_eps = fmr::param::getValue(
        argc, argv, fmr::param::PrescribedAccuracy.options, -0.01);
    const int bARRF = fmr::param::getValue(
        argc, argv, fmr::param::BalanceParameter.options, 10);
    /* DO NOT CONSIDER compression rates below 10%! */
    const int maxRank = fmr::param::getValue(
        argc, argv, fmr::param::MaximumRank.options, gridSize / 10);
#else
    int oversampling = 5;
    const int qRSI = 0;
    const Real_t prescribed_eps = -0.01;
    const int bARRF = 10;
    const int maxRank = gridSize / 10;
#endif

    /* ensure r > 0 */
    prescribed_rank = (prescribed_rank == 0) ? (int)nbCols : prescribed_rank;
    /* ensure r <= N */
    prescribed_rank = (prescribed_rank > (int)nbCols) ? (int)nbCols : prescribed_rank;
    /* ensure oversampling <= N - r */
    oversampling = (oversampling + 1 > (int)nbCols) ? (int)nbCols - prescribed_rank : oversampling;
    /* ensure r + oversampling <= N */
    prescribed_rank = (prescribed_rank + oversampling > (int)nbCols) ? (int)nbCols - oversampling : prescribed_rank;

    const int displaySize = (prescribed_rank < 10) ? prescribed_rank : 10;

    /* Display parameters */
    if (masterIO) {
        std::string wrapperName = "BlasDenseWrapper";
#ifdef DIODON_USE_CHAMELEON
        const bool uBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
        if (!uBlas){
            wrapperName = "ChameleonDenseWrapper";
        }
#endif
        if (verbose) {
            std::cout << "[diodon] Verbose: " << verbose << std::endl;
            std::cout << "[diodon] Real: " << (sizeof(Real_t) == 4 ? "float" : "double") << std::endl;
            std::cout << "[diodon] N: " << nbRows << std::endl;
            std::cout << "[diodon] Threads: " << nbThreads << std::endl;
            std::cout << "[diodon] Wrapper: " << wrapperName << std::endl;
#ifdef DIODON_USE_CHAMELEON
            std::cout << "[diodon] MPI: " << chameleon.getMpiNp() << std::endl;
            std::cout << "[diodon] Tile: " << chameleon.getTileSize() << std::endl;
#endif
        }
    }

    /* Fill the FMR specific additional parameters struct */
    struct diodon::method::fmrArgs<Real_t> fmrargs;
    fmrargs.eps = prescribed_eps;
    fmrargs.rank = prescribed_rank;
    fmrargs.oversampling = oversampling;
    fmrargs.powerIterations = qRSI;
    fmrargs.bARRF = bARRF;
    fmrargs.maxRank = maxRank;
    fmrargs.parameters = &parameters;
    fmrargs.isTimed = isTimed;
    DenseWrapperClass X;
    std::vector<Real_t> lambda;

    /* Perform the MDS */
    diodon::method::mds(distanceMat, X, lambda, fmrargs, svdMethod, verbose);

    if (masterIO){
        /* Show eigen values and vectors */
        if (verbose) {
            fmr::Display::vector(lambda.size(), lambda.data(), "Eigen values: ", displaySize, 1);
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///   MDS ENDS
    ///
    /////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///   SAVING RESULTS
    ///
    /////////////////////////////////////////////////////////////////////////////////////////////////
    const std::string outfileformat = fmr::param::getStr( argc, argv, OutputFileFormat.options, infileformat.c_str());
    const std::string outfiledelim = fmr::param::getStr( argc, argv, OutputFileDelim.options, infiledelim.c_str());
    const std::string outcolsdatasetname = fmr::param::getStr(argc, argv, OutputColTagsDataSetName.options, incolsdatasetname.c_str());
    const std::string outrowsdatasetname = fmr::param::getStr(argc, argv, OutputRowTagsDataSetName.options, inrowsdatasetname.c_str());

    /* Save points cloud and eigen values */
    if ( fmr::param::existParameter(argc, argv, OutputFile.options) ) {

        fmr::tools::Tic timeWrite;
        if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
            Chameleon::barrier();
#endif
            timeWrite.tic();
        }

        /* to find output filename default root */
        const size_t lastindex = infilename.find_last_of(".");

        std::string filename;
        if ( outfileformat == "h5" ) {
            /* save cloud and eigen in the same file */
            filename = infilename.substr(0, lastindex) + ".mds.h5";
        } else {
            /* save cloud and eigen in different files */
            filename = infilename.substr(0, lastindex) + "_cloud.mds." + outfileformat;
        }

        const std::string cloudfilename = fmr::param::getStr(argc, argv, OutputCloudFileName.options, filename.c_str());
        const std::string clouddatasetname = fmr::param::getStr(argc, argv, OutputCloudDataSetName.options, "points");

        /* 0 means we save all meaningfull values */
        const int ndigitstosave = fmr::param::getValue(argc, argv, NDigitsToSave.options, 0);
        /* 0 means we save all columns of the cloud X */
        const size_t ncolstosave = fmr::param::getValue(argc, argv, NColsToSave.options, 0);

        if (masterIO){
            std::cout << "[diodon] Save points cloud in file " << cloudfilename << " in format " << outfileformat << std::endl;
        }

        /* write the matrix X, point cloud, to file */
        diodon::io::writeFile(X, cloudfilename, outfileformat,
                              outfiledelim, clouddatasetname, ncolstosave,
                              false, colnames, outcolsdatasetname,
                              rowtags, rownames, outrowsdatasetname,
                              ndigitstosave);

        if ( outfileformat == "h5" ) {
            /* save additional parameters such as the internal svd method used */
            diodon::io::writeFileH5Att(parameters, cloudfilename, clouddatasetname);
        }

        if ( outfileformat == "h5" ) {
            /* save cloud and eigen in the same file */
            filename = cloudfilename;
        } else {
            /* save cloud and eigen in different files */
            filename = infilename.substr(0, lastindex) + "_eigenvalues.mds." + outfileformat;
        }

        const std::string eigenfilename = fmr::param::getStr(argc, argv, OutputEigenFileName.options, filename.c_str());
        const std::string eigendatasetname = fmr::param::getStr(argc, argv, OutputEigenDataSetName.options, "eigenvalues");

        if (masterIO){
            std::cout << "[diodon] Save eigen values in file " << eigenfilename << " in format " << outfileformat << std::endl;
        }

        /* write the vector lambda, eigen values, to file */
        diodon::io::writeFile(lambda, eigenfilename, outfileformat,
                              " ", eigendatasetname, 0,
                              false, colnames, outcolsdatasetname,
                              false, rownames, outrowsdatasetname,
                              ndigitstosave);

        if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
            Chameleon::barrier();
#endif
            timeWrite.tac();
            if (masterIO) std::cout << "[diodon] TIME:MDS:SAVE:DATA=" << timeWrite.elapsed() << " s" << "\n";
        }
    }

    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeMDS.tac();
        if (masterIO) std::cout << "[diodon] TIME:MDS=" << timeMDS.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

int main (int argc, char **argv) {

    using INT = unsigned long long int;

    ////////////////////////////////////////////////////////////////////
    ///
    /// Help and description
    ///
    ////////////////////////////////////////////////////////////////////

    HelpDescribeAndExit(
        argc, argv, "MDS driver - Reads a distance matrix from file and compute the MDS.\n\
        The data can be given from file, with the flag -if (e.g. -if ~/file.h5), in the following formats :\n\
        .csv (ASCII), .txt (ASCII), .gz (zlib), .bz2 (bzip2), .h5 (HDF5).\n\
        The input file format can be informed with -iff (e.g. -iff txt, or csv, gz, bz2, h5).\n\
        For the text formats (csv, txt, gz, bz2), the data delimiters must be given with -ifd (e.g. -ifd ';', -ifd ' ', -ifd $'\\t').\n\
        For the hdf5 files, the dataset name must be given with the flag -ids (e.g. -ids 'distance').\n\
        The results, positive eigen values L as well as a matrix of coordinates X, can be saved into files with the flag -of.\n\
        By default the path and name of the output file is the same as the input file but with the name of the method used added as a suffix.\n\
        The number of columns to save for X can be selected with the flag -ncs (e.g. -ncs 2).\n\
        The default method for SVD (-svdm rsvd) use a randomized SVD and requires to give an estimation of the input matrix rank with the flag -r (e.g. -r 42).\n\
        Otherwise it will select r=1/10 the size of the input matrix as a default value.\n\
        There is also a full SVD method (-svdm svd), more costly.\n\
        The number of threads to use is set with -t (e.g. -t 2, default=2).\n\
        Examples:\n\
        mds -if file.h5 -ids 'distance' -of -ncs 3 -r 300 \n\
        mds -if file.csv -iff csv -ifd ';' -of -ncs 3 -r 300\n\
        ",
        InputFileName,
        InputFileFormat,
        InputFileDelim,
        InputDataSetName,
        ColTags,
        InputColTagsDataSetName,
        RowTags,
        InputRowTagsDataSetName,
        OutputFile,
        OutputCloudFileName,
        OutputEigenFileName,
        OutputFileFormat,
        OutputFileDelim,
        OutputCloudDataSetName,
        OutputEigenDataSetName,
        OutputColTagsDataSetName,
        OutputRowTagsDataSetName,
        NColsToSave,
        NDigitsToSave,
#ifdef DIODON_SPECIFIC_ARGS
        GenerateRandomDataset,
        RandomDatasetSize,
        RandomDatasetRank,
#endif
        SVDMethod,
        // Next parameters come from FMR (random SVD)
        fmr::param::MatrixRank,
#ifdef DIODON_SPECIFIC_ARGS
        fmr::param::RangeFinderMethod,
        fmr::param::OverSampling,
        fmr::param::PowerIterations,
        fmr::param::PrescribedAccuracy,
        /*fmr::param::PrescribedMagnitudeAccuracy, */
        fmr::param::BalanceParameter,
        fmr::param::MaximumRank,
#endif
        UseDouble,
        UseBlas,
        fmr::param::NbThreads,
        NbGPUs,
        fmr::param::EnabledVerbose,
        IsSym
        );
    const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

#ifdef DIODON_USE_CHAMELEON
    const bool useBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
    if ( useBlas )
        if ( RealDouble ) {
            mdsMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
        } else {
            mdsMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
        }
    else {
        if ( RealDouble ) {
            mdsMain<fmr::ChameleonDenseWrapper<INT, double>>(argc, argv);
        } else {
            mdsMain<fmr::ChameleonDenseWrapper<INT, float>>(argc, argv);
        }
    }
#else
    if ( RealDouble ) {
        mdsMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
    } else {
        mdsMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
    }
#endif

    return EXIT_SUCCESS;
}
