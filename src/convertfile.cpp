/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file convertfile.cpp
 *
 * In this file we load a matrix from a file and save it in another file with a
 * chosen extension.
 *
 * Authors: Florent Pruvost (florent.pruvost@inria.fr)
 *
 * Date created: January 14th, 2022
 *
 * Example : Input data are stored in data.txt and must be stored in hdf5 file data.h5
 * ./src/convertfile -if data.txt -iff txt -ifd " " -of data.h5
 *
 */
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
#include <fmr/Utils/openblas_cblas.h>
#endif
#include <omp.h>
////////////////////////////////////////////////////////////////////
///            FMR Includes
// Definitions
#include "fmr/Algorithms/randomSVD.hpp"
// Matrix wrapper
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
//
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
#include "fmr/Utils/Parameters.hpp"
#include "fmr/Utils/Tic.hpp"
#include "fmr/Utils/MatrixIO.hpp"
///
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
///               Diodon includes
#include "diodon.hpp"
///
////////////////////////////////////////////////////////////////////

#define FMR_API

static const fmr::param::ParameterNames InputFileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file (default input.h5)."
};
static const fmr::param::ParameterNames InputFileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames InputFileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames InputDataSetName = {
    {"-ids", "--input-dataset-name", "-idasetname"},
    "To give a specific (default is 'data') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames ColTags = {
    {"-ict", "--input-coltags", "-icoltags"},
    "Use it to enable read columns tags in the file."
};
static const fmr::param::ParameterNames RowTags = {
    {"-irt", "--input-rowtags", "-irowtags"},
    "Use it to enable read rows tags in the file."
};
static const fmr::param::ParameterNames InputColTagsDataSetName = {
    {"-ictds", "--input-column-tags-dataset-name", "-ictdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to read."
};
static const fmr::param::ParameterNames InputRowTagsDataSetName = {
    {"-irtds", "--input-row-tags-dataset-name", "-irtdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to read."
};
static const fmr::param::ParameterNames OutputFileName = {
    {"-of", "--output-filename", "-ofilename"},
    "The filename where to save data (default output.h5)."
};
static const fmr::param::ParameterNames OutputFileFormat = {
    {"-off", "--output-fileformat", "-ofileformat"},
    "Format of the file, can be h5, csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames OutputFileDelim = {
    {"-ofd", "--output-filedelim", "-ofiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames OutputDataSetName = {
    {"-ods", "--output-dataset-name", "-odasetname"},
    "To give a specific (default is 'data') dataset name to write data to the hdf5 file (H5)."
};
static const fmr::param::ParameterNames OutputColTagsDataSetName = {
    {"-octds", "--output-column-tags-dataset-name", "-octdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to write."
};
static const fmr::param::ParameterNames OutputRowTagsDataSetName = {
    {"-ortds", "--output-row-tags-dataset-name", "-ortdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to write."
};
static const fmr::param::ParameterNames NColsToSave = {
    {"-ncs", "--number-cols-to-save", "-ncolstosave"},
    "To restrict the number of matrix columns to save."
};
static const fmr::param::ParameterNames NDigitsToSave = {
    {"-nds", "--number-digits-to-save", "-ndigitstosave"},
    "To restrict the number of digits to save."
};
static const fmr::param::ParameterNames SVDMethod = {
    {"-svdm", "--svd-method"}, "Define the method to use for the SVD: rsvd (RandomSVD), svd (SVD)"
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"
};
static const fmr::param::ParameterNames UseBlas = {
    {"-b", "--blas"}, "Use BlasDenseWrapper instead of ChameleonDenseWrapper"
};
/**
 * @brief Load data from file and save the matrix in a file in another extension
 * chosen by the user (default h5).
 *
 * Examples : ./src/convertfile -if data.txt -iff txt -ifd " " -of data.h5
 *
 * @param[in] argc number of arguments of the main program
 * @param[in] argv arguments of the main program
 *
 */
template<class DenseWrapperClass>
int convertfileMain(int argc, char *argv[]) {

    ////////////////////////////////////////////////////////////////////
    ///
    /// Input Matrix parameters
    ///
    ////////////////////////////////////////////////////////////////////

    using Size_t = typename DenseWrapperClass::int_type;

    /* input matrix filename and datasetname */
    const std::string infilename = fmr::param::getStr(argc, argv, InputFileName.options, "input.h5");
    const std::string infileformat = fmr::param::getStr( argc, argv, InputFileFormat.options, "h5");
    const std::string infiledelim = fmr::param::getStr( argc, argv, InputFileDelim.options, " ");
    const std::string indatasetname = fmr::param::getStr(argc, argv, InputDataSetName.options, "data");
    const std::string incolsdatasetname = fmr::param::getStr(argc, argv, InputColTagsDataSetName.options, "colnames");
    const bool coltags = fmr::param::existParameter(argc, argv, ColTags.options);
    const std::string inrowsdatasetname = fmr::param::getStr(argc, argv, InputRowTagsDataSetName.options, "rownames");
    const bool rowtags = fmr::param::existParameter(argc, argv, RowTags.options);

    /* column tags to read in file if required (see coltags and rowtags) */
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;

    /* check arguments coherency: either read data from a file or generate a
    random distance matrix */
    if ( infilename.size() == 0 ) {
        std::cerr << "[diodon] The filename is empty, please give a "
        "filename see -if flag." << std::endl;
        return EXIT_FAILURE;
    };

    /* number of threads used for chameleon and multi-threaded blas */
    const unsigned int nbThreads = fmr::param::getValue(argc, argv, fmr::param::NbThreads.options, 2);

    /* Set number of OMP threads */
    omp_set_num_threads(nbThreads);
#ifdef DIODON_USE_MKL_AS_BLAS
    mkl_set_num_threads(nbThreads);
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
    openblas_set_num_threads(nbThreads);
#endif

    /* Decide to time or not, synchronisations with MPI to get fair timing */
    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    /* Rank of MPI process, set to 0 if MPI not used */
    int mpiRank=0;
#ifdef DIODON_USE_CHAMELEON
    Chameleon::Chameleon chameleon(nbThreads);
    mpiRank = chameleon.getMpiRank();
#endif
    bool masterIO = mpiRank==0;

    fmr::tools::Tic timeConvert;
    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeConvert.tic();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                Read The data set and store the matrix in data
    ///
    //////////////////////////////////////////////////////////////////////////////////////////////////

    fmr::tools::Tic timeRead;
    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeRead.tic();
    }

    /* Declare data */
    DenseWrapperClass data;

    if (masterIO) {
        std::cout << "[diodon] Loading data from file" << std::endl;
        std::cout << "[diodon] Input file name: " << infilename << std::endl;
        std::cout << "[diodon] Input file format: " << infileformat << std::endl;
        std::cout << "[diodon] Input file delimiter (if txt or csv): " << infiledelim << std::endl;
        std::cout << "[diodon] Input file dataset name (if hdf5): " << indatasetname << std::endl;
    }
    /* Read data from file */
    diodon::io::loadFile(data, infilename, infileformat, infiledelim, indatasetname,
                         coltags, colnames, incolsdatasetname,
                         rowtags, rownames, inrowsdatasetname);

    Size_t nbRows = data.getNbRows();
    Size_t nbCols = data.getNbCols();
    if (masterIO) {
        std::cout << "[diodon] Data size: (" << nbRows << ", " << nbCols << ")" << std::endl;
    }

    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timeRead.tac();
        if (masterIO) std::cout << "[diodon] TIME:CONVERT:READ=" << timeRead.elapsed() << " s" << "\n";
    }
    if (masterIO) {
        std::cout << "[diodon] End of loading dataset" << std::endl;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///   Saving data
    ///
    /////////////////////////////////////////////////////////////////////////////////////////////////
    const std::string outfilename = fmr::param::getStr(argc, argv, OutputFileName.options, "output.h5");
    const std::string outfileformat = fmr::param::getStr( argc, argv, OutputFileFormat.options, infileformat.c_str());
    const std::string outfiledelim = fmr::param::getStr( argc, argv, OutputFileDelim.options, infiledelim.c_str());
    const std::string outdatasetname = fmr::param::getStr(argc, argv, OutputDataSetName.options, "data");
    const std::string outcolsdatasetname = fmr::param::getStr(argc, argv, OutputColTagsDataSetName.options, incolsdatasetname.c_str());
    const std::string outrowsdatasetname = fmr::param::getStr(argc, argv, OutputRowTagsDataSetName.options, inrowsdatasetname.c_str());

    /* Save points cloud and eigen values */
    fmr::tools::Tic timeWrite;
    if (isTimed) {
    #ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
    #endif
        timeWrite.tic();
    }


    /* 0 means we save all meaningfull values */
    const int ndigitstosave = fmr::param::getValue(argc, argv, NDigitsToSave.options, 0);
    /* 0 means we save all columns of the cloud Y */
    const size_t ncolstosave = fmr::param::getValue(argc, argv, NColsToSave.options, 0);

    if (masterIO) {
        std::cout << "[diodon] Save data in file" << std::endl;
        std::cout << "[diodon] Output file name: " << outfilename << std::endl;
        std::cout << "[diodon] Output file format: " << outfileformat << std::endl;
        std::cout << "[diodon] Output file delimiter (if txt or csv): " << outfiledelim << std::endl;
        std::cout << "[diodon] Output file dataset name (if hdf5): " << outdatasetname << std::endl;
    }

    /* write the matrix Y, point cloud, to file */
    diodon::io::writeFile(data, outfilename, outfileformat,
                          outfiledelim, outdatasetname, ncolstosave,
                          coltags, colnames, outcolsdatasetname,
                          rowtags, rownames, outrowsdatasetname,
                          ndigitstosave);

    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timeWrite.tac();
        if (masterIO) std::cout << "[diodon] TIME:CONVERT:SAVE:DATA=" << timeWrite.elapsed() << " s" << "\n";
    }

    if (isTimed) {
#if defined(CHAMELEON_USE_MPI)
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        timeConvert.tac();
        if (masterIO) std::cout << "[diodon] TIME:CONVERT=" << timeConvert.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

int main (int argc, char **argv) {

    using INT = unsigned long long int;

    ////////////////////////////////////////////////////////////////////
    ///
    /// Help and description
    ///
    ////////////////////////////////////////////////////////////////////

    HelpDescribeAndExit(
        argc, argv, "Convert file driver - Reads data from file and save it in another chosen format.\n\
        The data can be given from file, with the flag -if (e.g. -if ~/input.h5), in the following formats :\n\
        .csv (ASCII), .txt (ASCII), .gz (zlib), .bz2 (bzip2), .h5 (HDF5).\n\
        The input file format can be informed with -iff (e.g. -iff txt, or csv, gz, bz2, h5).\n\
        For the text formats (csv, txt, gz, bz2), the data delimiters must be given with -ifd (e.g. -ifd ';', -ifd ' ', -ifd $'\\t').\n\
        For the hdf5 files, the dataset name must be given with the flag -ids (e.g. -ids 'data').\n\
        The data is then saved into file name given with the flag -of (default output.h5).\n\
        The number of columns to save can be selected with the flag -ncs (e.g. -ncs 2).\n\
        The number of threads to use is set with -t (e.g. -t 2, default=2).\n\
        Examples:\n\
        convertfile -if file.h5 -ids 'data' -of file.csv -off csv -ofd ';' -ncs 3 \n\
        convertfile -if file.csv -iff csv -ifd ';' -of file.h5 -off h5 -ods 'data'\n\
        ",
        InputFileName,
        InputFileFormat,
        InputFileDelim,
        InputDataSetName,
        ColTags,
        InputColTagsDataSetName,
        RowTags,
        InputRowTagsDataSetName,
        OutputFileName,
        OutputFileFormat,
        OutputFileDelim,
        OutputDataSetName,
        OutputColTagsDataSetName,
        OutputRowTagsDataSetName,
        NColsToSave,
        NDigitsToSave,
        UseDouble,
        UseBlas,
        fmr::param::NbThreads,
        fmr::param::EnabledVerbose
        );
    const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

#ifdef DIODON_USE_CHAMELEON
    const bool useBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
    if ( useBlas )
        if ( RealDouble ) {
            convertfileMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
        } else {
            convertfileMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
        }
    else {
        if ( RealDouble ) {
            convertfileMain<fmr::ChameleonDenseWrapper<INT, double>>(argc, argv);
        } else {
            convertfileMain<fmr::ChameleonDenseWrapper<INT, float>>(argc, argv);
        }
    }
#else
    if ( RealDouble ) {
        convertfileMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
    } else {
        convertfileMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
    }
#endif

    return EXIT_SUCCESS;
}
