/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/**
 * @file example_pcamet_chameleon.cpp
 *
 */


#include "diodon.hpp"

/**
* \example example_pcamet_chameleon.cpp Example usage with Chameleon matrices
 * (not centralized, distributed in memory). We load a matrix A from a file.
 * Then we apply a Principal Component Analysis method on A (Y, V, L) to
 * characterize groups and save coordinates of the points clouds (Y) in a file
 * as well as eigen values (L) and eigen vectors (V).
 *
 * Your data A, M and Q should be stored in a input_pcamet.h5 hdf5 file with
*  dataset names 'A', 'M' and 'Q' respectively. Then
 *
 * Example usage: mpiexec -n 2 ./example_pcamet_chameleon
 *
 * It will generate points cloud coordinates and save in output_pcamet.h5 hdf5
*  file.
 *
 * You can then draw the 2d projection on the 2 highest magnitude eigen vectors
 * axis with : python3 ../../tools/post/nuage_png.py output_pcamet.h5
 *
 */

/**
 * @brief main program example
 * @param[in] argc number of arguments
 * @param[in] argv arguments
 */
int main (int argc, char **argv) {

    /* Initialize chameleon here with 2 threads for computation */
    Chameleon::Chameleon chameleon(2);

    /* Declare the input matrix (Diodon/FMR object) */
    fmr::ChameleonDenseWrapper<int, float> dataA;
    fmr::ChameleonDenseWrapper<int, float> dataM;
    fmr::ChameleonDenseWrapper<int, float> dataQ;

    /* Declare output matrix Y */
    fmr::ChameleonDenseWrapper<int, float> cloud;

    /* Declare output matrix V */
    fmr::ChameleonDenseWrapper<int, float> eigenvectors;

    /* Declare vector for eigen values */
    std::vector<float> eigenvalues;

    /* Fill data with values read from input_pca.h5 */
    const std::string filename = "input_pcamet.h5";
    diodon::io::loadFile(dataA, filename, "h5", "", "A");
    diodon::io::loadFile(dataM, filename, "h5", "", "M");
    diodon::io::loadFile(dataQ, filename, "h5", "", "Q");

    /* FMR arguments to be used for rsvd in the pca */
    struct diodon::method::fmrArgs<float> fmrargs;

    /* Call the PCA method on data */
    diodon::method::pcamet(dataA, dataM, dataQ, cloud, eigenvectors, eigenvalues, fmrargs, "rsvd", "standard");

    /* Save points cloud coordinates and eigen values/vectors in a hdf5 file */
    diodon::io::writeFile(cloud, "output_pcamet.h5", "h5", "", "points");
    diodon::io::writeFile(eigenvalues, "output_pcamet.h5", "h5", "", "eigenvalues");
    diodon::io::writeFile(eigenvectors, "output_pcamet.h5", "h5", "", "eigenvectors");

    return 0;
}
