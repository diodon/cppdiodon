/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/**
 * @file example_mds_chameleon.cpp
 *
 */


#include "diodon.hpp"

/**
* \example example_mds_chameleon.cpp Example usage with Chameleon matrices (not
 * centralized, distributed in memory). We load a (symmetric) matrix from a
 * file. Then we apply a Multidimensional Scaling method to characterize groups
 * and save coordinates of the points clouds in a file.
 *
 * Your data should be stored in a input_mds.h5 hdf5 file (hdf5 dataset name
 * must be 'distance'), then
 *
 * Example usage: mpiexec -n 2 ./example_mds_chameleon
 *
 * It will generate and save points cloud coordinates and eigen values in
 * output_mds.h5 hdf5 file.
 *
 * You can then draw the 2d projection on the 2 highest magnitude eigen vectors
 * axis with : python3 ../../tools/post/nuage_png.py output_mds.h5
 *
 */

/**
 * @brief main program example
 * @param[in] argc number of arguments
 * @param[in] argv arguments
 */
int main (int argc, char **argv) {

    /* Initialize chameleon here with 2 threads for computation */
    Chameleon::Chameleon chameleon(2);

    /* Declare the input matrix (Diodon/FMR object) */
    fmr::ChameleonDenseWrapper<int, float> data;

    /* Declare output matrix */
    fmr::ChameleonDenseWrapper<int, float> cloud;

    /* Declare vector for eigen values */
    std::vector<float> eigen;

    /* Fill data with values read from input_mds.h5 */
    const std::string filename = "input_mds.h5";
    diodon::io::loadFile(data, filename, "h5", "", "distance");

    /* FMR arguments to be used for rsvd in the mds */
    struct diodon::method::fmrArgs<float> fmrargs;

    /* Call the MDS method on data */
    diodon::method::mds(data, cloud, eigen, fmrargs, "rsvd");

    /* Save points cloud coordinates and eigen values in a hdf5 file */
    diodon::io::writeFile(cloud, "output_mds.h5", "h5", "", "points");
    diodon::io::writeFile(eigen, "output_mds.h5", "h5", "", "eigen");

    return 0;
}
