/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/**
 * @file example_svd_lapack.cpp
 *
 */

#include "diodon.hpp"

/**
 * @example example_svd_lapack.cpp Example usage with Lapack matrices
 * (centralized, shared memory). We load a matrix A from a file. Then we apply a
 * Singular Value Decomposition method on A (U, S, Vt) and save U, S and Vt in a
 * file.
 *
 * Your data should be stored in a input_svd.csv ascii file (with ; delimiters),
 * then
 *
 * ./example_svd_lapack
 *
 * It will save the SVD in U.csv, S.csv, Vt.csv ascii files.
 *
 */

/**
 * @brief main program example
 * @param[in] argc number of arguments
 * @param[in] argv arguments
 */
int main (int argc, char **argv) {

    /* Declare the input matrix (Diodon/FMR object) */
    fmr::BlasDenseWrapper<int, float> data;

    /* Declare output matrix U */
    fmr::BlasDenseWrapper<int, float> U;

    /* Declare output matrix Vt */
    fmr::BlasDenseWrapper<int, float> Vt;

    /* Declare vector for singular values */
    std::vector<float> S;

    /* Fill data with values read from input_svd.csv, values delimiter is ; */
    const std::string filename = "input_svd.csv";
    diodon::io::loadFile(data, filename, "csv", ";");

    /* FMR arguments to be used for rsvd in the svd */
    struct diodon::method::fmrArgs<float> fmrargs;

    /* Call the SVD method on data */
    diodon::method::svd(data, S, U, Vt, fmrargs, "rsvd");

    /* Save points cloud coordinates and eigen values in text files */
    diodon::io::writeFile(U, "U.csv", "csv", ";");
    diodon::io::writeFile(S, "S.csv", "csv", ";");
    diodon::io::writeFile(Vt, "Vt.csv", "csv", ";");

    return 0;
}
