/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/**
 * @file example_pcamet_lapack.cpp
 *
 */

#include "diodon.hpp"

/**
 * @example example_pcamet_lapack.cpp Example usage with Lapack matrices
 * (centralized, shared memory). We load a matrix A, plus metrics M and Q from
 * files. Then we apply a Principal Component Analysis method on A (Y, V, L) to
 * characterize groups and save coordinates of the points clouds (Y) in a file
 * as well as eigen values (L) and eigen vectors (V).
 *
 * Your data A should be stored in a input_pcamet_A.csv ascii file (with ;
 * delimiters), M and Q in input_pcamet_M.csv and input_pcamet_Q.csv
 * respectively. M and Q must be diagonal matrices stored as a vector. M of size
 * equal the number of rows in A and Q of size equal the number of columns in A.
 * Then
 *
 * ./example_pcamet_lapack
 *
 * It will generate points cloud coordinates and save in Y.csv, L.csv, V.csv
 * ascii files.
 *
 */

/**
 * @brief main program example
 * @param[in] argc number of arguments
 * @param[in] argv arguments
 */
int main (int argc, char **argv) {

    /* Declare the input matrix (Diodon/FMR object) */
    fmr::BlasDenseWrapper<int, float> dataA;
    fmr::BlasDenseWrapper<int, float> dataM;
    fmr::BlasDenseWrapper<int, float> dataQ;

    /* Declare output matrix Y */
    fmr::BlasDenseWrapper<int, float> cloud;

    /* Declare output matrix V */
    fmr::BlasDenseWrapper<int, float> eigenvectors;

    /* Declare vector for eigen values */
    std::vector<float> eigenvalues;

    /* Fill data with values read from input.csv, values delimiter is a space */
    diodon::io::loadFile(dataA, "input_pcamet_A.csv", "csv", ";");
    diodon::io::loadFile(dataM, "input_pcamet_M.csv", "csv", ";");
    diodon::io::loadFile(dataQ, "input_pcamet_Q.csv", "csv", ";");

    diodon::io::writeFile(dataA, "input_pcamet.h5", "h5", "", "A");
    diodon::io::writeFile(dataM, "input_pcamet.h5", "h5", "", "M");
    diodon::io::writeFile(dataQ, "input_pcamet.h5", "h5", "", "Q");

    /* FMR arguments to be used for rsvd in the pcamet */
    struct diodon::method::fmrArgs<float> fmrargs;

    /* Call the PCA method on data */
    diodon::method::pcamet(dataA, dataM, dataQ, cloud, eigenvectors, eigenvalues, fmrargs, "rsvd", "standard");

    /* Save points cloud coordinates, eigen values and eigen vectors in text files */
    diodon::io::writeFile(cloud, "Y.csv", "csv", ";");
    diodon::io::writeFile(eigenvalues, "L.csv", "csv", ";");
    diodon::io::writeFile(eigenvectors, "V.csv", "csv", ";");

    return 0;
}
