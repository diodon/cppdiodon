variables:
  GIT_SUBMODULE_STRATEGY: recursive

default:
  tags: ['guix', 'plafrim']
  interruptible: true

docker-ubuntu:
  stage: test
  tags: ['ci.inria.fr', 'linux']
  image: docker
  except:
    - schedules
    - tags
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -f tools/docker/dockerfile-ubuntu-$BUILDNAME -t $CI_REGISTRY_IMAGE/ubuntu/$BUILDNAME . | tee build.log
    - docker push $CI_REGISTRY_IMAGE/ubuntu/$BUILDNAME
  parallel:
    matrix:
      - BUILDNAME: ['lapack', 'chameleon', 'chameleonmpi']
  artifacts:
    untracked: true
  allow_failure: true

linux-sonar:
  stage: test
  except:
    - schedules
    - tags
  variables:
    GIT_DEPTH: 0
    MKL_NUM_THREADS: 2
    OMP_NUM_THREADS: 2
    OPENBLAS_NUM_THREADS: 2
  script:
    - ./tools/sonarqube/sonarqube.sh
  parallel:
    matrix:
      - BUILDNAME: ['chameleon-mkl-mpi']
  coverage: /^\s*lines:\s*\d+.\d+\%/
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    when: always
    paths:
      - sonar.log
      - sonar-project.properties
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: junit.xml

linux:
  stage: test
  except:
    - schedules
    - tags
  variables:
    MKL_NUM_THREADS: 2
    OMP_NUM_THREADS: 2
    OPENBLAS_NUM_THREADS: 2
  script:
    - ./tools/ci/buildtest_linux_generic.sh
  parallel:
    matrix:
      - BUILDNAME: ['lapack-mkl', 'lapack-openblas', 'chameleon-openblas-mpi']
  coverage: /^\s*lines:\s*\d+.\d+\%/
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: junit.xml

macosx:
  stage: test
  tags: ['macosx']
  except:
    - schedules
    - tags
  script:
    - ./tools/ci/buildtest_macosx.sh
  after_script:
    - xsltproc -o report.xml tools/ci/ctest-to-junit.xsl build/Testing/**/Test.xml
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    when: always
    reports:
      junit: junit.xml

pages:
  stage: test
  except:
    - schedules
    - tags
  variables:
    BUILDNAME: pages
  artifacts:
    expire_in: 1 week
    paths:
      - public
  script:
    - ./tools/ci/buildtest_linux_generic.sh

plafrim:
  stage: test
  only:
    - schedules
  timeout: 1 week
  script:
    - ./tools/ci/plafrim/plafrim.sh
  artifacts:
    expire_in: 1 week
    paths:
      - ./*.csv
      - ./*.err
      - ./*.out
      - ./*.png
  allow_failure: true

release:
  stage: deploy
  tags: ['ci.inria.fr', 'linux']
  image: docker
  only:
    - tags
  script:
    - docker build -t cppdiodon-release
                   -f ./tools/distrib/debian/dockerfile-release
                   --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG
                   --build-arg CI_PROJECT_ID=$CI_PROJECT_ID
                   --build-arg CI_JOB_TOKEN=$CI_JOB_TOKEN
                   .
