/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iostream>
#include <string>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <Python.h>
#include <diodon.hpp>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
#include <fmr/Utils/openblas_cblas.h>
#endif

namespace py = pybind11;

void init_threads(int nt, int ng) {

  if ( nt > 0 ) {
    /* Set number of OMP threads */
    omp_set_num_threads(nt);
#ifdef DIODON_USE_MKL_AS_BLAS
    mkl_set_num_threads(nt);
#endif
#ifdef DIODON_USE_OPENBLAS_AS_BLAS
    openblas_set_num_threads(nt);
#endif
  }

}

template <typename Real_t>
void revd (py::array_t <Real_t> a, int k,
           py::array_t <Real_t> l, py::array_t <Real_t> v,
           int nt=-1, int ng=0){

#ifdef DIODON_USE_CHAMELEON
  Chameleon::Chameleon chameleon(nt, ng);
#endif
  init_threads(nt, ng);

  /* return a buffer_info to the python object */
  auto a_arr = a.request();
  auto l_arr = l.request();
  auto v_arr = v.request();

  /* cast the pointers of the python object to a Real_t* */
  Real_t* a_arr_ptr = (Real_t*)a_arr.ptr;
  Real_t* l_arr_ptr = (Real_t*)l_arr.ptr;
  Real_t* v_arr_ptr = (Real_t*)v_arr.ptr;

  size_t m = a_arr.shape[0];
  size_t n = a_arr.shape[1];

  /* Declare the input matrix (Diodon/FMR object) */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#else
  fmr::BlasDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#endif

  /* Declare the output matrices */
  std::vector<Real_t> L;
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> V;
  fmr::ChameleonDenseWrapper<size_t, Real_t> VT;
#else
  fmr::BlasDenseWrapper<size_t, Real_t> V;
  fmr::BlasDenseWrapper<size_t, Real_t> VT;
#endif

  /* FMR arguments to be used for rsvd in the svd */
  struct diodon::method::fmrArgs<Real_t> fmrargs;
  fmrargs.rank = k;

  /* Call the EVD method on A */
  diodon::method::svd(A, L, V, VT, fmrargs, "revd");

  /* Copy values from C++ arrays into python numpy objects */
  std::copy(L.begin(), L.end(), l_arr_ptr);
  V.copyMatrix(v_arr_ptr);
}

template <typename Real_t>
void rsvd (py::array_t <Real_t> a, int k,
           py::array_t <Real_t> u, py::array_t <Real_t> s, py::array_t <Real_t> vt,
           int nt=-1, int ng=0){

#ifdef DIODON_USE_CHAMELEON
  Chameleon::Chameleon chameleon(nt, ng);
#endif
  init_threads(nt, ng);

  /* return a buffer_info to the python object */
  auto a_arr = a.request();
  auto u_arr = u.request();
  auto s_arr = s.request();
  auto vt_arr = vt.request();

  /* cast the pointers of the python object to a Real_t* */
  Real_t* a_arr_ptr = (Real_t*)a_arr.ptr;
  Real_t* u_arr_ptr = (Real_t*)u_arr.ptr;
  Real_t* s_arr_ptr = (Real_t*)s_arr.ptr;
  Real_t* vt_arr_ptr = (Real_t*)vt_arr.ptr;

  size_t m = a_arr.shape[0];
  size_t n = a_arr.shape[1];

  /* Declare the input matrix (Diodon/FMR object) */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#else
  fmr::BlasDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#endif

  /* Declare the output matrices */
  std::vector<Real_t> S;
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> U;
  fmr::ChameleonDenseWrapper<size_t, Real_t> VT;
#else
  fmr::BlasDenseWrapper<size_t, Real_t> U;
  fmr::BlasDenseWrapper<size_t, Real_t> VT;
#endif

  /* FMR arguments to be used for rsvd in the svd */
  struct diodon::method::fmrArgs<Real_t> fmrargs;
  fmrargs.rank = k;

  /* Call the SVD method on A */
  diodon::method::svd(A, S, U, VT, fmrargs, "rsvd");

  /* Copy values from C++ arrays into python numpy objects */
  std::copy(S.begin(), S.end(), s_arr_ptr);
  U.copyMatrix(u_arr_ptr);
  VT.copyMatrix(vt_arr_ptr);
}

template <typename Real_t>
void pca (py::array_t <Real_t> a, std::string premethod, int k, std::string svdmethod,
          py::array_t <Real_t> l, py::array_t <Real_t> y, py::array_t <Real_t> v,
          int nt=-1, int ng=0){

#ifdef DIODON_USE_CHAMELEON
  Chameleon::Chameleon chameleon(nt, ng);
#endif
  init_threads(nt, ng);

  /* return a buffer_info to the python object */
  auto a_arr = a.request();
  auto l_arr = l.request();
  auto y_arr = y.request();
  auto v_arr = v.request();

  /* cast the pointers of the python object to a Real_t* */
  Real_t* a_arr_ptr = (Real_t*)a_arr.ptr;
  Real_t* l_arr_ptr = (Real_t*)l_arr.ptr;
  Real_t* y_arr_ptr = (Real_t*)y_arr.ptr;
  Real_t* v_arr_ptr = (Real_t*)v_arr.ptr;

  size_t m = a_arr.shape[0];
  size_t n = a_arr.shape[1];

  /* Declare the input matrix (Diodon/FMR object) */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#else
  fmr::BlasDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#endif

  /* Declare the output matrices */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> Y;
  fmr::ChameleonDenseWrapper<size_t, Real_t> V;
#else
  fmr::BlasDenseWrapper<size_t, Real_t> Y;
  fmr::BlasDenseWrapper<size_t, Real_t> V;
#endif
  std::vector<Real_t> L;

  /* FMR arguments to be used for rsvd in the svd */
  struct diodon::method::fmrArgs<Real_t> fmrargs;
  fmrargs.rank = k;

  /* Call the SVD method on A */
  diodon::method::pca(A, Y, V, L, fmrargs, svdmethod, premethod);

  /* Copy values from C++ arrays into python numpy objects */
  std::copy(L.begin(), L.end(), l_arr_ptr);
  Y.copyMatrix(y_arr_ptr);
  V.copyMatrix(v_arr_ptr);
}

template <typename Real_t>
int mds (py::array_t <Real_t> a, int k, std::string svdmethod,
         py::array_t <Real_t> x, py::array_t <Real_t> s,
         int nt=-1, int ng=0){

#ifdef DIODON_USE_CHAMELEON
  Chameleon::Chameleon chameleon(nt, ng);
#endif
  init_threads(nt, ng);

  /* return a buffer_info to the python object */
  auto a_arr = a.request();
  auto s_arr = s.request();
  auto x_arr = x.request();

  /* cast the pointers of the python object to a Real_t* */
  Real_t* a_arr_ptr = (Real_t*)a_arr.ptr;
  Real_t* s_arr_ptr = (Real_t*)s_arr.ptr;
  Real_t* x_arr_ptr = (Real_t*)x_arr.ptr;

  size_t m = a_arr.shape[0];
  size_t n = a_arr.shape[1];

  /* Declare the input matrix (Diodon/FMR object) */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#else
  fmr::BlasDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#endif

  /* Declare the output matrices */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> X;
#else
  fmr::BlasDenseWrapper<size_t, Real_t> X;
#endif
  std::vector<Real_t> S;

  /* FMR arguments to be used for rsvd in the svd */
  struct diodon::method::fmrArgs<Real_t> fmrargs;
  fmrargs.rank = k;

  /* Call the SVD method on A */
  diodon::method::mds(A, X, S, fmrargs, svdmethod);

  /* Copy values from C++ arrays into python numpy objects */
  std::copy(S.begin(), S.end(), s_arr_ptr);
  X.copyMatrix(x_arr_ptr);

  /* return the size of S and X (number of columns) so that python arrays can be
  resized. recall that in input this size was k or n. it become k+ related to
  positive eigen values */
  return S.size();
}

template <typename Real_t>
void coa (py::array_t <Real_t> a, int k, std::string svdmethod,
          py::array_t <Real_t> l, py::array_t <Real_t> yr, py::array_t <Real_t> yc,
          int nt=-1, int ng=0){

#ifdef DIODON_USE_CHAMELEON
  Chameleon::Chameleon chameleon(nt, ng);
#endif
  init_threads(nt, ng);

  //std::cout << "Enter" << std::endl;
  //std::string start;
  //std::getline(std::cin, start);

  /* return a buffer_info to the python object */
  auto a_arr = a.request();
  auto l_arr = l.request();
  auto yr_arr = yr.request();
  auto yc_arr = yc.request();

  /* cast the pointers of the python object to a Real_t* */
  Real_t* a_arr_ptr = (Real_t*)a_arr.ptr;
  Real_t* l_arr_ptr = (Real_t*)l_arr.ptr;
  Real_t* yr_arr_ptr = (Real_t*)yr_arr.ptr;
  Real_t* yc_arr_ptr = (Real_t*)yc_arr.ptr;

  size_t m = a_arr.shape[0];
  size_t n = a_arr.shape[1];

  /* Declare the input matrix (Diodon/FMR object) */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#else
  fmr::BlasDenseWrapper<size_t, Real_t> A(m, n, a_arr_ptr);
#endif

  /* Declare the output matrices */
#ifdef DIODON_USE_CHAMELEON
  fmr::ChameleonDenseWrapper<size_t, Real_t> Yr;
  fmr::ChameleonDenseWrapper<size_t, Real_t> Yc;
#else
  fmr::BlasDenseWrapper<size_t, Real_t> Yr;
  fmr::BlasDenseWrapper<size_t, Real_t> Yc;
#endif
  std::vector<Real_t> L;

  /* FMR arguments to be used for rsvd in the svd */
  struct diodon::method::fmrArgs<Real_t> fmrargs;
  fmrargs.rank = k;

  /* Call the SVD method on A */
  diodon::method::coa(A, Yr, Yc, L, fmrargs, svdmethod);

  /* Copy values from C++ arrays into python numpy objects */
  std::copy(L.begin(), L.end(), l_arr_ptr);
  Yr.copyMatrix(yr_arr_ptr);
  Yc.copyMatrix(yc_arr_ptr);
}

PYBIND11_MODULE(cppdiodon_core,m){

  m.doc() = "Python pybind11 wrapper for cppdiodon (https://gitlab.inria.fr/diodon/cppdiodon)";

  m.def("rsvd",&rsvd<float>, "Randomized SVD of a general matrix (real simple precision)",
        py::arg("a"), py::arg("k"), py::arg("u"), py::arg("s"), py::arg("vt"),
        py::arg("nt")=-1, py::arg("ng")=0);
  m.def("rsvd",&rsvd<double>, "Randomized SVD of a general matrix (real double precision)",
        py::arg("a"), py::arg("k"), py::arg("u"), py::arg("s"), py::arg("vt"),
        py::arg("nt")=-1, py::arg("ng")=0);

  m.def("revd",&revd<float>, "Randomized EVD of a symmetric matrix (real simple precision)",
        py::arg("a"), py::arg("k"), py::arg("l"), py::arg("v"),
        py::arg("nt")=-1, py::arg("ng")=0);
  m.def("revd",&revd<double>, "Randomized EVD of a symmetric matrix (real double precision)",
        py::arg("a"), py::arg("k"), py::arg("l"), py::arg("v"),
        py::arg("nt")=-1, py::arg("ng")=0);

  m.def("pca",&pca<float>, "Principal Component Analysis of a general matrix (real simple precision)",
        py::arg("a"), py::arg("premethod"), py::arg("k"), py::arg("svdmethod"),
        py::arg("l"), py::arg("y"), py::arg("v"),
        py::arg("nt")=-1, py::arg("ng")=0);
  m.def("pca",&pca<double>, "Principal Component Analysis of a general matrix (real double precision)",
        py::arg("a"), py::arg("premethod"), py::arg("k"), py::arg("svdmethod"),
        py::arg("l"), py::arg("y"), py::arg("v"),
        py::arg("nt")=-1, py::arg("ng")=0);

  m.def("mds",&mds<float>, "Multidimensional Scaling of a distance matrix (real simple precision)",
        py::arg("a"), py::arg("k"), py::arg("svdmethod"),
        py::arg("x"), py::arg("s"),
        py::arg("nt")=-1, py::arg("ng")=0);
  m.def("mds",&mds<double>, "Multidimensional Scaling of a distance matrix (real double precision)",
        py::arg("a"), py::arg("k"), py::arg("svdmethod"),
        py::arg("x"), py::arg("s"),
        py::arg("nt")=-1, py::arg("ng")=0);

  m.def("coa",&coa<float>, "Correspondance Analysis of a contingency matrix (real simple precision)",
        py::arg("a"), py::arg("k"), py::arg("svdmethod"),
        py::arg("l"), py::arg("y"), py::arg("v"),
        py::arg("nt")=-1, py::arg("ng")=0);
  m.def("coa",&coa<double>, "Correspondance Analysis of a contingency matrix (real double precision)",
        py::arg("a"), py::arg("k"), py::arg("svdmethod"),
        py::arg("l"), py::arg("yr"), py::arg("yc"),
        py::arg("nt")=-1, py::arg("ng")=0);
}
