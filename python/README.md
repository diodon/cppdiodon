# Python wrapper for Cppdiodon

This python wrapper aims at providing some of the costly routines that arise in
multivariate data analysis such as *svd_grp* (randomized SVD with prescribed
rank k), *pca*, *mds* and *coa*. These functions exists in the
[pydiodon](https://gitlab.inria.fr/diodon/pydiodon) project and the goal here is
to provide the same functions but coming from the C++ implementation
[cppdiodon](https://gitlab.inria.fr/diodon/cppdiodon/).

## Install

### Install pydiodon

First install *pydiodon*, example on Ubuntu 24.04
```sh
# dependencies package
sudo apt update -y
sudo apt install -y git python-is-python3 python3-h5py python3-matplotlib python3-numpy python3-pip python3-pybind11 python3-scipy python3-setuptools

# download with git and install pydiodon
git clone https://gitlab.inria.fr/diodon/pydiodon.git
cd pydiodon
pip install .
cd ..
```

### Install cppdiodon

Then install *cppdiodon*, detailed instructions here : https://diodon.gitlabpages.inria.fr/cppdiodon/.
Several configurations can be installed, one relying on the multithreaded Intel MKL Lapack, and one relying on Chameleon.
For better performances we recommend using the version with Chameleon.

#### cppdiodon with Chameleon

```sh
# dependencies package
sudo apt install -y git cmake build-essential zlib1g-dev libbz2-dev gfortran pkg-config libmkl-dev libhdf5-dev libstarpu-dev

# download with git and install chameleon with cmake
wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz
tar xvf chameleon-1.3.0.tar.gz
cd chameleon-1.3.0 && mkdir build && cd build
cmake .. -DBLA_VENDOR=Intel10_64lp -DCHAMELEON_KERNELS_MT=ON -DBUILD_SHARED_LIBS=ON
# -DCHAMELEON_USE_CUDA=ON can be used if you have Nvidia GPUs and cuda-toolkit installed
make -j5
sudo make install

# download with git and install cppdiodon with cmake
git clone --recurse-submodules https://gitlab.inria.fr/diodon/cppdiodon.git
cd cppdiodon && mkdir build && cd build
cmake .. -DDIODON_USE_MKL_AS_BLAS=ON -DDIODON_USE_CHAMELEON=ON -DDIODON_USE_CHAMELEON_MPI=OFF
make -j5
# if you want to install in a specific directory add -DCMAKE_INSTALL_PREFIX=$DIODON_ROOT, DIODON_ROOT being an environment variable containing the path to the installation directory for cppdiodon
sudo make install
cd ../..
```

#### cppdiodon with Intel MKL Lapack
```sh
# dependencies package
sudo apt install -y git cmake build-essential zlib1g-dev libbz2-dev gfortran pkg-config libmkl-dev libhdf5-dev

# download with git and install cppdiodon with cmake
git clone --recurse-submodules https://gitlab.inria.fr/diodon/cppdiodon.git
cd cppdiodon
mkdir -p build
cd build
cmake .. -DDIODON_USE_MKL_AS_BLAS=ON
make -j5
# if you want to install in a specific directory add -DCMAKE_INSTALL_PREFIX=$DIODON_ROOT, DIODON_ROOT being an environment variable containing the path to the installation directory for cppdiodon
sudo make install
cd ../..
```

### Install python wrapper of cppdiodon

Then, install this Python wrapper
```sh
# if you have both hdf5 serial and openmpi in your environment you may want to force the serial version with: export CFLAGS="-I/usr/include/hdf5/serial/"
cd cppdiodon/python/ # considering your have cloned the cppdiodon project
pip install .
cd ../..
```

Try examples
```sh
cd cppdiodon/python/tests/
export CPPDIODON_PYTHON_DEMO=1
python test_revd.py 10 2
python test_rsvd.py 10 5 2
python test_pca.py ../../data/pca_template_withnames.txt 3
python test_mds.py ../../data/atlas_guyane_trnH.h5 distance 100
python test_coa.py ../../data/coa4tests_nonames.txt
```

## Usage


## Examples of svd_grp, pca, mds, coa in Python

Help MKL to find symbols
```sh
# example if Intel MKL is installed with the official Debian package
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libmkl_rt.so

# else, example if Intel MKL in /opt/intel/oneapi/mkl/
source /opt/intel/oneapi/mkl/latest/env/vars.sh intel64
export LD_PRELOAD=$MKLROOT/lib/intel64/libmkl_rt.so
```

Python example to compute a Randomized SVD
```python
import numpy as np
import cppdiodon as cppdio

m = 10
n = 5
k = 3

A1 = np.random.rand(m,k)
A2 = np.random.rand(k,n)
A = np.matmul(A1, A2)

Uc, Sc, VTc = cppdio.svd_grp(A, k)
print(np.matmul(Uc, np.matmul(np.diag(Sc), VTc)))

Un, Sn, VTn = np.linalg.svd(A, full_matrices=False)
print(np.matmul(Un, np.matmul(np.diag(Sn), VTn)))
```

Python example to compute a PCA
```python
import numpy as np
import pydiodon as dio
import cppdiodon as cppdio

m = 10
n = 5
k = 3

A1 = np.random.rand(m,k)
A2 = np.random.rand(k,n)
A = np.matmul(A1, A2)

Yp, Lp, Vp = dio.pca(A, k=k, meth='grp')
print(Yp)

Yc, Lc, Vc = cppdio.pca(A, k=k, meth='grp')
print(Yc)
```

Python example to compute a MDS
```python
import sys
import numpy as np
import matplotlib.pyplot as plt
import pydiodon as dio
import cppdiodon as cppdio

file = sys.argv[1]
dataset = sys.argv[2]
k = int(sys.argv[3])

A = dio.loadfile(filename=file, datasetname=dataset)

Xp, Sp = dio.mds(A, k=k, meth='grp')
Xc, Sc = cppdio.mds(A, k=k, meth='grp')

Sc = np.round(Sc[:Sc.shape[0]])
Sp = np.round(Sp[:Sp.shape[0]])
plt.plot(Sp,c="green")
plt.plot(Sc,c="blue")
plt.show()
```

Python example to compute a COA
```python
import sys
import pydiodon as dio
import cppdiodon as cppdio

file = sys.argv[1]
A = dio.loadfile(filename=file)

Lp, Yrp, Ycp = dio.coa(A)
print(Lp)
Lc, Yrc, Ycc = cppdio.coa(A)
print(Lc)

dio.plot_coa(Yrp, Ycp)
dio.plot_coa(Yrc, Ycc)
```

## Examples usage on a supercomputer, here Plafrim

On [Plafrim](https://plafrim-users.gitlabpages.inria.fr/doc/) the
[Guix](https://guix.gnu.org/fr/) package manager is used to install softwares.

First initialize Guix on your plafrim account
```sh
ssh plafrim
guix build hello # just the first time guix is invoked
guix pull # may be long
```

Then copy some jupyter notebooks or python scripts on plafrim, example
```sh
scp *.ipynb plafrim:
scp *.py plafrim:
```

Then lets work on Plafrim through an SSH connexion
```sh
ssh -L 50001:localhost:50001 plafrim # the port number must be unique per user, one can any valid port e.g. 50002 etc

# Choose between bora and sirocco nodes to perform experimentations on either CPUs only or CPUs+GPUs
export MACHINE=sirocco
if [[ $MACHINE == "bora" ]]; then
  # Node reservation, here a bora (see https://plafrim-users.gitlabpages.inria.fr/doc/#overview)
  salloc --nodes=1 --time=01:00:00 --constraint bora --exclusive --ntasks-per-node=1 --threads-per-core=1
elif [[ $MACHINE == "sirocco" ]]; then
  # or for GPUs, a sirocco (see https://plafrim-users.gitlabpages.inria.fr/doc/#overview)
  salloc --nodes=1 --time=01:00:00 --constraint sirocco,v100,bigmem --exclusive --ntasks-per-node=1 --threads-per-core=1
fi

# login on the node interactively
ssh -L 50001:localhost:50001 `echo $SLURM_NODELIST`

# Choose between mkl and chameleon for lapack operations
export LAPACK=chameleon
if [[ $LAPACK == "mkl" ]]; then
  # Build the working environment with jupyter, pydiodon et cppdiodon (version with Intel MKL here)
  guix shell --pure jupyter python python-scikit-learn python-pydiodon python-cppdiodon -- /bin/bash --norc
elif [[ $LAPACK == "chameleon" ]]; then
  # or with with cppdiodon + chameleon (cuda version, without mpi)
  guix shell --pure jupyter python python-scikit-learn python-pydiodon python-cppdiodon-chameleon --with-input=chameleon-mkl-mt-wompi=chameleon-cuda-mkl-mt-wompi -- /bin/bash --norc
fi

# Trick to help Intel MKL finding symbols and the CUDA driver library on sirocco
export LD_PRELOAD=/usr/lib64/libcuda.so:$GUIX_ENVIRONMENT/lib/libmkl_rt.so

# Some environment variables optional but useful for cppdiodon+chameleon (optimizations)
export FMR_CHAMELEON_TILE_SIZE=960
export STARPU_SILENT=1
export STARPU_CUDA_PIPELINE=4
export STARPU_NWORKER_PER_CUDA=2

# From here you can execute python scripts relying on pydiodon and cppdiodon
python3 ~/pca_comparison_bora.py
python3 ~/mds_comparison_sirocco.py

# or you can run a jupyter notebook
jupyter notebook --no-browser --port=50001

# copy/paste the url in your web browser and open your notebook
```

## Authors

- [Florent Pruvost](mailto:florent.pruvost@inria.fr)