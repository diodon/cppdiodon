import os
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import pydiodon as pydio
import cppdiodon as cppdio

def plot_cloud(X):
    nbin = 256
    Ap1 = X[:,0]
    Ap2 = X[:,1]
    H, _, _ = np.histogram2d(Ap1, Ap2, bins=nbin)
    H = np.log(1+H)
    plt.imshow(H)

def plot_eigen(S1, S2):
    S1 = np.round(S1[:S1.shape[0]])
    S2 = np.round(S2[:S2.shape[0]])
    plt.yscale('log')
    plt.plot(S1,c="blue")
    plt.plot(S2,c="green")
    plt.show()

def mds_compare(file, dataset, rank):
    A = pydio.loadfile(filename=file, datasetname=dataset)
    A = np.asanyarray(A, dtype='float32', order='F')

    start_time = time.time()
    Xp, Sp = pydio.mds(A, k=rank, meth='grp')
    print("--- pydiodon mds %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    Xc, Sc = cppdio.mds(A, k=rank, meth='grp', nt=35, ng=2)
    print("--- cppdiodon mds %s seconds ---" % (time.time() - start_time))

    fig=plt.figure(figsize=(10,5), dpi= 100, facecolor='w', edgecolor='k')
    #plt.subplot(1, 3, 1)
    #plot_cloud(Xp)
    plt.subplot(1, 2, 1)
    plot_cloud(Xc)
    plt.subplot(1, 2, 2)
    plot_eigen(Sp, Sc)

mds_compare('/beegfs/diodon/gordon/atlas_guyane_trnH.h5', 'distance', 1497)
mds_compare('/beegfs/diodon/gordon/10V-RbcL_S74.h5', 'distance', 3195)
mds_compare('/beegfs/diodon/gordon/26_rbcL.h5', 'distance', 3195)
mds_compare('/beegfs/diodon/gordon/L1-L10/L1.h5', 'distance', 3195)
mds_compare('/beegfs/diodon/gordon/L1-L10/L6.h5', 'distance', 3195)

