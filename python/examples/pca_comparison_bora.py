import time
import numpy as np
import matplotlib.pyplot as plt
import pydiodon as pydio
import cppdiodon as cppdio
import sklearn.decomposition as skl

def plot_eigen(S1, S2, S3):
    S1 = np.round(S1[:S1.shape[0]])
    S2 = np.round(S2[:S2.shape[0]])
    S3 = np.round(S3[:S3.shape[0]])
    plt.yscale('log')
    plt.xlabel("rank")
    plt.ylabel("Eigen values")
    plt.plot(S1,c="green")
    plt.plot(S2,c="blue")
    plt.plot(S3,c="red")

def pca_compare(m, n, rank):

    meth = "gauss"
    if meth=="uniform":
        A = -1 + 2*np.random.random((m,n))
    if meth=="gauss":
        A = np.random.randn(m,n)

    A = np.asanyarray(A, dtype='float64', order='F')

    start_time = time.time()
    pca = skl.PCA(n_components=rank, svd_solver='randomized')
    Ys = pca.fit_transform(A)
    print("--- sklearn pca %s seconds ---" % (time.time() - start_time))
    Ls = pca.singular_values_*pca.singular_values_

    start_time = time.time()
    Yp, Lp, Vp = pydio.pca(A, pretreatment="col_centering", k=rank, meth='grp')
    print("--- pydiodon pca %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    Yc, Lc, Vc = cppdio.pca(A, pretreatment="col_centering", k=rank, meth='grp', nt=35)
    print("--- cppdiodon pca %s seconds ---" % (time.time() - start_time))

    #pydio.plot_components_heatmap(Ys, bins=64)
    #pydio.plot_components_heatmap(Yp, bins=64)
    #pydio.plot_components_heatmap(Yc, bins=64)
    plot_eigen(Ls, Lp, Lc)
    name = 'pca_'+str(m)+'_'+str(n)+'_'+str(rank)
    plt.savefig(name, dpi=300)

pca_compare(10000, 2000, 495)
pca_compare(20000, 5000, 995)
pca_compare(40000, 7000, 1455)
pca_compare(100000, 10000, 1995)
pca_compare(200000, 20000, 3995)

