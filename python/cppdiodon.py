"""
Copyright 2022 INRAE, INRIA

This file is part of the cppdiodon software package for Multivariate
Data Analysis of large datasets.

This software is governed by the CeCILL-C license under French law
and abiding by the rules of distribution of free software. You can
use, modify and/or redistribute the software under the terms of the
CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
URL: "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
"""

import cppdiodon_core
import numpy as np

def svd_grp(A, k, nt=-1, ng=0):
    [U,S,VT] = rsvd(A, k, nt, ng)
    return U, S, VT

def rsvd(A, k, nt=-1, ng=0):

    # Get the real type of A (float or double)
    d = A.dtype

    # Get size of A
    (m,n) = A.shape

    if not A.flags.f_contiguous:

        # turn in column-major format
        Af = np.asfortranarray(A)

        # Initialize output arrays (column-major)
        S = np.empty(k, dtype=d)
        Uf = np.empty((m, k), dtype=d, order='F')
        VTf = np.empty((k, n), dtype=d, order='F')

        # compute rsvd from cppdiodon
        cppdiodon_core.rsvd(Af, k, Uf, S, VTf, nt, ng)

        # ensure we respect the matrix layout from python (row-major)
        U = np.asanyarray(Uf, order='C')
        VT = np.asanyarray(VTf, order='C')

    else:

        # Initialize output arrays (column-major)
        S = np.empty(k, dtype=d)
        U = np.empty((m, k), dtype=d, order='F')
        VT = np.empty((k, n), dtype=d, order='F')

        # compute rsvd from cppdiodon
        cppdiodon_core.rsvd(A, k, U, S, VT, nt, ng)

    return U, S, VT

def revd(A, k, nt=-1, ng=0):

    # Get the real type of A (float or double)
    d = A.dtype

    # Get size of A
    (m,n) = A.shape

    if not A.flags.f_contiguous:

        # turn in column-major format
        Af = np.asfortranarray(A)

        # Initialize output arrays (column-major)
        L = np.empty(k, dtype=d)
        Vf = np.empty((m, k), dtype=d, order='F')

        # compute rsvd from cppdiodon
        cppdiodon_core.revd(Af, k, L, Vf, nt, ng)

        # ensure we respect the matrix layout from python (row-major)
        V = np.asanyarray(Vf, order='C')

    else:

        # Initialize output arrays (column-major)
        L = np.empty(k, dtype=d)
        V = np.empty((m, k), dtype=d, order='F')

        # compute rsvd from cppdiodon
        cppdiodon_core.revd(A, k, L, V, nt, ng)

    return L, V

def pca(A, pretreatment="standard", k=-1, meth="svd", nt=-1, ng=0):

    methc = meth
    if meth == "grp":
        methc = "rsvd"
        if k<0:
            exit("cppdiodon: in pca(), a value for k must be given explicitly (> 0) with meth=grp")

    # Get the real type of A (float or double)
    d = A.dtype

    # Get size of A
    (m,n) = A.shape

    # transposition if necessary
    if n > m:
        print("more columns than rows => matrix is transposed")
        A = A.T
        transpose = True
    else:
        transpose = False

    # number of columns depending on svd method
    l = n
    if methc == 'revd' or methc == 'rsvd':
        l = k

    if not A.flags.f_contiguous:

        # turn in column-major format
        Af = np.asfortranarray(A)

        # Initialize output arrays (column-major)
        L = np.empty(l, dtype=d)
        Yf = np.empty((m, l), dtype=d, order='F')
        Vf = np.empty((n, l), dtype=d, order='F')

        # compute pca from cppdiodon
        cppdiodon_core.pca(Af, pretreatment, l, methc, L, Yf, Vf, nt, ng)

        # ensure we respect the matrix layout from python (row-major)
        Y = np.asanyarray(Yf, order='C')
        V = np.asanyarray(Vf, order='C')

    else:

        # Initialize output arrays (column-major)
        L = np.empty(l, dtype=d)
        Y = np.empty((m, l), dtype=d, order='F')
        V = np.empty((n, l), dtype=d, order='F')

        # compute pca from cppdiodon
        cppdiodon_core.pca(A, pretreatment, l, methc, L, Y, V, nt, ng)

    if meth in ['evd', 'svd']:
        if k > 0:
            Y = Y[:,0:k]
            L = L[0:k]
            V = V[:,0:k]

    if transpose:
        V = np.dot(A,V)
        Y = np.dot(A.T,V)

    return Y, L, V

def mds(A, k=-1, meth="svd", nt=-1, ng=0):

    methc = meth
    if meth == "grp":
        methc = "rsvd"
        if k<0:
            exit("cppdiodon: in mds(), a value for k must be given explicitly (> 0) with meth=grp")

    # Get the real type of A (float or double)
    d = A.dtype

    # Get size of A
    (m,n) = A.shape

    # Check this is a square matrix
    if n != m:
       exit("cppdiodon: in mds(), the input matrix A must be a square symmetric matrix")

    # number of columns depending on svd method
    l = n
    if methc == 'revd' or methc == 'rsvd':
        l = k

    if not A.flags.f_contiguous:

        # turn in column-major format
        Af = np.asfortranarray(A)

        # Initialize output arrays (column-major)
        S = np.empty(l, dtype=d)
        Xf = np.empty((m, l), dtype=d, order='F')

        # compute pca from cppdiodon
        npos = cppdiodon_core.mds(Af, l, methc, Xf, S, nt, ng)

        # ensure we respect the matrix layout from python (row-major)
        X = np.asanyarray(Xf, order='C')

    else:

        # Initialize output arrays (column-major)
        S = np.empty(l, dtype=d)
        X = np.empty((m, l), dtype=d, order='F')

        # compute pca from cppdiodon
        npos = cppdiodon_core.mds(A, l, methc, X, S, nt, ng)

    S = S[0:npos]
    X = X[:,0:npos]

    return X, S

def coa(A, k=-1, meth="svd", transpose=False, nt=-1, ng=0):

    methc = meth
    if meth == "grp":
        methc = "rsvd"
        if k<0:
            exit("cppdiodon: in coa(), a value for k must be given explicitly (> 0) with meth=grp")

    # Get the real type of A (float or double)
    d = A.dtype

    # transposes A if m < n
    if transpose:
        A	= A.T
        print("The array has been transposed (transpose=True).")

    # Get size of A
    (m,n) = A.shape

    # number of columns depending on svd method
    l = n
    if methc == 'revd' or methc == 'rsvd':
        l = k

    if not A.flags.f_contiguous:

        # turn in column-major format
        Af = np.asfortranarray(A)

        # Initialize output arrays (column-major)
        L= np.empty(l, dtype=d)
        Yrf = np.empty((m, l), dtype=d, order='F')
        Ycf = np.empty((n, l), dtype=d, order='F')

        # compute coa from cppdiodon
        cppdiodon_core.coa(Af, l, methc, L, Yrf, Ycf, nt, ng)

        # ensure we respect the matrix layout from python (row-major)
        Yr = np.asanyarray(Yrf, order='C')
        Yc = np.asanyarray(Ycf, order='C')

    else:

        # Initialize output arrays (column-major)
        L = np.empty(l, dtype=d)
        Yr = np.empty((m, l), dtype=d, order='F')
        Yc = np.empty((n, l), dtype=d, order='F')

        # compute coa from cppdiodon
        cppdiodon_core.coa(A, l, methc, L, Yr, Yc, nt, ng)

    return	L, Yr, Yc