import os
import sys
import numpy as np
import pydiodon as dio
import cppdiodon as cppdio

def pca_common(array_order):
    env_cppdiodon_python_demo = os.environ.get('CPPDIODON_PYTHON_DEMO', '0')
    cppdiodon_python_demo = int(env_cppdiodon_python_demo)

    if cppdiodon_python_demo == 1:
        file = sys.argv[1]
        k = int(sys.argv[2])
        A, rownames, colnames = dio.loadfile(filename=file, rownames=True, colnames=True)
        #A = dio.loadfile(filename=file)
    else:
        file = '../data/pca_template_withnames.txt'
        k = 3
        A, rownames, colnames = dio.loadfile(filename=file, rownames=True, colnames=True)
    A = np.asanyarray(A, dtype='float32', order=array_order)

    Yp, Lp, Vp = dio.pca(A, k=k, meth='grp')
    print(Yp)

    Yc, Lc, Vc = cppdio.pca(A, k=k, meth='grp')
    print(Yc)

def test_pca():
    pca_common('C')

def test_pca_fortran():
    pca_common('F')

if __name__ == '__main__':
    test_pca_fortran()
