import os
import sys
import time
import matplotlib.pyplot as plt
import numpy as np
import pydiodon as pydio
import cppdiodon as cppdio

def rsvd_common(array_order):

    env_cppdiodon_python_demo = os.environ.get('CPPDIODON_PYTHON_DEMO', '0')
    cppdiodon_python_demo = int(env_cppdiodon_python_demo)

    if cppdiodon_python_demo == 1:
        m = int(sys.argv[1])
        n = int(sys.argv[2])
        k = int(sys.argv[3])
    else:
        m = 10
        n = 5
        k = 2

    start_time = time.time()
    A1 = np.random.rand(m,k)
    A2 = np.random.rand(k,n)
    A = np.matmul(A1, A2)
    print("--- demo_rsvd.py building the matrix %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    A = np.asanyarray(A, dtype='float32', order=array_order)
    print("--- demo_rsvd.py conversion float32 and Fortran array %s seconds ---" % (time.time() - start_time))
    #print(A.dtype)
    #print(A.flags.f_contiguous)

    start_time = time.time()
    Up, Sp, VTp = pydio.svd_grp(A, k)
    print("--- demo_rsvd.py pydiodon %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    Uc, Sc, VTc = cppdio.rsvd(A, k, nt=4)
    print("--- demo_rsvd.py cppdiodon %s seconds ---" % (time.time() - start_time))

    #Sp = np.round(Sp[:Sp.shape[0]])
    #Sc = np.round(Sc[:Sc.shape[0]])
    #plt.plot(Sp,c="blue")
    #plt.plot(Sc,c="red")
    #plt.show()

def test_rsvd():
    rsvd_common('C')

def test_rsvd_fortran():
    rsvd_common('F')

if __name__ == '__main__':
    test_rsvd_fortran()
