import os
import sys
import numpy as np
import pydiodon as dio
import cppdiodon as cppdio

def coa_common(array_order):
    env_cppdiodon_python_demo = os.environ.get('CPPDIODON_PYTHON_DEMO', '0')
    cppdiodon_python_demo = int(env_cppdiodon_python_demo)

    if cppdiodon_python_demo == 1:
        file = sys.argv[1]
    else:
        file = '../data/coa4tests_nonames.txt'
    A = dio.loadfile(filename=file)
    A = np.asanyarray(A, dtype='float32', order=array_order)

    Lp, Yrp, Ycp = dio.coa(A)
    print(Lp)
    Lc, Yrc, Ycc = cppdio.coa(A)
    print(Lc)

    #dio.plot_coa(Yrp, Ycp)
    #dio.plot_coa(Yrc, Ycc)

def test_coa():
    coa_common('C')

def test_coa_fortran():
    coa_common('F')

if __name__ == '__main__':
    test_coa_fortran()
