import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import pydiodon as dio
import cppdiodon as cppdio

def mds_common(array_order):
    env_cppdiodon_python_demo = os.environ.get('CPPDIODON_PYTHON_DEMO', '0')
    cppdiodon_python_demo = int(env_cppdiodon_python_demo)

    if cppdiodon_python_demo == 1:
        file = sys.argv[1]
        dataset = sys.argv[2]
        k = int(sys.argv[3])
    else:
        file = '../data/atlas_guyane_trnH.h5'
        dataset = 'distance'
        k = 100
    A = dio.loadfile(filename=file, datasetname=dataset)
    A = np.asanyarray(A, dtype='float32', order=array_order)

    Xp, Sp = dio.mds(A, k=k, meth='grp')
    Xc, Sc = cppdio.mds(A, k=k, meth='grp')

    Sc = np.round(Sc[:Sc.shape[0]])
    print(Sc)
    Sp = np.round(Sp[:Sp.shape[0]])
    print(Sp)

    #plt.plot(Sp,c="green")
    #plt.plot(Sc,c="blue")
    #plt.show()

def test_mds():
    mds_common('C')

def test_mds_fortran():
    mds_common('F')

if __name__ == '__main__':
    test_mds_fortran()
