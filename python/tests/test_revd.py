import os
import sys
import time
import matplotlib.pyplot as plt
import numpy as np
import cppdiodon as cppdio

def revd_common(array_order):
    env_cppdiodon_python_demo = os.environ.get('CPPDIODON_PYTHON_DEMO', '0')
    cppdiodon_python_demo = int(env_cppdiodon_python_demo)

    if cppdiodon_python_demo == 1:
        n = int(sys.argv[1])
        k = int(sys.argv[2])
    else:
        n = 10
        k = 2

    start_time = time.time()
    A1 = np.random.rand(n,k)
    A2 = A1.transpose()
    A = np.matmul(A1, A2)
    print("--- demo_revd.py building the matrix %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    A = np.asanyarray(A, dtype='float32', order=array_order)
    print("--- demo_revd.py conversion float32 and Fortran array %s seconds ---" % (time.time() - start_time))
    #print(A.dtype)
    #print(A.flags.f_contiguous)

    # check with numpy
    #Ln, Vn = np.linalg.eig(A)

    start_time = time.time()
    Lc, Vc = cppdio.revd(A, k, nt=4)
    print("--- demo_revd.py cppdiodon %s seconds ---" % (time.time() - start_time))

    # check EVD
    for i in range(0,Lc.shape[0]):
        AV = A.dot(Vc[:,i])
        lamV = Lc[i] * Vc[:,i];
        DiffA = AV - lamV
        print("REVD - Norm ", i, " = ", np.linalg.norm(DiffA))

    #Sc = np.round(Sc[:Sc.shape[0]])
    #plt.plot(Sc,c="red")
    #plt.show()

def test_revd():
    revd_common('C')

def test_revd_fortran():
    revd_common('F')

if __name__ == '__main__':
    test_revd_fortran()
