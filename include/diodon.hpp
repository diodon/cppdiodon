/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file diodon.hpp
 *
 * Diodon API header
 *
 */
#ifndef DIODON_HPP
#define DIODON_HPP

#include "diodon/io_utils/BlockHdf5.hpp"
#include "diodon/io_utils/Dataset.hpp"
#include "diodon/io_utils/loadfile.hpp"
#include "diodon/io_utils/writefile.hpp"
#include "diodon/io_utils/matutils.hpp"

#include "diodon/pretreatments/centerscale.hpp"
#include "diodon/pretreatments/gram.hpp"

#include "diodon/methods/attributes.hpp"
#include "diodon/methods/coa.hpp"
#include "diodon/methods/mds.hpp"
#include "diodon/methods/pca.hpp"
#include "diodon/methods/pcamet.hpp"

#include "diodon/posttreatments/svFilter.hpp"

#endif
