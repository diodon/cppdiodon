/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file pca.hpp
 *
 * Define the Correspondence Analysis of a contingency table, COA method, with function __coa__.
 *
 */
#ifndef COA_HPP
#define COA_HPP
// Includes
// std
#include <iostream>
#include <vector>
#include <string>
// cstd
#include <cassert>

// mpi
#if defined(DIODON_USE_CHAMELEON_MPI)
#include "mpi.h"
#endif
// fmr
#include "fmr/Algorithms/randomSVD.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/MatrixNorms.hpp"
#include "fmr/Utils/MatrixTranspose.hpp"
// diodon
#include "diodon/pretreatments/precoa.hpp"
#include "diodon/methods/pca.hpp"
#include "diodon/methods/attributes.hpp"

namespace diodon {
namespace method {

/**
 *
 * @ingroup Methods
 * @brief Correspondence Analysis (COA) of a contingency table.
 *
 * Procedure: There are three steps
 *
 * 1. computes the pretreatments on a matrix A
 *    Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 *    Lets
 *    \f[r_i = \sum_j a_{ij} \\
 *       c_j = \sum_i a_{ij} \\
 *       sg  = \sum_{i,j} a_{ij}\f]
 *    A transformed gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 *    \f[ \bar{a}_{i,j} = \frac{a_{i,j}-r_i*c_j/sg}{ \sqrt{r_i*c_j} } \f]
 *
 * 2. runs a PCA A. PCA: \f$ Z, X, \Lambda\f$
 *
 * 3. computes and returns \f$Y_r = M^{-1}Z\f$, \f$Y_c = Q^{-1}X\f$,
 *    where \f$M^{-1}=diag(\frac{1}{\sqrt{r_i/sg}})\f$ and \f$Q^{-1}=diag(\frac{1}{\sqrt{c_j/sg}})\f$.
 *
 * @param[in] A The matrix as FMR matrix, BlasDenseWrapper or
 * ChameleonDenseWrapper, format
 * @param[out] Yr The matrix \f$Y_r = M^{-1}Z\f$ as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[out] Yc The matrix \f$Y_c = Q^{-1}X\f$ as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[out] L vector containing the eigen values
 * @param[in] fmrargs structure containing a set of parameter for the fmr RSVD
 * @param[in] svdmethod rSVD method used internally (full SVD use "svd", fixed
 * rank SVD use "rsvd", fixed accuracy SVD use "farsvd")
 * @param verbose Allows to specify the level of verbosity (0 by default)
 *
 */
template<class MATRIX>
int coa(MATRIX &A,
        MATRIX &Yr,
        MATRIX &Yc,
        std::vector<typename MATRIX::value_type> &L,
        struct fmrArgs<typename MATRIX::value_type> &fmrargs,
        const std::string& svdmethod="rsvd",
        const int verbose = 0){

    //using Size_t = typename MATRIX::int_type;
    using Real_t = typename MATRIX::value_type;

    // Get mpi infos
    int mpiRank = 0; // rank set to 0 if MPI isn't used
#if defined(DIODON_USE_CHAMELEON_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;

    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    // check matrix before pretreatments
    //fmr::Display::matrix(A.getNbRows(), A.getNbCols(), A.getBlasMatrix(), "[diodon] A: ", 10, 8);

    /* Compute Pretreatments */
    fmr::tools::Tic timePre;
    if (isTimed) {
        timePre.tic();
    }

    /* Vector to store diag( sqrt(r_i) ) used in posttreatments */
    std::vector<Real_t> M(A.getNbRows());

    /* Vector to store diag( sqrt(c_j) ) used in posttreatments */
    std::vector<Real_t> Q(A.getNbCols());

    /* Update the matrix with pretreatments */
    diodon::pre::coa(A, M, Q);

    if (isTimed) {
        timePre.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:COA:PRE=" << timePre.elapsed() << " s" << "\n";

    // check matrix after pretreatments
    //fmr::Display::matrix(A.getNbRows(), A.getNbCols(), A.getBlasMatrix(), "[diodon] A': ", (int)A.getNbRows(), (int)A.getNbCols());
    //fmr::Display::vector(M.size(), M.data(), "[diodon] M^-1: ", M.size(), 1);
    //fmr::Display::vector(Q.size(), Q.data(), "[diodon] Q^-1: ", Q.size(), 1);

    /* COA CORE SVD */
    svd(A, L, Yr, Yc, fmrargs, svdmethod, verbose);

    //fmr::Display::vector(L.size(), L.data(), "[diodon] singular values': ", L.size(), 1);
    //fmr::Display::matrix(Yr.getNbRows(), Yr.getNbCols(), Yr.getBlasMatrix(), "[diodon] Yr: ", (int)Yr.getNbRows(), (int)Yr.getNbCols());
    //fmr::Display::matrix(Yc.getNbRows(), Yc.getNbCols(), Yc.getBlasMatrix(), "[diodon] Yc: ", (int)Yc.getNbRows(), (int)Yc.getNbCols());

    /* POSTTREATMENTS */
    if (masterIO && verbose){
        std::cout << "[diodon] Posttreatments ... \n";
    }

    /* Compute Norm of A */
    fmr::tools::Tic timeFrob;
    if (isTimed) {
        timeFrob.tic();
    }
    Real_t normA = fmr::computeFrobenius(A);
    if (isTimed) {
        timeFrob.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:COA:FROBENIUS(A)=" << timeFrob.elapsed() << " s" << "\n";
    if (masterIO && verbose) std::cout << "[diodon] Frobenius Norm A: " << normA << std::endl;

    fmr::tools::Tic timePostCOA;
    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timePostCOA.tic();
    }

    fmr::tools::Tic timeY;
    if (isTimed) {
        timeY.tic();
    }

    /* Compute Z = U*S */
    Yr.scaleCols(Yr.getNbCols(), L.data());
    //fmr::Display::matrix(Yr.getNbRows(), Yr.getNbCols(), Yr.getBlasMatrix(), "[diodon] Z: ", (int)Yr.getNbRows(), (int)Yr.getNbCols());

    /* Compute Yr = M*Z */
    Yr.scaleRows(Yr.getNbRows(), M.data());

    if (isTimed) {
        timeY.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:COA:Yr=" << timeY.elapsed() << " s" << "\n";

    if (isTimed) {
        timeY.tic();
    }
    if ( svdmethod != "evd" ){
        /* V given is V^t actually, we need to transpose V^t to get V */
        MATRIX VT( Yc );
        Yc.reset( Yc.getNbCols(), Yc.getNbRows(), 0. );
        fmr::computeTranspose(VT, Yc);
    }
    /* Compute X = V*S */
    Yc.scaleCols(Yc.getNbCols(), L.data());
    //fmr::Display::matrix(Yc.getNbRows(), Yc.getNbCols(), Yc.getBlasMatrix(), "[diodon] X: ", (int)Yc.getNbRows(), (int)Yc.getNbCols());

    /* Compute Yc = Q X */
    Yc.scaleRows(Yc.getNbRows(), Q.data());

    if (isTimed) {
        timeY.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:COA:Yc=" << timeY.elapsed() << " s" << "\n";

    /* eigen values = (singular values)^2 */
    Real_t quality;
    Real_t psy = 0;
    for (std::size_t i=0; i < L.size(); i++){
        L[i] = L[i]*L[i];
        psy += L[i];
    }
    quality = psy / (normA*normA);
    if (masterIO && verbose) std::cout << "[diodon] rsvd quality estimation ||Psy_r|| / ||A|| = " << quality << std::endl;

    //fmr::Display::vector(L.size(), L.data(), "[diodon] eigen values': ", L.size(), 1);
    //fmr::Display::matrix(Yr.getNbRows(), Yr.getNbCols(), Yr.getBlasMatrix(), "[diodon] Yr: ", (int)Yr.getNbRows(), (int)Yr.getNbCols());
    //fmr::Display::matrix(Yc.getNbRows(), Yc.getNbCols(), Yc.getBlasMatrix(), "[diodon] Yc: ", (int)Yc.getNbRows(), (int)Yc.getNbCols());

#ifdef DIODON_USE_CHAMELEON
    Chameleon::barrier();
#endif

    if (isTimed) {
        timePostCOA.tac();
        if (masterIO) std::cout << "[diodon] TIME:COA:POSTTREATMENTS=" << timePostCOA.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

} // namespace method
} // namespace diodon

#endif
