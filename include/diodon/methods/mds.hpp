﻿/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file mds.hpp
 *
 * Define the MultiDimensional Scaling, MDS method, with function __mds__.
 *
 */
#ifndef MDS_HPP
#define MDS_HPP
// Includes
// std
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
// cstd
#include <cassert>

// mpi
#if defined(DIODON_USE_CHAMELEON_MPI)
#include "mpi.h"
#endif
// fmr
#include "fmr/Utils/MatrixNorms.hpp"
// diodon
#include "diodon/pretreatments/gram.hpp"
#include "diodon/posttreatments/svFilter.hpp"
#include "diodon/methods/attributes.hpp"
#include "diodon/methods/coresvd.hpp"

namespace diodon {
/**
 * @brief Diodon API functions for data analysis methods (i.e. algorithms such
 * as PCA, MDS, etc).
 * @defgroup Methods Data analysis methods
 **/
namespace method {

/**
 *
 * @ingroup Methods
 * @brief Multidimensional Scaling (MDS) of a distance or dissimilarity array.
 *
 * Builds a points cloud X where one point represents an item, such that the
 * discrepancies beween distances in a matrix A and between associated points in
 * \f$\mathbb{R}^k\f$ is as small as possible.
 *
 * Procedure: There are three steps
 * 1. computes the gram matrix G between items from distance matrix A (A becomes
 *    G in output).
 * 2. runs a SVD or randomized SVD of G. SVD: \f$ U S V^t\f$ is the SVD of G
 *    (which is symmetric, \f$V^t\f$ is the transpose of \f$U\f$).
 * 3. computes the coordinates from the SVD in X and give the eigen values in L
 *    \f$\Lambda = S^{1/2}\f$. \f$X\f$ is the matrix of coordinates of n items
 *    in \f$\mathbb{R}^r\f$, \f$X = U S^{1/2}\f$.
 *
 * Recommendations about SVD method:
 * - "svd" (default) is the standard Singular Value Decomposition from Lapack.
 *   It is precise but costly and cannot be used on very large systems. We
 *   recommend N < 10000 and to be used on shared memory only, multi-threaded
 *   Lapack is used internally.
 * - "evd" is the standard Eigen Value Decomposition from Lapack. See "svd". It
 *   can be used for symmetric matrices only.
 * - "rsvd" is a method approximating the "full" SVD with a randomization
 *   technique using a given rank. The parameter "rank" given in fmrargs is used
 *   as an approximation (or already known) rank of the input matrix. Giving a
 *   good rank leads to improve the rsvd precision. This is the method to use
 *   for large systems and it can use several nodes with MPI if Diodon is built
 *   with Chameleon enabled.
 * - "revd" is a method approximating the "full" EVD with a randomization
 *   technique using a given rank, see "rsvd". It must be used for symmetric
 *   matrices only which is the case in the MDS (distance matrix in input).
 * - "farsvd" is a method approximating the "full" SVD with a randomization
 *   technique with a given accuracy. Not implemented yet, please don't use it
 *   for now.
 *
 * @param[in] A The distance matrix as FMR matrix, BlasDenseWrapper or
 * ChameleonDenseWrapper, format
 * @param[out] X The matrix of coordinate as FMR matrix, BlasDenseWrapper or
 * ChameleonDenseWrapper, format
 * @param[out] L vector containing the eigen values
 * @param[in] fmrargs structure containing a set of parameter for the fmr RSVD
 * @param[in] svdmethod rSVD method used internally (full SVD use "svd", fixed
 * rank SVD use "rsvd", fixed accuracy SVD use "farsvd")
 * @param verbose Allows to specify the level of verbosity (0 by default)
 *
 */
template<class MATRIX>
int mds(MATRIX &A,
        MATRIX &X,
        std::vector<typename MATRIX::value_type> &L,
        struct fmrArgs<typename MATRIX::value_type> &fmrargs,
        const std::string& svdmethod="svd",
        const int verbose = 0){

    using Size_t = typename MATRIX::Int_type;
    using Real_t = typename MATRIX::value_type;

    // Get mpi infos
    int mpiRank = 0; // rank set to 0 if MPI isn't used
#if defined(DIODON_USE_CHAMELEON_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;

    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    const int displaySize = 10;
    Real_t normGram = 0;

    /* check matrix is square for mds */
    if ( A.getNbRows() != A.getNbCols() ) {
        std::cerr << "[diodon] MDS method requires a distance (square) matrix in input but nbRows != nbCols:"
                    << " " << A.getNbRows() << " " << A.getNbCols() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    /* BUILD GRAM MATRIX */
    if (masterIO && verbose){
        std::cout << "[diodon] compute Gram matrix ..." << std::endl;
    }
    fmr::tools::Tic timeGram;
    if (isTimed) {
        timeGram.tic();
    }
    diodon::pre::gram(A);
    if (isTimed) {
        timeGram.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:GRAM=" << timeGram.elapsed() << " s" << "\n";

    /* MDS CORE SVD */
    MATRIX UU;
    MATRIX VT;
    svd(A, L, UU, VT, fmrargs, svdmethod, verbose);

    /* POSTTREATMENTS */
    if (masterIO && verbose){
        std::cout << "[diodon] Posttreatments ... \n";
    }

    /* Compute Norm of similarity matrix (Frobenius norm) ... here because last
        time C is need and it can be deleted right after this block. */
    fmr::tools::Tic timeFrob;
    if (isTimed) {
    timeFrob.tic();
    }
    normGram = fmr::computeFrobenius(A);
    if (isTimed) {
        timeFrob.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:FROBENIUS(G)=" << timeFrob.elapsed() << " s" << "\n";
    if (masterIO && verbose) std::cout << "[diodon] Frobenius Norm (Gram Matrix): " << normGram << std::endl;

    fmr::tools::Tic timePostMDS;
    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timePostMDS.tic();
    }

    // Sort Eigenvalues to get positive ones. We also need to select the columns
    // of UU corresponding to the filtered sigma
    std::vector<Size_t> lambda_positive_idx;
    std::vector<Size_t> lambda_negative_idx;

    if (!svdmethod.compare("evd") || !svdmethod.compare("revd")){

      // find out the positive eigen values indexes
      diodon::post::filterPositiveEigenValuesIndexes(L, lambda_positive_idx);

    } else {

      // find out the positive eigen values indexes
      fmr::tools::Tic timeFilter;
      if (isTimed) {
          timeFilter.tic();
      }
      diodon::post::filterPositiveEigenValuesIndexes(UU, VT, lambda_positive_idx);
      if (isTimed) {
          timeFilter.tac();
      }
      if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:FILTER:S+=" << timeFilter.elapsed() << " s" << "\n";

    }

    // deduce the indexes of negative eigenvalues
    typename std::vector<Size_t>::iterator it=lambda_positive_idx.begin();
    for(Size_t i(0); (size_t)i < L.size(); ++i){
        if (*it == i){
            ++it;
        } else {
            lambda_negative_idx.push_back(i);
        }
    }
    // allocate U+ and U-
    Size_t npos = lambda_positive_idx.size();
    Size_t nneg = lambda_negative_idx.size();
    //MATRIXCLOUD Upos(UU.getNbRows(), npos);
    //MATRIXCLOUD Uneg(UU.getNbRows(), nneg);
    /* Let save some space and directly use X as U+. X is a user's object and one
        cannot know in advance the number of columns X will have. Hence we have to
        resize this matrix. */
    fmr::tools::Tic timeX;
    if (isTimed) {
        timeX.tic();
    }
    X.reset(UU.getNbRows(), npos);
    X.allocate();
    if (isTimed) {
        timeX.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:ALLOC:X=" << timeX.elapsed() << " s" << "\n";

    // extract the positive eigen values
    std::vector<Real_t> eigen_positive = L;
    diodon::post::extractEigenValues(lambda_positive_idx, eigen_positive);

    // extract the singular vectors associated with positive eigen values
    fmr::tools::Tic timeExtract;
    if (isTimed) {
        timeExtract.tic();
    }
    UU.extractCols(lambda_positive_idx, X);
    if (isTimed) {
        timeExtract.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:U+=" << timeExtract.elapsed() << " s" << "\n";

    // extract the negative eigen values
    std::vector<Real_t> eigen_negative = L;
    diodon::post::extractEigenValues(lambda_negative_idx, eigen_negative);
    // extract the eigen vectors associated with negative eigen values
    //UU->extractCols(lambda_negative_idx, Uneg);
    // print eigen values if required
    if (verbose > -1) {
        if (masterIO && verbose){
            auto displaySize1 = std::min( (int)npos, displaySize );
            fmr::Display::vector(npos, eigen_positive.data(), "[diodon] Eigen > 0: ", displaySize1, 0);
            displaySize1 = std::min( (int)nneg, displaySize );
            fmr::Display::vector(nneg, eigen_negative.data(), "[diodon] Eigen < 0: ", displaySize1, 0);
        }
    }

    // return X = U+ * S^(1/2)
    fmr::tools::Tic timeScale;
    if (isTimed) {
        timeScale.tic();
    }

    std::vector<Real_t> sqrt_eigen_positive(eigen_positive.size());
    for(std::size_t i=0; i < eigen_positive.size(); ++i){
           sqrt_eigen_positive[i] = std::sqrt(eigen_positive[i]);
    }

    X.scaleCols(npos, sqrt_eigen_positive.data());

    if (isTimed) {
        timeScale.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:MDS:SCALE:X=" << timeScale.elapsed() << " s" << "\n";

    // return Lambda (eigen values > 0)
    L = eigen_positive;

    // Compute quality estimation based on ratio norm (sigma) / norm (G)
    Real_t quality;
    // first compute quality of the RSVD on Gram, using all singular values
    Real_t psy = 0;
    for(std::size_t i=0; i < L.size(); ++i){
        psy += L[i] * L[i];
    }
    psy = std::sqrt(psy);
    quality = psy / normGram;
    if (masterIO) std::cout << "[diodon] rsvd quality estimation ||Psy_r|| / ||G|| = " << quality << std::endl;
    // second compute quality of the MDS on Gram, using all positive eigenvalues
    psy = 0;
    for(std::size_t i=0; i < eigen_positive.size(); ++i){
        psy += eigen_positive[i] * eigen_positive[i];
    }
    psy = std::sqrt(psy);
    quality = psy / normGram;
    if (masterIO) std::cout << "[diodon] mds quality estimation ||Psy^+_r|| / ||G|| = " << quality << std::endl;

    // Check the MDS with || G+ - X+X+^T || / || G+ ||, where G+ = U+ S+ V+^T
    // this operation may be costly in memory because we need to store G and
    // XX^T which are both of size of A (already allocated). Caution: this is
    // not valid with BlasDenseWrapper because fmrGemm is not considering the
    // beta factor (always 0.) with Blas, use Chameleon to perform the following
    bool mdscheck = false;
    const char *mds_check_s = getenv("DIODON_MDS_CHECK");
    if (mds_check_s != NULL) {
      if ( std::stoi(mds_check_s) == 1 ) {
        mdscheck = true;
      }
    }
    if (mdscheck) {
        // Compute G+
        MATRIX GP( X.getNbRows(), X.getNbRows() );
        // extract columns of U related to positive eigenvalues
        MATRIX UP( X.getNbRows(), npos );
        UU.extractCols(lambda_positive_idx, UP);
        if (!svdmethod.compare("evd") || !svdmethod.compare("revd")){
            MATRIX UPT( UP );
            // compute U+ L+
            UP.scaleCols(npos, eigen_positive.data());
            // compute G+ = U+ L+ U+^T
            fmr::fmrGemm<Size_t, Real_t>("N", "T", 1., UP, UPT, 0., GP);
        } else {
            // compute U+ S+
            UP.scaleCols(npos, eigen_positive.data());
            // extract columns of V related to positive eigenvalues
            MATRIX V( VT.getNbCols(), VT.getNbRows() );
            fmr::computeTranspose(VT, V);
            MATRIX VP( VT.getNbCols(), npos );
            V.extractCols(lambda_positive_idx, VP);
            // compute G+ = U+ S+ V+^T
            fmr::fmrGemm<Size_t, Real_t>("N", "T", 1., UP, VP, 0., GP);
        }
        Real_t normGP = fmr::computeFrobenius(GP);
        // Compute G+ - X+X+^T
        fmr::fmrGemm<Size_t, Real_t>("N", "T", -1., X, X, 1., GP);
        Real_t normErr = fmr::computeFrobenius(GP);
        quality = normErr / normGP;
        if (masterIO) std::cout << "[diodon] mds quality estimation || G+ - X+X+^T || / || G+ || = " << quality << std::endl;
    }

#ifdef DIODON_USE_CHAMELEON
    Chameleon::barrier();
#endif

    if (isTimed) {
        timePostMDS.tac();
        if (masterIO && verbose) std::cout << "[diodon] TIME:MDS:POSTTREATMENTS=" << timePostMDS.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

} // namespace method
} // namespace diodon

#endif
