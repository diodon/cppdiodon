/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file attributes.hpp
 *
 * To save string parameters given by diodon method (to give extra information
 * in output file)
 *
 */
#ifndef ATTRIBUTES_HPP
#define ATTRIBUTES_HPP


#include <string>
#include <iostream>
#include <tuple>

namespace diodon {
namespace method {

/**
 * @brief To save string parameters given by mds (to give extra information in
 * output file)
 */
class attributes {
    std::string _fileNameDataIn;
    std::string _method;
    std::string _methodParameter; ///< Specific parameters status depending on the SVD method used
public:
    /** @brief Set input file name to read (with absolute or relative path)
     *  @param name Input filename
     */
    void setDataInputName(const std::string & name)
    {
        _fileNameDataIn = name;
    }
    /** @brief Get Input file name */
    const std::string  getDataInputName()
    {
        return _fileNameDataIn ;
    }
    /** @brief Set SVD Method name (svd, rsvd, farsvd)
     *  @param name Method name
     */
    void setMethodName(const std::string & name)
    {
        _method = name;
    }
    /** @brief Get rSVD method name */
    const std::string & getMethodName()
    {
        return _method ;
    }
    /** @brief Set SVD Method additional parameters (rank, oversampling, epsilon, ...)
     *  @param methodParameter Method parameters name and values as string
     */
    void setMethodParameter(const std::string &methodParameter);
    /** @brief Get rSVD method additional parameters */
    const std::string & getMethodParameter()
    {
        return _methodParameter ;
    }
};

void attributes::setMethodParameter(const std::string &methodParameter)
{
    _methodParameter = methodParameter;
}

/**
 * @brief Possible parameters for the fmr Randomized SVD
 *
 * @ingroup Methods
 *
 */
template<typename Real_t>
struct fmrArgs{
    Real_t eps = -0.01;                  ///< Prescribed accuracy for fixed accuracy rSVD
    int rank = 0;                        ///< Prescribed rank for fixed rank rSVD
    int oversampling = 0;                ///< Oversampling for fixed rank rSVD
    int powerIterations = 0;             ///< Number of powerIteration for rSVD algorithms
    int bARRF = 10;                      ///< Adaptive parameter: balance
    int maxRank = 0;                     ///< Max rank allowed for fixed accuracy rSVD
    bool isTimed = false;                ///< True to time main operations
    attributes *parameters = nullptr;    ///< Attributes about the mds to log when saving
};


} // namespace method
} // namespace diodon

#endif // ATTRIBUTES_HPP
