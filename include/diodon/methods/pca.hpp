/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file pca.hpp
 *
 * Define the Principal Component Analysis, PCA method, with function __pca__.
 *
 */
#ifndef PCA_HPP
#define PCA_HPP
// Includes
// std
#include <iostream>
#include <vector>
#include <string>
// cstd
#include <cassert>

// mpi
#if defined(DIODON_USE_CHAMELEON_MPI)
#include "mpi.h"
#endif
// fmr
#include "fmr/Algorithms/randomSVD.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/MatrixNorms.hpp"
#include "fmr/Utils/MatrixTranspose.hpp"
// diodon
#include "diodon/pretreatments/centerscale.hpp"
#include "diodon/methods/attributes.hpp"

namespace diodon {
namespace method {

/**
 *
 * @ingroup Methods
 * @brief Principal Component Analysis (PCA) of an array.
 *
 * Procedure: There are three steps
 * 1. computes the pretreatments required by user such as centered-scaled matrix
 *    A -> A'
 * 2. runs a SVD or randomized SVD of A'. SVD: \f$ U S V^t\f$ is the SVD of A'
 * 3. computes and returns \f$Y = U S\f$, \f$V\f$, \f$\Lambda = S^2\f$.
 *
 * Recommendations about SVD method:
 * - "svd" will be more precise but cannot be used on very large systems. We
 *   recommend N < 10000 and to be used on shared memory only, multi-threaded
 *   Lapack is used internally.
 * - "rsvd" is a method approximating the "full" SVD with a randomization
 *   technique using a given rank. The parameter "rank" given in fmrargs is used
 *   as an approximation (or already known) rank of the input matrix. Giving a
 *   good rank leads to improve the rsvd precision. This is the method to use
 *   for large systems and it can use several nodes with MPI if Diodon is built
 *   with Chameleon enabled.
 * - "farsvd" is a method approximating the "full" SVD with a randomization
 *   technique with a given accuracy. Not implemented yet, please don't use it
 *   for now.
 *
 * @param[in] A The matrix as FMR matrix, BlasDenseWrapper or
 * ChameleonDenseWrapper, format
 * @param[out] Y The matrix of principal components as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[out] V The matrix V of eigenvectors (new basis) as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[out] L vector containing the eigen values
 * @param[in] fmrargs structure containing a set of parameter for the fmr RSVD
 * @param[in] svdmethod rSVD method used internally (full SVD use "svd", fixed
 * rank SVD use "rsvd", fixed accuracy SVD use "farsvd")
 * @param[in] premethod centering and scaling method applied as pretreatments:
 * - 'standard': for columnwise centering and scaling
 * - 'bicentering': for double averaging method
 * - 'col_centering': for columnise centering only
 * - 'row_centering': for rowwise centering only
 * - 'scaling': for scaling without centering
 * @param verbose Allows to specify the level of verbosity (0 by default)
 *
 */
template<class MATRIX>
int pca(MATRIX &A,
        MATRIX &Y,
        MATRIX &V,
        std::vector<typename MATRIX::value_type> &L,
        struct fmrArgs<typename MATRIX::value_type> &fmrargs,
        const std::string& svdmethod="rsvd",
        const std::string& premethod="standard",
        const int verbose = 0){

    //using Size_t = typename MATRIX::int_type;
    using Real_t = typename MATRIX::value_type;

    // Get mpi infos
    int mpiRank = 0; // rank set to 0 if MPI isn't used
#if defined(DIODON_USE_CHAMELEON_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;

    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    // check matrix before pretreatments
    //fmr::Display::matrix(A.getNbRows(), A.getNbCols(), A.getBlasMatrix(), "[diodon] A: ", 12, 7);

    /* Compute Pretreatments */
    fmr::tools::Tic timePre;
    if (isTimed) {
        timePre.tic();
    }
    if ( premethod.find("standard") != std::string::npos ){
        if (masterIO && verbose){
            std::cout << "[diodon] compute center-scaled matrix ..." << std::endl;
        }
        /* compute centered-scaled matrix transformation columnwise */
        diodon::pre::centerscale(A, true, false, "col");
        diodon::pre::centerscale(A, false, true, "col");

    }
    if (premethod.find("bicentering") != std::string::npos ){
        if (masterIO && verbose){
            std::cout << "[diodon] compute matrix double averaging ..." << std::endl;
        }
        /* compute bicentered matrix transformation */
        diodon::pre::centerscale(A, true, false, "bicenter");
    }
    if (premethod.find("col_centering") != std::string::npos ){
        if (masterIO && verbose){
            std::cout << "[diodon] compute matrix columnwise centering ..." << std::endl;
        }
        /* compute centered matrix transformation columnwise */
        diodon::pre::centerscale(A, true, false, "col");
    }
    if (premethod.find("row_centering") != std::string::npos ){
        if (masterIO && verbose){
            std::cout << "[diodon] compute matrix rowwise centering ..." << std::endl;
        }
        /* compute centered matrix transformation rowwise */
        diodon::pre::centerscale(A, true, false, "row");
    }
    if ( premethod.find("scaling") != std::string::npos ){
        if (masterIO && verbose){
            std::cout << "[diodon] compute matrix scaling columnwise ..." << std::endl;
        }
        /* compute scaled matrix transformation columnwise */
        diodon::pre::centerscale(A, false, true, "col");
    }
    if (isTimed) {
        timePre.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:PCA:PRE=" << timePre.elapsed() << " s" << "\n";

    // check matrix after pretreatments
    //fmr::Display::matrix(A.getNbRows(), A.getNbCols(), A.getBlasMatrix(), "[diodon] A': ", (int)A.getNbRows(), (int)A.getNbCols());

    /* PCA CORE SVD */
    svd(A, L, Y, V, fmrargs, svdmethod, verbose);

    //fmr::Display::vector(L.size(), L.data(), "[diodon] singular values': ", L.size(), 1);
    //fmr::Display::matrix(Y.getNbRows(), Y.getNbCols(), Y.getBlasMatrix(), "[diodon] U: ", (int)Y.getNbRows(), (int)Y.getNbCols());
    //fmr::Display::matrix(V.getNbRows(), V.getNbCols(), V.getBlasMatrix(), "[diodon] V: ", (int)V.getNbRows(), (int)V.getNbCols());

    /* POSTTREATMENTS */
    if (masterIO && verbose){
        std::cout << "[diodon] Posttreatments ... \n";
    }

    /* Compute Norm of A */
    fmr::tools::Tic timeFrob;
    if (isTimed) {
        timeFrob.tic();
    }
    Real_t normA = fmr::computeFrobenius(A);
    if (isTimed) {
        timeFrob.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:PCA:FROBENIUS(A)=" << timeFrob.elapsed() << " s" << "\n";
    if (masterIO && verbose) std::cout << "[diodon] Frobenius Norm A: " << normA << std::endl;

    fmr::tools::Tic timePostPCA;
    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timePostPCA.tic();
    }

    /* Compute Y = U*S*/
    fmr::tools::Tic timeY;
    if (isTimed) {
        timeY.tic();
    }
    Y.scaleCols(Y.getNbCols(), L.data());
    if (isTimed) {
        timeY.tac();
    }
    if (masterIO && isTimed) std::cout << "[diodon] TIME:PCA:Y=" << timeY.elapsed() << " s" << "\n";

    /* eigen values = (singular values)^2 */
    Real_t quality;
    Real_t psy = 0;
    for (std::size_t i=0; i < L.size(); i++){
        L[i] = L[i]*L[i];
        psy += L[i];
    }
    quality = psy / (normA*normA);
    if (masterIO && verbose) std::cout << "[diodon] rsvd quality estimation ||Psy_r|| / ||A|| = " << quality << std::endl;

    //fmr::Display::vector(L.size(), L.data(), "[diodon] eigen values': ", L.size(), 1);
    //fmr::Display::matrix(Y.getNbRows(), Y.getNbCols(), Y.getBlasMatrix(), "[diodon] Y: ", (int)Y.getNbRows(), (int)Y.getNbCols());
    //fmr::Display::matrix(V.getNbRows(), V.getNbCols(), V.getBlasMatrix(), "[diodon] VT: ", (int)V.getNbRows(), (int)V.getNbCols());

    if ( svdmethod != "evd" ){
        /* V given is V^t actually, we need to transpose V^t to get V */
        MATRIX VT( V );
        V.reset( V.getNbCols(), V.getNbRows(), 0. );
        fmr::computeTranspose(VT, V);
    }

#ifdef DIODON_USE_CHAMELEON
    Chameleon::barrier();
#endif

    if (isTimed) {
        timePostPCA.tac();
        if (masterIO) std::cout << "[diodon] TIME:PCA:POSTTREATMENTS=" << timePostPCA.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

} // namespace method
} // namespace diodon

#endif
