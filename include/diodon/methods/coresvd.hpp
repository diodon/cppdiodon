/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file coresvd.hpp
 *
 * Define the SVD method, with function __svd__.
 *
 */
#ifndef CORESVD_HPP
#define CORESVD_HPP
// Includes
// std
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
// cstd
#include <cassert>
// mpi
#if defined(DIODON_USE_CHAMELEON_MPI)
#include "mpi.h"
#endif
// fmr
#include "fmr/Algorithms/randomEVD.hpp"
#include "fmr/Algorithms/randomSVD.hpp"
// diodon
#include "diodon/methods/attributes.hpp"

namespace diodon {
namespace method {
/**
 *
 * @ingroup Methods
 * @brief Singular Values Decomposition (SVD) of an array.
 *
 * Recommendations about SVD method:
 * - "svd" (default) is the standard Singular Value Decomposition from Lapack.
 *   It is precise but costly and cannot be used on very large systems. We
 *   recommend N < 10000 and to be used on shared memory only, multi-threaded
 *   Lapack is used internally.
 * - "evd" is the standard Eigen Value Decomposition from Lapack. See "svd". It
 *   can be used for symmetric matrices only.
 * - "rsvd" is a method approximating the "full" SVD with a randomization
 *   technique using a given rank. The parameter "rank" given in fmrargs is used
 *   as an approximation (or already known) rank of the input matrix. Giving a
 *   good rank leads to improve the rsvd precision. This is the method to use
 *   for large systems and it can use several nodes with MPI if Diodon is built
 *   with Chameleon enabled.
 * - "revd" is a method approximating the "full" EVD with a randomization
 *   technique using a given rank, see "rsvd". It must be used for symmetric
 *   matrices only.
 * - "farsvd" is a method approximating the "full" SVD with a randomization
 *   technique with a given accuracy. Not implemented yet, please don't use it
 *   for now.
 *
 * @param[in] A The matrix as FMR matrix, BlasDenseWrapper or
 * ChameleonDenseWrapper, format
 * @param[out] S vector containing the singular values
 * @param[out] U The left singular vectors matrix as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[out] VT The right singular vectors matrix as FMR matrix,
 * BlasDenseWrapper or ChameleonDenseWrapper, format
 * @param[in] fmrargs structure containing a set of parameter for the fmr RSVD
 * @param[in] svdmethod rSVD method used internally (full SVD or EVD use "svd"
 * or "evd", fixed rank SVD or EVD use "rsvd" or "revd")
 * @param verbose Allows to specify the level of verbosity (0 by default)
 *
 */
template<class MATRIX>
int svd(MATRIX &A,
        std::vector<typename MATRIX::value_type> &S,
        MATRIX &U,
        MATRIX &VT,
        struct fmrArgs<typename MATRIX::value_type> &fmrargs,
        const std::string& svdmethod="svd",
        const int verbose = 0){

    using Size_t = typename MATRIX::Int_type;
    using Real_t = typename MATRIX::value_type;

    // Get mpi infos
    int mpiRank = 0; // rank set to 0 if MPI isn't used
#if defined(DIODON_USE_CHAMELEON_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;

    bool isTimed = false;
    const char* envTime_s = getenv("DIODON_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }

    int prescribed_rank;
    if ( fmrargs.rank == 0) {
        prescribed_rank = A.getNbCols();
    } else {
        prescribed_rank = fmrargs.rank;
    }
    const int rank = prescribed_rank;
    const int oversampling = fmrargs.oversampling;
    const int qRSI = fmrargs.powerIterations;
    std::string paramMethod;
    fmr::tools::Tic timeRandSVD;
    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timeRandSVD.tic();
    }

    Real_t energy2;
    Real_t estimator;
    if (!svdmethod.compare("revd") || !svdmethod.compare("rsvd")){
        if (prescribed_rank < 0){
            prescribed_rank = A.getNbCols() / 4;
        }
        if (masterIO && verbose) {
            if (!svdmethod.compare("revd")) {
                std::cout << "[diodon]  ---- REVD ---- " << std::endl;
            }
            if (!svdmethod.compare("rsvd")) {
                std::cout << "[diodon]  ---- RSVD ---- " << std::endl;
            }
            std::cout << "[diodon]   Prescribed rank: " << prescribed_rank << std::endl;
            std::cout << "[diodon]    Oversampling: " << oversampling << std::endl;
        }
        paramMethod = "{ r, os, q} = "+std::to_string(prescribed_rank) + " " +std::to_string(oversampling) + " "
            +std::to_string(qRSI);
        if ((Size_t)(prescribed_rank + oversampling) > A.getNbRows()){
            std::cerr << "[diodon] Oversampled rank should be lower than full rank" << std::endl;
            std::cerr << "[diodon] Please make sure that parameter rank (-r) and oversample (-os) add up to less than the size of the matrix" << std::endl;
            exit(EXIT_FAILURE);
        }
        if (masterIO && verbose) std::cout << "[diodon] Nb of subspace iterations: " << qRSI << std::endl;
        energy2 = 0.0; // useful energy2 ?
        estimator = 1.0; ///useful estimator ?
    }

    // Check the method argument to call the correct evd/svd version
    if (!svdmethod.compare("revd")){

        /* Fixed Rank Randomized EVD */
        if (fmrargs.parameters != nullptr ){
            fmrargs.parameters->setMethodName("Rank fixed REVD");
        }

        fmr::randomEVD(rank, oversampling, qRSI,
                       A, S, U, energy2, estimator);

    } else if (!svdmethod.compare("rsvd")){

        /* Fixed Rank Randomized SVD */
        if (fmrargs.parameters != nullptr ){
            fmrargs.parameters->setMethodName("Rank fixed RSVD");
        }

        fmr::randomSVD(rank, oversampling, qRSI,
                       A, S, U, VT, energy2, estimator);

    } else if (!svdmethod.compare("farsvd")) {

        /* Fixed Accuracy Randomized SVD */
        if (fmrargs.parameters != nullptr ){
            fmrargs.parameters->setMethodName("Adaptive RSVD");
        }
        // Error message while this method doesn't work
        std::cerr << "[diodon] Error in file " << __FILE__ << " on line " << __LINE__ << std::endl;
        std::cerr << "[diodon] method farsvd (fixed accuracy random svd) is not yet fully implemented" << std::endl;
        std::cerr << "[diodon] Please only use rsvd for now" << std::endl;
        exit(EXIT_FAILURE);
        //
        if (masterIO && verbose) {
            std::cout << "[diodon]  ---- API Adaptive Randomized Range Finder  ---- " << std::endl;
            std::cout << "[diodon]   Rank: not known a priori!" << std::endl;
            std::cout << "[diodon]   Adaptive parameters: balance=" << fmrargs.bARRF
                        << "           - accuracy:" << fmrargs.eps << std::endl;
            std::cout << "[diodon]     Nb of power iterations: " << qRSI << std::endl;
        }
        paramMethod = "{ eps, q } = "+std::to_string(fmrargs.eps) + " " +std::to_string(qRSI);
        //Real_t *U = nullptr;
        /*
            * fixedAccuracy is broken :
            * - Not compatible with Chameleon
            * - Probably need to use a Real_t* instead of a vector for S
            * - Maybe make a test to not run with chameleon ?
            */
        // Real_t energy2 = 0.0; // useful energy2 ?
        // Real_t estimator = 1.0; ///useful estimator ?
        // fmr::fixedAccuracySVD(fmrargs.eps, fmrargs.bARRF, qRSI, fmrargs.maxRank,
        //                       A, S, pU,
        //                       energy2, estimator);

    } else if (!svdmethod.compare("svd")){

        /* Full SVD */
        if (fmrargs.parameters != nullptr ){
            fmrargs.parameters->setMethodName("SVD");
        }
        if (masterIO && verbose) {
            std::cout << "[diodon]  ---- SVD  ---- " << std::endl;
        }
        //fmr::fullRankSVD(prescribed_rank, fmrargs.eps, A, S, UU, VT);
        Size_t minrc = std::min(A.getNbCols(), A.getNbRows());
        Real_t *sigma = new Real_t[ minrc ];
        fmr::SVD<Real_t, Size_t>::computeSVD(A, sigma, U, VT);
        S.insert(S.end(), &sigma[0], &sigma[minrc]);
        delete[] sigma;
        paramMethod = "{ r, eps } = " + std::to_string(U.getNbCols()) + " " + std::to_string(fmrargs.eps);

    } else if (!svdmethod.compare("evd")){

        /* Full EVD */
        if (fmrargs.parameters != nullptr ){
            fmrargs.parameters->setMethodName("EVD");
        }
        if (masterIO && verbose) {
            std::cout << "[diodon]  ---- EVD  ---- " << std::endl;
        }
        Size_t minrc = std::min(A.getNbCols(), A.getNbRows());
        Real_t *lambda = new Real_t[ minrc ];
        fmr::EVD<Real_t, Size_t>::computeEVD(A, lambda, U, true);
        S.insert(S.end(), &lambda[0], &lambda[minrc]);
        delete[] lambda;
        paramMethod = "{ r, eps } = " + std::to_string(U.getNbCols()) + " " + std::to_string(fmrargs.eps);

    } else { // Default case

        std::cerr << "[diodon] Error in file " << __FILE__ << " on line " << __LINE__ << std::endl;
        std::cerr << "[diodon] Unknown method " << svdmethod << " for svd: only use rsdv." << std::endl;
        exit(EXIT_FAILURE);

    }

    if (fmrargs.parameters != nullptr ){
        fmrargs.parameters->setMethodParameter(paramMethod);
    }

    if (isTimed) {
#ifdef DIODON_USE_CHAMELEON
        Chameleon::barrier();
#endif
        timeRandSVD.tac();
        if (masterIO) std::cout << "[diodon] TIME:SVD=" << timeRandSVD.elapsed() << " s" << "\n";
    }

    return EXIT_SUCCESS;
}

} // namespace method
} // namespace diodon

#endif // CORESVD_HPP
