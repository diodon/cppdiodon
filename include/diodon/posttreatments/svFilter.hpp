/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file svFilter.hpp
 *
 * To filter eigen vectors associated to positive/negative eigen values
 *
 */
#ifndef SV_FILTER_HPP
#define SV_FILTER_HPP

#include <vector>
#include <string>

namespace diodon {
/**
 * @brief Diodon API functions to compute posttreatments on the methods output
 * @defgroup PostTreatments Post-treatments
 **/
namespace post {

/**
 * @ingroup PostTreatments
 * @brief Filter indexes of positive eigenvalues.
 * @param[in] lambda vector representing the eigen values
 * @param[out] indexes indexes of the positive eigenvalues
 *
 */
template <typename T1, typename T2>
void filterPositiveEigenValuesIndexes(std::vector<T1> &lambda,
                                      std::vector<T2> &indexes){
  for (size_t i(0); i < lambda.size(); ++i){
    if (lambda[i] > 0){
      indexes.push_back(i);
    }
  }
}

/**
 * @ingroup PostTreatments
 * @brief Filter indexes of singular vectors associated with positive
 * eigenvalues.
 * @param[in] U matrix representing the left singular vectors
 * @param[in] VT matrix representing the right singular vectors
 * @param[out] indexes indexes of the positive eigenvalues
 * @param[in] oversampling oversampling number (if used, else 0)
 *
 */
template <class WrapperType>
void filterPositiveEigenValuesIndexes(const WrapperType &U, const WrapperType &VT,
                                      std::vector<typename WrapperType::Int_type> &indexes){

  using Size_t = typename WrapperType::Int_type;
  using Real_t = typename WrapperType::value_type;

  Size_t size = U.getNbCols();
  Real_t *first_line_u = new Real_t[size];
  Real_t *first_column_vt = new Real_t[size];
  U.getSubMatrix(0, 0, 1, size, first_line_u);
  VT.getSubMatrix(0, 0, size, 1, first_column_vt);

  for (Size_t i(0); i < size; ++i){
    if (first_line_u[i] * first_column_vt[i] > 0){
      indexes.push_back(i);
    }
  }
  delete [] first_line_u;
  delete [] first_column_vt;
}

/**
 * @ingroup PostTreatments
 * @brief Restrict lambda on some indexes
 *
 * @param[in] indexes The vector of indexes to keep
 * @param[in,out] lambda The vector to filter
 *
 */
template <class Size_t, class Real_t>
void extractEigenValues(std::vector<Size_t> &indexes,
                        std::vector<Real_t> &lambda
                        ){
  /* Extract the singular values */
  for(Size_t i(0); (size_t)i < indexes.size(); ++i){
    lambda.at(i) = lambda.at(indexes.at(i));
  }
  lambda.resize(indexes.size());
}

} // namespace post
} // namespace diodon

#endif // SV_FILTER_HPP
