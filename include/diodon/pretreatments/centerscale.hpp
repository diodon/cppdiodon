/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file centerscale.hpp
 *
 * Centering and scaling matrix transformation.
 *
 */

#ifndef CENTERSCALE_HPP
#define CENTERSCALE_HPP

#include <iostream>
#ifdef DIODON_USE_CHAMELEON
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace diodon {
namespace pre {

/**
 *
 * @ingroup PreTreatments
 * @brief Computes centered-scaled matrix transformation
 *
 * We compute a centering and or scaling on a matrix inplace either rowwise or
 * columnwise or both (bicenter).
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[g_i = \frac{1}{n} \sum_j a_{ij} \\
 *    g_j = \frac{1}{m} \sum_i a_{ij} \\
 *    g   = \frac{1}{mn} \sum_{i,j} a_{ij}\f]
 * A centered rowwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i \f]
 * A centered columnwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_j \f]
 * A bicentered gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i - g_j + g \f]
 * Lets
 * \f[n_i = || a_{i*} || = \sqrt{ \sum_j a_{ij}²} \\
 *    n_j = || a_{*j} || = \sqrt{ \sum_i a_{ij}²} \f]
 * A scaled rowwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{i*}' = \frac{a_{i*}}{n_i} \f]
 * A scaled columnwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{*j}' = \frac{a_{*j}}{n_j} \f]
 *
 * Input matrix have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[in] m number of rows of the matrix
 *  @param[in] n number of columns of the matrix
 *  @param[in] center whether or not to center the matrix (default: true)
 *  @param[in] scale whether or not to scale the matrix (default: true)
 *  @param[in] axis use 'row' to center rowwise, 'col' to center columnwise,
 *             'bicenter' to center on both axis (default: col)
 *
 */
template<typename Idx_t ,typename Real_t>
int centerscale(Real_t* A, const Idx_t m, const Idx_t n, const bool center=true, const bool scale=true, const std::string &axis="col")
{
    if ( axis != "row" && axis != "col" && axis != "bicenter" ) {
        std::cerr << "[diodon] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
        std::cerr << "[diodon] axis must be either row or col or bicenter" << std::endl;
        return EXIT_FAILURE;
    }

    if ( !center  && !scale ) {
        std::cerr << "[diodon] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
        std::cerr << "[diodon] either center or scale must should be true" << std::endl;
        return EXIT_FAILURE;
    }

    if ( center && scale && axis == "bicenter" ) {
        std::cerr << "[diodon] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
        std::cerr << "[diodon] bicenter method is not compatible with scaling" << std::endl;
        return EXIT_FAILURE;
    }

    Real_t p = 1./m;
    Real_t q = 1./n;

    /* barycenters on axis i and j */
    Real_t* g_i = new Real_t[m];
    Real_t* g_j = new Real_t[n];

    /* norm 2 on axis i and j */
    Real_t* d_i = new Real_t[m];
    Real_t* d_j = new Real_t[n];

    /* global mean */
    Real_t g = 0.0;

    /* compute means columnwise and global means */
#pragma omp parallel for shared(A, g_j, d_j, p) reduction(+:g)
    for (Idx_t j=0; j < n; j++) {
        g_j[j] = 0.0;
        d_j[j] = 0.0;
        for (Idx_t i=0; i < m; i++) {
            g_j[j] += A[j*m + i];
            g += A[j*m + i];
            d_j[j] += A[j*m + i]*A[j*m + i];
        }
        g_j[j] *= p;
        d_j[j] = std::sqrt(d_j[j]);
    }
    g *= p;
    g *= q;

   /* compute means rowwise */
#pragma omp parallel for shared(A, g_i, d_i, q)
    for (Idx_t i=0; i < m; i++) {
        g_i[i] = 0.0;
        d_i[i] = 0.0;
        for (Idx_t j=0; j < n; j++) {
            g_i[i] += A[j*m + i];
            d_i[i] += A[j*m + i]*A[j*m + i];
        }
        g_i[i] *= q;
        d_i[i] = std::sqrt(d_i[i]);
    }

    /* center and scale the matrix */
#pragma omp parallel for shared(A, g_i, g_j, g, d_i, d_j) collapse(2)
    for (Idx_t j=0; j < n; j++) {
        for (Idx_t i=0; i < m; i++) {
            if ( center ) {
                if ( axis == "col" ) {
                    A[j*m + i] = A[j*m + i] - g_j[j];
                } else if (axis == "row" ) {
                    A[j*m + i] = A[j*m + i] - g_i[i];
                } else {
                    A[j*m + i] = A[j*m + i] - g_i[i] - g_j[j] + g;
                }
            }
            if ( scale ) {
                if ( axis == "col" ) {
                    A[j*m + i] /= d_j[j];
                } else {
                    A[j*m + i] /= d_i[i];
                }
            }
        }
    }

    delete[] g_i;
    delete[] g_j;
    delete[] d_i;
    delete[] d_j;
    return EXIT_SUCCESS;
}

/**
 *
 * @ingroup PreTreatments
 * @brief Computes centered-scaled matrix transformation
 *
 * We compute a centering and or scaling on a matrix inplace either rowwise or
 * columnwise or both (bicenter).
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[g_i = \frac{1}{n} \sum_j a_{ij} \\
 *    g_j = \frac{1}{m} \sum_i a_{ij} \\
 *    g   = \frac{1}{mn} \sum_{i,j} a_{ij}\f]
 * A centered rowwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i \f]
 * A centered columnwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_j \f]
 * A bicentered gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i - g_j + g \f]
 * Lets
 * \f[n_i = || a_{i*} || = \sqrt{ \sum_j a_{ij}²} \\
 *    n_j = || a_{*j} || = \sqrt{ \sum_i a_{ij}²} \f]
 * A scaled rowwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{i*}' = \frac{a_{i*}}{n_i} \f]
 * A scaled columnwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{*j}' = \frac{a_{*j}}{n_j} \f]
 *
 * Input matrix have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[in] center whether or not to center the matrix (default: true)
 *  @param[in] scale whether or not to scale the matrix (default: true)
 *  @param[in] axis use 'row' to center rowwise, 'col' to center columnwise,
 *             'bicenter' to center on both axis (default: col)
 *
 */
template<class Matrix>
int centerscale(Matrix &A, const bool center=true, const bool scale=true, const std::string &axis="col")
{
    return centerscale( A.getMatrix(), A.getNbRows(), A.getNbCols(), center, scale, axis );
}

#ifdef DIODON_USE_CHAMELEON
/**
 *
 * @ingroup PreTreatments
 * @brief Computes centered-scaled matrix transformation
 *
 * We compute a centering and or scaling on a matrix inplace either rowwise or
 * columnwise or both (bicenter).
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[g_i = \frac{1}{n} \sum_j a_{ij} \\
 *    g_j = \frac{1}{m} \sum_i a_{ij} \\
 *    g   = \frac{1}{mn} \sum_{i,j} a_{ij}\f]
 * A centered rowwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i \f]
 * A centered columnwise gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_j \f]
 * A bicentered gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = a_{i,j} - g_i - g_j + g \f]
 * Lets
 * \f[n_i = || a_{i*} || = \sqrt{ \sum_j a_{ij}²} \\
 *    n_j = || a_{*j} || = \sqrt{ \sum_i a_{ij}²} \f]
 * A scaled rowwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{i*}' = \frac{a_{i*}}{n_i} \f]
 * A scaled columnwise gives \f[A' = (a_{i,j}')_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ a_{*j}' = \frac{a_{*j}}{n_j} \f]
 *
 * Input matrix have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[in] center whether or not to center the matrix (default: true)
 *  @param[in] scale whether or not to scale the matrix (default: true)
 *  @param[in] axis use 'row' to center rowwise, 'col' to center columnwise,
 *             'bicenter' to center on both axis (default: col)
 *
 */
template<class FSize, class FReal>
int centerscale(fmr::ChameleonDenseWrapper<FSize, FReal> &A, const bool center=true, const bool scale=true, const std::string &axis="col")
{
    int centercham = center ? 1 : 0 ;
    int scalecham = scale ? 1 : 0 ;
    cham_store_t axischam = ChamColumnwise;
    if ( axis == "row" ) {
        axischam = ChamRowwise;
    }
    if ( axis == "bicenter" ) {
        axischam = ChamEltwise;
    }
    return Chameleon::cesca( centercham, scalecham, axischam, A.getMatrix(), A.getMatrixWorkspaceCesca() );
}
#endif


} // namespace pre
} // namespace diodon

#endif
