/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file gram.hpp
 *
 * Gram matrix computation.
 *
 */

#ifndef GRAM_HPP
#define GRAM_HPP

#include <iostream>
#ifdef DIODON_USE_CHAMELEON
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace diodon {
/**
 * @brief Diodon API functions to compute pretreatments on two dimensional
 * matrices.
 * @defgroup PreTreatments Pre-treatments
 **/
namespace pre {

/**
 *
 * @ingroup PreTreatments
 * @brief Computes Gram matrix from a matrix (Real_t format)
 *
 * We compute a double centering on the A matrix, i.e. A^e (by default e=2).
 *
 * \f[d_{i.}^2   = (1/n) \sum_j d_{ij}^2 \\
 *    d_{..}^2   = (1/n^2) \sum_{i,j} d_{ij}^2 \\
 *    gram_{i,j} = -(1/2) (d_{ij}^2 - d_{i.}^2 - d_{.j}^2 + d_{..}^2) \f]
 *
 * Gram matrix have to be already allocated, i.e. it performs inplace
 * transformation. We do not use the fact that the matrix is symmetric.
 *
 *  @param[inout] A matrix (full matrix), Gram matrix in output
 *  @param[in] n size (number of rows) of the squared matrix
 *
 */
template<typename Idx_t ,typename Real_t>
int gram( Real_t* A, const Idx_t& n)
{
    Idx_t  k;
    const Real_t p = 1./n;
    std::size_t n2 = ((std::size_t)n)*n;

#pragma omp parallel for simd shared(n,n2,A)
    for (std::size_t i = 0 ; i < n2 ; ++i) {
        A[i] = A[i]*A[i];
    }

    Real_t* d_i = new Real_t[n];
    Real_t d_g = 0.;

    /* set default to shared for Gcc-9 (const p ) */
#if( __GNUC__ >= 9)
#pragma omp parallel for default(shared) shared(n, A, d_i) reduction(+:d_g)
#else
#pragma omp parallel for default(none) shared(n, A, d_i) reduction(+:d_g)
#endif
    for (std::size_t i = 0 ; i < (std::size_t)n ; ++i) {
        Real_t x=0,y=0;
        Real_t* col= &A[i*(std::size_t)n] ;
#pragma omp simd reduction(+:x)
        for (std::size_t j = 0 ; j < (std::size_t)n ; ++j) {
            x += col[j];
            //x += A[i*n+j];
        }
        y = x*p;
        d_i[i]  = y;
        d_g    += y;
    }
    d_g = d_g*p;

    /* compute in place gram matrix */
    constexpr Real_t coeff = -0.5 ;
#pragma omp parallel for  shared(n,d_i,A,d_g)  private(k) collapse(2)
    for (std::size_t i = 0 ; i < (std::size_t)n ; ++i) {
        for (std::size_t j = 0 ; j < (std::size_t)n ; ++j) {
            k = j + i*(std::size_t)n;
            A[k] = coeff*(A[k] - d_i[i] - d_i[j] + d_g);
        }
    }
    return EXIT_SUCCESS;
}

/**
 *
 * @ingroup PreTreatments
 * @brief Computes Gram matrix from a matrix (Matrix format)
 *
 * We compute a double centering on the A matrix, i.e. A^e (by default e=2).
 *
 * \f[d_{i.}^2   = (1/n) \sum_j d_{ij}^2 \\
 *    d_{..}^2   = (1/n^2) \sum_{i,j} d_{ij}^2 \\
 *    gram_{i,j} = -(1/2) (d_{ij}^2 - d_{i.}^2 - d_{.j}^2 + d_{..}^2) \f]
 *
 * Gram matrix have to be already allocated, i.e. it performs inplace
 * transformation. We do not use the fact that the matrix is symmetric.
 *
 *  @param[inout] A matrix (full matrix), Gram matrix in output
 *
 */
template<class Matrix>
int gram(Matrix &A) {
    /*static_cast<typename Matrix::Real_type*>*/
    return gram( A.getMatrix(), A.getNbRows() );
}

#ifdef DIODON_USE_CHAMELEON
/**
 *
 * @ingroup PreTreatments
 * @brief Computes Gram matrix from a matrix (ChameleonDenseWrapper format)
 *
 * We compute a double centering on the A matrix, i.e. A^e (by default e=2).
 *
 * \f[d_{i.}^2   = (1/n) \sum_j d_{ij}^2 \\
 *    d_{..}^2   = (1/n^2) \sum_{i,j} d_{ij}^2 \\
 *    gram_{i,j} = -(1/2) (d_{ij}^2 - d_{i.}^2 - d_{.j}^2 + d_{..}^2) \f]
 *
 * Gram matrix have to be already allocated, i.e. it performs inplace
 * transformation. The input matrix may be symmetric (parameter of the
 * ChameleonDenseWrapper format).
 *
 *  @param[inout] A input matrix (full matrix or upper/lower triangular), Gram
 *  matrix in output
 *
 */
template<class FSize, class FReal>
int gram(fmr::ChameleonDenseWrapper<FSize, FReal> &A) {
    return Chameleon::gram(A.getUpperLower(), A.getMatrix(), A.getMatrixWorkspaceGram());
}
#endif

} // namespace pre
} // namespace diodon

#endif
