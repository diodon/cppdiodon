/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file precoa.hpp
 *
 * Pretreatment of the Correspondence Analysis (COA) of a contingency table
 *
 */

#ifndef PRECOA_HPP
#define PRECOA_HPP

#include <iostream>
#ifdef DIODON_USE_CHAMELEON
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace diodon {
namespace pre {

/**
 *
 * @ingroup PreTreatments
 * @brief Computes COA pre-treatment transformation
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[r_i = \sum_j a_{ij} \\
 *    c_j = \sum_i a_{ij} \\
 *    sg  = \sum_{i,j} a_{ij}\f]
 * A transformed gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = \frac{a_{i,j}-r_i*c_j/sg}{ \sqrt{r_i*c_j} } \f]
 *
 * It also gives \f[ \frac{1}{\sqrt{r_i/sg}} \f] and \f[ \frac{1}{\sqrt{c_j/sg}} \f]
 * in vectors M and Q useful in the post-treatment of the COA.
 * M and Q must be already allocated.
 *
 * Input matrix A have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[inout] M vector of size m containing the 1/(sqrt(r_i)/sg)
 *  @param[inout] Q vector of size m containing the 1/(sqrt(c_j)/sg)
 *  @param[in] m number of rows of the matrix
 *  @param[in] n number of columns of the matrix
 *
 */
template<typename Idx_t ,typename Real_t>
int coa(Real_t* A, Real_t* M, Real_t* Q, const Idx_t m, const Idx_t n)
{
    /* global sum */
    Real_t sg = 0.0;

    /* sums on axis i and j */
    Real_t* r_i = new Real_t[m];
    Real_t* c_j = new Real_t[n];

    /* compute sums on rows and columns (marginals) */
#pragma omp parallel for shared(A, c_j, sg)
    for (Idx_t j=0; j < n; j++) {
        c_j[j] = 0.0;
        for (Idx_t i=0; i < m; i++) {
            c_j[j] += A[j*m + i];
        }
    }
#pragma omp parallel for shared(A, r_i)
    for (Idx_t i=0; i < m; i++) {
        r_i[i] = 0.0;
        for (Idx_t j=0; j < n; j++) {
            r_i[i] += A[j*m + i];
        }
    }

    /* compute global sum */
#pragma omp parallel for shared(r_i) reduction(+:sg)
    for (Idx_t i=0; i < m; i++) {
        sg += r_i[i];
    }

    /* update the matrix */
#pragma omp parallel for shared(A, r_i, c_j, sg) collapse(2)
    for (Idx_t j=0; j < n; j++) {
        for (Idx_t i=0; i < m; i++) {
            Real_t rc = r_i[i] * c_j[j];
            Real_t sqrc = sqrt(rc);
            A[j*m + i] -= rc / sg;
            A[j*m + i] /= sqrc;
        }
    }

    /* update the vectors M and Q */
#pragma omp parallel for shared(M, r_i, sg)
    for (Idx_t i=0; i < m; i++) {
        M[i] = 1.0/sqrt(r_i[i]/sg);
    }
#pragma omp parallel for shared(Q, c_j, sg)
    for (Idx_t j=0; j < n; j++) {
        Q[j] = 1.0/sqrt(c_j[j]/sg);
    }

    // printf("sg %f\n", sg);
    // printf("r_i\n");
    // for (Idx_t i=0; i < m; i++) {
    //     printf("%f ", r_i[i]);
    // }
    // printf("\n");
    // printf("c_j\n");
    // for (Idx_t j=0; j < n; j++) {
    //     printf("%f ", c_j[j]);
    // }
    // printf("\n");
    // printf("M\n");
    // for (Idx_t i=0; i < m; i++) {
    //     printf("%f ", M[i]);
    // }
    // printf("\n");
    // printf("Q\n");
    // for (Idx_t j=0; j < n; j++) {
    //     printf("%f ", Q[j]);
    // }
    // printf("\n");

    delete[] r_i;
    delete[] c_j;
    return EXIT_SUCCESS;
}

/**
 *
 * @ingroup PreTreatments
 * @brief Computes COA pre-treatment transformation
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[r_i = \sum_j a_{ij} \\
 *    c_j = \sum_i a_{ij} \\
 *    sg  = \sum_{i,j} a_{ij}\f]
 * A transformed gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = \frac{a_{i,j}-r_i*c_j/sg}{ \sqrt{r_i*c_j} } \f]
 *
 * It also gives \f[ \frac{1}{\sqrt{r_i/sg}} \f] and \f[ \frac{1}{\sqrt{c_j/sg}} \f]
 * in vectors M and Q useful in the post-treatment of the COA.
 * M and Q must be already allocated.
 *
 * Input matrix A have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[inout] M vector of size m containing the 1/(sqrt(r_i)/sg)
 *  @param[inout] Q vector of size m containing the 1/(sqrt(c_j)/sg)
 *
 */
template<class Matrix>
int coa(Matrix &A,
        std::vector<typename Matrix::value_type> &M,
        std::vector<typename Matrix::value_type> &Q)
{
    return coa( A.getMatrix(), M.data(), Q.data(), A.getNbRows(), A.getNbCols() );
}

#ifdef DIODON_USE_CHAMELEON
/**
 *
 * @ingroup PreTreatments
 * @brief Computes COA pre-treatment transformation
 *
 * Considering a matrix A of size m x n, \f[A = (a_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f]
 * Lets
 * \f[r_i = \sum_j a_{ij} \\
 *    c_j = \sum_i a_{ij} \\
 *    sg  = \sum_{i,j} a_{ij}\f]
 * A transformed gives \f[\bar{A} = (\bar{a}_{i,j})_{1 \leq i \leq m, 1 \leq j \leq n}\f] such that
 * \f[ \bar{a}_{i,j} = \frac{a_{i,j}-r_i*c_j/sg}{ \sqrt{r_i*c_j} } \f]
 *
 * It also gives \f[ \frac{1}{\sqrt{r_i/sg}} \f] and \f[ \frac{1}{\sqrt{c_j/sg}} \f]
 * in vectors M and Q useful in the post-treatment of the COA.
 * M and Q must be already allocated.
 *
 * Input matrix A have to be already allocated, i.e. it performs inplace
 * transformation, and given in column-major format.
 *
 *  @param[inout] A matrix m x n, centered-scaled matrix in output
 *  @param[inout] M vector of size m containing the 1/(sqrt(r_i)/sg)
 *  @param[inout] Q vector of size m containing the 1/(sqrt(c_j)/sg)
 *
 */
template<class FSize, class FReal>
int coa(fmr::ChameleonDenseWrapper<FSize, FReal> &A,
        std::vector<FReal> &M,
        std::vector<FReal> &Q)
{
    /* update matrix A */
    int err = Chameleon::cesca( true, true, ChamEltwise, A.getMatrix(), M.data(), Q.data(), A.getMatrixWorkspaceCesca() );

    /* update M and Q */
    FReal sg = 0.0;
    for (size_t i=0; i < M.size(); i++) {
        sg += M[i];
    }
    for (size_t i=0; i < M.size(); i++) {
        M[i] = 1.0/sqrt(M[i]/sg);
    }
    for (size_t i=0; i < Q.size(); i++) {
        Q[i] = 1.0/sqrt(Q[i]/sg);
    }

    return err;
}
#endif


} // namespace pre
} // namespace diodon

#endif
