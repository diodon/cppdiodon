/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file Dataset.hpp
 *
 * Handles HDF5 datasets
 *
 */
#ifndef DATASET_H
#define DATASET_H

#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <string>
#include <array>
#include "hdf5.h"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"

namespace diodon {
namespace io {

/**
 * @ingroup InputOutput
 * @brief Handles hdf5 datasets
 */
class Dataset {
private:
  std::string _fileName ;
  std::string _dataSetName ;
  hid_t   _h5File;
  hid_t   _dataSet ;
  std::array<hsize_t,2> _dims_out ;

public:
  /**
   * @brief Constructor from hdf5 file to array
   * @param[in] filename name of the hdf5 file
   * @param[in] datasetname name of the dataset in the file
   */
 Dataset(const std::string &filename, const std::string &datasetname) :
  _fileName(filename), _dataSetName(datasetname)
  {
    _h5File = H5Fopen(_fileName.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    _dataSet = H5Dopen2(_h5File, _dataSetName.c_str(), H5P_DEFAULT);
    hid_t dspace = H5Dget_space(_dataSet);
    H5Sget_simple_extent_dims(dspace, _dims_out.data(), NULL);
    H5Sclose(dspace);
  }

  ~Dataset()
  {
    ssize_t cnt = H5Fget_obj_count(_h5File, H5F_OBJ_ALL);
    if (cnt > 0){
      H5Dclose(_dataSet);
      H5Fclose(_h5File);
    }
  }

/**
 * @brief Constructor from hdf5 file to array
 * @param[in] filename name of the hdf5 file
 * @param[in] datasetname name of the dataset in the file
 * @param[out] n size of the matrix (number of rows)
 */
 template <typename INT>
 Dataset(const std::string &filename, const std::string &datasetname, INT &n) : Dataset(filename, datasetname)
 {
  n = this->getSize();
 }
 /** @brief Get size of the matrix (number of rows) */
 std::int32_t getSize() const{
   return _dims_out[0] ;
 }

/**
 * @brief Constructor from hdf5 file to BlasDenseWrapper
 * @param[out] matrix container for the dataset, matrix must be already
 * initialized (allocated)
 */
 template <typename Size, typename Real_t>
  void load( fmr::BlasDenseWrapper<Size, Real_t> &matrix) {

    hid_t file = H5Fopen(_fileName.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    hid_t dataset = H5Dopen2(file, _dataSetName.c_str(), H5P_DEFAULT);

    // Initialize matrix
    const auto H5_TYPE = (sizeof(Real_t) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    H5Dread(dataset, H5_TYPE, H5S_ALL, H5S_ALL, H5P_DEFAULT, matrix.getMatrix());

    H5Dclose(dataset);
    H5Fclose(file);

  }

  /**
   * @brief Constructor from hdf5 file to array (Real_t)
   * @param[out] matrix container for the dataset, matrix must be already
   * initialized (allocated)
   */
  template <typename Real_t>
  void load(Real_t *matrix)
  {
    const auto H5_TYPE = (sizeof(Real_t) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    H5Dread(_dataSet, H5_TYPE, H5S_ALL, H5S_ALL, H5P_DEFAULT, matrix);
  }

  /**
   * @brief Constructor from hdf5 file to array (Real_t), selecting a subset
   * @param[in] offsets specifies the offset of the starting element in the
   * matrix
   * @param[in] counts specifies the number of element to read in each dimension
   * @param[out] matrix container for the dataset, matrix must be already
   * initialized (allocated)
   */
  template <typename Real_t>
  void load(const hsize_t *offsets, const hsize_t *counts, Real_t *matrix) {

    hsize_t offset[2], count[2], stride[2], block[2];
    offset[0] = offsets[0];
    offset[1] = offsets[1];
    count[0] = counts[0];
    count[1] = counts[1];
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;

    // create memspace
    hid_t memspace = H5Screate_simple (2, count, NULL);
    // get dataspace of the dataset
    hid_t dspace = H5Dget_space(_dataSet);
    // select subset
    H5Sselect_hyperslab(dspace, H5S_SELECT_SET, offset, stride, count, block);

    // Read and initialize matrix
    const auto H5_TYPE = (sizeof(Real_t) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    H5Dread (_dataSet, H5_TYPE, memspace, dspace, H5P_DEFAULT, matrix);

    H5Sclose(dspace);
    H5Sclose(memspace);
  }

  /**
   * @brief Constructor ??
   * @todo ask Olivier explanation about this function
   */
  Dataset(const std::string &filename, int &nrows, int &ncols, bool headers = true, bool rownames = true)
  : _fileName(filename), _dataSetName("")
  {
    // open file
    std::ifstream stream(filename.c_str(), std::ios::in);

    ncols = 0;

    // Read the first line to have the number of cols
    std::string heads;
    std::getline(stream, heads);
    // cout << "File headers : " << heads << endl;
    std::regex rgx("\\s+"); // white space
    std::sregex_token_iterator iter(heads.begin(), heads.end(), rgx, -1);
    std::sregex_token_iterator end;
    while (iter != end) {
      ++ncols;
      ++iter;
    }

    // If headers
    if (headers)
      nrows = 0;
    else
      nrows = 1;

    // Reading a first time the whole file to determine the size of the matrix
    std::string line;
    while (std::getline(stream, line))
      nrows++;

    if (rownames)
      ncols = ncols - 1;

    return;
  }

  /**
   * @brief ??
   * @todo ask Olivier explanation about this function
   */
  template <typename Size, typename Real_t>
  void load(const std::string &filename, fmr::BlasDenseWrapper<Size, Real_t> &matrix,
            int nrows, int ncols, bool headers = true, bool rownames = true) {
    this->load(filename, matrix.getMatrix(), nrows, ncols, headers, rownames);
  }

  /**
   * @brief ??
   * @todo ask Olivier explanation about this function
   */
  template <typename Real_t>
  void load(const std::string &filename, Real_t *matrix, int nrows, int ncols,
            bool headers = true, bool rownames = true) {

    // open file
    std::ifstream stream(filename.c_str(), std::ios::in);

    // Reading and converting datas from std::string to double
    std::string line;
    if (headers)
      std::getline(stream, line); // The first line are headers

    // Read line by line and insert the data
    std::regex rgx("\\s+"); // white space
    std::size_t ir = 0;
    while (std::getline(stream, line)) {
      std::sregex_token_iterator it(line.begin(), line.end(), rgx, -1);
      std::sregex_token_iterator end;
      std::vector<std::string> elems;
      while (it != end) {
        elems.push_back(*it);
        ++it;
      }
      int ib = 0;
      if (rownames) {
        ib = 1;
      }
      std::size_t ic = 0;
      std::size_t ij;
      for (std::size_t i = ib; i < elems.size(); i++) {
        ij = ic + ir * ncols;
        matrix[ij] = atof(elems[i].c_str());
        ++ic;
      }
      ++ir;
    }
  }
};

} // namespace io
} // namespace diodon

#endif
