/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file loadfile.hpp
 *
 * Read data from files.
 *
 */

#ifndef LOADFILE_HPP
#define LOADFILE_HPP

#include <iostream>
#include <string>

#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/MatrixIO.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#ifdef DIODON_USE_CHAMELEON_MPI
#include "mpi.h"
#endif
#include "diodon/io_utils/BlockHdf5.hpp"
#include "hdf5.h"

/**
 * @brief Diodon classes and routines
 **/
namespace diodon {
/**
 * @brief Diodon API functions to read and write data from/to files (.csv, .h5,
 * ...).
 * @defgroup InputOutput Input and Output / Read and Write files
 **/
namespace io {

/* @brief default colnames when colnames is unused */
static std::vector<std::string> dummy_colnames;
/* @brief default rownames when rownames is unused */
static std::vector<std::string> dummy_rownames;

/**
 *
 * @ingroup InputOutput
 * @brief Read an array of string from a Hdf5 file
 *
 * @param[out] names stores the array values in output
 * @param[in] filename Name of the file where the matrix is stored
 * @param[in] datasetname The name of the hdf5 dataset to read
 * @param[in] n size of array 'names' and number of string values to read
 *
 */
void loadNames(std::vector<std::string> &names,
               const std::string &filename,
               const std::string &datasetname,
               const hsize_t n)
{
    /* read colnames and rownames is needed */
    // open the HDF5 file
    hid_t file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    // open the specific dataset
    hid_t dataset = H5Dopen2(file, datasetname.c_str(), H5P_DEFAULT);
    // get the datatype and its size
    hid_t filetype = H5Dget_type (dataset);
    size_t sdim = H5Tget_size (filetype);
    sdim++; // Make room for null terminator
    /*
     * Get dataspace and allocate memory for read buffer.  This is a
     * two dimensional dataset so the dynamic allocation must be done
     * in steps.
     */
    hid_t space = H5Dget_space (dataset);
    hsize_t dims[1] = {n};
    /*
     * Allocate array of pointers to rows.
     */
    char **rdata = (char **) malloc (dims[0] * sizeof (char *));
    /*
     * Allocate space for integer data.
     */
    rdata[0] = (char *) malloc (dims[0] * sdim * sizeof (char));
    /*
     * Set the rest of the pointers to rows to the correct addresses.
     */
    for (hsize_t i=1; i<dims[0]; i++)
        rdata[i] = rdata[0] + i * sdim;
    /*
     * Create the memory datatype.
     */
    hid_t memtype = H5Tcopy (H5T_C_S1);
    H5Tset_size (memtype, sdim);
    /*
     * Read the data.
     */
    H5Dread (dataset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rdata[0]);
    /*
     * Output the data to the screen.
     */
    for (hsize_t i=0; i<dims[0]; i++){
        names.push_back(rdata[i]);
    }
    free (rdata[0]);
    free (rdata);
    H5Dclose(dataset);
    H5Sclose(space);
    H5Tclose(filetype);
    H5Tclose(memtype);
    H5Fclose(file);
}

/**
 * @ingroup InputOutput
 * @brief Create a matrix from a file
 *
 * @param[out] matrix A reference to the Wrapper in which to load the matrix
 * @param[in] filename Name of the file where the matrix is stored
 * @param[in] fmt Format of the file i.e. extension, can be h5, metadata txt for
 * several hdf5 files is also handled by this fmt, csv, txt, gz (zlib), bz2
 * (bzip2)
 * @param[in] delimiter Delimiters between values in csv or txt files (default:
 * ' ', i.e. space)
 * @param[in] datasetname The name of the hdf5 dataset to load if the fmt is h5
 * (default: 'values')
 * @param[in] bcns true to read colnames in the first line of the file (default:
 * false)
 * @param[inout] colnames reads columns tags if bcns is true.
 * @param[in] cndatasetname The name of the hdf5 dataset to read for colnames
 * (default: 'colnames')
 * @param[in] brns true to read rownames in the first column of the file
 * (default: false)
 * @param[inout] rownames reads rows tags if brns is true.
 * @param[in] rndatasetname The name of the hdf5 dataset to read for rownames
 * (default: 'rownames')
 *
 */
template<typename FSize, typename FReal>
void loadFile(fmr::BlasDenseWrapper<FSize, FReal> &matrix,
              std::string filename,
              const std::string &fmt,
              const std::string& delimiter=" ",
              const std::string& datasetname="values",
              bool bcns=false,
              std::vector<std::string> &colnames=dummy_colnames,
              const std::string& cndatasetname="colnames",
              bool brns=false,
              std::vector<std::string> &rownames=dummy_rownames,
              const std::string& rndatasetname="rownames"){
  std::cout << "[diodon] Read matrix from file: ";
  std::cout << filename << "\n";

  if ( fmt == "h5" ) {
      if (filename.find(".h5") != std::string::npos) {
          /* init matrix / Read matrix in a hdf5 fmt */
          fmr::io::readHDF5(matrix, filename, datasetname);
          if ( bcns ) {
              loadNames(colnames, filename, cndatasetname, (hsize_t)matrix.getNbCols());
          }
          if ( brns ) {
              loadNames(rownames, filename, rndatasetname, (hsize_t)matrix.getNbRows());
          }
      } else {
          /* init matrix / Read matrix in a hdf5 fmt from a .txt file metadata */
          const std::size_t found = filename.find_last_of("/");
          const std::string datasetpathname(filename.substr(0,found+1));
          std::cout << "[diodon] Path of files " << datasetpathname << std::endl;
          BlockHdf5File matrixDescriptor(filename, datasetpathname);
          diodon::io::fillMatrix(matrixDescriptor, matrix);
      }
  } else {
      if ( fmt == "csv" || fmt == "txt") {
          fmr::io::readASCII<FSize, FReal, std::ifstream, fmr::BlasDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      } else if ( fmt == "gz" ) {
          fmr::io::readASCII<FSize, FReal, igzstream, fmr::BlasDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      } else if ( fmt == "bz2" ) {
          fmr::io::readASCII<FSize, FReal, ibzstream, fmr::BlasDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      }
  }
}

#ifdef DIODON_USE_CHAMELEON
/**
 * @ingroup InputOutput
 * @brief Create a matrix from a file
 *
 * @param[out] matrix A reference to the Wrapper in which to load the matrix
 * @param[in] filename Name of the file where the matrix is stored
 * @param[in] fmt Format of the file i.e. extension, can be h5, metadata txt for
 * several hdf5 files is also handled by this fmt, csv, txt, gz (zlib), bz2
 * (bzip2)
 * @param[in] delimiter Delimiters between values in csv or txt files (default:
 * ' ', i.e. space)
 * @param[in] datasetname The name of the hdf5 dataset to load if the fmt is h5
 * (default: 'values')
 * @param[in] bcns true to read colnames in the first line of the file (default:
 * false)
 * @param[inout] colnames reads columns tags if bcns is true.
 * @param[in] cndatasetname The name of the hdf5 dataset to read for colnames
 * (default: 'colnames')
 * @param[in] brns true to read rownames in the first column of the file
 * (default: false)
 * @param[inout] rownames reads rows tags if brns is true.
 * @param[in] rndatasetname The name of the hdf5 dataset to read for rownames
 * (default: 'rownames')
 *
 */
template<typename FSize, typename FReal>
void loadFile(fmr::ChameleonDenseWrapper<FSize, FReal> &matrix,
              std::string filename,
              const std::string &fmt,
              const std::string& delimiter=" ",
              const std::string& datasetname="values",
              bool bcns=false,
              std::vector<std::string> &colnames=dummy_colnames,
              const std::string& cndatasetname="colnames",
              bool brns=false,
              std::vector<std::string> &rownames=dummy_rownames,
              const std::string& rndatasetname="rownames") {
  int mpiRank = Chameleon::mpi_rank();
  bool masterIO = mpiRank == 0;

  if (masterIO){
    std::cout << "[diodon] Read matrix from file: ";
    std::cout << filename << "\n";
  }

  if( fmt == "h5" ) {
      if (filename.find(".h5") != std::string::npos) {
        /* init matrix / Read matrix in a hdf5 fmt */
        fmr::io::readHDF5(matrix, filename, datasetname);
        if ( bcns ) {
            Chameleon::barrier();
            loadNames(colnames, filename, cndatasetname, (hsize_t)matrix.getNbCols());
        }
        if ( brns ) {
            Chameleon::barrier();
            loadNames(rownames, filename, rndatasetname, (hsize_t)matrix.getNbRows());
        }
      } else {
        /* init matrix / Read matrix in a hdf5 fmt from a .txt file metadata */
        const std::size_t found = filename.find_last_of("/");
        const std::string datasetpathname(filename.substr(0,found));
        if (masterIO) {
          std::cout << "[diodon] Path of files " << datasetpathname << std::endl;
        }
        fmr::io::readHDF5(matrix, filename, datasetpathname);
      }
  } else {
      if( fmt == "csv" || fmt == "txt") {
          fmr::io::readASCII<FSize, FReal, std::ifstream, fmr::ChameleonDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      } else if ( fmt == "gz" ) {
          fmr::io::readASCII<FSize, FReal,igzstream, fmr::ChameleonDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      } else if ( fmt == "bz2" ) {
          fmr::io::readASCII<FSize, FReal, ibzstream, fmr::ChameleonDenseWrapper<FSize, FReal>>(matrix,
            filename, 0, *delimiter.c_str(), bcns, colnames, brns, rownames);
      }
  }
}
#endif

} // namespace io
} // namespace diodon

#endif //LOADFILE_HPP
