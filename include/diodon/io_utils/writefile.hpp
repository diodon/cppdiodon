/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file writefile.hpp
 *
 * Write data to files.
 *
 */

#ifndef WRITEFILE_HPP
#define WRITEFILE_HPP

#include <iostream>
#include <fstream>
#include <string>

#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/MatrixIO.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#ifdef DIODON_USE_CHAMELEON_MPI
#include "mpi.h"
#endif
#include "hdf5.h"

namespace diodon {
namespace io {

/**
 * @ingroup InputOutput
 * @brief Save a string array in a H5 file
 *
 * @param[in] sa the array of strings to save
 * @param[in] filename Name of the file where the sa values are saved
 * @param[in] datasetname The name of the hdf5 dataset to save (default:
 * 'stringarray')
 * @param[in] nvaltosave Number of values to save. If set to 0 all values
 * are saved (default).
 */
void writeH5SA(std::vector<std::string> &sa,
               const std::string& filename,
               const std::string& datasetname="stringarray",
               const size_t& nvaltosave=0)
{
    size_t nval = (nvaltosave==0) ? sa.size() : nvaltosave;

    hid_t file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

    /* if dataset already exist delete it */
    htri_t datasetexist = -1;
    H5E_BEGIN_TRY
    datasetexist = H5Lexists(file_id, datasetname.c_str(), H5P_DEFAULT);
    H5E_END_TRY
    if (datasetexist > 0) {
        H5Ldelete(file_id, datasetname.c_str(), H5P_DEFAULT);
    }

    /* Create file and memory datatypes */
    hid_t filetype_id = H5Tcopy (H5T_C_S1);
    H5Tset_size (filetype_id, H5T_VARIABLE);
    hid_t memtype_id = H5Tcopy (H5T_C_S1);
    H5Tset_size (memtype_id, H5T_VARIABLE);

    /* Create dataspace.  Setting maximum size to NULL sets the maximum
    size to be the current size */
    hsize_t dims[1] = {(hsize_t)nval};
    hid_t space_id = H5Screate_simple (1, dims, NULL);

    /* Create the dataset and write the variable-length string data to
    it */
    hid_t dset_id = H5Dcreate (file_id, datasetname.c_str(), filetype_id, space_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    /* need a char* array for hdf5 write interface */
    char *wdata[nval];
    for ( size_t i = 0; i < nval; i++ ){
        wdata[i] = new char[sa[i].size()+1];
        std::strcpy(wdata[i], sa[i].c_str());
    }

    /* save the char* array */
    H5Dwrite (dset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata);

    /* free memory char* array */
    for ( size_t i = 0 ; i < nval ; i++ ){
        delete [] wdata[i];
    }

    /* Close and release resources */
    H5Dclose (dset_id);
    H5Sclose (space_id);
    H5Tclose (filetype_id);
    H5Tclose (memtype_id);
    H5Fclose (file_id);
}

/**
 * @ingroup InputOutput
 * @brief Save a matrix to file
 *
 * @param[in] data two dimensional matrix
 * @param[in] filename Name of the file where the data values are saved
 * @param[in] datasetname The name of the hdf5 dataset to save (default:
 * 'values')
 * @param[in] ncolstosave number of columns to save of data matrix, may be lower
 * than the number of columns but not larger. If set to 0 ncolstosave is the
 * number of columns in data (default: 0).
 * @param[in] bcns to save colnames as a dataset named cndatasetname (default:
 * false)
 * @param[in] colnames the cols names (tags)
 * @param[in] cndatasetname The name of the hdf5 dataset to save for colnames
 * (default: 'colnames')
 * @param[in] brns true to save rownames as a dataset named rndatasetname
 * (default: false)
 * @param[in] rownames the rows names (tags)
 * @param[in] rndatasetname The name of the hdf5 dataset to save for rownames
 * (default: 'rownames')
 */
template< class MATRIX_T>
void writeH5(MATRIX_T &data,
             const std::string& filename,
             const std::string& datasetname="values",
             const size_t& ncolstosave=0,
             const bool& bcns=false,
             std::vector<std::string> &colnames=dummy_colnames,
             const std::string& cndatasetname="colnames",
             const bool& brns=false,
             std::vector<std::string> &rownames=dummy_rownames,
             const std::string& rndatasetname="rownames")
{
    /* write the matrix to file in parallel */
    fmr::io::writeHDF5(data, filename, datasetname, ncolstosave);

    int mpiRank = 0;
#if defined(CHAMELEON_USE_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;
    if ( masterIO ){
        /* save colnames as a dataset */
        if ( bcns ) {
            writeH5SA(colnames, filename, cndatasetname, ncolstosave);
        }
        /* save rownames as a dataset */
        if ( brns ) {
            writeH5SA(rownames, filename, rndatasetname);
        }
    }
}

/**
 * @ingroup InputOutput
 * @brief Save a matrix to file
 *
 * @param[in] data two dimensional matrix
 * @param[in] filename Name of the file where the data values are saved
 * @param[in] fmt Format of the file i.e. extension, can be h5, metadata txt for
 * several hdf5 files is also handled by this fmt, csv, txt, gz (zlib), bz2
 * (bzip2)
 * @param[in] delimiter Delimiters between values in csv or txt files (default:
 * ' ', i.e. space)
 * @param[in] datasetname The name of the hdf5 dataset to save for data if the
 * fmt is h5 (default: 'values')
 * @param[in] ncolstosave number of columns to save of data matrix, may be lower
 * than the number of columns but not larger. If set to 0 ncolstosave is the
 * number of columns in data (default: 0).
 * @param[in] bcns to save colnames in the first row of the file (default:
 * false)
 * @param[in] colnames the cols names (tags)
 * @param[in] cndatasetname The name of the hdf5 dataset to save for colnames
 * (default: 'colnames')
 * @param[in] brns true to save rownames in the first column of the file
 * (default: false)
 * @param[in] rownames the rows names (tags)
 * @param[in] rndatasetname The name of the hdf5 dataset to save for rownames
 * (default: 'rownames')
 * @param[in] data_fmt number of digits to save when writing ascii files
 * (default: 0 meaning save all meaningfull values)
 */
template< class MATRIX_T>
void writeFile(MATRIX_T &data,
               const std::string& filename,
               const std::string& fmt,
               const std::string& delimiter=" ",
               const std::string& datasetname="values",
               const size_t& ncolstosave=0,
               const bool& bcns=false,
               std::vector<std::string> &colnames=dummy_colnames,
               const std::string& cndatasetname="colnames",
               const bool& brns=false,
               std::vector<std::string> &rownames=dummy_rownames,
               const std::string& rndatasetname="rownames",
               const int& data_fmt=0)
{
    using Real_t = typename MATRIX_T::value_type;
    using Size_t = typename MATRIX_T::int_type;
    Size_t nskip = 0;

    if ( fmt == "h5" ) {
        writeH5(data, filename, datasetname, ncolstosave,
                bcns, colnames, cndatasetname,
                brns, rownames, rndatasetname);
    } else {
    if ( fmt == "csv" || fmt == "txt") {
        fmr::io::writeASCII<Size_t, Real_t, std::ofstream, MATRIX_T>(data,
          filename, nskip, *delimiter.c_str(), ncolstosave, true, bcns, colnames, brns, rownames, data_fmt);
    } else if ( fmt == "gz" ) {
        fmr::io::writeASCII<Size_t, Real_t, ogzstream, MATRIX_T>(data,
          filename, nskip, *delimiter.c_str(), ncolstosave, true, bcns, colnames, brns, rownames, data_fmt);
    } else if ( fmt == "bz2" ) {
        fmr::io::writeASCII<Size_t, Real_t, obzstream, MATRIX_T>(data,
          filename, nskip, *delimiter.c_str(), ncolstosave, true, bcns, colnames, brns, rownames, data_fmt);
    }
    }
}

/**
 * @ingroup InputOutput
 * @brief Save a vector to file
 *
 * @param[in] vectordata a vector to save
 * @param[in] filename Name of the file where the data values are saved
 * @param[in] fmt Format of the file i.e. extension, can be h5, metadata txt for
 * several hdf5 files is also handled by this fmt, csv, txt, gz (zlib), bz2
 * (bzip2)
 * @param[in] delimiter Delimiters between values in csv or txt files (default:
 * ' ', i.e. space)
 * @param[in] datasetname The name of the hdf5 dataset to save for data if the
 * fmt is h5 (default: 'values')
 * @param[in] ncolstosave number of columns to save of data matrix, may be lower
 * than the number of columns but not larger. If set to 0 ncolstosave is the
 * number of columns in data (default: 0).
 * @param[in] bcns to save colnames in the first row of the file (default:
 * false)
 * @param[in] colnames the cols names (tags)
 * @param[in] cndatasetname The name of the hdf5 dataset to save for colnames
 * (default: 'colnames')
 * @param[in] brns true to save rownames in the first column of the file
 * (default: false)
 * @param[in] rownames the rows names (tags)
 * @param[in] rndatasetname The name of the hdf5 dataset to save for rownames
 * (default: 'rownames')
 * @param[in] data_fmt number of digits to save when writing ascii files
 * (default: 0 meaning save all meaningfull values)
 */
template< class Real_t>
void writeFile(std::vector<Real_t> &vectordata,
               const std::string& filename,
               const std::string& fmt,
               const std::string& delimiter=" ",
               const std::string& datasetname="values",
               const size_t ncolstosave=0,
               const bool& bcns=false,
               std::vector<std::string> &colnames=dummy_colnames,
               const std::string& cndatasetname="colnames",
               const bool& brns=false,
               std::vector<std::string> &rownames=dummy_rownames,
               const std::string& rndatasetname="rownames",
               const int& data_fmt=0)
{
    fmr::BlasDenseWrapper<size_t, Real_t> data(vectordata.size(), 1, vectordata.data());

    int mpiRank = 0;
#if defined(CHAMELEON_USE_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;
    if ( masterIO ){
        writeFile(data, filename, fmt,
                  delimiter, datasetname, ncolstosave,
                  bcns, colnames, cndatasetname,
                  brns, rownames, rndatasetname,
                  data_fmt);
    }
}

/**
 * @ingroup InputOutput
 * @brief Save additional attributes associated with a HDF5 dataset
 *
 * @param[in] att additional attributes (strings) to save into the file (hdf5
 * format only)
 * @param[in] filename Name of the H5 file
 * @param[in] datasetname The name of the hdf5 dataset to which attributes will
 * be associated (default: 'values')
 */
template<class ATTRIBUTE_T>
void writeFileH5Att(ATTRIBUTE_T &att,
                    const std::string& filename,
                    const std::string& datasetname="values")
{
    int mpiRank = 0;
#if defined(CHAMELEON_USE_MPI)
    mpiRank = Chameleon::mpi_rank();
#endif
    bool masterIO = mpiRank == 0;
    if ( masterIO ) {
        hid_t file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        hid_t dataset_id = H5Dopen2(file_id, datasetname.c_str(), H5P_DEFAULT);
        hsize_t dims = 1;

        /* if attribute dataset already exist delete it */
        htri_t attributeexist = -1;
        std::string attributename = datasetname + "/Data";
        H5E_BEGIN_TRY
        attributeexist = H5Lexists(file_id, attributename.c_str(), H5P_DEFAULT);
        H5E_END_TRY
        if (attributeexist > 0) {
            H5Ldelete(file_id, attributename.c_str(), H5P_DEFAULT);
        }
        hid_t dataspace_id = H5Screate_simple(1, &dims, NULL);
        hid_t  atype = H5Tcopy(H5T_C_S1);
        H5Tset_size(atype, att.getDataInputName().size());
        H5Tset_strpad(atype, H5T_STR_NULLTERM);
        hid_t attribute_id = H5Acreate2 (dataset_id, "Data", atype, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
        H5Awrite(attribute_id, atype, att.getDataInputName().c_str());
        H5Aclose(attribute_id);
        H5Sclose(dataspace_id);

        /* if attribute dataset already exist delete it */
        attributeexist = -1;
        attributename = datasetname + "/Method";
        H5E_BEGIN_TRY
        attributeexist = H5Lexists(file_id, attributename.c_str(), H5P_DEFAULT);
        H5E_END_TRY
        if (attributeexist > 0) {
            H5Ldelete(file_id, attributename.c_str(), H5P_DEFAULT);
        }
        dataspace_id = H5Screate_simple(1, &dims, NULL);
        atype = H5Tcopy(H5T_C_S1);
        H5Tset_size(atype, att.getMethodName().size());
        H5Tset_strpad(atype, H5T_STR_NULLTERM);
        attribute_id = H5Acreate2 (dataset_id, "Method", atype, dataspace_id,
                                    H5P_DEFAULT, H5P_DEFAULT);
        H5Awrite(attribute_id, atype, att.getMethodName().c_str());
        H5Aclose(attribute_id);
        H5Sclose(dataspace_id);

        /* if attribute dataset already exist delete it */
        attributeexist = -1;
        attributename = datasetname + "/Method Parameter";
        H5E_BEGIN_TRY
        attributeexist = H5Lexists(file_id, attributename.c_str(), H5P_DEFAULT);
        H5E_END_TRY
        if (attributeexist > 0) {
            H5Ldelete(file_id, attributename.c_str(), H5P_DEFAULT);
        }
        dataspace_id = H5Screate_simple(1, &dims, NULL);
        atype = H5Tcopy(H5T_C_S1);
        H5Tset_size(atype, att.getMethodParameter().size());
        H5Tset_strpad(atype, H5T_STR_NULLTERM);
        attribute_id = H5Acreate2 (dataset_id, "Method Parameter", atype, dataspace_id,
                                    H5P_DEFAULT, H5P_DEFAULT);
        H5Awrite(attribute_id, atype, att.getMethodParameter().c_str());
        H5Aclose(attribute_id);
        H5Sclose(dataspace_id);
        H5Dclose(dataset_id);
        H5Fclose(file_id);
    }
}

} // namespace io
} // namespace diodon

#endif //WRITEFILE_HPP
