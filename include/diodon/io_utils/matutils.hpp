/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file matutils.hpp
 *
 * Utilities for matrices
 *
 */
#ifndef MATUTILS_HPP
#define MATUTILS_HPP

#include <iostream>
#include <cmath>
#include <limits>
#include <random>
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
using namespace std;

namespace diodon {
namespace io {

/**
 * @ingroup InputOutput
 * @brief Generates a random vector to use for tests
 *
 * @param[out] data A pointer to the Wrapper in which to generate the vector
 * @param[in] size The number of rows
 */
template<class VectorClass>
void genRandomVector(VectorClass &data,
                     std::size_t size){
  using Real_t = typename VectorClass::value_type;

  std::random_device rd; // obtain a random number from hardware
  std::mt19937 gen(rd()); // seed the generator
  std::uniform_real_distribution<Real_t> distr(1, 100); // define the range
  data.resize(size);
  for (std::size_t i = 0 ; i < (std::size_t)size ; ++i) {
    data[i] = distr(gen);
  }
}

/**
 * @ingroup InputOutput
 * @brief Generates a random matrix to use for tests
 *
 * @param[out] data A pointer to the Wrapper in which to generate the matrix
 * @param[in] nbRows The number of rows the generated matrix should have
 * @param[in] nbCols The number of columns the generated matrix should have
 */
template<class WrapperType>
void genRandomMatrix(WrapperType &data,
                     typename WrapperType::Int_type nbRows,
                     typename WrapperType::Int_type nbCols){
  using Real_t = typename WrapperType::value_type;
  fmr::tools::RandomClass<Real_t> randGen;
  data.reset(nbRows, nbCols, 0.);
  randGen.generateMatrix(data);
}

/**
 * @ingroup InputOutput
 * @brief Generates a random matrix of given rank to use for tests
 *
 * @param[out] data A pointer to the Wrapper in which to generate the matrix
 * @param[in] nbRows The number of rows the generated matrix should have
 * @param[in] nbCols The number of columns the generated matrix should have
 * @param[in] rank The rank the generated matrix should have
 * @param[in] isSym whether or not the matrix is considered as symmetric (false
 * by default)
 */
template<class WrapperType>
void genRandomMatrixRank(WrapperType &data,
                         typename WrapperType::Int_type nbRows,
                         typename WrapperType::Int_type nbCols,
                         typename WrapperType::Int_type rank,
                         const bool isSym=false){
  // generate a general matrix nbRows x nbCols of given rank
  using Real_t = typename WrapperType::value_type;
  /* init random matrices (set with 0.) */
  WrapperType randomA(nbRows, rank);
  WrapperType randomB(rank, nbCols);
  /* init resulting matrix */
  data.reset(nbRows, nbCols, isSym);
  data.allocate();
  /* generate 2 random matrices of size (M, K) and (K, N) */
  fmr::tools::RandomClass<Real_t> randGen;
  randGen.generateMatrix(randomA);
  randGen.generateMatrix(randomB);
  /* multiply the random matrices, result is of size (M, N) and rank K */
  fmr::fmrGemm(randomA, randomB, data);
}

/**
 *
 * @brief Fill a zero matrix with one single 1 on each line
 *
 * @param[inout] A matrix (full 0. matrix in input)
 * @param[in] nrows number of rows of the matrix
 * @param[in] ncols number of columns of the matrix
 *
 */
template<typename Idx_t, typename Real_t>
int fillTableOne(Real_t* A, const Idx_t& nrows, const Idx_t& ncols, Idx_t* indexes)
{
    /* fill matrix with one 1 on each line */
#pragma omp parallel for shared(nrows, A, indexes)
    for (std::size_t i = 0 ; i < (std::size_t)nrows ; ++i) {
            A[ i + indexes[i]*nrows] = 1.;
    }
    return EXIT_SUCCESS;
}
#ifdef DIODON_USE_CHAMELEON
    /**
     * @brief function to fill one single 1 in each line in chameleon tiles
     */
    template<class FSize, class FReal>
    static int chamMapTableOne(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                               const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
        FSize nrows, ncols, lda, row_min, col_min, col_max;

        // address of the tile to fill
        FReal *T = (FReal *)cham_tile->mat;
        // indexes list where to set 1.
        FSize *indexes = (FSize*)user_data;

        // sizes of the tile
        nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
        ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
        lda  = descA->get_blkldd( descA, m );

        // starting row index of the block
        row_min = m * descA->mb;
        // starting column index of the block
        col_min = n * descA->nb;
        col_max = col_min + ncols;

        for (FSize i = 0; i < nrows; i++)
        {
            FSize globalrowidx = i + row_min;
            FSize globalcoltofill = indexes[globalrowidx];
            if ( globalcoltofill >= col_min && globalcoltofill < col_max ) {
                T[i + (globalcoltofill-col_min) * lda] = 1.;
            }
        }

        return 0;
    }
/**
 *
 * @brief Fill a zero matrix with one single 1 on each line
 *
 * @param[inout] A matrix (full 0. matrix in input)
 * @param[in] nrows number of rows of the matrix
 * @param[in] ncols number of columns of the matrix
 *
 */
template<class FSize, class FReal>
int fillTableOne(CHAM_desc_t* A, const FSize& nrows, const FSize& ncols, FSize* indexes) {
    struct cham_map_data_s map_data = {
        .access = ChamRW,
        .desc   = A,
    };
    struct cham_map_operator_s map_op = {
        .name = "TableOne",
        .cpufunc = chamMapTableOne<FSize,FReal>,
        .cudafunc = NULL,
        .hipfunc = NULL,
    };
    return Chameleon::map( ChamUpperLower, 1, &map_data, &map_op, indexes );
}
#endif

/**
 * @ingroup InputOutput
 * @brief Generates a contingency table of size (M,N) to use for tests. Warning:
 * we use a size K >> M and K >> N to generate the table so that this function
 * can be very costly in memory. To be used with M and N < 1000 for example.
 *
 * @param[out] data A pointer to the Wrapper in which to generate the matrix
 * @param[in] nbRows The number of rows the generated matrix should have
 * @param[in] nbCols The number of columns the generated matrix should have by
 * default)
 */
template<class WrapperType>
void genContingency(WrapperType &data, typename WrapperType::Int_type nbRows, typename WrapperType::Int_type nbCols){
  /* generate a contingency table nbRows x nbCols */
  using Int_t  = typename WrapperType::Int_type;
  using Real_t = typename WrapperType::value_type;

  /* X and Y nb. rows should be much larger than nbRows and nbCols */
  Int_t maxi = max( nbRows, nbCols );
  maxi = 1000*maxi;

  /* init random matrices (set with 0.) */
  WrapperType randomA(maxi, nbRows, (Real_t)0.);
  WrapperType randomB(maxi, nbCols, (Real_t)0.);
  /* init resulting matrix */
  data.reset(nbRows, nbCols);
  data.allocate();
  /* generate 2 random matrices (full of 0 with only one 1 in each line) of size
  (K, M) and (K, N), K >> M and N */

  std::random_device rd; // obtain a random number from hardware
  std::mt19937 gen(rd()); // seed the generator
  std::uniform_int_distribution<> distrA(0, nbRows-1); // define the range
  std::uniform_int_distribution<> distrB(0, nbCols-1); // define the range

#if defined(CHAMELEON_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (!mpiinit) {
        MPI_Init(NULL, NULL);
    }
#endif

  /* choose which column of A to fill randomly in each line */
  Int_t* indexes = new Int_t[maxi];
  for (std::size_t i = 0 ; i < (std::size_t)maxi ; ++i) {
      indexes[i] = (Int_t)distrA(gen);
  }
#if defined(CHAMELEON_USE_MPI)
  MPI_Bcast(indexes, maxi, MPI_INT, 0, MPI_COMM_WORLD);
#endif

  fillTableOne<Int_t, Real_t>(randomA.getMatrix(), maxi, nbRows, indexes);
  /* choose which column of B to fill randomly in each line */
  for (std::size_t i = 0 ; i < (std::size_t)maxi ; ++i) {
      indexes[i] = (Int_t)distrB(gen);
  }
#if defined(CHAMELEON_USE_MPI)
  MPI_Bcast(indexes, maxi, MPI_INT, 0, MPI_COMM_WORLD);
#endif

  fillTableOne<Int_t, Real_t>(randomB.getMatrix(), maxi, nbCols, indexes);
  delete[] indexes;

  /* multiply the random matrices T = A^t B, result is of size (M, N) */
  fmr::fmrGemm<Int_t, Real_t>("T", "N", 1., randomA, randomB, 0., data);
}

/**
 * @ingroup InputOutput
 * @brief Are matrices equal or not
 * @param[in] n total number of values in the matrix
 * @param[in] mat1 matrix to compare
 * @param[in] mat2 matrix to compare
 * @return true matrices are similar at float precision in absolute value, and
 * false if matrices are differents
 */
template<typename T>
bool matrices_comp(const size_t n, const T* mat1, const T* mat2 ) {

  T s;
  T smin = 0.;
  for (size_t i = 0 ; i < n ; i++)
    {
      s = std::abs(mat1[i]) - abs(mat2[i]);
      if (s < smin) smin = s;
    }


  if (smin > std::numeric_limits<float>::epsilon()) return false;
  else return true;

}

/**
 * @ingroup InputOutput
 * @brief Maximum difference between matrices (infinite norm of the difference)
 * @param[in] n total number of values in the matrix
 * @param[in] mat1 matrix to compare
 * @param[in] mat2 matrix to compare
 * @return maximum difference
 */
template<typename T>
T arrays_comp(const size_t n, const T* mat1, const T* mat2 ) {

  T s;
  T smax = 0.;
  for (size_t i = 0 ; i < n ; i++)
    {
      s = std::abs(mat1[i]) - abs(mat2[i]);
      if ( s > smax ) smax = s;
    }
   return smax;
}

} // namespace io
} // namespace diodon

#endif
