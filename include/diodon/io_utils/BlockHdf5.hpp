/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file BlockHdf5.hpp
 *
 * Read matrix data from several Hdf5 files
 *
 */
#ifndef BLOCKHDF5_HPP
#define BLOCKHDF5_HPP
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <chrono>
#include "hdf5.h"
#include "fmr/Utils/MatrixIO.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"

namespace diodon {
namespace io {

/**
 * @ingroup InputOutput
 * @brief This class describes a squared matrix stored by block and each block
 * stored in a hdf5 file.
 *
 * The descriptor file has the following format:
 *
 * number of blocks in each dimension\n
 * uplo\n
 * i j hdf5_filename dataset_to_read\n
 *
 * uplo in the list: lower, upper, full.
 * i j the index position of the block
 * default value for dataset_to_read: 'distance'.
 *
 * Example:
 * 3
 * upper
 * 0	0	sym17_0.h5	distance
 * 0	1	sym17_1.h5	distance
 * 0	2	sym17_2.h5	distance
 * 1	1	sym17_3.h5	distance
 * 1	2	sym17_4.h5	distance
 * 2	2	sym17_5.h5	distance
 *
 **/
class BlockHdf5File {

private:
    std::string _decriptorFile ; // The name of the file containig the meta data
    std::string _format ; // The name of the file containig the meta data
    int _blockNumber ; // the number of blocks
    std::vector<std::string> _h5FileNames ; // the vector of filenames
    std::vector<std::string> _dataSetNames ; // the vector of dataset names
    std::array<hsize_t,2>      _dims_out ; // number of rows of the global matrix
    std::vector<std::pair<int,int> > _indexStart ; // The block b starts in the global matrix ( _indexStart[b].first , _indexStart[b].second)
    std::vector<std::pair<int,int> > _indexEnd   ; // The block b ends in the global matrix ( _indexStart[b].first , _indexStart[b].second)

public:

    BlockHdf5File() : _blockNumber(0)
    {}

    /**
     * @brief Initialize BlockHdf5File class from a .txt file containing the
     * metadata information about sub blocks stored in different hdf5 files.
     * @param[in] filename name of the metadata file .txt
     * @param[in] dirLocation directory root of hdf5 files (. by default)
     *
     * Example of input metadata file:
     * \code{.sh}
     * cat metadata.txt
     * 3
     * upper
     * 0	0	file_0.h5	distance
     * 0	1	file_1.h5	distance
     * 0	2	file_2.h5	distance
     * 1	1	file_3.h5	distance
     * 1	2	file_4.h5	distance
     * 2	2	file_5.h5	distance
     * \endcode
     *
     */
    BlockHdf5File(const std::string &filename, const std::string &dirLocation ="")
    {
        this->open( filename, dirLocation );
    }

    /**
     * @brief open read the metadata of the block hdf5 matrix
     * @param[in] filename the name of the file text format)
     * @param[in] dirLocation directory root of hdf5 files (./ by default)
     */
    void open(const std::string &filename, const std::string &dirLocation ="./"){
        std::fstream dataFile(filename, std::ios::in) ;
        dataFile >> _blockNumber ;
        dataFile >> _format ;
        std::transform(_format.begin(), _format.end(), _format.begin(),
                        [](unsigned char c){ return std::toupper(c); }
            );
        std::cout << "Nb block: "<<_blockNumber <<std::endl
                    << "Format:   "<<_format <<std::endl;
        int nbLines= _blockNumber*(_blockNumber+1)/2 ;
        if(_format == "FULL"){
            nbLines=  _blockNumber*_blockNumber;
        }
        _h5FileNames.resize(nbLines);_dataSetNames.resize(nbLines);
        int i, j , pos ;
        std::string name, dataset ;
        std::vector<std::pair<int,int> > index(nbLines);
        for (int l =0 ; l < nbLines ; ++l) {
            dataFile >> i >> j >> name >> dataset ;
            if (_format == "UPPER"){
                pos = i*(_blockNumber+1)-i*(i+1)/2+j-i;
            }
            else {
                pos = j*(_blockNumber+1) - j*(j+1)/2 + i-j ;  // To check
            }
            index[l].first= i ; index[l].second= j ;
            //                             std::cout << i << " " << j << " " << pos << "  " << name << "\n";
            _h5FileNames[pos]  = dirLocation + name ;
            _dataSetNames[pos] = dataset ; // "distance" ;
        }
        //
        std::vector<hid_t> _files;
        std::vector<hid_t> _dataSets;

        _files.resize(nbLines);
        _dataSets.resize(nbLines);
        int *rowsSize = new int [_blockNumber+1] ;
        int *colsSize = new int [_blockNumber+1] ;

        rowsSize[0] = 0 ; colsSize[0] = 0 ;
        for (int l =0 ; l < nbLines ; ++l) {
            _files[l] = H5Fopen(_h5FileNames[l].c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
            _dataSets[l] = H5Dopen2(_files[l], _dataSetNames[l].c_str(), H5P_DEFAULT);
            std::array<hsize_t,2> dims_out ;
            hid_t dspace = H5Dget_space(_dataSets[l]);
            H5Sget_simple_extent_dims(dspace, dims_out.data(), NULL);
            H5Sclose(dspace);
            rowsSize[index[l].first+1] = dims_out[0];
            colsSize[index[l].second+1] = dims_out[1];                // Only for UPPER
            //                std::cout << " l " << l << " i " << index[l].first << " j " << index[l].second << "  "<<dims_out[0]<< " "<< dims_out[1] << std::endl;
        }
        //        for ( int i = 0 ; i < _blockNumber+1 ; ++i){
        //                std::cout << "Bloc " << rowsSize[i] << " cols: "<<  colsSize[i] << std::endl;
        //            }
        // Build where the block starts in the full matrix
        for ( int i = 1 ; i < _blockNumber+1 ; ++i){
            rowsSize[i]  += rowsSize[i-1];
            colsSize[i]  += colsSize[i-1];
        }
        //        for ( int i = 0 ; i < _blockNumber+1 ; ++i){
        //                std::cout << "Bloc " << rowsSize[i] << " cols: "<<  colsSize[i] << std::endl;
        //            }
        _dims_out[0]  = rowsSize[_blockNumber] ;
        _dims_out[1]  = colsSize[_blockNumber] ;
        _indexEnd.resize(nbLines);    _indexStart.resize(nbLines);
        std::cout << " Matrix size: " <<  _dims_out[0]<< " x " << _dims_out[1] <<std::endl;
        for ( int i = 0 ; i < nbLines ; ++i){
            _indexStart[i].first  =   rowsSize[index[i].first] ;
            _indexStart[i].second =   colsSize[index[i].second] ;
            _indexEnd[i].first    =   rowsSize[index[i].first+1] ;
            _indexEnd[i].second   =   colsSize[index[i].second+1] ;
            std::cout << "Bloc " << i << " i " << index[i].first << " j " << index[i].second
                        << " rows: "<<  _indexStart[i].first << " cols: "<< _indexStart[i].second
                        << " N: "<<   _indexEnd[i].first - _indexStart[i].first << " M "<<   _indexEnd[i].second - _indexStart[i].second
                        << "  " <<  _h5FileNames[i] << std::endl<<std::flush;

        }
        for (int l =0 ; l < nbLines ; ++l){
            H5Dclose(_dataSets[l]);
            H5Fclose(_files[l]);
        }
        delete [] rowsSize;
        delete [] colsSize;
    }
    /** @brief Get size (number of rows) of the global matrix */
    std::int32_t getSize() const{
        return _dims_out[0];
    }
    /** @brief Get number of blocks to read in hdf5 files (ex: 6 for a 3x3 block
     * matrix) */
    int getBocksNumber() const{
        return static_cast<int>(_h5FileNames.size()) ;
    }
    /**
     * @brief Get hdf5 filename of block number b
     * @param[in] b number of the block
     */
    const std::string getFileName(const int b){
        return _h5FileNames[b] ;
    }
    /**
     * @brief Get hdf5 dataset name of block number b
     * @param[in] b number of the block
     */
    const std::string gedDataSetName(const int b){
        return _dataSetNames[b] ;
    }
    /**
     * @brief Get starting index in the global matrix of block number b
     * @param[in] b number of the block
     */
    const std::pair<int,int>  & getStartIndexBlock(const int b){
        return _indexStart[b] ;
    }
    /**
     * @brief Get ending index in the global matrix of block number b
     * @param[in] b number of the block
     */
    const std::pair<int,int>  & getEndIndexBlock(const int b){
        return _indexEnd[b] ;
    }

};

/**
 * @brief Fills a BlasDenseWrapper matrix from a BlockHdf5File class
 * @param[in] matrixDescriptor BlockHdf5File object storing the matrix as blocks
 * coming from HDF5 files
 * @param[inout] blockMatrix BlasDenseWrapper matrix to fill using the fillBlock
 * method
 */
template< typename INT_T, typename REAL_T>
void fillMatrix(BlockHdf5File matrixDescriptor,  fmr::BlasDenseWrapper<INT_T,REAL_T> &blockMatrix) {
    using DenseWrapperClass = fmr::BlasDenseWrapper<INT_T,REAL_T> ;
    //using dura =std::chrono::duration_cast<std::chrono::seconds> ;
    //auto system_start = std::chrono::high_resolution_clock::now();
    // Matrix allocation
    int  n_max = matrixDescriptor.getSize() ;
    blockMatrix.reset(n_max, n_max);
    blockMatrix.allocate();
    //
    //nbBloc = matrixDescriptor.getBocksNumber() ;
    //
    //#pragma omp parallel num_threads(3)
    {
        //#pragma omp single nowait
        {
            for  (int b = 0 ; b < matrixDescriptor.getBocksNumber() ; ++b ){
                // read the
                //    std::cout << b << "  Read: "<< matrixDescriptor.getFileName(b) <<std::endl ;
                DenseWrapperClass currentBloc;
                fmr::io::readHDF5(currentBloc, matrixDescriptor.getFileName(b), matrixDescriptor.gedDataSetName(b));
                // Copy the bloc
                const auto start = matrixDescriptor.getStartIndexBlock(b) ;
                const auto end   = matrixDescriptor.getEndIndexBlock(b) ;

                //    std::cout << "   N "<< start.first<< " " << start.second<< "  "
                //	      << end.first	      << "  " << end.second << "  "
                //	      << end.first-start.first	      << "  " << end.second-start.second<<std::endl;
                //#pragma omp task shared(currentBloc,start,end)
                blockMatrix.setSubMatrix(start.first,start.second,
                                         end.first-start.first,end.second-start.second,
                                         currentBloc.getMatrix());
                // if not diag transpose the bloc
                if (start.first != start.second){
                    //      std::cout <<  "   T " << start.second<< " " << start.first<< "  "
                    //		<< end.second << "  " << end.first
                    //		<< "  Sr " <<end.first-start.first<< " Sc " << end.second-start.second << std::endl;
                    //#pragma omp task shared(currentBloc,start,end)
                    blockMatrix.setSubMatrix(start.second,start.first,
                                          end.first-start.first,end.second-start.second,
                                          currentBloc.getMatrix(),
                                          "T");
                }
                //#pragma omp taskwait
            }
        } //end single
    } //end parallel region
}

} // namespace io
} // namespace diodon

#endif
