# Notes for developers

## Structure of the project

We follow these [guidelines](https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs).

## FMR subproject

FMR is a required third party library providing the Randomized SVD algorithm.
This project is independent and hosted here: https://gitlab.inria.fr/piblanch/fmr.

The project has been imported with git subtree in diodon
```
git remote add fmr git@gitlab.inria.fr:compose/legacystack/fmr.git
mkdir -p external/
git subtree add --prefix external/fmr fmr master --squash
```

To update fmr to the last revision of the *master* branch
```
git subtree pull --prefix external/fmr fmr master --squash
# conflicts
# eventually delete files in fmr, see files deleted by them
git rm ...
# fix conflict accepting incoming
git checkout --theirs external/fmr/
git add external/fmr/
# commit
git commit -m "Update FMR"
```

## Continuous integration

CI jobs are defined in *.gitlab-ci.yml*

The gitlab runners come from the "diodon" project on the [Inria ci portal](https://ci.inria.fr/project/diodon/slaves).

Two slaves have been defined.

One virtual machine Ubuntu with docker installed and the other one with guix installed.

The "docker" job allows to build a docker image with current state of Diodon installed in the system with Chameleon MPI enabled.

The "sonarqube" job performs a static/dynamic analysis of the source code and sends reports to [sonarqube](https://sonarqube.inria.fr/sonarqube/dashboard?id=diodon%3Acppdiodon).
The job is executed inside an existing [docker image](https://gitlab.inria.fr/solverstack/docker) where analysis tools are already installed as well as chameleon.
This image is private so that we use the [DOCKER_AUTH_CONFIG](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-an-image-from-a-private-container-registry) to authenticate (defined as an environment variable during the CI job).

The other jobs perform ctest checking inside a guix environment for different configurations: lapack-mkl, lapack-openblas, chameleon-mkl, chameleon-openblas.

## Release instructions

A source package archive (.tar.gz) and an Ubuntu package (.deb) are
automatically created when you push a new git tag to gitlab (e.g. 0.2), see
gitlab-ci job _release_ in the `.gitlab-ci.yml` file. Before pushing a new tag
remind to update the `./ChangeLog` file (contains all changes since the first
release) and the `./tools/distrib/debian/ChangeLog` file with last changes only.
The tag should be created from the master branch (stable).

## Notes

  - interface web binding python: https://panel.holoviz.org/, https://ct.gitlabpages.inria.fr/gallery/
  - performing pandas functions in parallel: https://modin.readthedocs.io/en/latest/
  - conda may be a good solution for packaging cppdiodon for python users (notice we already have guix, just need to add an entry in the doc)
  - a shell script adapted to the job scheduler (slurm at first) can be given to wrap cppdiodon executables
    - it could check the input matrix size and decide the number of nodes
    - then launch the job with slurm
    - the shell script can be installed beside the executables in bin/
    - display the output matrices and simple example of python posttreatments
