/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file testPreCoa.hpp
 *
 * Test on computing matrix centering and scaling.
 *
 */
#include <iostream>
#include <string>
////////////////////////////////////////////////////////////////////
///            FMR Includes
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
// Matrix wrapper
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#ifdef DIODON_USE_CHAMELEON
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
////////////////////////////////////////////////////////////////////
///            Diodon Includes
#include "diodon/io_utils/loadfile.hpp"
#include "diodon/io_utils/matutils.hpp"
#include "diodon/pretreatments/precoa.hpp"

#define FMR_API
static const fmr::param::ParameterNames FileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file."
};
static const fmr::param::ParameterNames FileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames FileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames FileTags = {
    {"-ift", "--input-filetags", "-ifiletags"},
    "If there are headers and rownames tags of data in the file."
};
static const fmr::param::ParameterNames H5DataSetName = {
    {"-ds", "--dataset-name", "-dasetname"},
    "To give a specific (default is 'distance') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"};
static const fmr::param::ParameterNames UseBlas = {
    {"-b", "--blas"}, "Use BlasDenseWrapper instead of ChameleonDenseWrapper"};

template<class DenseWrapperClass>
int testPreCoaMain(int argc, char *argv[]) {

    // using SizeType = typename DenseWrapperClass::int_type;
    using RealType = typename DenseWrapperClass::value_type;

    /* user's input parameters */
    const std::string filename = fmr::param::getStr(argc, argv, FileName.options, "../atlas_guyane_trnH.h5");
    const std::string fileformat = fmr::param::getStr( argc, argv, FileFormat.options, "h5");
    const std::string filedelim = fmr::param::getStr( argc, argv, FileDelim.options, " ");
    const std::string datasetname = fmr::param::getStr(argc, argv, H5DataSetName.options, "distance");

#ifdef DIODON_USE_CHAMELEON
    Chameleon::Chameleon chameleon(2);
#endif

    /* main data matrix */
    DenseWrapperClass data;

    /* load matrix from file */
    diodon::io::loadFile(data, filename, fileformat, filedelim, datasetname);

    /* Vector to store diag( sqrt(r_i) ) used in posttreatments */
    std::vector<RealType> M(data.getNbRows());

    /* Vector to store diag( sqrt(c_j) ) used in posttreatments */
    std::vector<RealType> Q(data.getNbCols());

    /* compute precoa on matrix */
    diodon::pre::coa(data, M, Q);

    /* display to standard output some matrix values */
    fmr::Display::matrix(data.getNbRows(), data.getNbCols(), data.getBlasMatrix(), "Data", 3);
    fmr::Display::vector(M.size(), M.data(), "M^-1: ", 3, 1);
    fmr::Display::vector(Q.size(), Q.data(), "Q^-1: ", 3, 1);

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {

  HelpDescribeAndExit(
                      argc, argv, "Test PreCoa - read a matrix then compute its pre coa transformation",
                      FileName,
                      FileFormat,
                      FileDelim,
                      FileTags,
                      H5DataSetName,
                      UseDouble,
                      UseBlas,
                      );
  using INT = unsigned long long int;
  const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

#ifdef DIODON_USE_CHAMELEON
    const bool useBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
    if ( useBlas )
        if ( RealDouble ) {
            testPreCoaMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
        } else {
            testPreCoaMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
        }
    else {
        if ( RealDouble ) {
            testPreCoaMain<fmr::ChameleonDenseWrapper<INT, double>>(argc, argv);
        } else {
            testPreCoaMain<fmr::ChameleonDenseWrapper<INT, float>>(argc, argv);
        }
    }
#else
    if ( RealDouble ) {
        testPreCoaMain<fmr::BlasDenseWrapper<INT, double>>(argc, argv);
    } else {
        testPreCoaMain<fmr::BlasDenseWrapper<INT, float>>(argc, argv);
    }
#endif

  return EXIT_SUCCESS;
}
