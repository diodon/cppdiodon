/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file readBlockHDF5Matrix.hpp
 *
 * Unit test on reading hdf5 dataset Using atlas_guyane_trnH_dissw dataset.
 *
 */
#undef NDEBUG

#include <string>
#include <iostream>
#include <cassert>
#include "diodon/io_utils/BlockHdf5.hpp"


// Matrix wrapper
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/Utils/Accurator.hpp"


using Real_t = double;
using DenseWrapperClass = fmr::BlasDenseWrapper<int,Real_t>;

void checkBlocks(diodon::io::BlockHdf5File matrixDescriptor, DenseWrapperClass &fullMatrix) {
  //
  // Matrix allocation
  int  n_max = matrixDescriptor.getSize() ;
  //
  auto B = fullMatrix.getMatrix() ;
  for  (int b = 0 ; b < matrixDescriptor.getBocksNumber() ; ++b ){
    // read the
    std::cout << b << "  Read: "<< matrixDescriptor.getFileName(b) <<std::endl ;
    DenseWrapperClass currentBloc;
    fmr::io::readHDF5(currentBloc, matrixDescriptor.getFileName(b), matrixDescriptor.gedDataSetName(b));
    auto A = currentBloc.getMatrix() ;
    const auto start = matrixDescriptor.getStartIndexBlock(b) ;
    const auto end   = matrixDescriptor.getEndIndexBlock(b) ;
    auto block_row   = end.first  - start.first ;
    auto block_col   = end.second - start.second ;
    std::cout << "---> " << block_row << "  " << currentBloc.getNbRows() << " |  " << block_col << "  " << currentBloc.getNbCols()<<std::endl;

    std::cout << start.first<< " " << start.second<< "  "
              << end.first	      << "  " << end.second << "  "
              << end.first-start.first	      << "  " << end.second-start.second<<std::endl;

    fmr::Accurater<Real_t> errorAB;
    for ( int j=0; j<block_col; ++j){
      for ( int  i= 0 ; i<block_row; ++i)
        //	A is row major while B is column major (symetrique)
        errorAB.add(A[i*block_col+j],B[(start.first+i)+(start.second+j)*n_max]);
      }
    std::cout << "Block " << b << "  " << errorAB <<std::endl;
  }

}

void checkBlockMatrix(diodon::io::BlockHdf5File matrixDescriptor, DenseWrapperClass  &blockMatrix, DenseWrapperClass &fullMatrix) {
  //
  // Matrix allocation
  int  n_max = matrixDescriptor.getSize() ;
  //
    auto A = blockMatrix.getMatrix() ;
    auto B = fullMatrix.getMatrix() ;
  for  (int b = 0 ; b < matrixDescriptor.getBocksNumber() ; ++b ){
    // read the
    const auto start = matrixDescriptor.getStartIndexBlock(b) ;
    const auto end = matrixDescriptor.getEndIndexBlock(b) ;
    auto block_row = end.first  - start.first ;
    auto block_col = end.second - start.second ;
    std::cout << "---> " << block_row << "  " << block_col << std::endl;

    std::cout << start.first<< " " << start.second<< "  "
              << end.first	      << "  " << end.second << "  "
              << end.first-start.first	      << "  " << end.second-start.second<<std::endl;

    fmr::Accurater<Real_t> errorAB;
    for ( int j=start.second; j<end.second; ++j){
      for ( int  i=start.first; i<end.first; ++i)
        //		std::cout << i << "  " << j <<"  " << A[i+j*nbRows]<< "  " << B[i+j*nbRows] << std::endl;
            errorAB.add(A[i+j*n_max],B[i+j*n_max]);
      }
    std::cout << "Bloc " << b << "  " << errorAB <<std::endl;

    // if not diag transpose the bloc
    if (start.first != start.second){
      std::cout << start.second<< " " << start.first<< "  " 	<< end.second << "  " << end.first
                << "  Sr " <<end.first-start.first<< " Sc " << end.second-start.second << std::endl;
      fmr::Accurater<Real_t> errorBA;
      for ( int j=start.first; j<end.first; ++j){
        for ( int  i=start.second; i<end.second; ++i)
          //		std::cout << i << "  " << j <<"  " << A[i+j*nbRows]<< "  " << B[i+j*nbRows] << std::endl;
          errorBA.add(A[i+j*n_max],B[i+j*n_max]);
      }
    std::cout << "Bloc (sym) " << b << "  " << errorBA <<std::endl;

    }

  }

}




int main(int argc, char* argv[]) {
    (void)argc;
    (void)argv;

    const std::string blockDescriptor("../atlas_guyane_trnH.txt");
    const std::string directory("../");

    diodon::io::BlockHdf5File matrixDescriptor(blockDescriptor, directory);
    auto  n_rows = matrixDescriptor.getSize();
    assert(n_rows == 1502 ) ;
    std::cout << " Matrix size is Ok " << n_rows<<std::endl;

    // Load the full matrix
    DenseWrapperClass blockMatrix;

    diodon::io::fillMatrix(matrixDescriptor, blockMatrix);

    // Declare and init distanceMat / Read matrix in a hdf5
    const std::string filename("../atlas_guyane_trnH.h5");
    std::string datasetname("distance");
    std::cout << "Read file: "<< filename << std::endl;
    DenseWrapperClass fullMatrix;
    fmr::io::readHDF5(fullMatrix, filename, datasetname);
    std::size_t nbRows = fullMatrix.getNbRows();
    assert(nbRows == 1502 ) ;
    //
    // Compare the two matrices

    ////////////////////////////////////////////////////////////////////
    /// Compare A and B
    fmr::Accurater<Real_t> errorAB;
    auto A = blockMatrix.getMatrix();
    auto B = fullMatrix.getMatrix();
    for ( std::size_t  i=0; i<nbRows; ++i)
      for ( std::size_t j=0; j<nbRows; ++j){
        //		std::cout << i << "  " << j <<"  " << A[i+j*nbRows]<< "  " << B[i+j*nbRows] << std::endl;
            errorAB.add(A[i+j*nbRows],B[i+j*nbRows]);
      }
    std::cout << errorAB <<std::endl;
    std::cout << "---------------------\n"<< "Check by blocks\n";
         checkBlockMatrix(matrixDescriptor, blockMatrix, fullMatrix) ;
    std::cout << "---------------------\n"<< "Check blocks\n";
    checkBlocks(matrixDescriptor, fullMatrix) ;

    return 0;
}
