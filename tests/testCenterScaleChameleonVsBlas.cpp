/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file testCenterScaleChameleon.hpp
 *
 * Unit test on computing Centered-Scaled matrix using a choosen
 * dataset with hdf5 format Chameleon is compared to a reference hand written
 * centralized (not MPI) algorithm.
 *
 */

#include <string>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
////////////////////////////////////////////////////////////////////
///            FMR Includes
#include "fmr/Algorithms/randomSVD.hpp"
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
// Matrix wrapper
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
////////////////////////////////////////////////////////////////////
///            Diodon Includes
#include "diodon/io_utils/BlockHdf5.hpp"
#include "diodon/io_utils/loadfile.hpp"
#include "diodon/io_utils/matutils.hpp"
#include "diodon/pretreatments/centerscale.hpp"

#define FMR_API
static const fmr::param::ParameterNames FileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file."
};
static const fmr::param::ParameterNames FileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames FileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames FileTags = {
    {"-ift", "--input-filetags", "-ifiletags"},
    "If there are headers and rownames tags of data in the file."
};
static const fmr::param::ParameterNames H5DataSetName = {
    {"-ds", "--dataset-name", "-dasetname"},
    "To give a specific (default is 'distance') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"};

template<class RealType>
int testCenterScaleChameleonVsBlasMain(int argc, char *argv[]) {

  using Size = int;

  // input matrix filename and datasetname
  const std::string filename = fmr::param::getStr(argc, argv, FileName.options, "../atlas_guyane_trnH.h5");
  const std::string fileformat = fmr::param::getStr( argc, argv, FileFormat.options, "h5");
  const std::string filedelim = fmr::param::getStr( argc, argv, FileDelim.options, " ");
  const std::string datasetname = fmr::param::getStr(argc, argv, H5DataSetName.options, "distance");

  // number of threads used for chameleon and multi-threaded blas
  const unsigned int nbThreads = fmr::param::getValue(argc, argv, fmr::param::NbThreads.options, 1);
#ifdef DIODON_USE_MKL_AS_BLAS
  // Set number of OMP threads used for Blas/Lapack MKL
  mkl_set_num_threads(nbThreads);
#endif
  // number of rows and columns in the block used internally to chameleon
  int tileSize = fmr::param::getValue(argc, argv, fmr::param::TileSize.options, 320);

  // initialize the chameleon context, set options
  Chameleon::Chameleon chameleon(nbThreads, 0, tileSize);
  int mpiRank = chameleon.getMpiRank();

  fmr::BlasDenseWrapper<Size, RealType> matBlas(0,0);
  fmr::ChameleonDenseWrapper<Size, RealType> matCham(0,0);

  // read data from file
  diodon::io::loadFile(matBlas, filename, fileformat, filedelim, datasetname);
  diodon::io::loadFile(matCham, filename, fileformat, filedelim, datasetname);

  auto nbRows = matBlas.getNbRows();
  auto nbCols = matBlas.getNbCols();

  // Get Matrices in a vector
  RealType* matBlasMat = matBlas.getBlasMatrix();
  RealType* matChameleonMat = matCham.getBlasMatrix();

  // Display distance matrix
  if (mpiRank == 0 ){
    fmr::Display::matrix(nbRows, nbCols, matBlasMat,"Matrix blas", 10);
    fmr::Display::matrix(nbRows, nbCols, matChameleonMat,"Matrix cham", 10);
  }

  // compute CenterScale matrix of the input distance matrix with Blas and
  // Chameleon formats
  diodon::pre::centerscale(matBlas, true, true, "col");
  diodon::pre::centerscale(matCham, true, true, "col");

  // Get Matrices in a vector
  matBlasMat = matBlas.getBlasMatrix();
  matChameleonMat = matCham.getBlasMatrix();

  // Matrices comparison
  if (mpiRank == 0 ){
    fmr::Display::matrix(nbRows, nbCols, matBlasMat,"CenterScale blas", 10);
    fmr::Display::matrix(nbRows, nbCols, matChameleonMat,"CenterScale cham", 10);

    RealType dmax = 0.;
    RealType vmax = 0.;
    RealType epsilon = std::numeric_limits<RealType>::epsilon();
    size_t n = nbRows*nbCols;
    for (size_t i = 0 ; i < n ; i++){
      dmax = std::max(dmax, std::abs(matBlasMat[i] - matChameleonMat[i]));
      vmax = std::max(vmax, std::abs(matBlasMat[i]));
    }
    std::cout << "Diff Max " << dmax << std::endl;
    std::cout << "Val Max " << vmax << std::endl;
    std::cout << "Epsilon " << epsilon << std::endl;
    RealType check = dmax / (vmax*nbRows*epsilon);
    std::cout << "Check " << check << std::endl;

    /* calls just to test the functions in MatUtils.hpp */
    bool is_equal = diodon::io::matrices_comp(n, matBlasMat, matChameleonMat);
    vmax = diodon::io::arrays_comp(n, matBlasMat, matChameleonMat);
    std::cout << "Matrices comparison " << is_equal << " " << vmax << std::endl;

    if ( check > 1.0 ){
      std::cout << "Matrices comparison failed" << std::endl;
      return EXIT_FAILURE;
    } else {
      std::cout << "Matrices comparison succeed" << std::endl;
    }
  }

  return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {

  HelpDescribeAndExit(
                      argc, argv, "Test CenterScaleChameleonVsBlas  - read a matrix and compute the Centered Scaled matrix comparing Chameleon with a reference algorithm",
                      FileName,
                      FileFormat,
                      FileDelim,
                      FileTags,
                      H5DataSetName,
                      UseDouble,
                      fmr::param::TileSize,
                      fmr::param::NbThreads,
                      );
  const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

  if (RealDouble){
    testCenterScaleChameleonVsBlasMain<double>(argc, argv);
  } else {
    testCenterScaleChameleonVsBlasMain<float>(argc, argv);
  }

  return EXIT_SUCCESS;
}
