/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file testWrite.cpp
 *
 * Unit test about writing data to a file (h5, csv, txt, gz or bz2).
 *
 */

#include <string>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
////////////////////////////////////////////////////////////////////
///            FMR Includes
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
#include "fmr/Utils/Display.hpp"
////////////////////////////////////////////////////////////////////
///            Diodon Includes
#include "diodon/io_utils/loadfile.hpp"
#include "diodon/io_utils/writefile.hpp"

#define FMR_API
static const fmr::param::ParameterNames InputFileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file."
};
static const fmr::param::ParameterNames InputFileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames InputFileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames InputDataSetName = {
    {"-ids", "--input-dataset-name", "-idasetname"},
    "To give a specific (default is 'distance') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames ColTags = {
    {"-ict", "--input-coltags", "-icoltags"},
    "Use it to enable read columns tags in the file."
};
static const fmr::param::ParameterNames RowTags = {
    {"-irt", "--input-rowtags", "-irowtags"},
    "Use it to enable read rows tags in the file."
};
static const fmr::param::ParameterNames InputColTagsDataSetName = {
    {"-ictds", "--input-column-tags-dataset-name", "-ictdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to read."
};
static const fmr::param::ParameterNames InputRowTagsDataSetName = {
    {"-irtds", "--input-row-tags-dataset-name", "-irtdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to read."
};
static const fmr::param::ParameterNames OutputFileName = {
    {"-of", "--output-filename", "-ofilename"},
    "To give the output filename (default is ./output.h5)."
};
static const fmr::param::ParameterNames OutputFileFormat = {
    {"-off", "--output-fileformat", "-ofileformat"},
    "Format of the file, can be h5, csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames OutputFileDelim = {
    {"-ofd", "--output-filedelim", "-ofiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames OutputDataSetName = {
    {"-ods", "--output-dataset-name", "-odasetname"},
    "To give a specific (default is 'distance') dataset name to write to the hdf5 file (H5)."
};
static const fmr::param::ParameterNames OutputColTagsDataSetName = {
    {"-octds", "--output-column-tags-dataset-name", "-octdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to write."
};
static const fmr::param::ParameterNames OutputRowTagsDataSetName = {
    {"-ortds", "--output-row-tags-dataset-name", "-ortdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to write."
};
static const fmr::param::ParameterNames NColsToSave = {
    {"-ncs", "--number-cols-to-save", "-ncolstosave"},
    "To restrict the number of matrix columns to save."
};
static const fmr::param::ParameterNames NDigitsToSave = {
    {"-nds", "--number-digits-to-save", "-ndigitstosave"},
    "To restrict the number of digits to save."
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"};

static const fmr::param::ParameterNames UseBlas = {
    {"-b", "--blas"}, "Use BlasDenseWrapper instead of ChameleonDenseWrapper"};

template<class DenseWrapper>
int testWriteMain(int argc, char *argv[]) {

    // input matrix filename and datasetname
    const std::string infilename = fmr::param::getStr(argc, argv, InputFileName.options, "../atlas_guyane_trnH.h5");
    const std::string infileformat = fmr::param::getStr( argc, argv, InputFileFormat.options, "h5");
    const std::string infiledelim = fmr::param::getStr( argc, argv, InputFileDelim.options, " ");
    const std::string indatasetname = fmr::param::getStr(argc, argv, InputDataSetName.options, "distance");
    const std::string incolsdatasetname = fmr::param::getStr(argc, argv, InputColTagsDataSetName.options, "colnames");
    const bool coltags = fmr::param::existParameter(argc, argv, ColTags.options);
    const std::string inrowsdatasetname = fmr::param::getStr(argc, argv, InputRowTagsDataSetName.options, "rownames");
    const bool rowtags = fmr::param::existParameter(argc, argv, RowTags.options);
    const std::string outfilename = fmr::param::getStr(argc, argv, OutputFileName.options, "./output.h5");
    const std::string outfileformat = fmr::param::getStr( argc, argv, OutputFileFormat.options, "h5");
    const std::string outfiledelim = fmr::param::getStr( argc, argv, OutputFileDelim.options, " ");
    const std::string outdatasetname = fmr::param::getStr(argc, argv, OutputDataSetName.options, "distance");
    const std::string outcolsdatasetname = fmr::param::getStr(argc, argv, OutputColTagsDataSetName.options, "colnames");
    const std::string outrowsdatasetname = fmr::param::getStr(argc, argv, OutputRowTagsDataSetName.options, "rownames");
    const size_t ncolstosave = fmr::param::getValue(argc, argv, NColsToSave.options, 0);
    const int ndigitstosave = fmr::param::getValue(argc, argv, NDigitsToSave.options, 0);


#ifdef DIODON_USE_CHAMELEON
    // initialize the chameleon context, set options
    Chameleon::Chameleon chameleon(2, 0, 320);
#endif

    DenseWrapper matrix;
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;

    /* load the file from filename */
    diodon::io::loadFile(matrix, infilename, infileformat, infiledelim, indatasetname,
                         coltags, colnames, incolsdatasetname,
                         rowtags, rownames, inrowsdatasetname);

    /* Print array */
    fmr::Display::matrix(matrix.getNbRows(), matrix.getNbCols(),
                         matrix.getBlasMatrix(), "matrix", 12, 7);

    /* Print tags if needed */
    if ( coltags ) {
        std::cout << "Colnames: " << colnames.size() << " entries" << std::endl;
        for (std::vector<std::string>::size_type i = 0; i < colnames.size(); i++) {
            std::cout << colnames[i] << " ";
        }
        std::cout << std::endl;
    }
    if ( rowtags ) {
        std::cout << "Rownames: " << rownames.size() << " entries" << std::endl;
        for (std::vector<std::string>::size_type i = 0; i < rownames.size(); i++) {
            std::cout << rownames[i] << " ";
        }
        std::cout << std::endl;
    }

    /* write the matrix to filename */
    diodon::io::writeFile(matrix, outfilename, outfileformat, outfiledelim, outdatasetname, ncolstosave,
                          coltags, colnames, outcolsdatasetname,
                          rowtags, rownames, outrowsdatasetname,
                          ndigitstosave);

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {

    HelpDescribeAndExit(
        argc, argv, "Test Write: write data to a file (h5, csv, txt, gz or bz2).",
        InputFileName,
        InputFileFormat,
        InputFileDelim,
        InputDataSetName,
        ColTags,
        InputColTagsDataSetName,
        RowTags,
        InputRowTagsDataSetName,
        OutputFileName,
        OutputFileFormat,
        OutputFileDelim,
        OutputDataSetName,
        OutputColTagsDataSetName,
        OutputRowTagsDataSetName,
        NColsToSave,
        NDigitsToSave,
        UseDouble,
        UseBlas
        );

    const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

#ifdef DIODON_USE_CHAMELEON
    const bool useBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
    if ( useBlas )
        if ( RealDouble ) {
            testWriteMain<fmr::BlasDenseWrapper<int, double>>(argc, argv);
        } else {
            testWriteMain<fmr::BlasDenseWrapper<int, float>>(argc, argv);
        }
    else {
        if ( RealDouble ) {
            testWriteMain<fmr::ChameleonDenseWrapper<int, double>>(argc, argv);
        } else {
            testWriteMain<fmr::ChameleonDenseWrapper<int, float>>(argc, argv);
        }
    }
#else
    if ( RealDouble ) {
        testWriteMain<fmr::BlasDenseWrapper<int, double>>(argc, argv);
    } else {
        testWriteMain<fmr::BlasDenseWrapper<int, float>>(argc, argv);
    }
#endif

    return EXIT_SUCCESS;
}
