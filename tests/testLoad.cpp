/* Copyright 2022 INRAE, INRIA
 *
 * This file is part of the cppdiodon software package for Multivariate 
 * Data Analysis of large datasets.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @file testLoad.cpp
 *
 * Unit test about loading a matrix from a file (h5, csv, txt, gz or bz2).
 *
 */

#include <string>
#ifdef DIODON_USE_MKL_AS_BLAS
#include <mkl_service.h>
#endif
////////////////////////////////////////////////////////////////////
///            FMR Includes
// Utilities
#include "fmr/Utils/ParameterNames.hpp"
#include "fmr/Utils/Display.hpp"
////////////////////////////////////////////////////////////////////
///            Diodon Includes
#include "diodon/io_utils/loadfile.hpp"
#include "diodon/io_utils/matutils.hpp"

#define FMR_API
static const fmr::param::ParameterNames FileName = {
    {"-if", "--input-filename", "-ifilename"},
    "To load a data set from a file."
};
static const fmr::param::ParameterNames FileFormat = {
    {"-iff", "--input-fileformat", "-ifileformat"},
    "Format of the file, can be h5 (metadata .txt for several hdf5 files is also handled by this format), csv, txt, gz, bz2."
};
static const fmr::param::ParameterNames FileDelim = {
    {"-ifd", "--input-filedelim", "-ifiledelim"},
    "Delimiters used between values in the csv or txt file."
};
static const fmr::param::ParameterNames ColTagsDataSetName = {
    {"-ictds", "--input-column-tags-dataset-name", "-ictdasetname"},
    "To give a specific (default is 'colnames') dataset name for cols tags to read."
};
static const fmr::param::ParameterNames RowTagsDataSetName = {
    {"-irtds", "--input-row-tags-dataset-name", "-irtdasetname"},
    "To give a specific (default is 'rownames') dataset name for rows tags to read."
};
static const fmr::param::ParameterNames ColTags = {
    {"-ict", "--input-coltags", "-icoltags"},
    "Use it to enable read columns tags in the file."
};
static const fmr::param::ParameterNames RowTags = {
    {"-irt", "--input-rowtags", "-irowtags"},
    "Use it to enable read rows tags in the file."
};
static const fmr::param::ParameterNames DataSetName = {
    {"-ids", "--input-dataset-name", "-idasetname"},
    "To give a specific (default is 'distance') dataset name to read in the hdf5 file (H5)."
};
static const fmr::param::ParameterNames UseDouble = {
    {"-d", "--double"}, "Use double instead of float as the datatype"};

static const fmr::param::ParameterNames UseBlas = {
    {"-b", "--blas"}, "Use BlasDenseWrapper instead of ChameleonDenseWrapper"};

template<class DenseWrapper>
int testLoadMain(int argc, char *argv[]) {

    // input matrix filename and datasetname
    const std::string filename = fmr::param::getStr(argc, argv, FileName.options, "../atlas_guyane_trnH.h5");
    const std::string fileformat = fmr::param::getStr( argc, argv, FileFormat.options, "h5");
    const std::string filedelim = fmr::param::getStr( argc, argv, FileDelim.options, " ");
    const std::string datasetname = fmr::param::getStr(argc, argv, DataSetName.options, "distance");
    const bool coltags = fmr::param::existParameter(argc, argv, ColTags.options);
    const std::string colsdatasetname = fmr::param::getStr(argc, argv, ColTagsDataSetName.options, "colnames");
    const bool rowtags = fmr::param::existParameter(argc, argv, RowTags.options);
    const std::string rowsdatasetname = fmr::param::getStr(argc, argv, RowTagsDataSetName.options, "rownames");

#ifdef DIODON_USE_CHAMELEON
    // initialize the chameleon context, set options
    Chameleon::Chameleon chameleon(2, 0, 320);
#endif

    DenseWrapper matrix;
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;

    /* load the file from filename */
    diodon::io::loadFile(matrix, filename, fileformat, filedelim, datasetname,
                         coltags, colnames, colsdatasetname,
                         rowtags, rownames, rowsdatasetname);

    /* Print array */
    fmr::Display::matrix(matrix.getNbRows(), matrix.getNbCols(),
                         matrix.getBlasMatrix(), "matrix", 12, 7);

    /* Print tags if needed */
    if ( coltags ) {
        std::cout << "Colnames: " << colnames.size() << " entries" << std::endl;
        for (std::vector<std::string>::size_type i = 0; i < colnames.size(); i++) {
            std::cout << colnames[i] << " ";
        }
        std::cout << std::endl;
    }
    if ( rowtags ) {
        std::cout << "Rownames: " << rownames.size() << " entries" << std::endl;
        for (std::vector<std::string>::size_type i = 0; i < rownames.size(); i++) {
            std::cout << rownames[i] << " ";
        }
        std::cout << std::endl;
    }

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {

    HelpDescribeAndExit(
        argc, argv, "Test Load: read a matrix from file (h5, csv, txt, gz or bz2).",
        FileName,
        FileFormat,
        FileDelim,
        DataSetName,
        ColTags,
        ColTagsDataSetName,
        RowTags,
        RowTagsDataSetName,
        UseDouble,
        UseBlas
        );

    const bool RealDouble = fmr::param::existParameter(argc, argv, UseDouble.options);

#ifdef DIODON_USE_CHAMELEON
    const bool useBlas = fmr::param::existParameter(argc, argv, UseBlas.options);
    if ( useBlas )
        if ( RealDouble ) {
            testLoadMain<fmr::BlasDenseWrapper<int, double>>(argc, argv);
        } else {
            testLoadMain<fmr::BlasDenseWrapper<int, float>>(argc, argv);
        }
    else {
        if ( RealDouble ) {
            testLoadMain<fmr::ChameleonDenseWrapper<int, double>>(argc, argv);
        } else {
            testLoadMain<fmr::ChameleonDenseWrapper<int, float>>(argc, argv);
        }
    }
#else
    if ( RealDouble ) {
        testLoadMain<fmr::BlasDenseWrapper<int, double>>(argc, argv);
    } else {
        testLoadMain<fmr::BlasDenseWrapper<int, float>>(argc, argv);
    }
#endif

    return EXIT_SUCCESS;
}
